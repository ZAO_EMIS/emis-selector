/* ��������� �������� �� `modelparametrization`.`model_id`, �������� ����� ������ �� ������ */
/* �� ����: :model_id - ID ������ */
/* �� ������: */
/* - modelparam_id  - */
/* - param_id  - ID ��������� (� ������ ������ - �����������, ���������� ��� ���)*/
/*  - codeorder  - ����� ������� � ����� ������, ������������ ����������*/
/* - independent - ���� ������������� ��������� (0 - ���������, 1 - �����������), �� ����� ����� ��������, ������ ����� ������� ��� � ����� ������ */
select
	`modelparametrization`.`id` as modelparam_id,
	`modelparametrization`.`param_id` as param_id,
    `modelparametrization`.`codeorder` as codeorder,
    `modelparametrization`.`independent` as independent
from
	`modelparametrization`
where
	`modelparametrization`.`model_id` = :model_id
    and
    `modelparametrization`.`codeorder` is not null
order by
	`modelparametrization`.`codeorder`
