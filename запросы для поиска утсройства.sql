/* ��� ����������� ��������� ������, ����� ������� �������������� � ����� ������� ������������� ��������, ���������� ������������� */
/* �� ����: ������������� ��������� (:param_id), ��������, �������� ������������� (:val) */
/* �� ������: ��������������� ���� ������� ��������������, ��������������� ������� ������; 
����������� �� ��� ��������� ��������� ��������� ������� �������������� ���������� 
������������� ��������� ������� �������������� �������������� ������������ � ���, ��� ���� � �� �� ���������� �� ����� ���� 
���������������� ����� � ��� �� ���������� ����� ������ ���� (���� parameter_id, model_id - ���������) 

����������� �� ���������� ���������� ������� �� ������ ����������� ����� ������� (�� ������ ��������� ���������)
����������� ������������ ������������� ����� ������� `modelline`.`id` in (29, 31, 16).
*/
/*
*/
select `modelparametrization`.`id`, `modelline`.`id`, `modelline`.`basename`
from
`modelline`, `modelparametrization`, `parameters`, `parametertypes`, `intparams`, `floatparams`, `stringparams`, `predefinedvalues`
where
`modelparametrization`.`model_id` = `modelline`.`id`
and
`modelparametrization`.`param_id` = `parameters`.`id`
and
`parameters`.`id` = :param_id
/* ����������� ����������� ���� ������� */
and
`modelline`.`id` in (29, 31, 16) /* ������ �������� ����� ������� */
and(
	(
		`parametertypes`.`id` = 5 and `parameters`.`paramtype_id` = `parametertypes`.`id` and `intparams`.`modelparametrization_id` = `modelparametrization`.`id`
		and :val between `intparams`.`minval` and `intparams`.`maxval`
	)
	or(
		`parametertypes`.`id` = 6 and `parameters`.`paramtype_id` = `parametertypes`.`id` and `floatparams`.`modelparametrization_id` = `modelparametrization`.`id`
		and :val between `floatparams`.`minval` and `floatparams`.`maxval`
	)
	or(
		`parametertypes`.`id` = 7 and `parameters`.`paramtype_id` = `parametertypes`.`id` and `stringparams`.`modelparametrization_id` = `modelparametrization`.`id`
		and `stringparams`.`value_id` = `predefinedvalues`.`id` and :val = `predefinedvalues`.`value`
	)
)
group by `modelparametrization`.`id`;

