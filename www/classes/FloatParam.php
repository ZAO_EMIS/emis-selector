<?php
/**
 * @todo Класс - наследник от
 * @seeParameter
 * отвечает за все операции с числовыми параметрами (float) - инициализация, сравнение, поиск, фильтрация, формирование интерфейса 
 * запроса значения, ... 
 * @property public $ComparationType; - тип сравнения для параметра
 * @property public $measurementUnits; - массив единиц измерения, если таковые есть
 * @property private $defaultMeasurementUnit; - единица измерения по умолчанию
 * @property private $isRange; - является ли параметр диапазонным
 * 
 * @property private static $pathFloatView = "float.php"; - страница для запроса числового параметра общего вида
 * @property private static $pathFloatList = "floatList.php"; - страница для выбора значения числового параметра
 * из списка возможных значений
 * @property private static $pathLogic = "logic.php"; - страница для ввода значения логического параметра (разновидность 
 * числового параметра, принимающая знаяения только 0 и 1)
 * */
class FloatParam extends Parameter{
	
	public $ComparationType;
	public $measurementUnits;
	private $defaultMeasurementUnit;
	private $measUnitId;
	private $isRange;
	protected $calcTemplate = false;
	//private $baseVariant = 0;
	
	private static $pathFloatView = "float.php";
	private static $pathFloatList = "floatList.php";
	private static $pathLogic = "logic.php";
	 
	//public function __construct(){}
	
	public function getMeasUnitId(){
		return $this->measUnitId;
	}
	
	public function setMeasUnitId($aMeasUnitId){
		$this->measUnitId = $aMeasUnitId;
	}
	
	public function findMeasUnitById($aMeasUnitId){
		
		$sql = "select `name` from `measurebase` where `id`=".$aMeasUnitId;
		
		$result = Db::getInstance()->executeS($sql);
		
		return $result[0]["name"];
	}
	
	/**
	 * метод выполняет приведение пользовательского значения к стандартным единицам измерения.
	 * возвращает приведённое значение, если коэффициент и сдвиг пересчёта !=1 и !=0 соответственно, или исходное 
	 * пользовательское значение, если пересчёт не нужен 
	 * */
	public static function recalcMeasureUnit($aMeasUnitId, $sourceValue){
		
		if($aMeasUnitId != null){
			
			$sql = 'select `mb`.`coeff`, mb.`shift`
				from `measurebase` `mb`
				where `mb`.`id` = '.$aMeasUnitId;
			
			$measUnitRecalcinfo = Db::getInstance()->executeS($sql);
			
			if($measUnitRecalcinfo != null){
				return $sourceValue * $measUnitRecalcinfo[0]["coeff"] + $measUnitRecalcinfo[0]["shift"];
			} else{
				return $sourceValue;
			}
		} else{
			return $sourceValue;
		}
	}
	
	
	/**
	 * */
	public function getCalcTemplate(){
		return $this->calcTemplate;
	}
	
	/**
	 * */
	public function setCalcTemplate($aCalcTemplate){
		$this->calcTemplate = $aCalcTemplate;
	}
	
	/**
 	* */
	public function getParamType(){
		return $this->paramType;
	}
	
	/**
 	* */
	public function  setIsRange($isRange){
		$this->isRange = $isRange;
	}
	
	/**
 	* */
	public function __construct($aParamObj, $aParamTypeId = null){
		$this->initFloatByExempair($aParamObj, $aParamTypeId);
		$this->paramId = $aParamObj->paramId;
		$this->modelParamId = $aParamObj->modelParamId;

	}
	
	/**
	 * @todo 
	 * */
	private function initFloatByExempair($aParamObj){
		
		$commonParamInfo = $aParamObj->getCommonParamInfo();
		$this->setCommonParamInfo($commonParamInfo);
		$this->initForCompare();
	}
	
	/**
	 абстрактный, приватный
	  
	 @todo устанавливает значения полей класса, используя в качестве исходной информации
	 ID параметра, однако в классе Parameter не реализуется; причина: инициализация
	 классов наследников происходит по разному для каждого класса
	 */
	public function initForCompare(){
		
		$sql = 'select p.`comparetype` as comparetype, p.`isRange` as isRange from `parameters` p where `p`.`id` =
				'.$this->getParamId();
		
		$result = Db::getInstance()->executeS($sql);
		
		$this->ComparationType = $result[0]['comparetype'];
		$this->isRange = $result[0]['isRange'];
	}
	
	/**
	 * @todo Инициализирует экземпляр класса информацией, необходимой для запроса пользователдьского значения.
	 *
	 **/
	public function initForInquire(){
		
		parent::initForInquire();
		
		$sql = 'select 
					mb.`id` as measureUnitId
					, mb.`base_id` as baseUnitId
					, mb.`name` as measureUnitName
					, mb.`coeff` as coeff
					, mb.`shift` as shift					
					from ((`parameters` p join `phisquantity` ph) join `measurebase` mb)
				where p.`phisquantity_id` = ph.`id` and mb.`phisquantity_id` = ph.`id` and p.`id` =  
				'.$this->paramId;		
		
		$result = Db::getInstance()->executeS($sql);					
		$this->measurementUnits = $result;
		
		if($result == null){
			
			//p(debug_backtrace());
			//d($sql);
		}
		
		if($result != null){
			
			foreach ($result as $oneMeasureUnit){
				/* воспользуемся тем, что у базовой	1 единицы измерения нет предка */
				if($oneMeasureUnit['baseUnitId'] == null){
					$this->defaultMeasurementUnit = $oneMeasureUnit;
				}
			}
		}
	}
	
	/**
	 *
	 **/
	//public function extendedInit($aModelId){
		//parent::extendedInit($aModelId);
	//}
	 
	/**
	 @todo возвращает множество (массив) единиц измерения для текущего параметра
	 */
	
	public function getParamMeasurementUnits(){
		return $this->measurementUnits;
	}
	
	/**
	 @todo возвращает единицу измерения для параметра, используемую по умолчанию
	 */
	public function getDefaultMeasurementUnit(){
		return $this->defaultMeasurementUnit;
	}
	
	/**
	 @todo возвращает тип сравнения для параметра (__OR__   __EQ__    __XOR__)
	 */
	public function getCompareType(){
		return $this->ComparationType;
	}
	
	/**
	 * @todo
	 * */
	public function parseFormulaTemplate($aFormulaTemplate, $aUserValue){

		$userValues = ObjectModel::getFromSessionByName("params");
		
		$strParam = "param";
		$strUNDERLINE = "_";
		$strMIN = "_min";
		$strMAX = "_max";
		
		$aFormulaTemplate = str_replace("userValue_min", $aUserValue['min'], $aFormulaTemplate);
		$aFormulaTemplate = str_replace("userValue_max", $aUserValue['max'], $aFormulaTemplate);
		
		while(strpos($aFormulaTemplate, $strParam) != false){
			// начало позиции вхождения параметра в строку условия
			$startParamPosition = strpos($aFormulaTemplate, $strParam);
			//
			$underlinePosition = strpos($aFormulaTemplate, $strUNDERLINE);
			$endParamPosition = $underlinePosition + strlen("_max");
			$strParamEntrance = substr($aFormulaTemplate, $startParamPosition, ($endParamPosition - $startParamPosition));
			
			if(strpos($strParamEntrance, $strMIN)){
				$userValueIndex = "min";
			} elseif(strpos($strParamEntrance, $strMAX)){
				$userValueIndex = "max";
			}
				
			$paramId = str_replace($strParam, "", str_replace($strMAX, "", str_replace($strMIN, "", $strParamEntrance)));
			//$paramUserValue = $userValues[$paramId][$userValueIndex];
			// выполнить приведение единиц измерения к стандартным
			$paramUserValue = self::recalcMeasureUnit($userValues[$paramId]["measUnitId"], $userValues[$paramId][$userValueIndex]);
			// в формулу попадают для использования значения уже в стандартных единицах измерения
			$aFormulaTemplate = str_replace($strParamEntrance, $paramUserValue, $aFormulaTemplate);
		}
		
		//p(eval($aFormulaTemplate));
		
		return $aFormulaTemplate;
	}
	
	/**
	 @todo приводит исходное пользовательское значение параметра к значениям параметра, использованного в
	 качестве характеристики прибора
	 */
	public function correctToStandartVariant($aUserValue){
		// 
		// если пользовательское значение - объёкт, получить из него массив [min, max]
		if(is_object($aUserValue)){
			$aUserValue = $aUserValue->getValue();
		}

		$aUserValue["min"] = self::recalcMeasureUnit($this->measUnitId, $aUserValue["min"]);
		$aUserValue["max"] = self::recalcMeasureUnit($this->measUnitId, $aUserValue["max"]);

		// сравнить вариант параметра, характерного для прибора, с вариантом параметра, указанным пользователем
		if($this->getCalcTemplate() != null){
			//p($this->calcTemplate);
			$evalExpression = $this->parseFormulaTemplate($this->getCalcTemplate(), $aUserValue);

			$recalc = eval($evalExpression);
			
			if($recalc == null){
				$recalc = $aUserValue;
			}
		} else{
			$recalc = $aUserValue;
		}
		
		/**
		if(($this->paramId == 77) and ($this->getCalcTemplate() != null)){
		
			p("------");
			p($this->modelParamId);
			p($this->getCalcTemplate());
			p($aUserValue);
			p($recalc);
			p("-----------------------------------");
		}
		*/
		return $recalc;
	}
	
	/**
	 * @todo возвращает флаг диапазонного параметра
	 */
	public function getIsRange(){
		return $this->isRange;
	}
	
	/**
	 * @todo сравнивает 2 значения параметра - пользовательское с эталонным
	 * @see Parameter
	 * */
	public function compareValues($aUserValue, $aEthalValue){
		
		$normalizedUserValue = $this->correctToStandartVariant($aUserValue);
		//p($normalizedUserValue);
		if(is_object($aEthalValue)){
			$aEthalValue = $aEthalValue->getValue();
		}
		
		if(($normalizedUserValue != null) and ($aEthalValue != null)){
				
			if($this->ComparationType == Parameter::_P_OR_TYPE_COMPARATION){		
				
				return $this->compareParamWithUserVal_TYPE_OR($normalizedUserValue, $aEthalValue);
			} else if($this->ComparationType == Parameter::_P_XOR_TYPE_COMPARATION){
		
				return $this->compareParamWithUserVal_TYPE_XOR($normalizedUserValue, $aEthalValue);
			} else if($this->ComparationType == Parameter::_P_EXACT_COMPARATION){
				
				return $this->compareParamWithUserVal_TYPE_EQ($normalizedUserValue, $aEthalValue);
			} else{
		
				return false;
			}
		} else{
				
			return true;
		}
	}
	
	/**
	 * @todo сравнивает эталонное значение с пользовательским
	 * @see Parameter
	 * @param unknown $anEthalonFloatValue
	 * @return boolean $isOK - true,если результат сравнения - правильный, false - в противном случае
	 */
	public function compareParamWithUserVal($anEthalonFloatValue){
		
		$userValue = Parameter::getUserParamById($this->getParamId());
		
		if($userValue == null){
			return true;
		} else{
			
			$isOK = $this->compareValues($userValue, $anEthalonFloatValue);
			
			return $isOK;
		}
	}
	
	/**
	 *  @todo сравнивает эталонное и пользовательское значения для типа сравнения __OR__ 
 	* */
	private function compareParamWithUserVal_TYPE_OR($anUser, $anEthal){
	
		$isOK = false;
		
		
		if(($anEthal['min'] != null) and ($anUser['min'] !== null) and ($anEthal['max'] != null) and ($anUser['max'] !== null)){
			$isOK = (($anEthal['min'] <= $anUser['min']) and ($anEthal['max'] >= $anUser['max']));
		}
		
		return $isOK;
	}
	
	/**
	 *  @todo сравнивает эталонное и пользовательское значения для типа сравнения __ХOR__
 	* */
	private function compareParamWithUserVal_TYPE_XOR($anUser, $anEthal){
	
		$isOK = false;
	
		if(($anEthal['min'] != null) and ($anEthal['max'] != null) and ($anUser['max'] != null)){
				
			$isOK = (($anUser['max'] >= $anEthal['min']) and ($anUser['max'] <= $anEthal['max']));
		}
	
		return $isOK;
	}
	
	/**
	 *  @todo сравнивает эталонное и пользовательское значения для типа сравнения __EQ__
 	* */
	private function compareParamWithUserVal_TYPE_EQ($anUser, $anEthal){
	
		$isOK = false;
		
		if(($anEthal['min'] != null) and ($anUser['min'] != null)
		and ($anEthal['max'] == $anEthal['min']) and ($anUser['max'] == $anUser['min'])){
				
			$isOK = ($anEthal['min'] == $anUser['min']);
		}
		
		return $isOK;
	}
	
	/**
	 * @see Parameter
	 * */
	public function getHTML(){
		
		if($this->checkForNeedInquire()){
			
			parent::getHTML();
			
			if ($this->paramType == Parameter::_P_LOGIC_VALUE) {
				include(__ES__ROOT_DIR . '\\views\\paramView\\' . self::$pathLogic);
			}elseif(count($this->values) > 0){
				include(__ES__ROOT_DIR . '\\views\paramView\\' . self::$pathFloatList);
			}else{
				include (__ES__ROOT_DIR . '\\views\\paramView\\' . self::$pathFloatView);
			}
			
			//parent::includeUserNotesView();
		}
	}
	
	/**
 	* */
	public function getParamCompareType(){
		return $this->ComparationType;
	}
	
	/**
	 *@todo
	 *@see Parameter
	 * */
	public function filterObjValue($anObjValue){
		
		if(($this->ComparationType == Parameter::_P_EXACT_COMPARATION) or ($this->ComparationType == Parameter::_P_OR_TYPE_COMPARATION)){
			return ($this->filterValues($anObjValue, $this->value) or $this->filterValues($this->value, $anObjValue));
		} else{
			return $this->filterValues($anObjValue, $this->value);
		}
	}
	
	/**
	 * @deprecated полный аналог compareValues($aUserValue, $aEthalValue)
	 * */
	private function filterValues($aUserValue, $aEthalValue){
		
		if(is_object($aUserValue)){
			$aUserValue = $aUserValue->getValue();
		}

		if(is_object($aEthalValue)){
			$aEthalValue = $aEthalValue->getValue();
		}
		
		if(($aUserValue != null) and ($aEthalValue != null)){
				
			if($this->ComparationType == Parameter::_P_OR_TYPE_COMPARATION){		
				return $this->compareParamWithUserVal_TYPE_OR($aUserValue, $aEthalValue);
				
			} else if($this->ComparationType == Parameter::_P_XOR_TYPE_COMPARATION){
		
				return $this->compareParamWithUserVal_TYPE_XOR($aUserValue, $aEthalValue);
			} else if($this->ComparationType == Parameter::_P_EXACT_COMPARATION){
		
				return $this->compareParamWithUserVal_TYPE_EQ($aUserValue, $aEthalValue);
			} else{
		
				return false;
			}
		} else{
				
			return true;
		}
	}
	
	/**
	 * @see Parameter
	 * */
	public function selectDefaultValueIdsFromDb(){
		
		$sql = 'select fp.`id` as value_id
				from `floatparams` fp
				where
				`fp`.`modelparametrization_id` = '.$this->modelParamId.'
				and
				fp.`priority` = (select MAX(fp.`priority`)
					from `floatparams` fp
					where `fp`.`modelparametrization_id` = '.$this->modelParamId.')';
		
		$resultSql = Db::getInstance()->executeS($sql);
		$result = array();
			
		foreach($resultSql as $one_val){
			$result[] = $one_val['value_id'];
		}
			
		return $result;
	}
	
	/**
	 *@see Parameter
	 * */
	public function getAllValueForModelParam(){
		
		$sql = '
				select
				`modelparametrization`.`id` as modelparamid,
			    `floatparams`.`id` as value_id,
			    `floatparams`.`minval` as minval,
			    `floatparams`.`maxval` as maxval,
			    `floatparams`.`codeval` as codeval
			from
				`modelparametrization`, `floatparams`
			where
			    `modelparametrization`.`id` = '.$this->modelParamId.'
				and
				`floatparams`.`modelparametrization_id` = `modelparametrization`.`id`';
		
		
		$result = Db::getInstance()->executeS($sql);
		
		$arrValues = array();
		
		foreach ($result as $recValue){
			
			$objValue = Value::createInstance($this->paramId);
			$objValue->init($recValue['value_id'], $this->modelParamId);
			$arrValues[$objValue->getValueId()] = $objValue;
		}
		
		return $arrValues;
	}
	
	/**
	 * @access private
	 * @todo метод, определяя тип параметра, по типу параметра выбирает запрос, выполняемый к БД
	 * (в зависимости от типа параметра запрос осуществляется к соответствующей таблице БД)
	 * @param $paramId - ID параметра, по которому производится выбор модели из множества, задаваемого параметром $models
	 * @param $value - наименьшее значение параметра
	 * @param $valueMax - наибольшее значение параметра
	 * @param $models - множество (массив) моделей, в котором ведётся поиск; структура элемента этого массива
	 * (
	 [modelparam_id] - ID характеристики (параметризации модели), по которому модель прошла проверку
	 [param_id] - ID параметра (параметризации модели), по которому модель прошла проверку
	 [codeorder] - код к строке заказа модели, соответствующий этому параметру
	 [model_id] - ID модели
	 [modelname] - наименование модели
	 )
	 * @return $sql - sql - запрос (все значения параметров в запросе уже проставлены, его нужно просто выполнитьЫ)
	 * */
	public function getSearchQuery($paramId, $value, $valueMax, $modelsTerm = null){
		
		if($value == ""){
			$value = $valueMax;
		}
		
		if($valueMax == ''){
			$valueMax = $value;
		}
		
		// приведение пользовательских значений к стандартным единицам измерения
		$value = self::recalcMeasureUnit(ObjectModel::getFromSessionByName("params")[$paramId]["measUnitId"], $value);
		$valueMax = self::recalcMeasureUnit(ObjectModel::getFromSessionByName("params")[$paramId]["measUnitId"], $valueMax);

		//p($paramId);
		//p($value);
		//p($valueMax);
		//p(ObjectModel::getFromSessionByName("params")[$paramId]);
		//p($modelsTerm);
		
		$sql = 'select
		`vmp`.`mpid` as modelparam_id
		,`vmp`.`paramID` as param_id
		,ifnull(`vmp`.`codeorder`, 0) as codeorder
		,vmp.`modelID` as model_id
		,vmp.`modelname` as modelname
		from
		`vmodelparametrization` vmp join `floatparams` fp
		where
		`vmp`.`devtype` = '.Parameter::MAIN_DEVICE_TYPE.'	
		'.$modelsTerm.'
		and `fp`.`modelparametrization_id` = `vmp`.`mpid`
		and `vmp`.`paramID` =  '.$paramId.'
		and(
				(vmp.`cmptype` = 1
						and `fp`.`minval` <= '.$value.' and `fp`.`maxval` >= '.$value.'
						and `fp`.`minval` <= '.$valueMax.' and `fp`.`maxval` >= '.$valueMax.'
				) or (vmp.`cmptype` = 2
						and '.$value.' >= `fp`.`minval`
				) or (vmp.`cmptype` = 0
						and '.$value.' = `fp`.`minval`
				) OR(
						`countOfParamVariants`(`vmp`.`mpid`) > 0
				)
				)
						group by
						vmp.`modelID`';
		
		return $sql;
	}
	
	/**
	 * @see Parameter
 	* */
	public function getDefaultValue(){
	
		$sql = 'select fp.`id` as defaultval_id
				from
					`floatparams` fp, `vmodelparametrization` vmp
				where
					vmp.`paramtype_id` <> '.Parameter::_P_PREDDEF_VALUE.'
					and
					vmp.`mpid` = fp.`modelparametrization_id`
					and
					vmp.`mpid` = '.$this->modelParamId.'
					and
					fp.`priority` = (select MAX(_fp.priority)
									from `floatparams` _fp, `vmodelparametrization` _vmp
                    				where _fp.`modelparametrization_id` = `_vmp`.`mpid`
                    				and `_vmp`.`mpid` = '.$this->modelParamId.')';
	
		$result = Db::getInstance()->executeS($sql);
		
		$defaultValue = Value::createInstance($this->paramId, $this->paramType);
		$defaultValue->init($result[0]['defaultval_id'], $this->modelParamId);
		
		return $defaultValue;
	}
	

	/**
	 * @see Parameter
	 * */
	public function searchSatisfyValues($aUserValue = null){
		
		if($aUserValue == null){
			$aUserValue = ObjectModel::getFromSessionByName('params')[$this->paramId];
		}
		
		$normalizedUserValue = $this->correctToStandartVariant($aUserValue);
		
		$sql = 'select
					`modelparametrization`.`id` as modelparam_id,
				    `floatparams`.`id` as value_id,
					`floatparams`.`minval` as minval,
					`floatparams`.`maxval` as maxval,
				    `parameters`.`id`,
				    `modelparametrization`.`codeorder`,
					ifnull(`floatparams`.codeval, "") as codeval
				from
					`modelparametrization`, `parameters`, `floatparams`
				where
					`modelparametrization`.`param_id` = `parameters`.`id`
				and
					`floatparams`.`modelparametrization_id` = `modelparametrization`.`id`
				and
					`modelparametrization`.`id` = '.$this->modelParamId.'
				and
					`floatparams`.`minval` <= '.$normalizedUserValue['min'].' and `floatparams`.`maxval` >= '.$normalizedUserValue['max'];
		
		$arrValuesInfo = Db::getInstance()->executeS($sql);
		
		$objValues = array();
		
		foreach ($arrValuesInfo as $oneValue){
			
			$oneObjValue = Value::createInstance($this->paramId, $this->paramType);
			$oneObjValue->init($oneValue['value_id'], $this->modelParamId);
			$objValues[$oneValue['value_id']] = $oneObjValue;
		}
		
		return $objValues;
	}
}
?>