<?php
/** 
 * @todo Класс поиска и автозагрузки классов
 * 
 * @access protected
 * @static
 * @property $instance = null -  экземпляр класса. Класс реализует паттерн Singleton (Одиночка), как 
 * следствие, может присутствовать в системе лишь в единственном экземпляре.
 * 
 * @access protected
 * @property $root_dir = null - путь к корневому каталогу.
 * 
 * @access protected
 * @property $index = array() - 
 */
class AutoLoad {
	
	/***
	 * @access
	 */
	protected static $instance = null;
	
	/***
	 * @access
	 */
	protected $root_dir = null;
	
	/***
	 * @access
	 */
	protected $index = array();
	
	/***
	 * @access
	 */
	public static function getInstance(){
		
		if (!self::$instance) {
			
			self::$instance = new AutoLoad();
			self::$instance->generateIndex();
		}
		
		return self::$instance;
	} 
	
	/***
	 * @access
	 */
	protected function __construct() {
		$root_dir = __ES__ROOT_DIR.'/';
	}
	
	/***
	 * @access
	 */
	public function load($classname){
		
		if (!isset($this->index[$classname])){
			$this->generateIndex();
		}
		
		require($this->root_dir.$this->index[$classname]['path']);
	}
	
	/**
	 * @access
	 * Retrieve recursively all classes in a directory and its subdirectories
	 *
	 * @param string $path Relativ path from root to the directory
	 * @return array
	 */
	protected function getClassesFromDir($path){
		
		$classes = array();
	
		$all_files = scandir($this->root_dir.$path) ;		
		
		foreach ($all_files as $file){			
			
			if ($file[0] != '.'){
				
				if (is_dir($this->root_dir.$path.$file)){
					$classes = array_merge($classes, $this->getClassesFromDir($path.$file.'/'));
				} else if (substr($file, -4) == '.php'){
					
					$content = file_get_contents($this->root_dir.$path.$file);
					$pattern = '#\W((abstract\s+)?class|interface)\s+(?P<classname>'.basename($file, '.php').'(?:Core)?)'
							.'(?:\s+extends\s+[a-z][a-z0-9_]*)?(?:\s+implements\s+[a-z][a-z0-9_]*(?:\s*,\s*[a-z][a-z0-9_]*)*)?\s*\{#i';
					
					if (preg_match($pattern, $content, $m)){
						
						$classes[$m['classname']] = array(
								'path' => $path.$file,
								'type' => trim($m[1])
						);
					}
				}
			}
		}
	
		return $classes;
	}
	
	/***
	 * @access
	 */
	public function generateIndex(){
		
		$classes = array_merge(
				$this->getClassesFromDir('classes/'),
				$this->getClassesFromDir('controllers/')
		);		
		
		ksort($classes);
		
		$content = '<?php return '.var_export($classes, true).'; ?>';
		
		$this->index = $classes;
	}
}