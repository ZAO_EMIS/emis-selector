<?php
class FloatValue extends Value{
	private $minVal;
	private $maxVal;

	/**
	 * 
	 * @param unknown $aParamId - идентификатор параметра
	 * @param string $aValueId - идентификатор значения
	 * @param string $aModelParamId - идентификатор параметризации модели
	 */
	public function __construct($aParamId, $aValueId = null, $aModelParamId = null){
		$this->paramId = $aParamId;
	}
	
	/**
	 * @see Value
	 * */
	protected function initValueFromDb(){
			
		if(!is_object($this->valueId)){
			
			$sql = 'select
					fp.codeval as code
					, fp.priority as priority
					, fp.`minval` as ethalMinValue
					, fp.`maxval` as ethalMaxValue
					from floatparams fp where fp.id = '.$this->valueId;
				
			$result = Db::getInstance()->executeS($sql);
			
			//d($result);
				
			$this->code = $result[0]['code'];
			$this->priority = $result[0]['priority'];
			$this->minVal = $result[0]['ethalMinValue'];
			$this->maxVal = $result[0]['ethalMaxValue'];
				
		} else{
			
			p(debug_backtrace());
		}
	}
	
	/**
	 * установка минимального и максимального значений (используется в случае инициализации значения как пользовательского)
	 **/
	public function setMinMax($minValue, $maxValue){
		
		if($minValue == ''){
			$minValue = $maxValue;
		}
		elseif($maxValue == ''){
			$maxValue = $minValue;
		}
		
		$this->minVal = $minValue;
		$this->minVal = $maxValue;
	}
	
	/**
	 * @see Value::getValue()
	 */
	public function getValue(){
		
		$value = array();
		
		$value['min'] = $this->minVal;
		$value['max'] = $this->maxVal;
		
		return $value;
	}
}
?>