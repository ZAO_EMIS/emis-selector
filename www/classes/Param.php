<?php
/**
 * @todo класс отвечает за реализацию опреаций с параметрами, по которым подбирается оборудование (получение и установка 
 * пользовательских значений, определение типа параметра, типа сравнения параметра, ...)
 * 
 * 
 * @property const MAIN_DEVICE_TYPE = 11;
 * 
 * 
 * @property const MAX_WEIGHT = 10;
 * @property const MIN_WEIGHT_FOR_COMMON_PARAMS = 5;
 * 
 * @property const _P_INT_VALUE_TYPE = 5;
 * @property const _P_FLOAT_VALUE_TYPE = 6;
 * @property const _P_PREDDEF_VALUE = 7; 
 * @property const _P_LOGIC_VALUE = 8;
 * 
 * @property const _P_RANGE_COMPARATION = 1;
 * @property const _P_EXACT_COMPARATION = 0;
 * @property const _P_NON_EXCEED_COMPARATION = 2;
 * 
 * @property public $type = null
 * @property public $id = null 
 * @property public $values = null 
 * @property public $isRange = null 
 * @property public $value_id = null 
 * @property public $name = null 
 * @property public $measurment_units = array()
 * @property private $multiselection = false
 * @property private $groupId = array()
 * */
class Param extends ObjectModel {
	/*
	//константа, определяющая основной тип подбираемого оборудования
	const MAIN_DEVICE_TYPE = 11;
	//константы, определяющие характерные веса параметров 
	const MAX_WEIGHT = 10;
	const MIN_WEIGHT_FOR_COMMON_PARAMS = 5;
	// константы, определяющие тип параметров
	const _P_PREDDEF_VALUE = 7; 
	const _P_FLOAT_VALUE_TYPE = 6;
	const _P_INT_VALUE_TYPE = 5;
	const _P_LOGIC_VALUE = 8;
	// константы, определяющие способ сравнения параметров
	const _P_OR_TYPE_COMPARATION = 1; // прямой тип сравнения (значения пользовательского диапазона должны попадать в определённый диапазон)
	const _P_RANGE_COMPARATION = 1; // прямой тип сравнения (значения пользовательского диапазона должны попадать в определённый диапазон)
	
	const _P_EXACT_COMPARATION = 0; // точное совпедение (значение должно точно совпадать с заданным)
	
	const _P_XOR_TYPE_COMPARATION = 2; // обратный тип сравнения (значение не должно превышать заданного)
	const _P_NON_EXCEED_COMPARATION = 2; // обратный тип сравнения (значение не должно превышать заданного)
	//
	public $type = null; /* тип параметра */
	//public $id = null; /* идентификатор */
	//public $values = null; /* значения */
	//public $isRange = null; /* диапазонный? */
	//public $value_id = null; /*  */
	//public $name = null; /* наименование */
	//public $measurment_units = array(); /* единицы измерения */
	
	//private $multiselection = false;
	//private $groupId = array();
	
	/**
	 * @access	public
	 * @todo метод возвращает пользовательские значения параметров, сохранённые в $_SESSION на указанном шаге
	 * @param $cuurStep	- шаг процесса подбора, если null вернутся все параметры
	 * @return $session_params - массив вида [param_id]=>array([min],[max])
	 
	public static function getUserParams($cuurStep = NULL) {
		
		if ($cuurStep == null) {			
			
			$session_params = $_SESSION['params']; // запрашиваем из сессии все параметры, сохранённые в ней		
			$_SESSION['params'] = $session_params;	
		} else { // иначе
			
			$temp_params = $_SESSION['params']; // читаем параметры из сесси во временную переменную
			$_SESSION['params'] = $temp_params;
			$temp_keys = array_keys ( $temp_params ); // получаем все ключи массива
			
			foreach ( $temp_keys as $key ) { // и выбираем из них в $session_params только те, у которых шаг = $currStep
				
				$sp_temp = $temp_params[$key];
				
				if ($sp_temp['step'] == $cuurStep) {
					$session_params[$key] = $sp_temp;
				}
			}
			
			$_SESSION['params'] = $temp_params;
		}
		
		return $session_params;
	}*/
	
	/**
	 * @todo сохраняет в сессии только те параметры, которыебыли запрошены на шаге процесса подбора,
	 * отличном от указанного в качестве параметра
	 * @param $step - шаг процесса подбора, для которого доложны быть удалены из сессии запрошенные на этом шаге параметры
	 * @return $forDeletionFromInquired - параметры, подлежащие удалению из массива запрошенных
	 
	public static function deleteParamValueForStep($step){
		
		$paramIds = array();
		
		$params = self::getUserParams();
		$result = array();
		
		foreach ($params as $key => $par){
			
			if($par["step"] != $step){
				$result[$key] = $par;
			} else{
				$forDeletionFromInquired[] = $key;
			}
		}
		
		$_SESSION['params'] = $result;
		
		return $forDeletionFromInquired;
	}**/
	
	/**
	 * @access	public
	 * @todo метод возвращает по ID параметра его имя 
	 * @param $id - 
	 * @return $name['name'] - имя параметра
	 
	public static function getParamName($id){
		
		$sql='select pr.`name` as name
			  from parameters pr
			  where pr.`id` = '.$id;
		
		$name = Db::getInstance()->getRow($sql);
		
		return $name['name'];
	}* */
	
	/**
	 * @access	public
	 * @todo метод возвращает из переменной $_SESSION пользовательское значение для параметра с указанным ID
	 * @param $id - ID параметра
	 * @return $value - пользовательское значение параметра, сохранённое в $_SESSION, или false в случае, если такой параметр
	 * не вводился пользователем 
	 
	public static function getUserParamById($id) {
		
		$temp_params = $_SESSION ['params']; // читаем параметры из сесси во временную переменную		
		
		foreach ( $temp_params as $paramId => $value ) { // 
			
			if ($id == $paramId) {
				return $value;
			}
		}
		
		return false;
	}* */
	
	/**
	 * @access	public
	 * @todo метод возвращает последний пройденный шаг процесса подбора из переменной $_SESSION
	 * @param void
	 * @return $maxStep - последний пройденный шаг процесса подбора
	 
	public static function getLastIterationStep() {
		//p("вызов Param->getLastIterationStep");
		
		if (is_array ( $_SESSION ) && count ( $_SESSION ) > 0) {
			
			$params = $_SESSION ['params'];
			$keys = array_keys ( $params );
			$maxStep = 0;
			
			foreach ( $keys as $key ) {
				
				if ($maxStep < $params [$key] ['step']) {
					$maxStep = $params [$key] ['step'];
				}
			}
		}
		// d($maxStep);// отладочная информация
		return $maxStep;
	}* */
	
	/**
	 * @access	public
	 * @todo метод возвращает информацию о пользовательских значениях параметров, сохраняемых в переменную $_SESSION из 
	 * переменной $_POST
	 * @param $step = null - шаг процесса подбора 
	 * @return $params - массив, содержащий описание пользовательских значений параметров, сохраняемых в $_SESSION
	 
	public static function setParamsFromPost($step = null) {
		
		$params = array ();
		
		if ($step < 0) {
			$step = 0;
		}
		
		if(Dispatcher_Single::$post['ajax'] == 1){
			$paramsFromPost = Dispatcher_Single::$post['params'];
		} else{
			$paramsFromPost = Dispatcher_Single::$post;
		}
		//d($paramsFromPost);
		if (! empty( $paramsFromPost )) {
			
			foreach ( $paramsFromPost as $paramId => $value ) {
				if (substr_count ( $paramId, "param" ) && ($value != null) && ($value != '')) {
					$key = str_replace ( "param", '', $paramId );
					
					if (substr_count ( $key, '_min' )) {
						
						$id = str_replace ( '_min', '', $key );
						$params [$id] ['min'] = str_replace ( ',', '.', $value );
						$params [$id] ['step'] = $step;
						
						if($params [$id] ['max'] == null){
							$params [$id] ['max'] = '';
						}
						
					} elseif (substr_count ( $key, '_max' )) {									
						$id = str_replace ( '_max', '', $key );
						$params [$id] ['max'] = str_replace ( ',', '.', $value );
						
						if($params [$id] ['min'] == null){
							$params [$id] ['min'] = '';
						}
						$params [$id] ['step'] = $step;
					} else {
						
						$params [$key] ['step'] = $step;
						$params [$key] ['min'] = str_replace ( ',', '.', $value );
						$params [$key] ['max'] = str_replace ( ',', '.', $value );
					}
				}
			}
				
			$session_params = $_SESSION['params'];
			
			if ($session_params != null) {
				
				$paramsToSave = $params;
				
				foreach ( $session_params as $paramId => $value ) {
					
					if(array_key_exists($paramId, $params)){
						$paramsToSave[$paramId] = $params[$paramId];
					} elseif ((array_key_exists($paramId, $paramsFromPost)) &&
							(($paramsFromPost[$paramId] == '') || ($paramsFromPost[$paramId] == null))){
						continue;
					} 
					else{
						$paramsToSave[$paramId] = $value;
					}
				}
			}
			else{
				$paramsToSave = $params;
			}
			
			$_SESSION['params'] = $paramsToSave;
		} else {
			$params = null;
		}
		
		return $params;
	}* */
	
	/**
	 * @access	public
	 * @todo Конструктор. Создаёт объект класса Param, инициализируя его поля $this->groupId, $this->type, $this->id, $this->name, 
	 * $this->isRange на основе ID параметра.
	 * @param $id - ID параметра
	 * @return экземпляр класса
	 
	public function __construct($id) {
		
		$query = 'select p.`paramtype_id`, p.`name`, p.`isRange`, p.`parametergroups` as paramgroup from `parameters` p where p.`id` = ' . $id;	//, p.`multiselection`
		
		$db = Db::getInstance ();
		$result = $db->getRow ( $query );
		
		$this->groupId = $result['paramgroup'];
		$this->type = $result ['paramtype_id'];
		$this->id = $id;
		$this->name = $result ['name'];
		$this->isRange = $result ['isRange'];
	}* */
	
	/**
	 * @deprecated
	 * @access	public
	 * @todo 
	 * @param void
	 * @return
	 
	public function getParamGroup(){		
		return $this->groupId;
	}* */
	
	/**
	 * @deprecated
	 * @access	public
	 * @todo 
	 * @param void
	 * @return
	 
	public function getGroupInfo(){
		
		$sql = 'select pg.`name` as name, pg.`id` as id from `parametergroups` pg where pg.`id` = ' .$this->groupId;
		$result = Db::getInstance()->getRow($sql);
		
		$sqlcount = 'select count(*) as count from `parameters` p where p.`parametergroups` = '. $this->groupId;
		$resultcount = Db::getInstance()->getRow($sqlcount);

		$result['count'] = $resultcount['count'];		
		
		return $result;
	}* */
	
	/**
	 * @access	public
	 * @todo метод возвращает для текущего экземпляра параметра все допустимые для него единицы измерения 
	 * @param void
	 * @return $mu_names - массив единиц измерения параметра в виде SQL-запроса с полями
	 * mb_id, mb_name, coeff, shift
	 
	public function getParamMeasurementUnits() {
		
		$sql = '
				select
					`mb`.`id` as mb_id, `mb`.`name` as mb_name, `mb`.`coeff` as coeff, `mb`.`shift` as shift
				from
					`parameters` p, `phisquantity` ph, `measurebase` mb
				where
    				`p`.`phisquantity_id` = `ph`.`id`
    				and
    				`ph`.`id` = `mb`.`phisquantity_id`
    				and
					`p`.`id` = ' . $this->id . ';';		
		
		$mu_names = Db::getInstance()->executeS($sql);
				
		return $mu_names;
	}* */
	
	/**
	 * @abstract must be !!! - метод может существовать только для параметров с предопределёнными значениями
	 * @access	public
	 * @todo метод возвращает список предопределёных значений для характеристики прибора, если они есть 
	 * @param $id - ID параметра
	 * @return $predDefValues_result - массив предопределённых значений в виде результата SQL-запроса с полями
	 * value, value_id, descr
	 
	public static function getAllPreddefVals($id){
		
		$db = Db::getInstance ();
		
		$predDefValues_sql = 'select `predefinedvalues`.`value` as value, `predefinedvalues`.`id` as value_id,
										`predefinedvalues`.`description` as descr
									   from (`parameters` join `predefinedvalues`)
									   WHERE
									   `predefinedvalues`.`param_id` = `parameters`.`id`
										and
										`parameters`.`id` = ' . $id . ';';
		
		$predDefValues_result = $db->executeS ( $predDefValues_sql );
		
		return $predDefValues_result;
		
	}* */
	
	/**
	 * @deprecated
	 * @access	public
	 * @todo метод сохраняет значение параметра с указанными ID параметра и значением в переменной  $_SESSION
	 * @param $id - ID параметра
	 * @param $value - значения параметра
	 * @return void
	 
	public static function setUserParam($id, $value){
		$_SESSION['params'][$id]['min'] = $value['minval'];
		$_SESSION['params'][$id]['max'] = $value['maxval'];
	}* */
	
	/**
	 * @abstract must be !!! - принцип получения значения и форма представления значения различны для 
	 * параметров различных типов 
	 * @access	public
	 * @todo метод возвращает по id параметра и id характеристики прибора значение характеристики прибора getRow($sql) 
	 * @param $paramId - ID параметра
	 * @param $value_id - ID значения
	 * @return $result описание структуры (+ пример):
	 * Array
		(
		    [id] => 188 - ID комбинации
		    [value_id] => 956 - ID предопределённого значения
		    [modelparametrization_id] => 155 - ID параметризации модели
		    [codeval] => С - код, возвращаемый в карту заказа
		    [priority] => 9 - приоритет параметризации модели
		    [codeorder] => 7 - позиция в карте заказа
		    [value] => соединение типа сэндвич - предопределённое значение
		)
	 
	public static function getValue($paramId, $value_id, $type = null) {
		
		if($type == null){
			$type = self::getTypeParam ( $paramId );
		}
		
		if (($type == self::_P_INT_VALUE_TYPE)) {
			$sql = 'select *
					from `intparams` i
					where i.`id` = ' . $value_id;
		} elseif (($type == self::_P_FLOAT_VALUE_TYPE)  || ($type == self::_P_LOGIC_VALUE)) {
			$sql = 'select i.* , `modelparametrization`.codeorder as codeorder
					from `floatparams` i, `modelparametrization`
					where (i.`id` = ' . $value_id . ') and (`modelparametrization`.`id` = i.`modelparametrization_id`)';			
		} elseif ($type == self::_P_PREDDEF_VALUE) {
			$sql = 'select i.*, `modelparametrization`.`codeorder` as codeorder, p.value as value
					from `stringparams` i, `modelparametrization`, `predefinedvalues` p			
					where (i.`id` = ' . $value_id . ') and (`modelparametrization`.`id` = i.`modelparametrization_id`) and (p.`id` = i.`value_id`)';											
		}
		$result = Db::getInstance ()->getRow ( $sql );
		$result['ptype'] = $type;
		//p($result);
		return $result;
	}* */
	
	/**
	 * @abstract must be !!! - принцип получения значения по умолчанию и форма представления различны для 
	 * параметров различных типов
	 * @access	public
	 * @todo метод возвращает массив значений с наибольшим приоритетом для заданной параметризации модели
	 * @param $modelParamID - ID параметризации модели
	 * @param $param_id = 0 - ID параметра
	 * @return $result - массив значений с наибольшим приоритетом для заданной параметризации модели
	 
	public static function getDefaulVal($modelParamID, $param_id = 0){
		
		if($param_id == 0){
			
			$param_id = self::getParamIdByModel_parId($modelParamID);
			$param_id = $param_id['param_id'];
		}

		$paramType = self::getTypeParam($param_id);
			
		if($paramType == Param::_P_INT_VALUE_TYPE ){
			$sql = 'select ip.`id` as value_id
				from `intparams` ip
				where
				`ip`.`modelparametrization_id` = '.$modelParamID.'
				and
				ip.`priority` = (select MAX(ip.`priority`)
					from `intparams` ip
					where `ip`.`modelparametrization_id` = '.$modelParamID.')';
		}
			
		if($paramType == Param::_P_FLOAT_VALUE_TYPE || $paramType == Param::_P_LOGIC_VALUE){
			$sql = 'select fp.`id` as value_id
				from `floatparams` fp
				where
				`fp`.`modelparametrization_id` = '.$modelParamID.'
				and
				fp.`priority` = (select MAX(fp.`priority`)
					from `floatparams` fp
					where `fp`.`modelparametrization_id` = '.$modelParamID.')';
		}
			
		if($paramType == Param::_P_PREDDEF_VALUE){
			$sql = 'select sp.`id` as value_id
				from `stringparams` sp
				where
				`sp`.`modelparametrization_id` = '.$modelParamID.'
				and
				sp.`priority` = (select MAX(sp.`priority`)
					from `stringparams` sp
					where `sp`.`modelparametrization_id` = '.$modelParamID.')';
		}
			
		$resultSql = Db::getInstance()->executeS($sql);
		$result = array();
			
		foreach($resultSql as $one_val){
			$result[] = $one_val['value_id'];
		}
			
		return $result;
	}* */
	
	/**
	 * @todo метод возвращает вес параметра по его параметризации модели (modelparam_id)
	 * @access	public
	 * @param $modelparam_id - ID параметризации модели
	 * @return $result['weight'] - вес параметра
	 
	public static function getWeight($modelparam_id){
		
		$result = self::getParamIdByModel_parId($modelparam_id);
		
		return $result['weight'];
	}* */
	
	/** 
	 * @todo метод возвращает тип параметра по id параметра 
	 * @access	public
	 * @param $param_id - ID параметра
	 * @return $result [0] ['paramtype_id'] - ID типаЫ параметра
	 
	public static function getTypeParam($param_id) {
		
		$sql = 'select p.paramtype_id from parameters p where p.id = ' . $param_id;
		$result = Db::getInstance ()->executeS ( $sql );
		
		return $result [0] ['paramtype_id'];
	}* */
	
	/** 
	 * @access	public
	 * @todo метод возвращает описание параметра и рекомендации по его выбору 
	 * @param $id - id параметра
	 * @return описание параметра
	 
	public function getDescrRecm($id) {
		
		$sql = 'select 
				description as dsc, 
				recomendations as rcm 
				from 
				parameters p 
				where p.devicetype_id = 11 and p.parameter_id = ' . $this->id . '';
		
		$result = Db::getInstance ()->executeS ( $sql );
		$res = $result [0] ["dsc"] . '; ' . $result [0] ["rcm"];
		
		return $res;
	}* */
	
	/** 
	 * @access	public
	 * @todo метод возвращает массив характеристик прибора по id параметризации 
	 * @param $model_par_id - ID параметризации модели
	 * @return $result - в виде 1 строки SQL - запроса (набор характеристик прибора всегда однозначен!)
	 
	public static function get____ParamIdByModel_parId($model_par_id) {
		
		$sql = ' select 
	    `modelparametrization`.`id` AS `id`,
    	`parameters`.`id` AS `param_id`,
	    `parameters`.`paramtype_id` AS `ptype`,
    	`modelparametrization`.`codeorder` AS `codeorder`,
	    `modelparametrization`.`weight` AS `weight`,
    	`parameters`.`comparetype` AS `cmptype` 
  		from 
    	(`modelparametrization` join `parameters`)
		where 
  		`modelparametrization`.`param_id` = `parameters`.`id`
    	and 
    	modelparametrization.`id` =' . $model_par_id;
		
		$result = Db::getInstance ()->getRow ( $sql );
		//p($result);
		return $result;
	}* */

	/**
	 * @access	public
	 * @todo метод возвращает тип сравнения для параметра с указанным ID
	 * @param $paramID - ID параметра 
	 * @return $result[0]['comparetype'] - тип сравнения для параметра с указанным ID
	 
	public static function getCompareType($paramID){
		
		$sql = 'select p.comparetype from parameters p where p.id = '.$paramID;
		$result = Db::getInstance()->getRow($sql);		
		
		return $result['comparetype'];
	}* */
	
	/**
	 *@access public static
	 *@todo метод по указанному пользователем значению $value_id параметра $param_id, находит в массиве $modelMatches
	 *параметры, связанные с ним, и возвращает массив $result связанных параметров вместе с массивами их значений, 
	 *совместимых со значением параметра, выбранным пользователем
	 *@param $modelMatches - массив исходных комбинаций, из которых выбираются подходящие значения параметровю
	 *В точности совпадает с полем $completeMatch класса Selection: $modelMatches = Selection->$completeMatch   
	 *@param $param_id - ID параметра, для которого пользователь ввёл значение
	 *@param $value_id - ID значения, выбранное пльзователем для параметра $param_id
	 *@param $model_id - ID модели, для которой ищутся подходящие значения параметров 
	 *@return $result - массив вида {[param_id]=>arr[val_id=>val_str, ..., val_id=>val_str], ..., }
	 
	public static function  getSuitableValuesForParamValue($modelMatches, $model_id, $param_id, $value_id){
		$result = array();
		$selectionVariant = array();
		// найдём среди $modelMatches тот SelectionVariant, который 
		foreach ($modelMatches as $oneMatch){
			
			if($oneMatch['model_id'] == $model_id){
				$selectionVariant = $oneMatch;
			} else{
				$result = false;
				break;
			}
		}
		
		return $result;
	}***/
	
	/**
	 * 
	 * @todo изпользуя ID параметра и ID модели, возвращает ID параметризации модели 
	 * @param
	 * @param
	 
	public static function getModelParamIdByModelIdParamId($aModelId, $aParamId){
		
		$sql = "select id from modelparametrization mp where mp.model_id = ".$aModelId." and mp.param_id = ".$aParamId;
		$result = Db::getInstance()->executeS($sql);
		//p($result[0]['id']);
		
		return $result[0]['id'];
	}* */
}
