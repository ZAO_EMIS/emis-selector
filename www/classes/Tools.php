<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Tools extends ObjectModel
{
	protected static $file_exists_cache = array();
	protected static $_forceCompile;
	protected static $_caching;
	protected static $_user_plateform;
	protected static $_user_browser;
/**
 * 
 * @param unknown relative $url
 * @param string $headers
 */
	public static function redirect($url, $headers = null)
	{
	
		// Send additional headers
		if ($headers)
		{
			if (!is_array($headers))
				$headers = array($headers);
	
			foreach ($headers as $header)
				header($header);
		}
		header('Refresh: 0; url='.$url);
		echo '<h1>Редирект на </h1><a href='.$url.'>'.$url.'</a>';
		exit;
	}
	/**
	* Random password generator
	*
	* @param integer $length Desired length (optional)
	* @param string $flag Output type (NUMERIC, ALPHANUMERIC, NO_NUMERIC)
	* @return string Password
	*/
	public static function passwdGen($length = 8, $flag = 'ALPHANUMERIC')
	{
		switch ($flag)
		{
			case 'NUMERIC':
				$str = '0123456789';
				break;
			case 'NO_NUMERIC':
				$str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
			default:
				$str = 'abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
		}

		for ($i = 0, $passwd = ''; $i < $length; $i++)
			$passwd .= Tools::substr($str, mt_rand(0, Tools::strlen($str) - 1), 1);
		return $passwd;
	}

	public static function strReplaceFirst($search, $replace, $subject, $cur = 0)
	{
		return (strpos($subject, $search, $cur))?substr_replace($subject, $replace, (int)strpos($subject, $search, $cur), strlen($search)):$subject;
	}



	public static function getHttpHost($http = false, $entities = false, $ignore_port = false)
	{
		$host = (isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST']);
		if ($ignore_port && $pos = strpos($host, ':'))
			$host = substr($host, 0, $pos);
		if ($entities)
			$host = htmlspecialchars($host, ENT_COMPAT, 'UTF-8');
		return $host;
	}



	/**
	* Get the server variable SERVER_NAME
	*
	* @return string server name
	*/
	public static function getServerName()
	{
		if (isset($_SERVER['HTTP_X_FORWARDED_SERVER']) && $_SERVER['HTTP_X_FORWARDED_SERVER'])
			return $_SERVER['HTTP_X_FORWARDED_SERVER'];
		return $_SERVER['SERVER_NAME'];
	}

	/**
	* Get the server variable REMOTE_ADDR, or the first ip of HTTP_X_FORWARDED_FOR (when using proxy)
	*
	* @return string $remote_addr ip of client
	*/
	public static function getRemoteAddr()
	{
		// This condition is necessary when using CDN, don't remove it.
		if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] && (!isset($_SERVER['REMOTE_ADDR']) || preg_match('/^127\..*/i', trim($_SERVER['REMOTE_ADDR'])) || preg_match('/^172\.16.*/i', trim($_SERVER['REMOTE_ADDR'])) || preg_match('/^192\.168\.*/i', trim($_SERVER['REMOTE_ADDR'])) || preg_match('/^10\..*/i', trim($_SERVER['REMOTE_ADDR']))))
		{
			if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ','))
			{
				$ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
				return $ips[0];
			}
			else
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		return $_SERVER['REMOTE_ADDR'];
	}

	/**
	* Check if the current page use SSL connection on not
	*
	* @return bool uses SSL
	*/
	public static function usingSecureMode()
	{
		if (isset($_SERVER['HTTPS']))
			return in_array(Tools::strtolower($_SERVER['HTTPS']), array(1, 'on'));
		// $_SERVER['SSL'] exists only in some specific configuration
		if (isset($_SERVER['SSL']))
			return in_array(Tools::strtolower($_SERVER['SSL']), array(1, 'on'));
		// $_SERVER['REDIRECT_HTTPS'] exists only in some specific configuration
		if (isset($_SERVER['REDIRECT_HTTPS']))
			return in_array(Tools::strtolower($_SERVER['REDIRECT_HTTPS']), array(1, 'on'));
		if (isset($_SERVER['HTTP_SSL']))
			return in_array(Tools::strtolower($_SERVER['HTTP_SSL']), array(1, 'on'));

		return false;
	}


	/**
	* Get a value from $_POST / $_GET
	* if unavailable, take a default value
	*
	* @param string $key Value key
	* @param mixed $default_value (optional)
	* @return mixed Value
	*/
	public static function getValue($key, $default_value = false)
	{
		if (!isset($key) || empty($key) || !is_string($key))
			return false;
		$ret = (isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : $default_value));

		if (is_string($ret) === true)
			$ret = urldecode(preg_replace('/((\%5C0+)|(\%00+))/i', '', urlencode($ret)));
		return !is_string($ret)? $ret : stripslashes($ret);
	}


	/**
	* Sanitize a string
	*
	* @param string $string String to sanitize
	* @param boolean $full String contains HTML or not (optional)
	* @return string Sanitized string
	*/
	public static function safeOutput($string, $html = false)
	{
		if (!$html)
			$string = strip_tags($string);
		return @Tools::htmlentitiesUTF8($string, ENT_QUOTES);
	}

	public static function htmlentitiesUTF8($string, $type = ENT_QUOTES)
	{
		if (is_array($string))
			return array_map(array('Tools', 'htmlentitiesUTF8'), $string);

		return htmlentities((string)$string, $type, 'utf-8');
	}

	public static function htmlentitiesDecodeUTF8($string)
	{
		if (is_array($string))
		{
			$string = array_map(array('Tools', 'htmlentitiesDecodeUTF8'), $string);
			return (string)array_shift($string);
		}
		return html_entity_decode((string)$string, ENT_QUOTES, 'utf-8');
	}

	public static function safePostVars()
	{
		if (!isset($_POST) || !is_array($_POST))
			$_POST = array();
		else
			$_POST = array_map(array('Tools', 'htmlentitiesUTF8'), $_POST);
	}



	/**
	* Display an error according to an error code
	*
	* @param string $string Error message
	* @param boolean $htmlentities By default at true for parsing error message with htmlentities
	*/
	public static function displayError($string = 'Fatal error', $htmlentities = true, Context $context = null)
	{
		global $_ERRORS;

		if (is_null($context))
			$context = Context::getContext();

		@include_once(_PS_TRANSLATIONS_DIR_.$context->language->iso_code.'/errors.php');

		if (defined('_PS_MODE_DEV_') && _PS_MODE_DEV_ && $string == 'Fatal error')
			return ('<pre>'.print_r(debug_backtrace(), true).'</pre>');
		if (!is_array($_ERRORS))
			return $htmlentities ? Tools::htmlentitiesUTF8($string) : $string;
		$key = md5(str_replace('\'', '\\\'', $string));
		$str = (isset($_ERRORS) && is_array($_ERRORS) && array_key_exists($key, $_ERRORS)) ? $_ERRORS[$key] : $string;
		return $htmlentities ? Tools::htmlentitiesUTF8(stripslashes($str)) : $str;
	}

	/**
	 * Display an error with detailed object
	 *
	 * @param mixed $object
	 * @param boolean $kill
	 * @return $object if $kill = false;
	 */
	public static function dieObject($object, $kill = true)
	{
		echo '<xmp style="text-align: left;">';
		print_r($object);
		echo '</xmp><br />';

		if ($kill)
			die('END');

		return $object;
	}

	/**
	* Display a var dump in firebug console
	*
	* @param object $object Object to display
	*/
	public static function fd($object, $type = 'log')
	{
		$types = array('log', 'debug', 'info', 'warn', 'error', 'assert');
		
		if(!in_array($type, $types))
			$type = 'log';
		
		echo '
			<script type="text/javascript">
				console.'.$type.'('.Tools::jsonEncode($object).');
			</script>
		';
	}

	/**
	* ALIAS OF dieObject() - Display an error with detailed object
	*
	* @param object $object Object to display
	*/
	public static function d($object, $kill = true)
	{
		return (Tools::dieObject($object, $kill));
	}
	
	public static function debug_backtrace($start = 0, $limit = null)
	{
		$backtrace = debug_backtrace();
		array_shift($backtrace);
		for ($i = 0; $i < $start; ++$i)
			array_shift($backtrace);
		
		echo '
		<div style="margin:10px;padding:10px;border:1px solid #666666">
			<ul>';
		$i = 0;
		foreach ($backtrace as $id => $trace)
		{
			if ((int)$limit && (++$i > $limit ))
				break;
			$relative_file = (isset($trace['file'])) ? 'in /'.ltrim(str_replace(array(__ES__ROOT_DIR, '\\'), array('', '/'), $trace['file']), '/') : '';
			$current_line = (isset($trace['line'])) ? ':'.$trace['line'] : '';

			echo '<li>
				<b>'.((isset($trace['class'])) ? $trace['class'] : '').((isset($trace['type'])) ? $trace['type'] : '').$trace['function'].'</b>
				'.$relative_file.$current_line.'
			</li>';
		}
		echo '</ul>
		</div>';
	}

	/**
	* ALIAS OF dieObject() - Display an error with detailed object but don't stop the execution
	*
	* @param object $object Object to display
	*/
	public static function p($object)
	{
		return (Tools::dieObject($object, false));
	}

	/**
	* Check if submit has been posted
	*
	* @param string $submit submit name
	*/
	public static function isSubmit($submit)
	{
		return (
			isset($_POST[$submit]) || isset($_POST[$submit.'_x']) || isset($_POST[$submit.'_y'])
			|| isset($_GET[$submit]) || isset($_GET[$submit.'_x']) || isset($_GET[$submit.'_y'])
		);
	}



	/**
	* Encrypt password
	*
	* @param string $passwd String to encrypt
	*/
	public static function encrypt($passwd)
	{
		return md5(_COOKIE_KEY_.$passwd);
	}

	/**
	* Encrypt data string
	*
	* @param string $data String to encrypt
	*/
	public static function encryptIV($data)
	{
		return md5(_COOKIE_IV_.$data);
	}

	/**
	* Get token to prevent CSRF
	*
	* @param string $token token to encrypt
	*/
	public static function getToken($page = true, Context $context = null)
	{
		if (!$context)
			$context = Context::getContext();
		if ($page === true)
			return (Tools::encrypt($context->customer->id.$context->customer->passwd.$_SERVER['SCRIPT_NAME']));
		else
			return (Tools::encrypt($context->customer->id.$context->customer->passwd.$page));
	}

	/**
	 * Replace all accented chars by their equivalent non accented chars.
	 *
	 * @param string $str
	 * @return string
	 */
	public static function replaceAccentedChars($str)
	{
		/* One source among others:
			http://www.tachyonsoft.com/uc0000.htm
			http://www.tachyonsoft.com/uc0001.htm
			http://www.tachyonsoft.com/uc0004.htm
		*/
		$patterns = array(

			/* Lowercase */
			/* a  */ '/[\x{00E0}\x{00E1}\x{00E2}\x{00E3}\x{00E4}\x{00E5}\x{0101}\x{0103}\x{0105}\x{0430}]/u',
			/* b  */ '/[\x{0431}]/u',
			/* c  */ '/[\x{00E7}\x{0107}\x{0109}\x{010D}\x{0446}]/u',
			/* d  */ '/[\x{010F}\x{0111}\x{0434}]/u',
			/* e  */ '/[\x{00E8}\x{00E9}\x{00EA}\x{00EB}\x{0113}\x{0115}\x{0117}\x{0119}\x{011B}\x{0435}\x{044D}]/u',
			/* f  */ '/[\x{0444}]/u',
			/* g  */ '/[\x{011F}\x{0121}\x{0123}\x{0433}\x{0491}]/u',
			/* h  */ '/[\x{0125}\x{0127}]/u',
			/* i  */ '/[\x{00EC}\x{00ED}\x{00EE}\x{00EF}\x{0129}\x{012B}\x{012D}\x{012F}\x{0131}\x{0438}\x{0456}]/u',
			/* j  */ '/[\x{0135}\x{0439}]/u',
			/* k  */ '/[\x{0137}\x{0138}\x{043A}]/u',
			/* l  */ '/[\x{013A}\x{013C}\x{013E}\x{0140}\x{0142}\x{043B}]/u',
			/* m  */ '/[\x{043C}]/u',
			/* n  */ '/[\x{00F1}\x{0144}\x{0146}\x{0148}\x{0149}\x{014B}\x{043D}]/u',
			/* o  */ '/[\x{00F2}\x{00F3}\x{00F4}\x{00F5}\x{00F6}\x{00F8}\x{014D}\x{014F}\x{0151}\x{043E}]/u',
			/* p  */ '/[\x{043F}]/u',
			/* r  */ '/[\x{0155}\x{0157}\x{0159}\x{0440}]/u',
			/* s  */ '/[\x{015B}\x{015D}\x{015F}\x{0161}\x{0441}]/u',
			/* ss */ '/[\x{00DF}]/u',
			/* t  */ '/[\x{0163}\x{0165}\x{0167}\x{0442}]/u',
			/* u  */ '/[\x{00F9}\x{00FA}\x{00FB}\x{00FC}\x{0169}\x{016B}\x{016D}\x{016F}\x{0171}\x{0173}\x{0443}]/u',
			/* v  */ '/[\x{0432}]/u',
			/* w  */ '/[\x{0175}]/u',
			/* y  */ '/[\x{00FF}\x{0177}\x{00FD}\x{044B}]/u',
			/* z  */ '/[\x{017A}\x{017C}\x{017E}\x{0437}]/u',
			/* ae */ '/[\x{00E6}]/u',
			/* ch */ '/[\x{0447}]/u',
			/* kh */ '/[\x{0445}]/u',
			/* oe */ '/[\x{0153}]/u',
			/* sh */ '/[\x{0448}]/u',
			/* shh*/ '/[\x{0449}]/u',
			/* ya */ '/[\x{044F}]/u',
			/* ye */ '/[\x{0454}]/u',
			/* yi */ '/[\x{0457}]/u',
			/* yo */ '/[\x{0451}]/u',
			/* yu */ '/[\x{044E}]/u',
			/* zh */ '/[\x{0436}]/u',

			/* Uppercase */
			/* A  */ '/[\x{0100}\x{0102}\x{0104}\x{00C0}\x{00C1}\x{00C2}\x{00C3}\x{00C4}\x{00C5}\x{0410}]/u',
			/* B  */ '/[\x{0411}]]/u',
			/* C  */ '/[\x{00C7}\x{0106}\x{0108}\x{010A}\x{010C}\x{0426}]/u',
			/* D  */ '/[\x{010E}\x{0110}\x{0414}]/u',
			/* E  */ '/[\x{00C8}\x{00C9}\x{00CA}\x{00CB}\x{0112}\x{0114}\x{0116}\x{0118}\x{011A}\x{0415}\x{042D}]/u',
			/* F  */ '/[\x{0424}]/u',
			/* G  */ '/[\x{011C}\x{011E}\x{0120}\x{0122}\x{0413}\x{0490}]/u',
			/* H  */ '/[\x{0124}\x{0126}]/u',
			/* I  */ '/[\x{0128}\x{012A}\x{012C}\x{012E}\x{0130}\x{0418}\x{0406}]/u',
			/* J  */ '/[\x{0134}\x{0419}]/u',
			/* K  */ '/[\x{0136}\x{041A}]/u',
			/* L  */ '/[\x{0139}\x{013B}\x{013D}\x{0139}\x{0141}\x{041B}]/u',
			/* M  */ '/[\x{041C}]/u',
			/* N  */ '/[\x{00D1}\x{0143}\x{0145}\x{0147}\x{014A}\x{041D}]/u',
			/* O  */ '/[\x{00D3}\x{014C}\x{014E}\x{0150}\x{041E}]/u',
			/* P  */ '/[\x{041F}]/u',
			/* R  */ '/[\x{0154}\x{0156}\x{0158}\x{0420}]/u',
			/* S  */ '/[\x{015A}\x{015C}\x{015E}\x{0160}\x{0421}]/u',
			/* T  */ '/[\x{0162}\x{0164}\x{0166}\x{0422}]/u',
			/* U  */ '/[\x{00D9}\x{00DA}\x{00DB}\x{00DC}\x{0168}\x{016A}\x{016C}\x{016E}\x{0170}\x{0172}\x{0423}]/u',
			/* V  */ '/[\x{0412}]/u',
			/* W  */ '/[\x{0174}]/u',
			/* Y  */ '/[\x{0176}\x{042B}]/u',
			/* Z  */ '/[\x{0179}\x{017B}\x{017D}\x{0417}]/u',
			/* AE */ '/[\x{00C6}]/u',
			/* CH */ '/[\x{0427}]/u',
			/* KH */ '/[\x{0425}]/u',
			/* OE */ '/[\x{0152}]/u',
			/* SH */ '/[\x{0428}]/u',
			/* SHH*/ '/[\x{0429}]/u',
			/* YA */ '/[\x{042F}]/u',
			/* YE */ '/[\x{0404}]/u',
			/* YI */ '/[\x{0407}]/u',
			/* YO */ '/[\x{0401}]/u',
			/* YU */ '/[\x{042E}]/u',
			/* ZH */ '/[\x{0416}]/u');
			
			// ö to oe
			// å to aa
			// ä to ae

		$replacements = array(
				'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 'ss', 't', 'u', 'v', 'w', 'y', 'z', 'ae', 'ch', 'kh', 'oe', 'sh', 'shh', 'ya', 'ye', 'yi', 'yo', 'yu', 'zh',
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'V', 'W', 'Y', 'Z', 'AE', 'CH', 'KH', 'OE', 'SH', 'SHH', 'YA', 'YE', 'YI', 'YO', 'YU', 'ZH'
			);

		return preg_replace($patterns, $replacements, $str);
	}

	/**
	* Truncate strings
	*
	* @param string $str
	* @param integer $max_length Max length
	* @param string $suffix Suffix optional
	* @return string $str truncated
	*/
	/* CAUTION : Use it only on module hookEvents.
	** For other purposes use the smarty function instead */
	public static function truncate($str, $max_length, $suffix = '...')
	{
	 	if (Tools::strlen($str) <= $max_length)
	 		return $str;
	 	$str = utf8_decode($str);
	 	return (utf8_encode(substr($str, 0, $max_length - Tools::strlen($suffix)).$suffix));
	}

	/*Copied from CakePHP String utility file*/
	public static function truncateString($text, $length = 120, $options = array())
	{
		$default = array(
			'ellipsis' => '...', 'exact' => true, 'html' => true
		);

		$options = array_merge($default, $options);
		extract($options);

		if ($html)
		{
			if (Tools::strlen(preg_replace('/<.*?>/', '', $text)) <= $length) 
				return $text;

			$totalLength = Tools::strlen(strip_tags($ellipsis));
			$openTags = array();
			$truncate = '';
			preg_match_all('/(<\/?([\w+]+)[^>]*>)?([^<>]*)/', $text, $tags, PREG_SET_ORDER);

			foreach ($tags as $tag)
			{
				if (!preg_match('/img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param/s', $tag[2]))
				{
					if (preg_match('/<[\w]+[^>]*>/s', $tag[0]))
						array_unshift($openTags, $tag[2]);
					elseif (preg_match('/<\/([\w]+)[^>]*>/s', $tag[0], $closeTag))
					{
						$pos = array_search($closeTag[1], $openTags);
						if ($pos !== false)
							array_splice($openTags, $pos, 1);
					}
				}
				$truncate .= $tag[1];
				$contentLength = Tools::strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $tag[3]));

				if ($contentLength + $totalLength > $length)
				{
					$left = $length - $totalLength;
					$entitiesLength = 0;

					if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $tag[3], $entities, PREG_OFFSET_CAPTURE))
					{
						foreach ($entities[0] as $entity)
						{
							if ($entity[1] + 1 - $entitiesLength <= $left)
							{
								$left--;
								$entitiesLength += Tools::strlen($entity[0]);
							}
							else
								break;
						}
					}

					$truncate .= Tools::substr($tag[3], 0, $left + $entitiesLength);
					break;
				}
				else
				{
					$truncate .= $tag[3];
					$totalLength += $contentLength;
				}

				if ($totalLength >= $length)
					break;
			}
		}
		else
		{
			if (Tools::strlen($text) <= $length)
				return $text;

			$truncate = Tools::substr($text, 0, $length - Tools::strlen($ellipsis));
		}

		if (!$exact)
		{
			$spacepos = Tools::strrpos($truncate, ' ');
			if ($html)
			{
				$truncateCheck = Tools::substr($truncate, 0, $spacepos);
				$lastOpenTag = Tools::strrpos($truncateCheck, '<');
				$lastCloseTag =  Tools::strrpos($truncateCheck, '>');

				if ($lastOpenTag > $lastCloseTag)
				{
					preg_match_all('/<[\w]+[^>]*>/s', $truncate, $lastTagMatches);
					$lastTag = array_pop($lastTagMatches[0]);
					$spacepos =  Tools::strrpos($truncate, $lastTag) + Tools::strlen($lastTag);
				}

				$bits = Tools::substr($truncate, $spacepos);
				preg_match_all('/<\/([a-z]+)>/', $bits, $droppedTags, PREG_SET_ORDER);

				if (!empty($droppedTags))
				{
					if (!empty($openTags))
					{
						foreach ($droppedTags as $closingTag)
							if (!in_array($closingTag[1], $openTags))
								array_unshift($openTags, $closingTag[1]);
					}
					else
					{
						foreach ($droppedTags as $closingTag)
							$openTags[] = $closingTag[1];
					}
				}
			}

			$truncate = Tools::substr($truncate, 0, $spacepos);
		}

		$truncate .= $ellipsis;

		if ($html)
			foreach ($openTags as $tag)
				$truncate .= '</' . $tag . '>';

		return $truncate;
	}

	public static function normalizeDirectory($directory)
	{
		$last = $directory[strlen($directory) - 1];

		if (in_array($last, array('/', '\\'))) {
			$directory[strlen($directory) - 1] = DIRECTORY_SEPARATOR;
			return $directory;
		}

		$directory .= DIRECTORY_SEPARATOR;
		return $directory;
	}

	/**
	* Generate date form
	*
	* @param integer $year Year to select
	* @param integer $month Month to select
	* @param integer $day Day to select
	* @return array $tab html data with 3 cells :['days'], ['months'], ['years']
	*/
	public static function dateYears()
	{
		$tab = array();
		for ($i = date('Y'); $i >= 1900; $i--)
			$tab[] = $i;
		return $tab;
	}

	public static function dateDays()
	{
		$tab = array();
		for ($i = 1; $i != 32; $i++)
			$tab[] = $i;
		return $tab;
	}

	public static function dateMonths()
	{
		$tab = array();
		for ($i = 1; $i != 13; $i++)
			$tab[$i] = date('F', mktime(0, 0, 0, $i, date('m'), date('Y')));
		return $tab;
	}

	public static function hourGenerate($hours, $minutes, $seconds)
	{
	    return implode(':', array($hours, $minutes, $seconds));
	}

	public static function dateFrom($date)
	{
		$tab = explode(' ', $date);
		if (!isset($tab[1]))
		    $date .= ' '.Tools::hourGenerate(0, 0, 0);
		return $date;
	}

	public static function dateTo($date)
	{
		$tab = explode(' ', $date);
		if (!isset($tab[1]))
		    $date .= ' '.Tools::hourGenerate(23, 59, 59);
		return $date;
	}

	public static function strtolower($str)
	{
		if (is_array($str))
			return false;
		if (function_exists('mb_strtolower'))
			return mb_strtolower($str, 'utf-8');
		return strtolower($str);
	}

	public static function strlen($str, $encoding = 'UTF-8')
	{
		if (is_array($str))
			return false;
		$str = html_entity_decode($str, ENT_COMPAT, 'UTF-8');
		if (function_exists('mb_strlen'))
			return mb_strlen($str, $encoding);
		return strlen($str);
	}

	public static function stripslashes($string)
	{
		if (_PS_MAGIC_QUOTES_GPC_)
			$string = stripslashes($string);
		return $string;
	}

	public static function strtoupper($str)
	{
		if (is_array($str))
			return false;
		if (function_exists('mb_strtoupper'))
			return mb_strtoupper($str, 'utf-8');
		return strtoupper($str);
	}

	public static function substr($str, $start, $length = false, $encoding = 'utf-8')
	{
		if (is_array($str))
			return false;
		if (function_exists('mb_substr'))
			return mb_substr($str, (int)$start, ($length === false ? Tools::strlen($str) : (int)$length), $encoding);
		return substr($str, $start, ($length === false ? Tools::strlen($str) : (int)$length));
	}

	public static function strpos($str, $find, $offset = 0, $encoding = 'UTF-8')
	{
		if (function_exists('mb_strpos'))
			return mb_strpos($str, $find, $offset, $encoding);
		return strpos($str, $find, $offset);
	}

	public static function strrpos($str, $find, $offset = 0, $encoding = 'utf-8')
	{
		if (function_exists('mb_strrpos'))
			return mb_strrpos($str, $find, $offset, $encoding);
		return strrpos($str, $find, $offset);
	}	

	public static function ucfirst($str)
	{
		return Tools::strtoupper(Tools::substr($str, 0, 1)).Tools::substr($str, 1);
	}

	public static function ucwords($str)
	{
		if (function_exists('mb_convert_case'))
			return mb_convert_case($str, MB_CASE_TITLE);
		return ucwords(strtolower($str));
	}

	public static function orderbyPrice(&$array, $order_way)
	{
		foreach ($array as &$row)
			$row['price_tmp'] = Product::getPriceStatic($row['id_product'], true, ((isset($row['id_product_attribute']) && !empty($row['id_product_attribute'])) ? (int)$row['id_product_attribute'] : null), 2);
		if (strtolower($order_way) == 'desc')
			uasort($array, 'cmpPriceDesc');
		else
			uasort($array, 'cmpPriceAsc');
		foreach ($array as &$row)
			unset($row['price_tmp']);
	}

	public static function iconv($from, $to, $string)
	{
		if (function_exists('iconv'))
			return iconv($from, $to.'//TRANSLIT', str_replace('¥', '&yen;', str_replace('£', '&pound;', str_replace('€', '&euro;', $string))));
		return html_entity_decode(htmlentities($string, ENT_NOQUOTES, $from), ENT_NOQUOTES, $to);
	}

	public static function isEmpty($field)
	{
		return ($field === '' || $field === null);
	}


	/**
	 * returns the rounded value down of $value to specified precision
	 *
	 * @param float $value
	 * @param int $precision
	 * @return float
	 */
	public static function ceilf($value, $precision = 0)
	{
		$precision_factor = $precision == 0 ? 1 : pow(10, $precision);
		$tmp = $value * $precision_factor;
		$tmp2 = (string)$tmp;
		// If the current value has already the desired precision
		if (strpos($tmp2, '.') === false)
			return ($value);
		if ($tmp2[strlen($tmp2) - 1] == 0)
			return $value;
		return ceil($tmp) / $precision_factor;
	}

	/**
	 * returns the rounded value up of $value to specified precision
	 *
	 * @param float $value
	 * @param int $precision
	 * @return float
	 */
	public static function floorf($value, $precision = 0)
	{
		$precision_factor = $precision == 0 ? 1 : pow(10, $precision);
		$tmp = $value * $precision_factor;
		$tmp2 = (string)$tmp;
		// If the current value has already the desired precision
		if (strpos($tmp2, '.') === false)
			return ($value);
		if ($tmp2[strlen($tmp2) - 1] == 0)
			return $value;
		return floor($tmp) / $precision_factor;
	}

	/**
	 * file_exists() wrapper with cache to speedup performance
	 *
	 * @param string $filename File name
	 * @return boolean Cached result of file_exists($filename)
	 */
	public static function file_exists_cache($filename)
	{
		if (!isset(self::$file_exists_cache[$filename]))
			self::$file_exists_cache[$filename] = file_exists($filename);
		return self::$file_exists_cache[$filename];
	}

	/**
	 * file_exists() wrapper with a call to clearstatcache prior
	 *
	 * @param string $filename File name
	 * @return boolean Cached result of file_exists($filename)
	 */
	public static function file_exists_no_cache($filename)
	{
		clearstatcache();
		return file_exists($filename);
	}

	public static function file_get_contents($url, $use_include_path = false, $stream_context = null, $curl_timeout = 5)
	{
		if ($stream_context == null && preg_match('/^https?:\/\//', $url))
			$stream_context = @stream_context_create(array('http' => array('timeout' => $curl_timeout)));
		if (in_array(ini_get('allow_url_fopen'), array('On', 'on', '1')) || !preg_match('/^https?:\/\//', $url))
			return @file_get_contents($url, $use_include_path, $stream_context);
		elseif (function_exists('curl_init'))
		{
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
			curl_setopt($curl, CURLOPT_TIMEOUT, $curl_timeout);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
			if ($stream_context != null) {
				$opts = stream_context_get_options($stream_context);
				if (isset($opts['http']['method']) && Tools::strtolower($opts['http']['method']) == 'post')
				{
					curl_setopt($curl, CURLOPT_POST, true);
					if (isset($opts['http']['content']))
					{
						parse_str($opts['http']['content'], $datas);
						curl_setopt($curl, CURLOPT_POSTFIELDS, $datas);
					}
				}
			}
			$content = curl_exec($curl);
			curl_close($curl);
			return $content;
		}
		else
			return false;
	}

	public static function simplexml_load_file($url, $class_name = null)
	{
		return @simplexml_load_string(Tools::file_get_contents($url), $class_name);
	}
	
	public static function copy($source, $destination, $stream_context = null)
	{
		if (is_null($stream_context) && !preg_match('/^https?:\/\//', $source))
			return @copy($source, $destination);
		return @file_put_contents($destination, Tools::file_get_contents($source, false, $stream_context));
	}



	/**
	* Translates a string with underscores into camel case (e.g. first_name -> firstName)
	* @prototype string public static function toCamelCase(string $str[, bool $capitalise_first_char = false])
	*/
	public static function toCamelCase($str, $catapitalise_first_char = false)
	{
		$str = Tools::strtolower($str);
		if ($catapitalise_first_char)
			$str = Tools::ucfirst($str);
		return preg_replace_callback('/_+([a-z])/', create_function('$c', 'return strtoupper($c[1]);'), $str);
	}

	/**
	 * Transform a CamelCase string to underscore_case string
	 *
	 * @param string $string
	 * @return string
	 */
	public static function toUnderscoreCase($string)
	{
		// 'CMSCategories' => 'cms_categories'
		// 'RangePrice' => 'range_price'
		return strtolower(trim(preg_replace('/([A-Z][a-z])/', '_$1', $string), '_'));
	}

	public static function getBrightness($hex)
	{
		$hex = str_replace('#', '', $hex);
		$r = hexdec(substr($hex, 0, 2));
		$g = hexdec(substr($hex, 2, 2));
		$b = hexdec(substr($hex, 4, 2));
		return (($r * 299) + ($g * 587) + ($b * 114)) / 1000;
	}



	/**
	 * Convert an array to json string
	 *
	 * @param array $data
	 * @return string json
	 */
	public static function jsonEncode($data)
	{
		if (function_exists('json_encode'))
			return json_encode($data);
		else
		{
			include_once(_ES_TOOL_DIR_.'json/json.php');
			$pear_json = new Services_JSON();
			return $pear_json->encode($data);
		}
	}

	/**
	 * Display a warning message indicating that the method is deprecated
	 */
	public static function displayAsDeprecated($message = null)
	{
		$backtrace = debug_backtrace();
		$callee = next($backtrace);
		$class = isset($callee['class']) ? $callee['class'] : null;
		if ($message === null)
			$message = 'The function '.$callee['function'].' (Line '.$callee['line'].') is deprecated and will be removed in the next major version.';
		$error = 'Function <b>'.$callee['function'].'()</b> is deprecated in <b>'.$callee['file'].'</b> on line <b>'.$callee['line'].'</b><br />';

		Tools::throwDeprecated($error, $message, $class);
	}

	/**
	 * Display a warning message indicating that the parameter is deprecated
	 */
	public static function displayParameterAsDeprecated($parameter)
	{
		$backtrace = debug_backtrace();
		$callee = next($backtrace);
		$error = 'Parameter <b>'.$parameter.'</b> in function <b>'.(isset($callee['function']) ? $callee['function'] : '').'()</b> is deprecated in <b>'.$callee['file'].'</b> on line <b>'.(isset($callee['line']) ? $callee['line'] : '(undefined)').'</b><br />';
		$message = 'The parameter '.$parameter.' in function '.$callee['function'].' (Line '.(isset($callee['line']) ? $callee['line'] : 'undefined').') is deprecated and will be removed in the next major version.';
		$class = isset($callee['class']) ? $callee['class'] : null;

		Tools::throwDeprecated($error, $message, $class);
	}

	public static function displayFileAsDeprecated()
	{
		$backtrace = debug_backtrace();
		$callee = current($backtrace);
		$error = 'File <b>'.$callee['file'].'</b> is deprecated<br />';
		$message = 'The file '.$callee['file'].' is deprecated and will be removed in the next major version.';
		$class = isset($callee['class']) ? $callee['class'] : null;
		
		Tools::throwDeprecated($error, $message, $class);
	}

	public static function str_replace_once($needle, $replace, $haystack)
	{
		$pos = strpos($haystack, $needle);
		if ($pos === false)
			return $haystack;
		return substr_replace($haystack, $replace, $pos, strlen($needle));
	}

	/**
	 * Function property_exists does not exist in PHP < 5.1
	 *
	 * @deprecated since 1.5.0 (PHP 5.1 required, so property_exists() is now natively supported)
	 * @param object or class $class
	 * @param string $property
	 * @return boolean
	 */
	public static function property_exists($class, $property)
	{
		Tools::displayAsDeprecated();

		if (function_exists('property_exists'))
			return property_exists($class, $property);

		if (is_object($class))
			$vars = get_object_vars($class);
		else
			$vars = get_class_vars($class);

		return array_key_exists($property, $vars);
	}

	/**
	 * @desc identify the version of php
	 * @return string
	 */
	public static function checkPhpVersion()
	{
		$version = null;

		if (defined('PHP_VERSION'))
			$version = PHP_VERSION;
		else
			$version  = phpversion('');

		//Case management system of ubuntu, php version return 5.2.4-2ubuntu5.2
		if (strpos($version, '-') !== false)
			$version  = substr($version, 0, strpos($version, '-'));

		return $version;
	}

	/**
	 * @desc try to open a zip file in order to check if it's valid
	 * @return bool success
	 */
	public static function ZipTest($from_file)
	{
		if (class_exists('ZipArchive', false))
		{
			$zip = new ZipArchive();
			return ($zip->open($from_file, ZIPARCHIVE::CHECKCONS) === true);
		}
		else
		{
			require_once(__ES_ROOT_DIR_.'/tools/pclzip/pclzip.lib.php');
			$zip = new PclZip($from_file);
			return ($zip->privCheckFormat() === true);
		}
	}

	/**
	 * @desc extract a zip file to the given directory
	 * @return bool success
	 */
	public static function ZipExtract($from_file, $to_dir)
	{
		if (!file_exists($to_dir))
			mkdir($to_dir, 0777);
		if (class_exists('ZipArchive', false))
		{
			$zip = new ZipArchive();
			if ($zip->open($from_file) === true && $zip->extractTo($to_dir) && $zip->close())
				return true;
			return false;
		}
		else
		{
			require_once(__ES_ROOT_DIR_.'/tools/pclzip/pclzip.lib.php');
			$zip = new PclZip($from_file);
			$list = $zip->extract(PCLZIP_OPT_PATH, $to_dir);
			foreach ($list as $file)
				if ($file['status'] != 'ok' && $file['status'] != 'already_a_directory')
					return false;
			return true;
		}
	}

	public static function chmodr($path, $filemode)
	{
	    if (!is_dir($path))
	        return @chmod($path, $filemode);
	    $dh = opendir($path);
	    while (($file = readdir($dh)) !== false)
		{
	        if ($file != '.' && $file != '..')
			{
	            $fullpath = $path.'/'.$file;
	            if (is_link($fullpath))
	                return false;
	            elseif (!is_dir($fullpath) && !@chmod($fullpath, $filemode))
	                    return false;
	            elseif (!Tools::chmodr($fullpath, $filemode))
	                return false;
	        }
	    }
	    closedir($dh);
	    if (@chmod($path, $filemode))
	        return true;
	    else
	        return false;
	}



	/**
	 * Display error and dies or silently log the error.
	 *
	 * @param string $msg
	 * @param bool $die
	 * @return bool success of logging
	 */
	public static function dieOrLog($msg, $die = true)
	{
		if ($die || (defined('_PS_MODE_DEV_') && _ES_MODE_DEV_))
			die($msg);
		return PrestaShopLogger::addLog($msg);
	}

	/**
	 * Convert \n and \r\n and \r to <br />
	 *
	 * @param string $string String to transform
	 * @return string New string
	 */
	public static function nl2br($str)
	{
		return str_replace(array("\r\n", "\r", "\n"), '<br />', $str);
	}


	/**
	 * @params string $path Path to scan
	 * @params string $ext Extention to filter files
	 * @params string $dir Add this to prefix output for example /path/dir/*
	 *
	 * @return array List of file found
	 * @since 1.5.0
	 */
	public static function scandir($path, $ext = 'php', $dir = '', $recursive = false)
	{
		$path = rtrim(rtrim($path, '\\'), '/').'/';
		$real_path = rtrim(rtrim($path.$dir, '\\'), '/').'/';
		$files = scandir($real_path);
		if (!$files)
			return array();

		$filtered_files = array();

		$real_ext = false;
		if (!empty($ext))
			$real_ext = '.'.$ext;
		$real_ext_length = strlen($real_ext);

		$subdir = ($dir) ? $dir.'/' : '';
		foreach ($files as $file)
		{
			if (!$real_ext || (strpos($file, $real_ext) && strpos($file, $real_ext) == (strlen($file) - $real_ext_length)))
				$filtered_files[] = $subdir.$file;

			if ($recursive && $file[0] != '.' && is_dir($real_path.$file))
				foreach (Tools::scandir($path, $ext, $subdir.$file, $recursive) as $subfile)
					$filtered_files[] = $subfile;
		}
		return $filtered_files;
	}



	
	/**
	 * Delete a substring from another one starting from the right
	 * @param string $str
	 * @param string $str_search
	 * @return string
	 */
	public static function rtrimString($str, $str_search)
	{
		$length_str = strlen($str_search);
		if (strlen($str) >= $length_str && substr($str, -$length_str) == $str_search)
			$str = substr($str, 0, -$length_str);
		return $str;
	}

	/**
	 * Format a number into a human readable format
	 * e.g. 24962496 => 23.81M
	 * @param     $size
	 * @param int $precision
	 *
	 * @return string
	 */
	public static function formatBytes($size, $precision = 2)
	{
		if (!$size)
			return '0';
		$base = log($size) / log(1024);
		$suffixes = array('', 'k', 'M', 'G', 'T');

		return round(pow(1024, $base - floor($base)), $precision).$suffixes[floor($base)];
	}


	public static function getUserPlatform()
	{
		if (isset(self::$_user_plateform))
			return self::$_user_plateform;

		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		self::$_user_plateform = 'unknown';

		if (preg_match('/linux/i', $user_agent))
			self::$_user_plateform = 'Linux';
		elseif (preg_match('/macintosh|mac os x/i', $user_agent))
			self::$_user_plateform = 'Mac';
		elseif (preg_match('/windows|win32/i', $user_agent))
			self::$_user_plateform = 'Windows';

		return self::$_user_plateform;
	}

	public static function getUserBrowser()
	{
		if (isset(self::$_user_browser))
			return self::$_user_browser;

		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		self::$_user_browser = 'unknown';

		if(preg_match('/MSIE/i',$user_agent) && !preg_match('/Opera/i',$user_agent))
			self::$_user_browser = 'Internet Explorer';
		elseif(preg_match('/Firefox/i',$user_agent))
			self::$_user_browser = 'Mozilla Firefox';
		elseif(preg_match('/Chrome/i',$user_agent))
			self::$_user_browser = 'Google Chrome';
		elseif(preg_match('/Safari/i',$user_agent))
			self::$_user_browser = 'Apple Safari';
		elseif(preg_match('/Opera/i',$user_agent))
			self::$_user_browser = 'Opera';
		elseif(preg_match('/Netscape/i',$user_agent))
			self::$_user_browser = 'Netscape';

		return self::$_user_browser;
	}
	
	/**
	 * addCSS return stylesheet path.
	 *
	 * @param mixed $css_uri
	 * @param string $css_media_type
	 * @param bool $need_rtl
	 * @return string
	 */
	public static function getCSSPath($css_uri, $css_media_type = 'all', $need_rtl = true)
	{
		return Tools::getMediaPath($css_uri, $css_media_type);
	}
	
	public static function getMediaPath($media_uri, $css_media_type = null)
	{
		
		if (is_array($media_uri) || $media_uri === null || empty($media_uri))
			return false;
		
		$url_data = parse_url($media_uri);
		if (!is_array($url_data))
			return false;

		$file_uri = '';
		if (!array_key_exists('host', $url_data))
		{
			$media_uri_host_mode ='/'.ltrim(str_replace(str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, __ES__ROOT_DIR), _ES_ROOT_URL, $media_uri), '/\\');
			$media_uri = '/'.ltrim(str_replace(str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, __ES__ROOT_DIR), _ES_ROOT_URL, $media_uri), '/\\');
			
			$url_data['path'] = $media_uri;
			// remove PS_BASE_URI on _PS_ROOT_DIR_ for the following
			$file_uri = __ES__ROOT_DIR.Tools::str_replace_once(_ES_ROOT_URL, DIRECTORY_SEPARATOR, $url_data['path']);
			$file_uri_host_mode = __ES__ROOT_DIR.Tools::str_replace_once(_ES_ROOT_URL, DIRECTORY_SEPARATOR, Tools::str_replace_once(__ES__ROOT_DIR, '', $url_data['path']));						
		}
		// check if css files exists
		if (!array_key_exists('host', $url_data))
		{
			if (!@filemtime($file_uri) || @filesize($file_uri) === 0)
			{
				$media_uri = $media_uri_host_mode;
				$file_uri = $file_uri_host_mode;
			}
		}
	
		if (!array_key_exists('host', $url_data))
			$media_uri = str_replace('//', '/', $media_uri);
	
		if ($css_media_type)
		{	
			$result = array($media_uri => $css_media_type);
			return $result;
		}

		return $media_uri;
	}
	
	/**
	 * @access public
	 * @todo по заданному абсолютному пути $js_uri получает относительный путь к файлам JavaScript
	 * @param $js_uri - абсолютный путь к файлам JavaScript
	 * @return false в случае если передаваемый параметр пуст, с неопределённым значением (null) или является массивом
	 * 		$js_file - относительный путь к файлу в проитвном случае
	 * */
	public static function  getJsPath($js_uri){
		return Tools::getMediaPath($js_uri);
	}
}
