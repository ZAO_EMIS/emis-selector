<?php
/**
 * @extends ObjectModel
 * @todo Класс отвечает за все операции с комбинациями значений параметров, созданными на основе Depend. Структура информации
 * внутри класса - точно такая же, как в Depend ([значение определяемого параметра] -> [массив значений определяющих параметров]).
 * Зависимость (Depend) - шаблон для создания экземпляра комбинации (Combination). Шаблон, образуемый классами Depend и
 * Combination - фабричный метод. 
 * @see Depend->getDependValuesForValueID($model_param, $value_id, $currentStep = 2);
 * @property private $depend_id - ID комбинации
 * @property private $definedValue - значение определяемого параметра (экземпляр подкласса Value)
 * @property private $definingValues - массив вида ([paramID]->[Value]) значений определяющих параметров
 * @property private $summPriority = 0 - суммарное значение приоритетов для значений определяющих параметров
 * @see Depend
 * */
class Combination extends ObjectModel{
	
	/**
	 * @property ID комбинации
	 * */
	private $depend_id;
	
	/**
	 * @property Сумма приоритетов значений всех параметров в комбинации. В случае, если пользовательским условиям 
	 * удовлетворяет более одной комбинации, то при прочих равных условиях выбирается комбинация с наибольшим 
	 * суммарным приоритетом значений параметров.
	 * */
	private $summPriority = 0;
	
	/**
	 * @property Массив значений (экземпляров подклассов Values) определяющих параметров в комбинации. шаблоном для создания массива значений определяющих
	 * параметров является массив определяющих параметров в экземпляре зависимости (Depend).
	 * */
	private $definingValues = array(); // элементы массива - экземпляры подклассов Value (взамен $paramsValues)
	
	/**
	 * @property Значение определяемого парамметра (экземпляр подкласса Value)
	 * */
	private $definedValue;
	
	/**
	 * @todo Возврашает значение определяемого параметра в комбинации.
	 * */
	public function getDefinedValue(){
		return $this->definedValue;
	}
	
	/**
	 * @todo Возвращает массив [paramId]->[Value] значений определяющих параметров в коминации.
	 * */
	public function getDefiningValues(){
		return $this->definingValues;
	}
	
	/**
	 * @access	public 
	 * @todo метод возвращает массив ([paramID]->[valID]) для всех параметров комбинации - определяющих и определяемых
	 * @return $result - массив значений всех ID параметров комбинации
	 * */
	public function getParamsValueIds() {
		
		$result = array();
		
		foreach ($this->definingValues as $objDefiningValue){
			$result[$objDefiningValue->getParamId()] = $objDefiningValue->getValueId();
		}
		
		$result[$this->definedValue->getParamId()] = $this->definedValue->getValueId();
		
		return $result;
	}
	
	/**
	 * @access	public
	 * @todo метод возвращает массив пар ([paramID]->objValue) для всех параметров комбинации - определяющих и определяемых
	 * @param void
	 * @return $result - массив значений всех ID параметров комбинации
	 * */
	public function getParamsValues() {
	
		$result = array();
	
		foreach ($this->definingValues as $objDefiningValue){
			$result[$objDefiningValue->getParamId()] = $objDefiningValue;
		}
	
		$result[$this->definedValue->getParamId()] = $this->definedValue;
	
		return $result;
	}
	
	/**
	 * @access	public 
	 * @todo метод возвращает значение суммарного приоритета комбинации
	 * @param void
	 * @return $this->summPriority - значение суммарного приоритета
	 * */
	public function getSummPriority(){
		return $this->summPriority;
	} 
	
	/**
	 * @access	public 
	 * @todo метод возвращает ID комбинации
	 * @param void
	 * @return $this->depend_id - ID комбинации
	 * */
	public function getDepend_ID(){
		return $this->depend_id;
	}

	/**
	 * @access	public 
	 * @todo метод ворзвращает ID значения определяемого комбинацией параметра 
	 * @param void
	 * @return $this->value_id - ID значения определяемого параметра
	 * */
	public function getVal_ID(){
		//return $this->definedValue;
		return $this->definedValue->getValueId();
	}
	
	/**
	 * @access	public 
	 * @todo метод инициализирует поле $this->definedValue значением $aValue
	 * @param $aValue - значение определяемого параметра
	}
	* */
	public function setDefinedValue($aValue){
		$this->definedValue = $aValue;
	}
	
	/**
	 * @access	public 
	 * @todo конструктор. создаёт объект класса Combination, инициализируя его поля $this->depend_id, $this->definedValue
	 * @param $depend_id - ID комбинации 
	 * @param $aDefinedvalue - значение определяемого параметра
	 * */
	 public function __construct($depend_id, $aDefinedvalue){
	 	
		$this->depend_id = $depend_id;
		$this->definedValue = $aDefinedvalue;
	}
	
	/**
	 * @todo добавление значения определяющего параметра
	 * @param $definingValue  -значение определяющего параметра
	 * */
	public function addDefiningValue($definingValue){
		$this->definingValues[] = $definingValue;
	}
	
	/**
	 * @access	public 
	 * @todo Метод проверяет комбинацию на удовлетворение ползовательским значениям параметров. В случае, если значения 
	 * параметров, составляющие комбинацию, удовлетворяют пользовательским значениям, возвращает true, иначе - false.
	 * Попутно вычисляет (и устанавливает) суммарный приоритет значений параметров, если комбинация удовлетворяет 
	 * пользовательским значениям.
	 * @param $UserParams = null - массив пользовательских значений параметров
	 * @return $isOk - флаг соответствия комбинации пользовательским значениям параметров
	 * */
	public function checkForUserParams($UserParams = null, $modelParamsArr = null){
		
		$definingValues = array();
		$paramsVal = array();
		
		if(count($this->definingValues) > 0){
			$paramsVal = $this->definingValues;
		} else{
			$paramsVal = $this->getValuesForParams();
		}
		
		$isOk = true;		
		
		foreach($paramsVal as $val){
			
			if($val->getParamId() != null && $val->getParamId() != ''){
				
				$this->summPriority += $val->getPriority();
				
				$currentModelParam = $modelParamsArr[$val->getParamId()];
				
				$flag = $currentModelParam->compareParamWithUserVal($val);

				if ($flag){
					$definingValues[$val->getParamId()] = $val;
				} else{
					
					$isOk = false;
					$definingValues = null;
					
					break;
				}
			}
		}
		if($isOk){			
			
			$this->definingValues = $definingValues;
			$this->summPriority += $this->definedValue->getPriority();
			$this->isOk = $isOk;
		}			
		//p($this);
		return $isOk;
	}

	/**
	 * @access	public 
	 * @todo метод возвращает информацию об определяющих параметрах комбинации по указанному ID комбинации
	 * структура информации:
	 * mp_id - ID параметризации модели (определяющей) 
	 * value_id - ID значения определяющей параметризации модели
	 * paramtype_id - ID типа параметра
	 * id - ID параметра
	 * @return $result - результат SQL-запроса. см. структура информации. 
	 * */
	private function getValuesForParams(){

		$sql = 'select 
				  dp.`modelparametrization_independent_id` as mp_id
				, dp.`value_id` as value_id
				, p.`paramtype_id` as param_type
				, p.`id` as param_id
				from ((`dependence_parametrization` dp join `modelparametrization` `mp`) join `parameters` p)
				where 
                dp.`dependence_id` = '.$this->depend_id.'
                and
                `dp`.`modelparametrization_independent_id` = `mp`.`id`
                and
                 mp.`param_id` = p.`id`';
		//p($sql);

		$sqlResult = Db::getInstance()->executeS($sql);
		
		if($sqlResult == null){
			//p($sql);
			//d(debug_backtrace());
		}
		
		$definingValues = array();
		/* вернуть надо не результат запроса, а массив объектов Parameter */
		foreach ($sqlResult as $arrValueInfo){
			
			$value = Value::createInstance($arrValueInfo['param_id'], $arrValueInfo['param_type']);
			
			if(is_object($value)){
				
				$value->init($arrValueInfo['value_id'], $arrValueInfo['mp_id']);
				$definingValues[$arrValueInfo['param_id']] = $value;
			} else{
				//d($arrValueInfo);
			}
		}
		
		return $definingValues;
	}
/*	
	public function getValues($paramsInfoArr){
		//p(debug_backtrace());	
		//d($paramsInfoArr);
		//p($paramsInfoArr);
		$floatParams = "";
		$stringParams = "";
		
		foreach($paramsInfoArr as &$parInfo){
			//p($parInfo);
			
			if(($parInfo->getParamType() == Parameter::_P_FLOAT_VALUE_TYPE) or ($parInfo->getParamType() == Parameter::_P_LOGIC_VALUE)){
				//$floatParams .= $parInfo["value_id"] . ", ";
				$valueOfparam = $parInfo->getValues();
				$floatParams .= $valueOfparam[0]->getValueId() . ", ";
			} else{
				//$stringParams .= $parInfo["value_id"] . ", ";
				$valueOfparam = $parInfo->getValues();
				$stringParams .= $valueOfparam[0]->getValueId() . ", ";
			}
			
		}
		
		//p($floatParams);
		//p($stringParams);
		
		$floatParams = substr($floatParams, 0, count($floatParams) - 3);
		$stringParams = substr($stringParams, 0, count($stringParams) - 3);
		
		$db = Db::getInstance();		
		
		$floatSql = 'select						
		                `fp`.`minval`, `fp`.`maxval`, `fp`.`priority`, `fp`.`id`
						from `floatparams` fp
						where `fp`.`id` in ('.$floatParams.')';
		$floatResult = $db->executeS($floatSql);
		$stringSql = 'select
						`sp`.`priority`, `sp`.`value_id`, `sp`.`id`
						from `stringparams` sp
						where `sp`.`id` in ('.$stringParams.')';
		
		$stringResult = $db->executeS($stringSql);
		$result = array();
		
		foreach($paramsInfoArr as &$o_par){
			p(&$o_par);
			if(($o_par["paramtype_id"] == Param::_P_FLOAT_VALUE_TYPE) or ($o_par["paramtype_id"] == Param::_P_LOGIC_VALUE)){
				
				foreach($floatResult as &$floatPar){
					
					if ($floatPar['id'] == $o_par["value_id"]) {
						$o_par["value"] = array('minval' => $floatPar["minval"], "maxval" => $floatPar['maxval']);
					}
				}
			} else{
				
				foreach($stringResult as &$stringPar){
					
					if ($stringPar['id'] == $o_par["value_id"]) {
						$o_par["value"] = array('value_id' => $stringPar["value_id"]);
					}
				}
			}
		}		
		
		return $paramsInfoArr;
	}
*/	
	/**
	 * @access	public 
	 * @todo метод возвращает для данного объекта комбинации массив символов, входящих в карту заказа
	 * @param	void
	 * @return $result - массив символов, определённых значениями параметров данной комбинации, обарзующих подмножество
	 * символов в карте заказа на прибор. [paramId]->[$definingValue->getCode()]
	 * */
	public function getCodes() {
		
		$result = array();
		
		foreach ($this->definingValues as $paramId=>$definingValue){
			//$value = Param::getValue($paramId, $val_id);
			//$modelParam = Param::getParamIdByModel_parId($value['modelparametrization_id']);
			
			if($definingValue->getCode() != "null"){
				$result[$definingValue->getParamId()]=$definingValue->getCode();
			}
		}		
		//p($result);		
		return $result;
	}
	
	/**
	 * @access	public 
	 * @todo метод возвращает оп заданному ID параметра разность между максимальным значением параметра, взятым из комбинации и
	 * пользовательским значением параметра 
	 * @param 	$param_id - ID параметра 
	 * @return $diff - значение разности между пользовательским значением параметра и значением парамтера из комбинации
	 * 
	public function getMaxValueDifferenceForParam($param_id) {
		//p("вызов Combination->getMaxValueDifferenceForParam");
		
		$userParams = Parameter::getParamUserValuesByStep();
		$userVals = $userParams[$param_id];
		$combVal = $this->getValuesForParams();
		p($combVal);		
		$combVal = Param::getValue($param_id, $this->paramsValues[$param_id]);
		p($combVal);
		$diff = $combVal['maxval'] - $userVals['max'];
		
		return $diff;
	}
	*/
}