<?php
/**
 * Класс отвечает за представление и операции с параметром Вязкость различных типов.
 * */
class ViscosityParam extends FloatParam{
	
	const __DYNAMIC_VISCOSITY_TYPE__ = 1;
	const __CINEMATIC_VISCOSITY_TYPE__ = 0;
	
	const __VISCOSITY_ID__ = 77; // 
	const __VISCOSITYTYPE_ID__ = 143; //
	const __DENSITY_ID__ = 114;  //	 константа, определяющая плотность	 
	
	/**
	 * наличие этого поля - обязательно. почему:
	 * 
	 * если выбран массовый расход, и, соответственно, задана плотность среды, и, при этом, задана динамическая вязкость:
	 * если класс не сможет отличить, какой тип вязкости задан, он не сможет определить, нужен ли пересчёт.
	 * поле $viscosityType хранит признак необходимости выполнять вычисление динамической вязкости.
	 * */
	//private $viscosityType = self::__DYNAMIC_VISCOSITY_TYPE__;
	
	private static $pathViscosity = "viscosity.php";
	
	/**
	 * @see Parameter
	 * */
	public function getHTML(){
		
		if(!$this->checkForNeedInquire()){
			
			$dynamicViscosityParam = new FloatParam(new Parameter(self::__VISCOSITY_ID__));
			$dynamicViscosityParam->initForInquire();
			$dynamicViscosityParam->setCommonParamInfo(Parameter::paramInfoFromDb(self::__VISCOSITY_ID__));
			
			include(__ES__ROOT_DIR . '\\views\\paramView\\' . self::$pathViscosity);
		}
	}
	
	/**
	 * @see Parameter
	 * */
	protected function checkForNeedInquire(){
		
		$inquiredParamsIds = ObjectModel::getFromSessionByName('inquiredParamsIds');
		
		if(in_array(self::__VISCOSITY_ID__, array_keys($inquiredParamsIds))){
			return true;
		} else{
			return false;
		}
	}
	
	/**
	 * @see Parameter
	 * */
	 public function getSearchQuery($paramId, $value, $valueMax, $modelsTerm = null){
	
		if($value == ""){
			$value = $valueMax;
		}
		if($valueMax == ''){
			$valueMax = $value;
		}
	
		if($paramId != self::__DENSITY_ID__){
			$sql = parent::getSearchQuery($paramId, $value, $valueMax, $modelsTerm);
		} else{
			$sql = false;
		}
		//d($sql);
		return $sql;
	}
}
?>