<?php
class ObjectModel {
	
	public static $debug = 0;
	public static $startTime;

	protected static $debugList = array();
	
	/**
	 * сохранить в сессии объект (в текущем состоянии)
	 * @param $name - ключ, под которым будет сохраняться в сессии сохраняемая переменная $entityForSaving
	 * @param $entityForSaving - то, что бдует сохраняться в сессии - объект (в т. ч. $this) или не-объект (массив, значение, ...)
	 * */
	public static  function saveToSession($name, $entityForSaving){
		
		if ($name != null){
			
		} elseif(is_object($entityForSaving)){
			
			$name = get_class($entityForSaving);
			
			$n = 0;
				
			while(array_key_exists($name, $_SESSION)){
					
				$name = $name.$n;
				$n++;
			}
				
		} else{
			
			echo("Нельзя сохранить объект, не указав его имени.");
			die;
		}
		
		$_SESSION[$name] = $entityForSaving;
			
		return $name;
	}
	
	public function printTime(){
		$time = microtime(true) - self::$startTime;
		p($time);
		echo("\n");
	}
	
	/**
	 * получить из сессии не-объект или объект (в данном случае разницы нет) с сохранённым состоянием
	 * */
	public static function getFromSessionByName($name = null){
		
		$obj_val = null;
		/* возвращается объект, если имя объекта не пусто, и в сессии сохранён объект с таким ключом (=$name)
		 * иначе ничего не возвращается
		 * если в предоставленном имени нет числа - идентификатора, то метод возвращает ближайший объект с подходящим ключом (именем)
		 *  */
		if($name == null){
			$name = get_class($this) . "_Obj";
		}
		
		if(isset($_SESSION[$name])){
			$obj_val = $_SESSION[$name];
			//d($_SESSION);
		}
		else{
			$obj_val = false;
		}
		//p($obj_val);
		return $obj_val; 
	}
	
	public static function unsetInSessionByName($name = null){
		unset($_SESSION[$name]);
	}
}