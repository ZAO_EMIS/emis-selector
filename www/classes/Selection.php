<?php
/**
 * @name Selection - подбор
 
 * @todo головной класс, отвечающий за реализацию процесса пробора оборудования
 * 
 * @property private $completeMatch - массив прошедших проверку вариантов исполнения оборудования
 * каждый элемент массива - экземпляр класса SelectionVariant (см. спецификацию на класс SelectionVariant, 
 * файл SelectionVariant.php);
 * 
 * @property private $unmatch - массив вариантов исполнения, которые не подходят (точно) хотя бы по одному параметру,
 * но при этом по параметрам с начального экрана они проверку прошли;
 * 
 * @property private $curentStep - текущий шаг процесса подбора расходомера (оборудования);
 * 
 * @property public $modelInUse - на это свойство нет ссылок в как внутри кода класса, так и за его пределами
 * вероятно, полсе уточнения свойство будет исключено и з стуктуры класса;
 * */

class Selection extends ObjectModel{
	
	//константа, определяющая основной тип подбираемого оборудования 
	const _DEVICE_TYPE_FLOWMETER = 11;
	
	private $completeMatch=array();
	private $unmatch = array();
	private $curentStep;
	private $selectionId;
	//public $modelInUse = array();
	
	public function __construct($aModelId = null){}
	
	/**
	 * 
	 * */
	public function getSelectionId(){
		return $this->selectionId;
	}
	
	/**
	 * 
	 * */
	public function setSelectionId($aSelectionId){
		$this->selectionId = $aSelectionId;
	}
	
	/**
	 * 
	 * */
	public function deleteFromInquiredParams($paramIdsForDelete){
		//p($paramIdsForDelete);
		$inquiredParamsIds = ObjectModel::getFromSessionByName('inquiredParamsIds');
		
		foreach ($inquiredParamsIds as $key => $val){
			
			foreach ($paramIdsForDelete as $idForDeletion){
				//p($val);
				//p($idForDeletion);
				if($val == $idForDeletion){
					unset($inquiredParamsIds[$key]);
				}
			}
		}
		
		ObjectModel::saveToSession('inquiredParamsIds', $inquiredParamsIds);
	}
	
	/**
	 * @param $name - наименование параметра в сессии, из которой инициавлизируется экземпляр класса; 
	 * @todo Инициализирует экземпляр класса параметрами, сохранёнными в $_SESSION, откуда получает их по имени $name.
	 * В случае, если имя параметра $name не задано (null) - имя параметра генерируется внутри метода, как <имя_класса><_Obj>.
	 * Далее - собственно процесс инициализиции,
	 * @see ObjectModel::getFromSessionByName($name)
	 * @return $exemplair - экземпляр класса Selection, восстановленный из сессии
	 * */
	public function init($name = null){
		//если имя параметра не указано, то его надо составить. составляется из имени класса и суффикса _Obj.
		if($name == null){
			$name = get_class($this)."_Obj";
		}
		//получение по имени параметра из переменной $_SESSION экземпляра класса (разумеется, вместе с его состоянием)
		
		//d($name);
		$exemplair = ObjectModel::getFromSessionByName($name);
		//d($exemplair);
				
		if($exemplair){//если экземпляр класса не пуст (!= null)
			//инициализируем текущий экземпляр состоянием, взятым из экземпляра считанного из $_SESSION
			$this->setCompleteMatch($exemplair->getCompleteMatch());
			$this->setCurrentStep($exemplair->getCurrentStep());
			$this->setSelectionId($exemplair->getSelectionId());
		} else{ //в противном случае - текущий шаг =0, а список подборов - пуст.
			$this->curentStep = 0;
		}
		//p($this);
		return $exemplair;
	}
	
	/**
	 * @todo возвращает текущий шаг процесса подбора расходомера
	 * @return $this->curentStep - текущий шаг процесса подбора расходомера
	 * */
	public function getCurrentStep(){
		return $this->curentStep;
	}
	
	/**
	 * @todo установка текущего шага процесса подбора расходомера 
	 * @param $newCurrStep - новое значение шага процесса подбора расходомера
	 * @return null
	 * */
	public function setCurrentStep($newCurrStep){
		$this->curentStep = $newCurrStep;
	}
	
	/**
	 * @todo возвращает все подобранные на текущий момент (текущий шаг процесса подбора) исполнения (объекты типа SelectionVariant)
	 * @return $this->completeMatch - массив вариантов исполнения прибора (объектов типа SelectionVariant)
	 */
	public function getCompleteMatch(){
		return $this->completeMatch;
	}
	
	/**
	 * @todo инициализирует поле completeMatch текущего экземпляра класса массивом вариантов исполнения (объекты типа SelectionVariant),
	 * полученными в качестве входного параметра
	 * @param $matches - массив вариантов исполнения, которым будет инициализирован текущий экземпляр класса
	 * @return null
	 * */
	 public function setCompleteMatch($matches){
		$this->completeMatch = $matches;
	}
	
	
	
	/**
	 * @access public
	 * 
	 * @todo метод возвращает массив параметров подбираемого оборудования, запрашиваемых на новом шаге процесса подбора,
	 * одновременно заполняя массив ID запрошенных параметров $inquiredParamsIds.
	 * перед первым по счёту (нулевым - $this->curentStep == 0) шагом предыдущего шага нет.
	 * если параметры метода не указаны, то корректные значения по умолчанию им присваиваются внутри метода
	 * 
	 * @param $userParams = null - массив ID параметров, значения которых уже введены пользователем
	 *  
	 * @param $device_type = null - ID типа оборудования, подбор которого ведётся на данном этапе
	 * планируется, что в будущем будет вестись подбор не только расходомеров, но и 
	 * другого оборудования - как основного, так и дополнительного, с конечной целью формрования
	 * комплектов оборудования (возможно - стандартных комплектов для определённых условий работы).
	 *  
	 * @return $result -  массив объектов типа Param (см. Param.php)
	 * пример структуры объекта типа Param (полную спецификацию см. в классе Param, файл Param.php):
	 * Param Object(
     *     [type]=>7; [id]=>51; [values]=>; [isRange]=>; [value_id]=>; [name]=>Тип измеряемой среды; [measurment_units]=>Array(); 
     *     [multiselection:private]=>; [groupId:private]=>;
     * )
	 **/
	public function getParamsToInquire($userParams = null, $device_type = 11, $modelId = null, $doCheckForInquiring = true){
		
		$result = array();
		
		if($this->curentStep == 0){/* запросить параметры для первой итерации (начальный экран) */
			$result = Parameter::getStartStepForDeviceType($device_type);
		} else if($modelId != null){
			
			foreach ($this->completeMatch as &$model){
				
				if ($model->getModelId() == $modelId) {
					
					foreach ($model->checkForNeedParams() as $paramObj){
						
						$paramObj->doCheckForInquiring = $doCheckForInquiring;
						$result[$paramObj->getParamId()] = $paramObj;
					}
				}
				//d($result);
			}
		} else{
			/* в случае шага процесса подбора > 0 получаем список общих параметров для подобранных моделей
			 * (см. спецификацию метода $this->getCommonParamsForModels())
			 * */
			$needCommonParams = $this->getCommonParamsForModels();
			//p($needCommonParams);
			/*если общие параметры у подобранных моделей есть */
			if(count($needCommonParams) > 0){
				/* формируем из них список (массив) для запроса значений от пользователя,
				 * и добавляем их ID в масссив запрошенных параметров
				 * */
				$inquiredParamsIds = ObjectModel::getFromSessionByName('inquiredParamsIds');
				$withoutCheckInquiring = true;
				
				foreach ($needCommonParams as $paramObj){					
					
					if(!in_array($paramObj->getParamId(), $inquiredParamsIds)){
						
						$paramObj->doCheckForInquiring = $doCheckForInquiring;
						$result[$paramObj->getParamId()] = $paramObj;
					} else{
						//p($paramObj->getParamId());
					}
				}
				
				//d($result);
			} else{/* в противном случае (общих параметьров у моделей нет) - возвращаем false, 
					как признак отсутствия общих параметров у моделей */
				$result = false;
			}
		}
		/* шаг процесса подбора инкрементим */
		$this->curentStep = $this->curentStep + 1;		
		
		return $result;
	}
	
	/**
	 * @access private
	 * @todo метод фомирует массив запрашиваемых параметров, общих для всех текущих вариантов исполнения
	 * @return $result - множество (массив) запрашиваемых параметров, общих для всех (текущих) вариантов исполнения
	 * каждый элемент массива - экземпляр класса Param (см. файл Param.php)
	 **/
	private function getCommonParamsForModels(){
		/* массив счётчиков ссылок на ID параметров */
		$needParamsModels = array();
		/* проходя по всем зависимостям */
		$allParams = array();
		
		//d(count($this->completeMatch));
		
		foreach($this->completeMatch as $selectionVariant){
			/* создать новый объект вариант подбора $modelObject (тип - SelectionVariant, см. SelectionVariant.php) 
			 * и инициализировать его текущим значением варианта подбора $selectionVariant
			 * */
			$modelObject = new SelectionVariant();
			$modelObject->init($selectionVariant);
			/* создать массив $needParams для параметров, которые будут запрашиваться у пользователя */
			$needParams = array();
			/* инициализировать его из текущего значения $modelObject через вызов метода checkForNeedParams()
			 * см. спецификацию метода $SelectionVariant->checkForNeedParams()
			 *  */
			$needParams = $modelObject->checkForNeedParams();
			//p($needParams);
			/* для каждого элемента $needParams */
			foreach($needParams as $paramObj){
				$paramId = $paramObj->getParamId();
				/* если ссылка на элемент в массиве уже присутствует (счётчик > 0)*/
				if (array_key_exists($paramId, $needParamsModels)) {
					$needParamsModels[$paramId] +=1; /* увеличиваем счётчик */
				} else{ /* если счётчик ещё пуст (= 0), то просто устанавливаем его = 1 */
					$needParamsModels[$paramId] = 1;
					$allParams[$paramId] = $paramObj;
				}
			}
		}
		
		/* сотрируем массив счётчиков по значениям $needParamsModels */
		arsort($needParamsModels);
		/* получаем количество подобранных вариантов исполнения в $countModels */
		$countModels = count($this->completeMatch);
		$percentRate = 50;
		$countModelsPercent = $countModels*$percentRate/100;
		
		$result = array();
		
		//p($countModels);
		//p($needParamsModels);
		//d($allParams);
		
		/* проходя по всем счётчикам ссылок на параметры */
		foreach($needParamsModels as $paramId => $modelNeedCount){
			/* включаем в результирующий массив только те, у которых процент по отношению к $countModels > 50 */
			if ($modelNeedCount > $countModelsPercent) {
				$result[] = $allParams[$paramId];
			} else{
				break;
			}
		}
		//d($result);
		return $result;
	}
	
	/**
	 * @access public
	 * @param void
	 * @todo метод осуществляет основной подбор приборов возвращает количество подходящих моделей
	 * @return количество прошедших проверку вариантов конфигурации оборудования
	 */
	public function select(){
		//d($this->curentStep);
		$models = array(); /* список вариантов конфигураций, подходящих под текущее состояние запрошенных у пользователя параметров */
		/* после первого шага процесса подбора могут бытьподобраны только перспективные модели приборов */
		
		if($this->curentStep == 1){/* текущий шаг итерации = 1, следовательно шаг 0 - уже пройден */
			
			if(Dispatcher_Single::$post['model_id'] == null){
				$models = $this->getModelsArray();/* в этот момент $models - экземпляр параметризации модели */
			} else{
				$models = $this->getModelsArray(null, Dispatcher_Single::$post['model_id']);/* в этот момент $models - экземпляр параметризации модели */
			}
		} else{ /* далее - после первого шага - анализу подвергаются уже варианты подбора (тип - SelectionVariant) */
			
			if(Dispatcher_Single::$post['model_id'] == null){
				$models = $this->getModelsSelectionVariant();/* здесь $models - экземпляр варианта конфигурации модели */
			} else{
				$models = $this->getModelsSelectionVariant(Dispatcher_Single::$post['model_id']);/* здесь $models - экземпляр варианта конфигурации модели */
			}
		}
		
		/* обработать модели - см. спецификацию на метод $this->proceedModels($models)
		 * результат обработки сохраняется в переменную $_SESSION["Selection"]
		 *  */
		
		//d($models);
		
		$this->proceedModels($models);				
		//p($models);
		if(parent::$debug){
			parent::$debugList[] = ('колическво подходящих вариантов исполнения приборов: '.count($this->completeMatch));
		}		
		/* возвратить количество подходящих (прошедших проверку) вариантов подбора */
		//self::printTime();
		//p("конец вызова Selection->select()");
		
		return count($this->completeMatch);
	}
	
	/**
	 * @access private 
	 * 
	 * @todo метод отвечает за формирование списка полностью подходящих вариантов исполнения моделей
	 * 
	 * @param $models - 1: масссив моделей, если шаг процесса подбора =1; 2:вариантов конфигурации моделей, если шаг >1 
	 * структура элемента массива:
	 * в случае 1:
	 * [modelparam_id] => ID характеристики (параметризации) модели
     * [param_id] => ID параметра (по которому модель попала в список рассматриваемых)
     * [codeorder] => позиция параметра param_id в карте заказа на данную модель, определяемая данным параметром (если параметр 
     * 		в формировании строки заказа не участвует, значение  = 0)
     * [model_id] => ID модели
     * [modelname] => наименование модели
     * в случае 2: - структура элементов массива в точности соответствует структуре объекта типа SelectionVariant
     * см. спецификацию класса SelectionVariant, файл SelectionVariant.php
	 * 
	 * @return массив всех исполнений всех моделей, подходящих по вем параметрам, для которых пользователь
	 * 		указал значения или для которых взяты значения по умолчанию
	 **/
	private function proceedModels($models){
		//d($models);		
		/* массив подходящих вариантов исполнения формируется здесь */
		$this->completeMatch = array();
		/* $model_in_use - массив вариантов исполнения моделей, подлежащих рассмотрению */
		$model_in_use = array(); // модель попадает в список рассматриваемых сразу же, как только для неё 
								//создан хотя бы один экземпляр варианта исполнения
		/* если массив моделей/вариантов исполнения не пуст */
		if(!empty($models)){
			//d($models);
			foreach($models as $one_model){/* для всех вариантов исполнения, подхдящих по параметрам с первого экрана */ 
				/* получить ID модели в переменную $_o_model_par */
				//p($one_model);
				$o_modelId = $this->getModelID($one_model);				
				if(!in_array($o_modelId, $model_in_use)){/* если модель не находится в списке рассмативаемых */ 
					
					$select_var = new SelectionVariant();/* экземпляр варианта исполнения создать */ 
					$select_var->init($one_model);/* и инициализировть */ 
					$model_in_use[] = $select_var->getModelId();/* ID модели занести в массив рассматриваемых */
					$params = Parameter::getParamUserValuesByStep($this->curentStep);/* запросить список параметров, введённых на предыдущем шаге */ 					
					/* проверить, удовлетворяет ли вариант исполнения $select_var значениям параметров из массива $params */
					$isOk = $select_var->checkThis($params, $this->curentStep);
					
					if(!$isOk){
						//p($this->curentStep);
						//d($params);
					}
					
					if($isOk){/* если удовлетворяет, то такой вариант исполнения заносится в список подходящих */
						$this->completeMatch[$select_var->getModelId()] = $select_var;
						//p($this->completeMatch);
					} else{
						$this->unmatch[] = $select_var;
					}
				}
			}
		}
		//p($this->completeMatch);
		//self::printTime();
		return $this->completeMatch;
	}
	
	/**
	 * @access private
	 * @param $model - массив моделей/вариантов исполнения моделей (в зависимости от шага процесса подбора)
	 * @todo метод возвращает ID модели независимо от формы представления входного параметра $model (массив или объект)
	 * в ОО - версии метод изменится (не нужно будет анализировать тип входной информации)
	 * @deprecated
	 * */
	private function getModelID($model){
		
		$model_id = 0;
		
		if(is_array($model)){
			$model_id = $model['model_id'];
		} else{
			$model_id = $model->getModelId();
		}
		
		return $model_id;
	}
	
	/**
	 * @access private
	 * @todo метод, используя номер шага процесса подбора, возвращает массив моделей, прошедших проверку на этом шаге
	 * @param$ step = null - шаг процесса подбора
	 * @return $models - массив моделей, к текущему шагу процесса подбора прошедших проверку
	 * структура возвращаемого значения:
	 * (
            [modelparam_id] => 146 - ID характеристики (параметризации модели), по которому модель прошла проверку
            [param_id] => 88 - ID параметра (параметризации модели), по которому модель прошла проверку
            [codeorder] => 1 - код к строке заказа модели, соответствующий этому параметру
            [model_id] => 9 - ID модели
            [modelname] => ЭМИС-ВИХРЬ 200 - наименование модели
        )
	 **/
	private function getModelsArray($step = null, $modelIDs = null){
		
		if($step == null){
			$step = $this->curentStep;
		}
		
		if($modelIDs == null){
			//$models = array();
			$models = null;
		} else{
			$models = $modelIDs;
		}
		
		$params = Parameter::getParamUserValuesByStep($this->curentStep - 1);
		
		if(is_array($params)){
			
			$param_IDs = array();
			$param_IDs = array_keys($params);
			
			foreach($params as $paramId => $userValue){
				
				$models = $this->selectModelsForParamFromDb($paramId, $userValue['min'], $userValue['max'], $models);
				
				if (count($models) < 1) {
					
					$models = null;
					break; // нет смысла проверять далее пользовательские значения, если по предыдущим значениям отброшены все модели
				}				
			}
			
			//d($models);
		}
		
		return $models;
	}
	
	/**
	 * @access private 
	 * @todo метод по заданному шагу процесса подбора возвращает варианты исполнения всех подходящих моделей,
	 * прошедших проверку на указанном шаге
	 * @param $step = null - номер шага процесса подбора
	 * @return $models_res - массив прошедших проверку вариантов исполнения оборудования; каэдый элемент массива - 
	 * экземпляр класса SelectionVariant (см. SelectionVariant.php)
	 * @deprecated возможно, после проверки метод будет удалён
	 */
	private function getModelsSelectionVariant($step = null, $modelIDs = null){
		
		$models_res = array();
		
		foreach($this->completeMatch as $model){
			$models_res[] = $model;
		}
		
		unset($this->completeMatch);
		
		return $models_res;
	}
	
	/**
	 * @access private
	 * @todo метод из исходного множества моделей отсеивает неподходящие по указанному 
	 * параметру (и, соответственно, пользовательским значениям для него)
	 * @param $paramId - ID параметра, по которому производится выбор модели из множества, задаваемого параметром $models
	 * @param $valueMin - наименьшее значение параметра
	 * @param $valueMax - наибольшее значение параметра
	 * @param $models - множество (массив) моделей, в котором ведётся поиск; структура элемента этого массива 
	 * в точности совпадает со структурой возвращаемого результата (за исключением номера строки в результате запроса)
	 * @return $query_result - результат запроса к БД
	 * структура результата запроса: 
	 * $query_result[i][modelparam_id][param_id][codeorder][model_id][modelname]
	 * образец структуры и расшифровка:
	 * (	[i] - номер строки в результате запроса
            [modelparam_id] => 146 - ID характеристики (параметризации модели), по которому модель прошла проверку
            [param_id] => 88 - ID параметра (параметризации модели), по которому модель прошла проверку
            [codeorder] => 1 - код к строке заказа модели, соответствующий этому параметру
            [model_id] => 9 - ID модели
            [modelname] => ЭМИС-ВИХРЬ 200 - наименование модели
        )
	 **/
	private function selectModelsForParamFromDb($paramId, $valueMin, $valueMax, $models = null) {
					
		$ojbParam = Parameter::createParamObj(new Parameter($paramId));
		//p($models);
		$sql = $ojbParam->getSearchQuery($paramId, $valueMin, $valueMax, $this->generateModelTerm($models));// new
		if($sql == false){
			$result = $models;
		} else{
			$result = Db::getInstance()->executeS($sql);
		}		
		
		return $result;
	}
	
	/**
	 * @access private
	 * @todo метод генерирует предложение in(...), включаемое в sql-запросы, отбирающие варианты исполнения по
	 * значениям пользовательских параметров
	 * @param $models - массив вариантов исполнения оборудования, прошедших проверку (см. методы getSearchХХХQuery)
	 * @return $sql_model - строка, представляющая собой правильно сформированное sql-предложение IN(...)
	 **/
	 private function generateModelTerm($models){
		
		$sql_model = '';
		// поскольку $models ранее уже инициализирована, как массив то она уже не NULL (!!! - экземпляр массива уже существует)
		if (is_array($models) && count($models) > 0){ 
			
			$sql_model =' and `vmp`.`modelID` in (';
			// предваритьельно из массива моделей получим только идентификаторы (до того в массиве моделей лишняя информация)
			$arr_model_id = array();
			foreach ($models as $_models){
				$arr_model_id[] = $_models['model_id'];
			}
			/* собираем все ID моделей через запятую */
			foreach ($arr_model_id as $model_id){
				$sql_model.=$model_id.',';
			}
			/* из результирующей строки убираем последнюю запятую */
			$sql_model = rtrim($sql_model,',').')';
		} elseif($models > 0){
			$sql_model =' and `vmp`.`modelID` in ('.$models.')';
		} else{
			$sql_model = '';
		}
		
		return $sql_model;
	}
	
	/**
	 * @access public
	 * @todo возвращает массив пар (ID, имя прибора) для всех вариантов исполнения, прошедших проверку на точное 
	 * удовлетворение пользовательским значениям параметров
	 * @return $result - массив пар (id, name) для всех вариантов исполнения обрудования, имеющихся в $this->completeMatch
	 * в текущий момент 
	 * out:
	 * 		array([]=>array('id'=>id,'name'=>name))
	 * */
	public function getCompleteMatchInfo(){
		
		$result = array();
				
		foreach($this->completeMatch as $compVarObj){
			
			$compVarArr = array();
			$compVarArr['id'] = $compVarObj->getModelId();
			$compVarArr['name'] = $compVarObj->model_name;
			$result[] = $compVarArr;
		}
		
		return $result;
	}
	
	/**
	 * @todo Метод возвращает наиболее общую информацию о моделях оборудования, получая её из БД.
	 * @param $deviceType - тип оборудования (по умолчанию - расходомеры, )
	 * */
	public static function getAllModelsInfo($deviceType = self::_DEVICE_TYPE_FLOWMETER){
		
		$sql = 'select id, basename, description from modelline where modelline.`shipmentenable` is '.
				'not null and modelline.devicetype_id = '.$deviceType;
		//return $sql; 
		return Db::getInstance()->executeS($sql);
	}
	
	/**
	 * @access public
	 * @todo по значению находит элемент в $this->inquiredParamIds и удаляет его 
	 * @param
	 * @return void
	 * */
	public function deleteParamFromInquired($paramIdsForDeleting){
		
		if(count($paramIdsForDeleting) > 0){
			
			$inquiredParamsIds = ObjectModel::getFromSessionByName('inquiredParamsIds');
			
			foreach ($paramIdsForDeleting as $key => $oneParamId){
					
				foreach ($inquiredParamsIds as $key => $oneInquiredParamId){
			
					if($oneInquiredParamId == $oneParamId){
						unset($inquiredParamsIds[$key]);
					}
				}
			}
			
			ObjectModel::saveToSession('inquiredParamsIds', $inquiredParamsIds);
		}
	}
	
	/**
	 * откат состояния процессса подбора к предыдущему шагу
	 * */
	public function restoreToPreviousStep(){
		
		$paramIdsForDel = Parameter::deleteParamValuesByStep($this->curentStep);
		$this->deleteFromInquiredParams($paramIdsForDel);
		//новое состояние процесса подбора в сессии сохраняем
		ObjectModel::saveToSession("Selection", $this);
	}
}
