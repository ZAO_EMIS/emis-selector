<?php
abstract class Controller extends ObjectModel {
	
	protected $css_files = array();
	protected $js_file;
	protected $isAuthorized;
	
	
	static function getController($controller_name) {		
		return new $controller_name();
	}
	
	// метод должен реализовываться по разному разными контроллерами
	public abstract function run();
	
	/**
	 * Add a new stylesheets in page header.
	 *
	 * @param mixed $css_uri Path to css file, or list of css files like this : array(array(uri => media_type), ...)
	 * @param string $css_media_type
	 * @return void
	 */
	public function addCSS($css_uri, $css_media_type = 'all', $offset = null){
		
		$css_uri = __ES__ROOT_DIR.$css_uri;
		
		if (!is_array($css_uri)){
			$css_uri = array($css_uri);
		}
		
		foreach ($css_uri as $css_file => $media){
			
			if (is_string($css_file) && strlen($css_file) > 1){
				$css_path = Tools::getCSSPath($css_file, $media);
			} else{
				$css_path = Tools::getCSSPath($media, $css_media_type);				
			}
							
			$key = is_array($css_path) ? key($css_path) : $css_path;	
					
			if ($css_path && (!isset($this->css_files[$key]) || ($this->css_files[$key] != reset($css_path)))){
				
				$size = count($this->css_files);
				
				if ($offset === null || $offset > $size || $offset < 0 || !is_numeric($offset)){
					$offset = $size;
				}
				$this->css_files = array_merge(array_slice($this->css_files, 0, $offset), $css_path, array_slice($this->css_files, $offset));
			}
		}				
	}
	
	/**
	 * @todo по заданному абсолютному(??) пути расположения JavaScript - файлов получает массив их относительных путей 
	 * @access public
	 * @param $js_uri - абсолютный(??) путь расположения
	 * @return
	 * */
	public function addJS($js_uri){		
		$this->js_file = Tools::getJsPath(__ES__ROOT_DIR.$js_uri, 'all');
	}
}