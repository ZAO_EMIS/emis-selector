<?php
/*
 * Класс - Контроллер отображения фронтэнда
*/
abstract class FrontController extends Controller {	
	
	protected $template = null;
	
	protected function initContent($params = null){
		
		$selection = new Selection();
		$selection->init("Selection");
		
		if(Dispatcher_Single::getFrom_GETByName("back") == "1"){
			// если выполняется шаг назад, подбор нужно начинать "с нуля", не используя предыдущий результат (нельзя инициализироваться с него!)
			ObjectModel::unsetInSessionByName("Selection"); // потому предыдущее состояние процесса подбора удаляем
			$selection->init("Selection"); // и инициализируем $selection тем, что осталось, то есть null 
			
			$params = array();
			
			$selection->setCurrentStep(Dispatcher_Single::getFrom_GETByName("step"));
			$currentStep = $selection->getCurrentStep();
			
			while($currentStep > 0){
					
				$userParams = Parameter::getParamUserValuesByStep($currentStep);
					
				if($userParams == null){
					$userParams = array();
				}
					
				foreach ($userParams as $paramId => $oneUserParam){
			
					$params[$paramId] = $oneUserParam;
					$inquiredParamsIds[$paramId] = $paramId;
				}
					
				$currentStep--;
			}
			
			ObjectModel::saveToSession('params', $params);
			ObjectModel::saveToSession('inquiredParamsIds', $inquiredParamsIds);
		}
		
		return $selection;
	}
	// метод runAjax - не абстрактный, иначе придётся во всех наследниках делать "заглушки" с пустой реализацией
	protected function runAjax($params = null){
		//p("Ajax из FrontController");
	}
	//protected abstract function display();
	public function run(){		
		// инициалиизации сеанса, css - оформления, ...		
		$domain = Tools::getHttpHost();
		
		parent::addCSS("css/styles.css");
		
		// отладочная информация, если флаг отладки включен
		foreach (self::$debugList as $str){
			p($str);
		}
		// будет ли контроллер реагировать на посылку именно от AJAX - определяется только наличием в GET-запросе флага
		// AJAX = 1
		//d($_POST); 
		//if($_POST['ajax'] == 1){
		if(Dispatcher_Single::$post['ajax'] == 1){
			$result = $this->runAjax();
		} else{			
			$result = $this->initContent();
			$css_files = $this->css_files;
			//d($_SESSION);
			$js_file = $this->js_file;
			include(__ES__ROOT_DIR.'/views/includes/head.php');
			include(__ES__ROOT_DIR.'/views/index.php');						
		}
	}
}