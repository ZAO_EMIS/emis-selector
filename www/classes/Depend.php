<?php
/**  
 * @todo класс отвечает за анализ зависмостей ([список значений определяющих параметров]-->[значение определяющего параметра]), 
 * и на этой основе формирует список наиболее подходящих ($bestCombinations) сочетаний значений параметров.
 * 
 * @property $dependParam - параметр, определяемый в зависимости. Экземпляр подкласса Parameter (@see Parameter).
 * 
 * @property $definingParams = array() - массив параметров, определяющих зависимость; каждый элемент массива - экземпляр 
 * класса Parameter (@see Parameter)
 *  
 * @property $combinations - массив комбинаций зависимости (экземпляров класса @seeCombination)
 * @property $bestCombinations - массив комбинаций, наилучшим образом соответствующих пользовательским значениям параметров
 * элементы массива - экземпляры класса @seeCombination
 * @property $analyzed - флаг анализа (учтена зависимость или нет).
 * @property $analyzedParams = array() - массив ID параметризации модели (modelParamId), по которым зависимость проверена
 * (чтобы не проверять одному и тому же параметру зависимость дважды)
 * */
class Depend extends ObjectModel{	
	
	//public $params=array(); /* ID параметризаций моделей, образующих зависимость */	
	//private $weight;
	//public $paramId;
	//public $isOk;
	/* ID параметризации модели, которой определяются зависимости (одна параметризация может определить несколько зависимостей) */
	//public $model_param;
	//private $cmpType;
	//private $paramType;
	//public $code_order;/* позиция в карте заказа (в качестве конечной цели подбора - именно карта заказа на оборудование) */
	
	private $dependParam; // экземпляр класса Parameter
	private $definingParams = array(); // экземпляры класса Parameter
	private $combinations = array(); /* ID значений, образующих экземпляр зависимости */
	private $bestCombinations = array();
	public $analyzed = 0;
	public $analyzedParams = array(); // массив ID параметризации модели (modelParamId), по которым зависимость проверена
	
	/**
	 * @todo Конструктор. При создании объекта инициализирует его поля:
	 * $this->model_param значением $model_param и $this->paramId значением ID параметра, соответствующего $model_param (такое
	 * значение одно и только одно)
	 * @access public
	 * @param $model_param
	 * @return void
	 * */
	public function __construct($aDependParam){
		$this->dependParam = $aDependParam;
	}
	/**
	 * 
	 * */
	public function getDependParam(){
		return $this->dependParam;
	}
	
	/**
	 *
	 * */
	public function getDefiningParams(){
		return $this->definingParams;
	}	
	/**
	 * @todo для данного экземпляра зависимости возвращает комбинации, наилучшим образом соответствующие пользовательским значениям
	 * парамтеров
	 * @access public
	 * @param void
	 * @return $this->bestCombinations
	 * */
	public function getBestComb(){
		return $this->bestCombinations;
	}
	
	public function getCombinations(){
		return $this->combinations;
	}
	
	/**
	 * @todo инициализирует поле $this->weight текущего объекта Depend значением $weight
	 * @access public
	 * @param $weight - значение веса параметризации модели (характеристики), которой определяется данный экземпляр зависимости
	 * целое число
	 * @return void
	 * */
	public function setWeight($weight){
		$this->weight = $weight;
	} 
		
	/**
	 * @todo для текущего объекта Depend получает значение веса параметризации модели, определяющей данный экземпляр зависимости
	 * @access public
	 * @param void
	 * @return $this->weight;
	 * */
	public function getWeight()	{
		return $this->weight;
	}
	
	/** 
	 * @todo добавление параметра к массиву, формирующему зависимость. n-кратным вызовом этого метода формируется массив
	 * параметров, значения которых определяют значение определяемого параметра в образце зависимости.
	 * @access public 
	 * @param $param - экземпляр класса Param.
	 * @return void
	 * */
	public function addParam($aDefiningParam){
		$this->definingParams[$aDefiningParam->getParamId()] = $aDefiningParam;
	}
	
	/**
	 * @todo метод находит параметр с наибольшим весом (среди всех параметров - определяющих и определяемых)
	 * @access private 
	 * @param void
	 * @return $result - параметризация модели, имеющая наибольший вес из всех, участвующих в данной зависимости (как
	 * определяемых, так и определяющих)
	 * */
	private function getParamWithMaxWeight(){
		
		$result = array();
		$maxWt = $this->weight;
		$result['weight'] = $this->weight;
		$result['model_param'] = $this->model_param;
		
		foreach ($this->params as $par){
			
			if($par['weight'] >= $maxWt){
				$maxWt = $par['weight'];
				$result['model_param'] = $par['model_param'];
				$result['id'] = $par['id'];
				$result['weight'] = $maxWt;
			}
		}
		
		return $result;
	}

	/**
	 * @todo инициализирует поле $this->combinations комбинациями со значениями определяющих параметров по умолчанию.
	 * @access public
	 * @param void
	 * @return $this->combinations, инициализированная значениями по умолчанию
	 * */
	public function selectDefaultVal(){
		
		$defaultValueIds = $this->dependParam->selectDefaultValueIdsFromDb();// NEW
		
		//получить все комбинации для параметра с наибольшим весом (известны ID параметризации модели 
		//и значение по умолчанию)					
		//должна быть добавлена только одна комбинация из значений по умолчанию
		foreach($defaultValueIds as $value_id){
			
			$sql = 'select c.`id`
					from `combinations` c
					where
					c.`modelparam_id` = '.$this->dependParam->getModelParamId().'
					and
					c.`val_id` = '.$value_id;
			
			$resultSql  = Db::getInstance()->executeS($sql);
			
			foreach ($resultSql as $depend_id){
				
				$definedValue = Value::createInstance($this->dependParam->getParamId(), $this->dependParam->getParamType());
				$definedValue->init($value_id, $this->dependParam->getModelParamId());
				//d($definedValue);
				
				$combForChecking = new Combination($depend_id['id'], $definedValue);
				//$comb = $this->checkCombForUserParam($depend_id['id'], $value_id, $this->dependParam->getModelParamId());// OLD						
				$comb = $this->checkCombForUserParam($combForChecking);// NEW
				
				if($comb){
					$this->combinations[] = $comb;
				}
			}
		}
		//d($this->combinations);
		return $this->combinations;
	}
	
	/**
	 * @access public 
	 * @todo метод возвращает массив всех значений всех параметров для всех комбинаций данного объекта зависимости
	 * структура результата работы метода:
	 * @param $best = false - флаг, указывающий, из какого поля выбираются рассматриваемые комбинации.
	 * true - комбинации выбираются из $this->bestCombinations, false - из $this->combinations.
	 * @return $paramsValues - массивы значений всех параметров, встречающихся во всех комбинациях из тех, 
	 * которые установлены активными 
	 * */
	public function getParamsValues($best = false){
		
		$paramsValues = array();
	 	
	 	if($best){
	 		$activeCombinations = $this->bestCombinations;
	 	} else{
	 		$activeCombinations = $this->combinations;
	 	}	 	
	 	//d($activeCombinations);
	 	foreach($activeCombinations as $objCombination){
	 		
	 		if(is_object($objCombination)){
	 			
	 			$combVals = $objCombination->getParamsValues();
	 			$paramsValues[] = $combVals;
	 		}
	 		//$paramsValues[$objCombination->getDependValue()->getParamId()] = $combVals;
	 	}
	 	//p($paramsValues);
	 	return $paramsValues;
	}
	
	/**
	* @todo Метод определяет параметры, значения для которых необходимы для однозначного определения строки заказа. 
	* @access public 
	* @param $combinations = null - соответствующие текущему объекту образца зависимости комбинации. 
	* параметр не является обязательным, так как присваивание значения локальной переменной $combinations происходит в любом случае
	* - если параметр $combinations = null, то локальная переменная $combinations = $this->bestCombinations
	* - если параметр $combinations != null или count($this->bestCombinations) == 0 
	* то локальная переменная $combinations = $this->сombinations, поэтому от значения параметра 
	* $combinations результат работы метода не зависит. Параметр можно удалить. 
	* @return $result - массив, содержащий для каждого из дополнительно запрашиваемых параметров количество 
	* различных подходящих значений и их список.
	* */
	public function getNeedParams($combinations = null){
		
		$paramsVarValues = array();
		$countDiffs = array();		
		$prevValues = array();
		
		$combinations = $this->combinations;
		
		if(count($combinations) == 0){
			
			$result['countDiffs'] = array();
			
			$countDiffs = array(10000,10000,10000);//old
			//$countDiffs = array(10000);//new
			
			foreach($this->definingParams as $objDefParam){
				
				$i = 1;
				
				while ($i < 4){
					
					$paramsVarValues[$objDefParam->getParamId()] = $objDefParam->getParamWeight();
					$i++;
				}
			}
		} else{
			
			foreach ($combinations as $comb){			
				// получим значения параметров
				//$combParamVals = $comb->getParamsValues();
				$combParamVals = $comb->getParamsValueIds();
				//$combParamVals={paramId=>Value <- экземпляр потомка Value}
				// сравним его с предыдущим 
				$diffArray = array_diff_assoc($combParamVals, $prevValues);
				// получим массив id различающихся параметров
				$diffKeys = array_keys($diffArray);
				
				for ($key = 0; $key < count($diffKeys); $key++ ){
					
					$paramId = $diffKeys[$key];
					// если значение новое
					if(!($paramsVarValues[$paramId]) or !in_array($diffArray[$paramId], $paramsVarValues[$paramId])){
						// увеличим счетчик различающихся значений для параметра
						$countDiffs[$paramId]++;
						
						if(!is_array($paramsVarValues[$paramId])){
							$paramsVarValues[$paramId] = array();
						}
						//p($paramsVarValues);
						// добавим значение параметра в массив различающихся значений для данного параметра
						$paramsVarValues[$paramId][$diffArray[$paramId]] = $this->findCombValueByValueId($paramId, $diffArray[$paramId]);// NEW
					}
				}
				
				$prevValues = $combParamVals;			
			}
		}
		
		$result = array();
		$result["countDiffs"] = $countDiffs;
		$result["paramsVars"] = $paramsVarValues;
		
		return $result;
	}
	
	/**
	 * @todo метод проверяет, определяется ли данная зависимость указанной параметризацией модели $model_param 
	 * @access public 
	 * @param $aModelParamId - паарметриация модели, проверяемая на участие в формировании зависимости
	 * @return - true, если параметризация модели усчаствует в формировании зависимости, или false - в противном случае 
	 * */
	public function isDependOfParam($aModelParamId){

		if($this->dependParam->getModelParamId() == $aModelParamId){
			return true;
		} else{
			
			foreach($this->definingParams as $objDefiningParam){
					
				if($objDefiningParam->getModelParamId() == $aModelParamId){
					return true;
				}
			}
		}
		
		return false;
	} 
	
	/**
	 * @todo метод получает из БД все комбинации, определяемые независимым параметром $indepModelParam с ID значения $value_id из
	 * множества принадлежащих данному образцу зависимости (см. строку в sql-запросе $this->model_param) 
	 * @access private 
	 * @param $indepModelParam, $value_id
	 * @return $result - в виде SQL-запроса с полями `dependence_id`, `modelparametrization_independent_id`
	 * */
	private function getCombinationsFromDb($indepModelParam, $value_id){
		
		$result = array();
		
		$sql = 'select
				c.`id` as dep_id, c.`val_id` as dependValId, d.`value_id` as definingValId
				from (`combinations` c join `dependence_parametrization` d)
				where
                c.`modelparam_id` = '.$this->dependParam->getModelParamId().'
                and
				d.`value_id` = '.$value_id.'
				and
				d.modelparametrization_independent_id = '.$indepModelParam.'
				and 
                d.`dependence_id` = c.id';
		//d($sql);
		$result = Db::getInstance()->executeS($sql);
		
		return $result;
	}
	
	/**
	 * @todo метод получает для модели по её ид 
	 * - все зависимости, которые ей соответствуют
	 * - для каждой зависимости получить вектор значений параметров 
	 * @access public 
	 * @param $model_id - ID модели, для которой формируется массив зависимостей
	 * @return $result описание структуры:
	 * Array
        (
            [147] => Array - ID параметризации модели, образующий образец зависимости
                (
                    [paramsValues] => Array - описание значений параметризаций модели, определяющих зависимость
                        (
                            [0] => Array
                                (
                                    [mp_id] => 155 - ID параметризации модели
                                    [value_id] => 188 - ID значения
                                    [comparetype] => 0 ID типа сравнения
                                    [paramtype_id] => 7 - ID типа параметра
                                    [id] => 91 - ID паарметра
                                )
                            [1] => Array
                                (
                                    [mp_id] => 145
                                    [value_id] => 440
                                    [comparetype] => 1
                                    [paramtype_id] => 6
                                    [id] => 45
                                )
                            [2] => Array
                                (
                                    [mp_id] => 153
                                    [value_id] => 182
                                    [comparetype] => 0
                                    [paramtype_id] => 7
                                    [id] => 51
                                )
                            [3] => Array
                                (
                                    [mp_id] => 148
                                    [value_id] => 451
                                    [comparetype] => 1
                                    [paramtype_id] => 6
                                    [id] => 98
                                )
                        )
                    [weight] => 8 - вес определяющего параметра
                    [codeOrder] => - символ, возвращаемый определяемым параметром в карту заказа
                )
        )
	 * */
	public static function getDependInfoForModel($model_id){
		//p("вызов Depend->getDependsForModel");
		$sql = 'select 
					p.id as param_id, 
					p.paramtype_id as param_type, 
					mp.`id` as model_par_id, 
					mp.`weight` as weight, 
					mp.`codeorder` as codeorder
				from (`parameters` p join `modelparametrization` mp)
				where 
					mp.`model_id` = '.$model_id.'
                    and
					mp.independent = 0
					and
					p.id = mp.`param_id`
							order by mp.`weight`';
		
		$db = Db::getInstance();
		return $db->executeS($sql);			

	}
	
	/**
	 * @todo Метод инициализирует массив определяющих параметров для зависимости с определяемым параметром $aDependModelParamId 
	 * @param $aDependModelParamId - парамтеризация модели, определяемая в зависимости.
	 * @return $definingParams - массив определяющих параметров (подклассов 
	 * @see Parameter 
	 * ) для $aDependModelParamId
	 * */
	public static function getDefiningParamsInfo($aDependModelParamId){
		
		$sql = 'select 
				p.id as param_id ,
				p.paramtype_id as param_type, 
				dp.`modelparametrization_independent_id` as model_par_id, 
				mp.weight as weight,
				mp.codeorder as codeorder
				from ((`dependence_parametrization` dp join `modelparametrization` mp) join `parameters` p)
				where
				`dp`.`dependence_id` = (select min(comb.`id`) as id 
				from `combinations` comb where `comb`.`modelparam_id` = '.$aDependModelParamId.')
				and
				`dp`.`modelparametrization_independent_id` = mp.`id`
				and
				mp.`param_id` = p.`id`';
		//d($sql);
		return Db::getInstance()->executeS($sql);

	}
	
	/**
	 * @todo метод возвращает информацию о принадлежащих образцу зависимости комбинациях (или обо всех, 
	 * или о наиболее подходящих)
	 * @access public  
	 * @param $forBestCombs = false - параметр определяет, будут ли выбираться сведения только о лучших комбинациях, 
	 * или же будут возвращены сведения обо всех комбинациях
	 * @return $result описание одного элемента структуры:
	 * [paramsValues] => Array
        (
            [0] => Array - массив определяющих параметров
                (
                    [91] => 188
                    [45] => 440
                    [51] => 182
                    [98] => 451
                    [75] => 426
                )
                ...
            [value_id] => Array - массив ID значений
        	(
            [0] => 426
            [1] => 426
            ...
            [dependence_id] => Array - массив ID зависимостей
        (
            [0] => 500
            [1] => 501
            [2] => 502
            ...
	 * */
	public function getCombinationInfo($forBestCombs = false){
		
		if ($forBestCombs) {
			if(count($this->bestCombinations) < 1) {
				$this->getBestCombinationAccordWeight();
			}
			$combinations = $this->bestCombinations;
		}
		else{
			$combinations = $this->combinations;
		}
		
		$result = array();
		
		foreach($combinations as $comb){
			// !!! надо смотреть на вызовы метода; но скорее всего, метод должен возвращать все экземпляры Combination
			$result['paramsValues'][] = $comb->getParamsValues();
			$result['value_id'][] = $comb->getVal_ID();
			$result['dependence_id'][] = $comb->getDepend_ID();
		}
		
		//p($combinations);
		//p($result);
		return $result;
	}		
	
	/**
	 * @todo получает для текущей зависимости строку заказа
	 * @access public 
	 * @param void
	 * @return $order - строка заказа
	 * */
	public function generateString($useBestWayOnly = true){
		
		$order = array();
		
		if((count($this->bestCombinations) > 0) and ($useBestWayOnly === true)){
			$combinations = $this->bestCombinations;
		} elseif((count($this->combinations) > 0) and ($useBestWayOnly === false)){
			$combinations = $this->combinations;
		} else{			
			
			$combinations = $this->selectDefaultVal();
			$this->combinations = $combinations;
		}
		
		$order = $this->generateStringFromActiveCombinationSet($combinations);
		
		return $order;
	}
	
	/**
	 * генерация строки заказа для активного набора комбинаций
	 * */
	private function generateStringFromActiveCombinationSet($activeCombinations){
		
		$order = array();
		
		foreach($activeCombinations as $comb){
			
			$value = $this->findCombValueByValueId($this->dependParam->getParamId(), $comb->getVal_ID());
				
			if(($this->dependParam->getCodeOrder() == "null") or ($this->dependParam->getCodeOrder() == null))
				continue;
				
			$signs[$this->dependParam->getCodeOrder()] = $value->getCode();
			$arraySignsOfDefiningValues = $comb->getCodes();
			//d($this->definingParams);
			foreach ($this->definingParams as $oneDefParam){
		
				if($oneDefParam->getCodeOrder() != null){
					$signs[$oneDefParam->getCodeOrder()] = $arraySignsOfDefiningValues[$oneDefParam->getParamId()];
				}
			}
				
			$order[] = $signs;
		}
		
		return $order;
	}
	
	/**
	 * @todo метод возвращает ID значения определяемого параметра по ID зависимости
	 * @access private
	 * @param $depId - ID зависимости
	 * @return $result['val_id'] - ID значения определяемого параметра
	 * */
	private function getValueIdByDepId($depId){
		//p("вызов Depend->getValueIdByDepId");
		$sql = 'select d.val_id
				from `combinations` d
				where d.`id` = ' .$depId;
		
		$result = Db::getInstance()->getRow($sql);
		
		return $result['val_id'];
	}
	
	/**
	 * @deprecated метод не используется
	 * @todo получить зависимости, в которых параметризация ЗАВИСИМАя
	 * @access public 
	 * @param $model_par
	 * @return $result - 
	 * */
	public static function getDependByModelPar($model_par){
		//p("вызов Depend->getDependByModelPar");
		$sql = 'select dp.`id` from dependences dp where dp.`modelparametrization_id` = ' .$model_par;
		$result = Db::getInstance()->executeS($sql);
		//p($result);
		return $result;
	}
	
	/**
	 * @todo метод возвращает все комбинации, принадлежащие данному образцу зависимости
	 * @access public 
	 * @param void
	 * @return $this->combinations
	 * */
	public function getAllCombination(){
		return $this->combinations;
	}
	
	/**
	 * @todo получить ID параметра по ID параметризации модели
	 * */
	public function getParamInfoByModelParamId($aModelParamId){
		
		$result = array();
		
		if($this->dependParam->getModelParamId() == $aModelParamId){
			
			$result['paramId'] = $this->dependParam->getParamId();
			$result['paramTypeId'] = $this->dependParam->getParamType();
		} else{
			
			foreach ($this->definingParams as $definingParam){
				
				if($definingParam->getModelParamId() == $aModelParamId){
					
					$result['paramId'] =$definingParam->getParamId();
					$result['paramTypeId'] = $definingParam->getParamType();
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * @todo метод отвечает за подбор комбинаций (отсеивание или сохранение). после его отработки в $this->combinations остаются
	 * только те комбинации. которые удовлетворяют пользовательским условиям для пары $model_param и $value_id  
	 * @access public
	 * @param $model_param -  ID параметризация модели
	 * @param $value_id - ID значения для заданной параметризации модели
	 * @param $currentStep = 2 - шаг итерации (процесса подбора)
	 * @return count($this->combinations) - число подходящих комбинаций
	 * */
	public function getDependValuesForValueID($model_param, $value_id, $currentStep = 2) {
		//d($this);
		$this->bestCombinations = array();
		
		if(($this->dependParam->getModelParamId() != $model_param) && (count($this->combinations) > 0) && ($currentStep > 1)){
			//если шаг процесса подбора >1, и комбинации уже есть, проверку ведём только среди уже подобранных комбинаций
			$avaible_comb = $this->combinations;
			$this->combinations = array();
		} elseif ($this->dependParam->getModelParamId() == $model_param){
			
			$avaible_comb = $this->getAllCombinationsForVal($value_id);
			
			if($this->analyzed != $model_param){
				$this->combinations = array();
			}
			
			$dependParamInfo = $this->getParamInfoByModelParamId($this->dependParam->getModelParamId());
		} else{
			/* иначе - если ещё ни одной комбинации нет, и нет возможности ориентироваться на параметр, определяющий 
			 * зависимость (пару $model_param, $value_id) - запросить все комбинации, в формировании которых заданный параметр 
			 * модели принимает участие */
			$avaible_comb = $this->getCombinationsFromDb($model_param, $value_id);			
			$dependParamInfo = $this->getParamInfoByModelParamId($this->dependParam->getModelParamId());
		}				
		foreach ($avaible_comb as $combinationInfo){
				// значение зависимого параметра
			
			if(is_array($combinationInfo)){
				
				$dependValue = Value::createInstance($dependParamInfo['paramId']);
				$dependValue->init($combinationInfo['dependValId'], $this->dependParam->getModelParamId());
				$combination = new Combination($combinationInfo['dep_id'], $dependValue);
				//p($combination);
			} else{
				$combination = $combinationInfo;
			} 			
			$combsAdded = $this->getCombIdList();			
			if(!in_array($dep_id, $combsAdded)){// исключить повторное включение комбинации в список
				// проверяем, подходит ли комбинация по значениям пользовательских параметров
				$comb = $this->checkCombForUserParam($combination);				
				if($comb != false){
					$this->combinations[] = $comb;/* если подходит - добавляем в массив */
				} else{/* если комбинация не подходит - удаляем её */					
					unset($comb);
				}
			}
		}

		if(!in_array($model_param, $this->analyzedParams)){
			$this->analyzedParams[$model_param] = $model_param;
		}			
		
		$this->analyzed = $model_param;
		//d($this->combinations);
		if($this->dependParam->getParamId() == 132){
			//d($this->combinations);
		}
		return count($this->combinations);
		
	}
	
	/**
	 * @todo метод проверяет комбинацию с указанными ID, параметризацией модели и ID значения на 
	 * соответствие пользовательским значениям параметра
	 * @access private 
	 * @param $depend_id - ID комбинации
	 * @param $combValId - ID значения
	 * @param $modelParam - ID параметризации модели
	 * @return true, если указанная комбинация соответствуетпользовательским значениям параметра, false - в противном случае 
	 * */
	//private function checkCombForUserParam($depend_id, $combValId, $modelParam){
	private function checkCombForUserParam($objCombination){
		
		$combValOk = false;
		// создаём компаратор (экземпляр подходящего подкласса Parameter, способного сравнивать значение нужного типа)
		//$paramSkeleton = new Parameter($this->dependParam->getParamId());
		//$comparatorParam = Parameter::createParamObj($paramSkeleton);
		// сравниваем определяемое значение с эталонным
		
		$combValOk = $this->dependParam->compareParamWithUserVal($objCombination->getDefinedValue());
		
		//p($objCombination);
		//d($combValOk);
		// если сравнение успешно, то проверям у зависимости определяющие параметры
		//p($combValOk);
		if($combValOk){
			// 
			$modelParamArr = array();
			
			if($this->dependParam->getModelParamId() == 367){
				//d($this->definingParams);
			}
			
			$isOk = $objCombination->checkForUserParams(null,$this->definingParams);

			if($isOk/* and (count($this->definingParams) == count($objCombination->getDefiningValues()))*/) {
				//p($objCombination);
				return $objCombination;
			} else {
				return false;
			}
		}
	}
	
	/**
	 * @todo метод возвращает все ID комбинаций dependence_id для указанного ID значения value_id
	 * @access private 
	 * @param $value_id - ID значения определяемого параметра
	 * @return $result - результат SQL-запроса с полями value_id, dependence_id
	 * */
	private function getAllCombinationsForVal($value_id){
		
		$sql = 'select c.val_id as dependValId, c.id as dep_id
				from `combinations` c
				where c.`val_id` = ' . $value_id.'
					  and c.`modelparam_id` = ' .$this->dependParam->getModelParamId();
		
		$result = Db::getInstance()->executeS($sql);
		
		return $result;
	}
	
	/**
	 * по заданному $aParamId возвращает modelParamId определяющего параметра, если таковой есть; в противном случае возвращает false
	 * */
	public function getDefiningModelParamIdByParamId($aParamId){
		
		foreach ($this->definingParams as $definingParam){
			if($definingParam->getParamId() == $aParamId){
				return $definingParam->getModelParamId();
			}
		}
		
		return false;
	}
		
	/**
	 * @todo Метод фильтрует комбинации, оставляет только те, для которых пользовательское значение параметра $paramId
	 * находится в массиве значений $values
	 * @param $paramId - ID параметра 
	 * @param $values - массив ID допустимых значений
	 * @return count($this->combinations) - количество подходящих комбинаций, принадлежащих данному образцу зависимости
	 * @see Parameter->filterObjValue($oneObjValue);
	 */
	public function filterCombinationsByParamValue($paramId, $values){
		//p($values);
		//d($this);
		$resultCombinations = array();
		
		if(!count($values)) return false;
		//d($this->combinations);
		if(count($this->combinations) < 1){
			
			if($this->dependParam->getParamId() != $paramId){
				
				if(is_object($model_param = $this->definingParams[$paramId])){
					$model_param = $this->definingParams[$paramId]->getModelParamId();
				} 
			} else{
				$model_param = $this->dependParam->getModelParamId();
			}
			//p($model_param);
			foreach($values as $valueId){
				$this->getDependValuesForValueID($model_param, $valueId, 0);
			}
		}
		//d($this->definingParams);
		$combinations = $this->combinations;
		/* оба массива - комбинаций и наиболее подходящих комбинаций - пересоздаём заново */
		$this->combinations = array();
		$this->bestCombinations = array();
		$objValues = array();
		
		
		foreach($values as &$o_val){
			
			$value = Value::createInstance($paramId);
			$value->init($o_val, $aModelParamId);
			$objValues[] = $value;
		}
		
		$objParam = ($this->dependParam->getParamId() == $paramId) ? $this->dependParam : $this->definingParams[$paramId];
		//d($combinations);
		foreach ($combinations as $comb){			
			
			$isOK = true;
			$combParamsValues = $comb->getParamsValues();			
			$objParam->value = $combParamsValues[$paramId];
			
			foreach ($objValues as $oneObjValue){				

				$isOK = $objParam->filterObjValue($oneObjValue);
				
				if(in_array($combParamsValues[$paramId], $values) or ($isOK and !in_array($comb, $this->combinations))){
					$this->combinations[] = $comb;
				}
				
				if ($isOK == false) {//всё бы так, но формирование $isOK??? ТУТ ЧТО-ТО НЕВЕРНО: если раскомментировать
					// break; //исчезают ВСЕ КОМБИНАЦИИ!!! между тем, при введённых параметрах так быть не может!
				}
			}
		}
		
		return count($this->combinations);
	}
	
	/**
	 * @deprecated
	 * @todo метод отбирает комбинации, подходящие по параметрам типа FLOAT с типом сравнения OR  или  EXACT
	 * @access private 
	 * @param $paramId - ID параметра 
	 * @param $values - массив ID допустимых значений
	 * @return void
	 * */
	private function filterCombinationsFloatRangeParamByValue($paramId, $values, $aParamType, $aModelParamId){
		
		$combinations = $this->combinations;
		/* оба массива - комбинаций и наиболее подходящих комбинаций - пересоздаём заново */
		$this->combinations = array();
		$this->bestCombinations = array();
		
		$realValues = array();
		// надо передавать объекты значений, а не создавать их здесь
		foreach($values as &$o_val){
			
			$value = Value::createInstance($paramId, $aParamType);
			$value->init($o_val, $aModelParamId);
			$realValues[] = $value;
		}
		
		foreach($combinations as $comb){
				
			$paramsValues = $comb->getParamsValueIds();// это делать не надо - здесь нужны объекты-значения, а не ID значений
			$combParamsValues = $comb->getParamsValues();//это верно - вернёт все значения в комбинации
			
			$combValueForCompare = $combParamsValues[$paramId];// в комбинации нас интересует только значение для известного параметра
				
			foreach ($realValues as $val){

				// найти подходящий компаратор
				$comparator = ($this->dependParam->getParamId() == $paramId)? 
								$this->dependParam : $this->definingParams[$paramId];
				// сравнить значения
				$isOK = (($comparator->compareValues($val, $combValueForCompare)) or 
								($comparator->compareValues($combValueForCompare, $val)));

			}
				
			if(in_array($paramsValues[$paramId], $values) || $isInclude){
				$this->combinations[] = $comb;
			}
		}
	}
	
	/**
	 * @deprecated
	 * @todo метод отбирает комбинации, подходящие по параметрам типа FLOAT с типом сравнения XOR (возможно 
	 * будет заменён на INVERSE)
	 * @access  private 
	 * @param $paramId - ID параметра 
	 * @param $values - массив ID допустимых значений
	 * @return void
	 * */
	 private function filterCombinationsFloatNonExceedParamByValue($paramId, $values){
	 	//p("вызов Depend->filterCombinationsFloatNonExceedParamByValue");
	 	$combinations = $this->combinations;
	 	/* оба массива - комбинаций и наиболее подходящих комбинаций - пересоздаём заново */
	 	$this->combinations = array();
	 	$this->bestCombinations = array();
	 		
	 	foreach($combinations as $comb){
	 	
	 		$paramsValues = $comb->getParamsValues();
	 		$paramCombInfo = Param::getValue($paramId, $paramsValues[$paramId]);
	 		$paramCombMinVal = $paramCombInfo['minval'];
	 		$paramCombMaxVal = $paramCombInfo['maxval'];
	 	
	 		foreach ($values as $val){
	 	
	 			$paramInfo = Param::getValue($paramId, $val);
	 				
	 			$paramMinVal = $paramInfo['minval'];
	 			$paramMaxVal = $paramInfo['maxval'];
	 				
	 			$isNonExceed = $paramCombMaxVal <= $paramMaxVal;	 			
	 		}
	 	
	 		if(in_array($paramsValues[$paramId], $values) || $isNonExceed){
	 			$this->combinations[] = $comb;
	 		}
	 	}
	}
	
	/**
	 * @deprecated
	 * @todo метод отбирает комбинации, подходящие по параметрам типа FLOAT с типом сравнения RANGE  или  EXACT
	 * @access  private 
	 * @param $paramId - ID параметра 
	 * @param $values - массив ID допустимых значений
	 * @return void
	 * */
	private function filterCombinationsPredefParamByValue($paramId, $values){
		
		$combinations = $this->combinations;
		/* оба массива - комбинаций и наиболее подходящих комбинаций - пересоздаём заново */
		$this->combinations = array();
		$this->bestCombinations = array();
		
		foreach($combinations as $comb){
			 
			$paramsValues = $comb->getParamsValueIds();

			if(in_array($paramsValues[$paramId], $values) ){
				$this->combinations[] = $comb;
			}
		}
	}
	
	/**
	* @access public 
	* @todo метод формирует массив, содержащий только наиболее подходящие комбинации, соответствующие пользовательским занчениям
	* с учётом приоритета характеристик модели (бывает что 2 зависимости зависят от одинакоывх параметров, чтобы выбрать 
	* лучшую комбинацию, необходимо учитывать значения этих параметров как уже определенных) 
	* 
	* @param $paramsValues - массив значений параметров (внутри метода не используется, параметр можно удалить)
	* @return void
	* */
	public function getBestCombinationAccordWeight($paramsValues = null) {
		//p("вызов Depend->getBestCombinationAccordWeight");
		if(count($this->combinations) < 1){
			$this->selectDefaultVal();
		}
				
		$this->bestCombinations = array();
		//		
		$maxPriority = 0;
		$bestCombinationsPriority = array();
		
		foreach ($this->combinations as $one_comb){
				
			$combPriority = $one_comb->getSummPriority();
				
			if($maxPriority < $combPriority){
				
				$bestCombinationsPriority = array($one_comb);
				$maxPriority = $combPriority;
			} elseif($maxPriority == $combPriority){
				$bestCombinationsPriority[] = $one_comb;
			} else{
				continue;
			}
		}
		//d($bestCombinationsPriority);
		$bestCombinationsUserParams = array();
		$userParamPresent = false;
		
		foreach ($this->definingParams as $objDefiningParam/*$one_param*/){// получим значение введенное пользователем для данного параметра
			$userParamValue = Parameter::getUserParamById($objDefiningParam->getParamId());
			if ( $userParamValue ){
				
				$userParamPresent = true;
				$combinations = (count($bestCombinationsUserParams) > 0)? $bestCombinationsUserParams : $bestCombinationsPriority;
				$currentBestCombs = $this->getBestAccordUserParam($userParamValue, $objDefiningParam, $combinations);
				if (count($currentBestCombs > 0)){
					$bestCombinationsUserParams = $currentBestCombs;
				}
			}
		}
		
		if(!$userParamPresent){
			$this->bestCombinations = $bestCombinationsPriority;
		} else{
			$this->bestCombinations = $bestCombinationsUserParams;
		}
	}
	
	/**
	 * @todo Метод отбирает наиболее подходящие к пользовательскому значению ($userParamValue) комбинации ($bestCombinations) 
	 * по наиболее значимому параметру из множества входящих комбинаций ($in_combinations). Если множество входящих 
	 * комбинаций пусто, то отбор ведётся из ближайшего непоустого множества - $this->bestCombinations или $this->combinations.
	 * @access private 
	 * @param $userParamValue - пользовательское значение параметра
	 * @param $aMostImportantParam - наиболее существеный параметр в зависимости 
	 * @param $in_combinations - множество проверяемых комбинаций
	 * @return $bestCombinations - множество комбинаций, наиболее подходящих для пары ($userParamValue, $aMostImportantParam)
	 * */
	private function getBestAccordUserParam($userParamValue, $aMostImportantParam, $in_combinations){
		//p("вызов Depend->getBestAccordUserParam");
		if($in_combinations == null){
			
			if(count($this->bestCombinations)){
				$in_combinations = $this->bestCombinations;
			} else{
				$in_combinations = $this->combinations;
			}
		}					
		
		// пробежимся по всем комбинациям, выберем наиболее подходящие по максимальному значению
		$type = $aMostImportantParam->getParamType();// NEW

		$bestCombinations = array();
		$maxPriority = 0;		
		
		if ($type == Parameter::_P_PREDDEF_VALUE) {
			
			foreach($in_combinations as $o_comb){									

				$objCombValues = $o_comb->getDefiningValues();
				$objValue = $objCombValues[$aMostImportantParam->getParamId()];
				$value = $objValue->getValue();
				
				if(($value['min'] == $userParamValue['min']) && ($maxPriority < $objValue->getPriority())){					
					$bestCombinations = array($o_comb);
					$maxPriority = $objValue->getPriority();
				} elseif(($value['min'] == $userParamValue['min']) && ($maxPriority == $objValue->getPriority())){
					$bestCombinations[] = $o_comb;
				} else{
					continue;
				}
			}
		} else{
			
			$combinations = $this->getBestAccordUserMaxVal($userParamValue, $aMostImportantParam, $in_combinations);
			
			if(count($combinations) > 0){
				$bestCombinations = $this->getBestAccordUserMinVal($userParamValue, $aMostImportantParam, $combinations);
			} else{
				$bestCombinations = $in_combinations;
			}
		}
		//d($bestCombinations);
		return $bestCombinations;
	}
	
	/**
	 * @access  private
	 * @todo метод возвращает массив комбинаций, для которых максимальное значение параметра $mostImportantId
	 * наиболее точно подходит пользовательскому значению $userParamValue
	 * @param $userParamValue - пользовательское значение параметра
	 * @param $mostImportantId - ID параметра, для которого передаётся пользовательское значение в комбинации
	 * @param $combinations - массив анализируемых комбинаций
	 * @return $minDiffCombForMaxVal - массив комбинаций, наиболее подходящих для указанного пользовательского значения параметра
	 * */
	private function getBestAccordUserMaxVal($userParamValue, $aMostImportantParam, $combinations){
		//p("вызов Depend->getBestAccordUserMaxVal");
		if($userParamValue['max'] == '') return $combinations;
		
		$minDiffCombForMaxVal = array();		
		$errorMinDiff = 100000;
		
		$normalizedUserParamValue = $aMostImportantParam->correctToStandartVariant($userParamValue);
		$currentParamId = $aMostImportantParam->getParamId();
		
		foreach($combinations as $o_comb){			
			// получим фактическое значение комбинации для параметра
			$paramsValues = $o_comb->getParamsValues();
			//d($paramsValues);
			
			if(!is_object($paramsValues[$currentParamId])){
				d($this);
			}
			
			$currentValueId = $paramsValues[$currentParamId]->getValueId();
			$objValue = $this->findCombValueByValueId($currentParamId, $currentValueId);// NEW
			
			$value = $objValue->getValue();
			$haveLowPriority = false;
			
			$aimValue = (2/3) * ($value['max'] - $value['min']);
			
			if($haveLowPriority) continue;
			// 	если значение находится ближе к 2/3 от диапазона, сохраним эту комбинацию
			$currDiff = abs($aimValue - $normalizedUserParamValue['max']);
			
			if ($errorMinDiff > $currDiff) {
				
				$errorMinDiff = $currDiff; //abs((2/3) * ($value['maxval'] - $userParamValue['max']));
				$minDiffCombForMaxVal = array($o_comb);
			} elseif($errorMinDiff == $currDiff){
				$minDiffCombForMaxVal[] = $o_comb;
			} else{
				continue;
			}
		}
		
		return $minDiffCombForMaxVal;
	}
	
	/**
	 * @access private 
	 * @todo метод возвращает массив комбинаций, для которых минимальное значение параметра $mostImportantId
	 * наиболее точно подходит пользовательскому значению $userParamValue
	 * @param $userParamValue - пользовательское значение параметра
	 * @param $mostImportantId - ID параметра, для которого передаётся пользовательское значение в комбинации
	 * @param $combinations - массив анализируемых комбинаций
	 * @return $minDiffCombForMinVal - массив комбинаций с наиболее подходящими к пользовательсим минимальными значениями
	 * */
	private function getBestAccordUserMinVal($userParamValue, $aMostImportantParam, $combinations){
		//p("вызов Depend->getBestAccordUserMinVal");
		$minDiffCombForMinVal = array();
		$errorMinDiff = 10000;
		
		if($userParamValue['min'] == '') return $combinations;
		
		$currentParamId = $aMostImportantParam->getParamId();
		$normalizedUserParamValue = $aMostImportantParam->correctToStandartVariant($userParamValue);
		
		foreach($combinations as $o_comb){						
			
			$paramsValues = $o_comb->getParamsValues();			
			
			$currentValueId = $paramsValues[$currentParamId]->getValueId();
			$value = $this->findCombValueByValueId($currentParamId, $currentValueId)->getValue();// NEW
			$diffCur = abs($normalizedUserParamValue['min'] - $value['min']);
			
			if ($errorMinDiff > $diffCur) {
				
				$errorMinDiff = $diffCur;
				$minDiffCombForMinVal = array($o_comb);
			} elseif($errorMinDiff == $diffCur){
				$minDiffCombForMinVal[] = $o_comb;
			} else{
				continue;
			}
		}
		
		return $minDiffCombForMinVal;
	}
	
	/**
	 * @todo метод возвращает массив ID всех комбинаций для данной зависимости 
	 * @access public 
	 * @param void
	 * @return $comb_ids - массив ID комбинаций зависимости
	 * */
	public function getCombIdList(){
		//p("вызов Depend->getCombIdList");
		$comb_ids = array();
		
		if(count($this->combinations) > 0){
			
			foreach ($this->combinations as $comb){
				$comb_ids[] = $comb->getDepend_ID();
			}
		}
		
		return $comb_ids;
	}
	
	/**
	 * 
	 * @todo Удаляет из списка параметризаций, по которым проанализирована зависимость указанную параметризацию
	 * @param $aModelParamId - удаляемая параметризация
	 * */
	public function deleteFromAnalyzed($aModelParamId){
		
		//$this->combinations = array();
		
		foreach ($this->analyzedParams as $key => $val){
			
			if($this->analyzedParams[$key] == $aModelParamId){
				unset($this->analyzedParams[$key]);
			}
		}
	}

	/**
	 * @todo метод по заданным ID параметра $aParamId и ID значения $aValueId находит в заданной зависимости $aDepend
	 * соответствующее значение, и в зависимости от типа параметра возвращает его атрибуты в виде массива [min, max]
	 * или в виде ID предопределённого значения; если ничего не найдено, возвращает false.
	 *
	 *   создан для того, чтобы избавиться от обращения к БД всюду, где использутеся вызов
	 *   Param::getValue($objFirstLevelParam->getParamId(), $value_id);
	 *   в частности, в строке 654 (solveComplexDepends)
	 * */
	public function findCombValueByValueId($aParamId, $aValueId){
		
		foreach ($this->combinations as $oneCombination){
			
			
			if(($oneCombination->getDefinedValue()->getValueId() == $aValueId)
							and ($oneCombination->getDefinedValue()->getParamId() == $aParamId)){
				return $oneCombination->getDefinedValue();
			} else{
				
				$definingValues = $oneCombination->getDefiningValues();
				$currentDefiningValue = $definingValues[$aParamId];
				
				if(is_object($currentDefiningValue)){
					if($currentDefiningValue->getValueId() == $aValueId){
						return $currentDefiningValue;
					}
				}
			}
		}
	
		return false;
	}
	
	/**
	 * @todo Метод находит по $aParamId указанный параметр среди всех параметров, участвующих в данной зависимости.
	 * @param $aParamId - ID искомого параметра.
	 * когда вся необходимая информация из БД уже получена, не стоит напрягать экземпляр СУБД снова - проще и быстрее 
	 * найти уже существующий экземпляр класса Parameter
	 * */
	public function findParamByParamId($aParamId){
	
		if($this->dependParam->getParamId() == $aParamId){
			return $this->dependParam;
		}elseif($this->definingParams[$aParamId] != null){
			return $this->definingParams[$aParamId];
		} else{
			return false;
		}
	}
}