<?php
/** 
 * @author Дайнеко
 * @todo Класс отвечает за подбор допустимых вариантов исполнения оборудования в рамках одной 
 * модели (из числа определённых на первичной стадии подбора)
 * 
 * @property $model_id - ID модели
 * @property $depends - массив зависимостей, существующих для этой модели; каждый элемент массива - экземпляр класса
 * Depend (см. Depend.php)
 * @property $unmatchParams - массив параметров, по которым какая-било зависимость не проходит отбор
 * @property $model_name - наименование модели
 * @property $autonomicParams - массив автономных параметров в рамках одной модели; автономный 
 * параметр не принимает участия в формировании зависимостей ни в качестве определяемого, ни в качестве определяющего
 * @propertyструктура автономного параметра:
 * (
 *	[param_id] 117 - ID параметра
 *	[paramtype_id] 7 - ID типа параметра
 *	[code] - код, возвращаемый в карту заказа данной модели данным параметром (если нет =null)
 *	[name] - наименование параметра
 *	[modelparam_id] - ID характеристики модели, обеспечиваемой данным параметром
 *  [values] - массив значений данного параметра, удовлетворяющих условиям пользователя
 *  [bestvalue] - это поле ещё не используется
 *  [default] - значение параметра по умолчанию, если пользователь значение не указал (не выбрал)
 *	[weight] - вес параметра
 *	)
 * */
/**
 * $arrAutonomicCombs - массив всех возможных комбинаций значений автономных параметров
 * пример структуры:
 * Array(
 *		[0] => Array(
 *
 *    			[1] => –
 *    			[4] => -
 *    			[3] => А
 *    			[6] => Хс
 *    			[8] => -
 *    			[14] => ГП
 *		)
 *	)
 * структура массива зависимых комбинаций - аналогична: [позиция] => [значение]
 * множества позиций зависимых и автономных комбинаций в пределах одной модели не пересекаются
 **/

class SelectionVariant extends ObjectModel{
	
	const _BEST_WAY_ONLY_ = TRUE;
	const _ALL_COMBINATIONS_ = FALSE;
	/* по ID модели всегда можно получить полный список зависимостей для неё */
	public $model_id;	//	$model_id=>$depends
	private $depends = array();	/* $depends=> depend_params - зная ID зависимости, можно найти все параметры, определяющие её
		Зависимость исключается из списка, если она хотя бы по одному параметру не проходит.
	 */
	private $unmatchParams = array();
	public $model_name;	/* это значение хранить не обязательно - в итоге наименование прибора
						должно появиться только на последнем этапе - формировании строки заказа на оборудование	*/
	/**
	 * структура автономного параметра
	 * Array(
     *	[param_id] => 117
     *	[paramtype_id] => 7
     *	[code] => 
     *	[name] => Условия работы
     *	[modelparam_id] => 144
     *  [values] - массив значений, удовлектворяющих условиям пользователя
     *  [bestvalue] - это поле ещё не используется
     *  [default] - значение параметра по умолчанию, если пользователь значение не указал (не выбрал)
     *	[weight] => 0
	 *	)
	 * */
	public $autonomicParams = array();//должны содержать: ID, тип, номер в строке заказа, значение в строке заказа 
										//(для автономных параметров нет комбинаций значений) 	
	public $linkedModelIds = array(); // Id связанных моделей
	/*
	public function get$linkedModelIds(){
		return $this->otherVariants;
	}
	
	public function getOtherVariantByIndex($variantIndex){
		return $this->otherVariants[$variantIndex];
	}
	
	public function setOtherVariants($anOtherVariant){
		$this->otherVariants[] = $anOtherVariant;
	}
	*/
	/**
	 * @access public
	 * @todo возвращает массив зависимостей для данного варианта подбора
	 * @return $this->depends - массив зависимостей; каждый элемент массива - объект класса Depend (см. файл Depend.php)
	 * */
	public function getDepends(){
		return $this->depends;
	}
	
	public function getAutonomicParams(){
		return $this->autonomicParams;
	}
	
	/**
	 * @access public
	 * @todo возвращает ID модели, для которой создан экземпляр класса
	 * @return $this->model_id - ID модели
	 * */
	public function getModelId(){
		return $this->model_id;
	} 
	
	/**
	 * @access public
	 * @todo инициализирует экземпляр класса значениями, связанными с моделью прибора (поступающей в качестве параметра на вход)
	 * @param $one_model:
	 * - на начальном шаге процесса подбора это массив вида (model_id, modelname)
	 * - после начального шага (и далее) - экземпляр класса SelectionVariant
	 * */
	public function init($one_model){
		
		if(is_array($one_model)){// инициализация в случае, если $one_model - массив
			$this->initById($one_model['model_id'], $one_model['modelname']);
		} else{// инициализация в случае, если $one_model - объект
			
			$this->depends = $one_model->depends;
			$this->model_id = $one_model->model_id;
			$this->model_name = $this->getModelName();
			$this->autonomicParams = $one_model->autonomicParams;
			//$this->bestCombinations = $one_model->bestCombinations;
			//d($this->autonomicParams);
			unset($one_model);
		}
		
		if(parent::$debug){
			parent::$debugList[] = '<p>Модель: '.$this->model_name.", ИД = ".$this->model_id.' Количество комбинаций для модели: '.count($this->depends).'</p>';
		}		
	}
	
	/**
	 * @todo метод производит поиск значения по умолчанию для параметра типа FLOAT; значение по умолчанию - то которое
	 * имеет максимальный для этого параметра приоритет. такое значение всегда только одно:
	 * если приоритеты всех значений равны, будет возвращено первое значение из результата запроса
	 * @access private
	 * @param $autoModelParamID - ID автономного параметра
	 * @return значение по умолчанию, устанавливаемое данным автономным параметром в строку заказа;
	 * если параметр не устанавливает никакой позиции в строке заказа, метод возвратит null
	 
	private function setDefaultValueFLOAT($autoModelParamID){
		//d("вызов SelectionVariant->setDefaultValueFLOAT");
		$sql = 'select sp.`codeval` as defaultval
				from
					`floatparams` sp, `vmodelparametrization` vmp
				where
					vmp.`paramtype_id` <> '.Param::_P_PREDDEF_VALUE.'
					and
					vmp.`mpid` = sp.`modelparametrization_id`
					and
					vmp.`mpid` = '.$autoModelParamID.'
					and
					sp.`priority` = (select MAX(_sp.priority)
									from `floatparams` _sp, `vmodelparametrization` _vmp
                    				where _sp.`modelparametrization_id` = `_vmp`.`mpid`
                    				and `_vmp`.`mpid` = '.$autoModelParamID.')';
		
		$result = Db::getInstance()->executeS($sql);
		
		return $result[0]['defaultval'];
	}
	* */
	/**
	 * @todo метод производит поиск значения по умолчанию для параметра типа STRING; значение по умолчанию - то которое
	 * имеет максимальный для этого параметра приоритет. такое значение всегда только одно:
	 * если приоритеты всех значений равны, будет возвращено первое значение из результата запроса
	 * @access private
	 * @param $autoModelParamID - ID автономного параметра
	 * @return значение по умолчанию, устанавливаемое данным автономным параметром в строку заказа;
	 * если параметр не устанавливает никакой позиции в строке заказа, метод возвратит null
	 
	private function setDefaultValueSTRING($autoModelParamID){
		//d("вызов SelectionVariant->setDefaultValueSTRING");
		$sql = 'select sp.`codeval` as defaultval
				from
					`stringparams` sp, `vmodelparametrization` vmp
				where
					vmp.`paramtype_id` = '.Param::_P_PREDDEF_VALUE.'
					and
					vmp.`mpid` = sp.`modelparametrization_id`
					and
					vmp.`mpid` = '.$autoModelParamID.'
					and
					sp.`priority` = (select MAX(_sp.priority)
									from `stringparams` _sp, `vmodelparametrization` _vmp 
                    				where _sp.`modelparametrization_id` = `_vmp`.`mpid`
                    				and `_vmp`.`mpid` = '.$autoModelParamID.')';
		
		$_result = Db::getInstance()->executeS($sql);
		//p($autoModelParamID);
		//p($_result);
		//$autoParam['default'] = $_result[0]['defaultval'];
		return $_result[0]['defaultval'];
	}
	* */
	/**
	 * @todo метод производит поиск значения по умолчанию для параметра типа INT; значение по умолчанию - то которое
	 * имеет максимальный для этого параметра приоритет. такое значение всегда только одно:
	 * если приоритеты всех значений равны, будет возвращено первое значение из результата запроса
	 * @access private
	 * @param $autoModelParamID - ID автономного параметра
	 * @return значение по умолчанию, устанавливаемое данным автономным параметром в строку заказа;
	 * если параметр не устанавливает никакой позиции в строке заказа, метод возвратит null
	 
	private function getDefaultValueINT($autoModelParamID){
		//d("вызов SelectionVariant->getDefaultValueINT");
		$sql = 'select sp.`codeval` as defaultval
				from
					`intparams` sp, `vmodelparametrization` vmp
				where
					vmp.`mpid` = sp.`modelparametrization_id`
					and
					vmp.`mpid` = '.$autoModelParamID.'
					and
					sp.`priority` = (select MAX(_sp.priority)
									from `intparams` _sp, `vmodelparametrization` _vmp
                    				where _sp.`modelparametrization_id` = `_vmp`.`mpid`
                    				and `_vmp`.`mpid` = '.$autoModelParamID.')';
		
		$_result = Db::getInstance()->executeS($sql);
		//d($_result);
		//$autoParam['default'] = $_result[0]['defaultval'];
		return $_result[0]['defaultval'];
	}
	* */
	/**
	 * @todo метод инициализирует экземпляр класса по ID модели (наименование модели является дополнительным параметром,
	 * позволяющим не лазить постоянно в БД с мелочными запросами)
	 * @access private
	 * @param $model_id - ID модели прибора
	 * @param $model_name - наименование модели прибора
	 * */
	private function initById($model_id, $model_name = null){
		
		
		//d($this);
		$this->model_id = $model_id;
		//$this->model_name = $this->getModelName();
		
		$this->initAutonomicParameters();
		// получим для модели все комбинации параметров
		$this->initDepends();
		//p($this->depends);
	}
	
	private function initDepends($aModelId){
		
		// получим для модели все комбинации параметров
		$dependInfoForModel = Depend::getDependInfoForModel($this->model_id);
		
		//d($dependInfoForModel);
		
		foreach ($dependInfoForModel as $oneDependInfo){
				
			$dependParam = Parameter::createParamObj(new Parameter($oneDependInfo['param_id']));
				
			$dependParam->setModelParam($oneDependInfo['model_par_id']);
			$dependParam->setCodeOrder($oneDependInfo['codeorder']);
			$dependParam->setWeight($oneDependInfo['weight']);
			
			$depend = new Depend($dependParam);
				
			$definingParamsInfo = Depend::getDefiningParamsInfo($oneDependInfo['model_par_id']);
				
			foreach ($definingParamsInfo as $oneDefiningParamInfo){
		
				$definingParam = Parameter::createParamObj(new Parameter($oneDefiningParamInfo['param_id']));
		
				$definingParam->setModelParam($oneDefiningParamInfo['model_par_id']);
				$definingParam->setCodeOrder($oneDefiningParamInfo['codeorder']);
				$definingParam->setWeight($oneDefiningParamInfo['weight']);
				
				$depend->addParam($definingParam);
			}
				
			$this->depends[$depend->getDependParam()->getModelParamId()] = $depend;
		}
		//p($this->depends);
	}
	
	/**
	 * 
	 * */
	private function initAutonomicParameters(){
		
		$sqlAutoParams = 'select
								mp.`param_id` as param_id,
								p.paramtype_id as param_type,
								mp.codeorder as codeorder,
								mp.id as model_par_id,
								mp.weight as weight
							from `modelparametrization` mp, parameters p
							where
							p.id = mp.param_id
				/*
							and
							mp.codeorder > 0
				*/
							and
							mp.`id` not in (select distinct dp.`modelparametrization_independent_id` from `dependence_parametrization` dp)
							and
							mp.`id` not in (select distinct c.`modelparam_id` from `combinations` c)
							and
							mp.`model_id` = '.$this->model_id;
		
		$arrAutoParams = array();
		/* получить массив всех автономных для данной модели параметров */
		$arrAutoParams = Db::getInstance()->executeS($sqlAutoParams);
		//d($sqlAutoParams);
		
		/* для каждого автономного параметра получаем значение по умолчанию; в зависимости от типа параметра
		 * будет вызван соответствующий метод, возвращающий значение по умолчанию
		*  */
		foreach ($arrAutoParams as $ap){
				
			$autonomicParameter = Parameter::createParamObj(new Parameter($ap['param_id']));
			$autonomicParameter->setModelParam($ap['model_par_id']);
			$autonomicParameter->setCodeOrder($ap['codeorder']);
			$autonomicParameter->setWeight($ap['weight']);
			
			$this->autonomicParams[$ap['param_id']] = $autonomicParameter;
		}
	}
	
	/**
	 * 
	 * */
	private function searchParamVariant($aParamObj){
		//d($aParamObj);
		$returnResult = false;
		
		if((get_parent_class($aParamObj) == "FloatParam") or (get_class($aParamObj) == "FloatParam")){
			
			$modelParamCalcVariants = $this->getAllCalcVariantsForModelParam($aParamObj->getModelParamId());
			
			foreach ($modelParamCalcVariants as $calcVariant){
			
				$evalCondition = $this->parseConditionTemplate($calcVariant['condition']);
				$conditionResult = eval($evalCondition);
				
				if($conditionResult == true){
					
					$returnResult = $calcVariant['calcTemplate'];
					break;
				}
			}
		}
		
		return $returnResult;
	}
	
	/**
	 * 
	 * */
	private function parseConditionTemplate($evalCondition){
		
		$userValues = ObjectModel::getFromSessionByName("params");
		
		$strParam = "param";
		$strUNDERLINE = "_";
		$strMIN = "_min";
		$strMAX = "_max";
		
		while(strpos($evalCondition, $strParam) != false){
			// начало позиции вхождения параметра в строку условия
			$startParamPosition = strpos($evalCondition, $strParam);
			// 
			$underlinePosition = strpos($evalCondition, $strUNDERLINE);
			$endParamPosition = $underlinePosition + strlen($strMIN);
			$strParamEntrance = substr($evalCondition, $startParamPosition, ($endParamPosition - $startParamPosition));
			
			if(strpos($strParamEntrance, $strMIN) > 0){
				$userValueIndex = "min";
			} elseif(strpos($strParamEntrance, $strMAX) > 0){
				$userValueIndex = "max";
			}
			
			$paramId = str_replace($strParam, "", str_replace($strMAX, "", str_replace($strMIN, "", $strParamEntrance)));
			
			$paramUserValue = FloatParam::recalcMeasureUnit($userValues[$paramId]["measUnitId"], $userValues[$paramId][$userValueIndex]);
			$evalCondition = str_replace($strParamEntrance, $paramUserValue, $evalCondition);
		}
		
		$evalCondition = $this->parseMeasureUnitInCondition($evalCondition);
		//d($evalCondition);
		return $evalCondition;
	}
	
	/**
	 * 
	 * */
	private function parseMeasureUnitInCondition($anEvalCondition){
		
		$userValues = ObjectModel::getFromSessionByName("params");
		
		$strMeasUnit = "measUnit";
		$strEQ = "==";
		// если будет предполагаться условие на единицы измерения более чем одного параметра, фрагмент кода до return взять в цикл.
		$strMeasUnitPosition = strpos($anEvalCondition, $strMeasUnit);
		$strEQPosition = strpos($anEvalCondition, $strEQ, $strMeasUnitPosition);
		
		$measUnitEntrance = substr($anEvalCondition, $strMeasUnitPosition, $strEQPosition - $strMeasUnitPosition);
		$paramId = str_replace(" ", "", str_replace($strMeasUnit, "", $measUnitEntrance));
		// смотря по тому, есть ли у параметра $paramId единица измерения, заменять его вхождение на id единицы измерения
		//p($userValues[$paramId]["measUnitId"]); 
		if($userValues[$paramId]["measUnitId"] != null){
			$evalCondition = str_replace($strMeasUnit.$paramId, $userValues[$paramId]["measUnitId"], $anEvalCondition);
		} else{
			$evalCondition = str_replace($strMeasUnit.$paramId, 0, $anEvalCondition);
		}
		//p($evalCondition);
		return $evalCondition;
	}	
	
	/**
	 *
	 * */
	private function getAllCalcVariantsForModelParam($aModelParamId){
		
		$sql = 'select rc.`condition` as `condition`, `rc`.`formulaTemplate` as calcTemplate
				from (`recalcs` `rc` join `paramvariants` pv)
				where
					rc.`id` = pv.`recalc_id`
					and
					`pv`.`modelparam_id` = '.$aModelParamId;
		
		return Db::getInstance()->executeS($sql);
	}

	/**
	 * @access private
	 * @todo возвращает для данного варианта подбора имя модели прибора, для которой прибор подбирался 
	 * @param void
	 * @return имя прибора, к которому данный вариант подбора относится
	 * */
	public function getModelName(){
		
		$sql = 'select basename from modelline where modelline.id = '.$this->model_id;
		
		$result = Db::getInstance()->getRow($sql);
		$name = $result['basename'];
		
		return $name;
	}
	
	/**
	 * @access public
	 * @todo Конструктор. Создаёт неинициализированный экземпляр класса.
	 * @param void
	 * */
	public function __construct(){}
	
	/**
	 * @todo метод ищет удовлетворяющие комбинации значений зависимости(группы параметров) для данного значения параметра
	 * @access private
	 * @param $val = array('modelparam_id'=xx,'values_id'=xx);
	 * @param $currentStep - текущий шаг процесса подбора
	 * @return void
	 * */
	private function checkDependForValue($val, $currentStep){
		//p($this->depends);
		//d($val);
		$isOk = false;
		
		foreach($this->depends as $o_dep){
			//p($val->getModelParamId());
			if($o_dep->isDependOfParam($val->getModelParamId())){
				
				if($o_dep->getDependParam()->getModelParamId() == 146){
					//('12345');
				}
				
				if(($o_dep->analyzed == 0) or ($o_dep->analyzed == $val->getModelParamId())){ // NEW

					$isOk = $o_dep->getDependValuesForValueID($val->getModelParamId(), $val->getValueId(), $currentStep);
					
					if($isOk < 1) return false;			
				} else{
					continue;
				}
			} else{
				continue;
			}
		}
		
		return true;
	}
	
	/**
	 * @todo Открытый метод, точка входа в процесс подбора в рамках одной модели. Для проверки удовлетворяет ли данная модель
	 * значениям параметров, введеных пользователем.
	 * @access public
	 * @param $userParams: пользовательские значения параметров array(param_id=>array('min','max'))
	 * @param $currentStep = 100 - текущий шаг процесса подбора
	 * @return $isOk:bool - удовлетворяет ли рассматриваемая параметризация модели пользовательским значениям параметров 
	 * */
	public function checkThis($userParams, $currentStep = 100){				
		
		$isOk = false;		
		
		//d($this->depends);
		
		if(count($this->depends) > 0){
			
			foreach ($this->depends as $oneDepend){
				//$oneDepend->analyzed = 0;
				// ИНИЦИАЛИЗАЦИЯ ФОРМУЛ ПЕРЕСЧЁТА ЗДЕСЬ
				$dependParamCalcTemplate = $this->searchParamVariant($oneDepend->getDependParam());
				$oneDepend->getDependParam()->setCalcTemplate($dependParamCalcTemplate);
					
				foreach ($oneDepend->getDefiningParams() as $oneDefiningParam){
				
					$definingParamCalcTemplate = $this->searchParamVariant($oneDefiningParam);
					$oneDefiningParam->setCalcTemplate($definingParamCalcTemplate);
				}
			}
		}
		//d($userParams);
		if(is_array($userParams) && count($userParams) > 0){
			
			foreach($userParams as $paramId => $value ){
				
				if($this->hasParam($paramId) < 1){
					continue;
				}
				
				//$value_ind - информация о характеристиках моделей, удовлетворяющих условиям поиска
				/** 
				 * ID характеристики модели из dependence_parametrization
				 * ID строки со значениями из dependence_parametrization
				 * codeorder, если есть (иначе - null)
				 * codevalue, если есть (иначе - null)
				 * в случае, если пользователь не указал значение параметра, codevalue должен принять значение по умолчанию
				 * (значение с максимальным приоритетом)
				 * */
				$objParam = Parameter::createParamObj(new Parameter($paramId));
				$objParam->extendedInit($this->model_id);
				// возможно, правильнее не создавать новый объект Parameter, а найти подходящий среди уже существующих
				// ИНИЦИАЛИЗАЦИЯ ФОРМУЛ ПЕРЕСЧЁТА ЗДЕСЬ
				$objParam->setCalcTemplate($this->searchParamVariant($objParam));
				//d($this->depends);
				
				$value_ind = $objParam->searchSatisfyValues($value);//new
				// проверим все возможные комбинации зависимых параметров, которые удовлетворяют данному значению
				if((is_array($value_ind)) && (count($value_ind) >= 1)){
					// проверим все возможные комбинации зависимых параметров, которые удовлетворяют данному значению
					foreach($value_ind as $val){
						
						if(array_key_exists ($paramId, $this->autonomicParams)){
							//$value_ind - информация о характеристиках моделей, удовлетворяющих условиям поиска
							/*в Parameter надо вернуть values; добавить для него методы get и set 
							 * values хранить с ключом ValueId
							 * */
							//d($this->autonomicParams);
							$this->autonomicParams[$paramId]->values[$val->getValueId()] = $val;
							$isOk = true;
						} else {
							
							$isOk = $this->checkDependForValue($val, $currentStep);
							//d($isOk);
							if(($isOk < 1) && ($currentStep > 1)){	
								$this->unmatchParams[] = $paramId;									
								$isOk = false;								
							}
						}
					}
				} else{
					
					$this->unmatchParams[] = $paramId;
					$isOk = false;
					//d($paramId);
					break;
				}
			}
			//p($this);
			//зависимости с конфликтующими значениями разрешаются этим вызовом метода solveDependsConflicts() 
			$this->solveDependsConflicts();
			//разрешим транзитивные зависимости (сложные, когда определяющим является зависимый параметр)
			$this->solveComplexDepends();
					
			/****************************************************************
			 * разделить 2 действия - инициализацию $isOk и обнуление $one_dep->analyzed - 
			 * единственно верный выход: иначе при неподходящей одной зависимости
			 * все остальные (следующие за ней) не обнуляют $one_dep->analyzed,
			 * и таким образом он может оказаться > 0. После разделения этих действий всё работает предсказуемо.
			 *****************************************************************/
			foreach($this->depends as $one_dep){
				//p($one_dep->paramId);
				//
				// - если зависимость проанализирована ($one_dep->analyzed > 0), и в ней не осталось 
				// комбинаций ($combinationCount == 0),
				// то $isOk = false - вариант подбора должен быть исключён
				// 
				// во всех остальных случаях вариант подбора продолжает рассматриваться, как возможный
				//				
				$combinationCount = count($one_dep->getAllCombination());
				
				//$isOk = true;
				
				if(($one_dep->analyzed > 0) and ($combinationCount == 0)){
					
					$this->unmatchParams[] = $paramId;

					$isOk = false;					
					$one_dep->analyzed = 0;
					break;
				} 
			}
			
			foreach ($this->depends as $one_dep){
				if($one_dep->analyzed != 0){
					$one_dep->analyzed = 0;
				}
			}
		}

		/* лучшие комбинации могут формироваться только тогда, когда $isOK == true, то есть подходящие комбинации есть. */
		if($isOk == true){
			//
			foreach($this->depends as $one_dep){
				$one_dep->getBestCombinationAccordWeight();
			}
			
			$this->getBestAutomicValue();
			
			if(parent::$debug){
				
				self::$debugList[]='<p>Модель '.$this->model_id.'</p>';
				self::$debugList[]='<p>Число зависимостей '.count($this->depends).'</p>';
				$d = $this->depends;
				
				foreach ($d as $dep){
					
					self::$debugList[]='<p>Число комбинаций '.count($dep->getAllCombination()).'</p>';
					self::$debugList[]='<p>Число лучших комбинаций'.count($dep->getBestCombinationInfo()).'<p>';
				}
			}
		}	
		
		//d($this->depends);
		//d($this->autonomicParams);
		//p($isOk);
		
		return $isOk;
	}//end of checkThis
	
	/**
	 * @access
	 * @todo
	 * @param
	 * @return
	 * */
	public function getSelectVariantInfo($isExpand = false){
		
		$result = array();
		$result["name"] = $this->model_name;
		$result["id"] = $this->model_id;
		
		return $result;
	}
	
	/**
	 * @access private
	 * @todo метод подбирает значения автономных параметров, наиболее точно удовлетворяющие пользовательским значениям 
	 * запрошенных параметров, сохраняя найденные значения в $this->autonomicParams['values']
	 * @param void
	 * @return void
	 * */
	private function getBestAutomicValue(){
		
		$diffMax = 1000;
		$diffMin = 1000;
		
		foreach($this->autonomicParams as $paramId => &$param){
			
			//$userParam = Param::getUserParamById($paramId);//OLD
			$userParam = $param->correctToStandartVariant(Parameter::getUserParamById($paramId));// NEW
			//d($this->autonomicParams);
			
			$values = $this->autonomicParams[$paramId]->values;
			
			if (count($values) > 0) {
				//d($values);
				foreach($values as $valForAnalyzeByMax){
					
					$currentValue = $valForAnalyzeByMax->getValue();
						
					if ($diffMax > abs($currentValue['max'] - $userParam['max'])) {
						
						$diffMax = abs($currentValue['max'] - $userParam['max']);
						$param->values = array($valForAnalyzeByMax);
					} elseif($diffMax == abs($currentValue['max'] - $userParam['max'])){
						$param->values[$valForAnalyzeByMax->getValueId()] = $valForAnalyzeByMax;
					}else{
						continue;
					}
				}
				
				$values = $param->values;				
				
				foreach($values as $valForAnalyzeByMin){
					
					$currentValue = $valForAnalyzeByMin->getValue();
						
					if($diffMin > abs($currentValue['min'] - $userParam['min'])){
						
						$diffMin = abs($currentValue['min'] - $userParam['min']);
						$param->values = array($valForAnalyzeByMin);
					} elseif($diffMin == abs($val['min'] - $userParam['min'])){
						$param->values[$valForAnalyzeByMin->getValueId()] = $valForAnalyzeByMin;
					}else{
						continue;
					}
				}
									
			}
		}		
	}
	
	/**
	 * @access private
	 * @todo Метод разрешает конфликты в зависимостях (Когда несколько зависимостей определяются одним и тем же параметром,
	 * но в процессе подбора приводят к несовместимым комбинациям)
	 * @return void
	 */
	private function solveDependsConflicts(){
				
		$conflictDeps = array();
		$allDependsParams = array();// массив, хранящий объекты зависимостей
		// найдем все параметры от которые определяют несколько зависимостей
		foreach($this->depends as &$objDepend){
			//p($dep);
			foreach ($objDepend->getDefiningParams() as $definingParam){
				
				if(array_key_exists($definingParam->getParamId(), $allDependsParams)){
					$allDependsParams[$definingParam->getParamId()][] = $objDepend;// сохраним их
				} else{
					$allDependsParams[$definingParam->getParamId()] = array($objDepend);
				}
			}
			
			if(array_key_exists($objDepend->getDependParam()->getParamId(), $allDependsParams)){
				$allDependsParams[$objDepend->getDependParam()->getParamId()][] = $objDepend;
			}
		}
		
		$result = true;
		
		//d($allDependsParams);
		foreach($allDependsParams as $paramId=>$depends){
			
			$paramValues = array();
			// для каждой зависимости найдем допустимые значения для параметра
			foreach ($depends as $one_depend){
				
				$paramValuesDepend = $one_depend->getParamsValues();// {paramId => object Value}
				//d($paramValuesDepend);
				foreach($paramValuesDepend as $paramValueDepend){
					
					if($paramValues[$one_depend->getDependParam()->getParamId()] == null){
						$paramValues[$one_depend->getDependParam()->getParamId()] = array();
					}
					
					if(is_object($paramValueDepend[$paramId])){
						$paramValues[$one_depend->getDependParam()->getParamId()][] = $paramValueDepend[$paramId]->getValueId();
					}
				}
			}

			
			//d($paramValues);
			$commonValues = array_pop($paramValues);

			foreach($paramValues as $dep_values){
				//p($commonValues);
				//p($dep_values);
				$commonValues = array_intersect($commonValues, $dep_values);
			}
			
			if(is_array($commonValues)){	
				
				$commonValues = array_unique($commonValues);

				foreach($depends as $one_depend){// оставим только подходящие комбинации
					
					$one_depend->findParamByParamId($paramId);
					$result = $one_depend->filterCombinationsByParamValue($paramId, $commonValues);
				}
			} else{
				$result = false;
			}
		}
	}
	
	/**
	 * @todo Метод разрешает транзитивные (сложные) зависимости (когда зависимость, определяется завимым параметром)
	 * @access private
	 * @param void
	 * @return void
	 * */
	private function solveComplexDepends(){
		// получим все зависимые model_param
		$dependsParams = array_keys($this->depends);
		//p($dependsParams);
		$relationDeps = array();
		// найдём сложные зависимости (транзитивные)
		foreach ($dependsParams as $depModelParamId){
			
			foreach ($this->depends as $one_dep){
				
				if($one_dep->isDependOfParam($depModelParamId)){
					
					if($one_dep->getDependParam()->getModelParamId() == $depModelParamId) 
						continue;
					
					if($relationDeps[$depModelParamId] == null) 
						$relationDeps[$depModelParamId] = array();
					// paramId всех найденных транзитивных зависимостей помещаются в $relationDeps
					// с ключом, взятым от зависимости (modelParamId)
					$relationDeps[$depModelParamId][] = $one_dep->getDependParam()->getModelParamId();
				}
			}
		}
		//d($relationDeps);
		// получили массив, ключи - model_param которые зависят только от параметров, значения - массив зависимых model_param,
		// которые зависят от зависимого параметра той же модели (класс отвечает за реализацию процесса подбора в рамках)
		// одной модели, => для каждой модлеи экземпляр класса свой
		foreach($relationDeps as $firstLevelDepModelPar => $dependSecondLvlModelPars){
			
			$depObjFst = $this->depends[$firstLevelDepModelPar];
			//d($depObjFst);
			// получим лучшие комбинации для всех простых зависимостей
			$depInfo = $depObjFst->getCombinationInfo();
			//d($depInfo);
			// проведем проверку транзитивных зависимостей на наилучшее значение простой зависимости	
			if(count($depInfo) > 0){
				
				foreach($dependSecondLvlModelPars as $scdDepend){
					
					$depObjScnd = $this->depends[$scdDepend];
					//d($depObjScnd);
					//$paramId = Parameter::getParamIdByModel_parId($firstLevelDepModelPar);// И НЕ ЛОГИЧНО, И НЕПРАВИЛЬНО
					//d($paramId);
					$objFirstLevelParam = $this->depends[$firstLevelDepModelPar]->getDependParam();
					$objFirstLevelDepend = $this->depends[$firstLevelDepModelPar];// комбинацию сохраняем для поиска значения по 
					// ID параметра + ID значения (см стр 647, $realVal = ... и метод findCombValueByValueId)
					//p($depInfo['value_id']);
					//d($objFirstLevelDepend);
					if((count($depObjScnd->getAllCombination()) < 1) 
									&& ($objFirstLevelParam->getParamType() != Parameter::_P_PREDDEF_VALUE)){
						
						$valueArr = array();
						$values_ids = array();
						
						foreach($depInfo['value_id'] as $value_id){
							// значение проще и быстрее найти, а не получать из бд	
							//$realVal = Param::getValue($objFirstLevelParam->getParamId(), $value_id);// OLD
							$realVal = $objFirstLevelDepend->findCombValueByValueId($objFirstLevelParam->getParamId(), 
											$value_id)->getValue();
							//d($realVal);
							//$valueArr = array('min'=>$realVal['min'],'max'=>$realVal['max']);
							//$valuesForVal = $this->searchSatisfyValues($objFirstLevelParam->getParamId(), $realVal);// OLD
							$valuesForVal = $objFirstLevelParam->searchSatisfyValues($realVal);// NEW
							
							if(!is_array($valuesForVal)){
								//d($realVal);
								//p($objFirstLevelParam->getParamId());
								//p($value_id);
								//p($this->depends);
								
								//d($valueArr);
							}
								
							foreach ($valuesForVal as $one_value){
								$values_ids[] = $one_value->getValueId();
							}
						}
						//p("разрешение сложных зависимостей ".$this->model_name);
						$depObjScnd->filterCombinationsByParamValue($objFirstLevelParam->getParamId(), $values_ids, 'Complex');
					} else{
						//p($objFirstLevelParam->getParamType());
						$depObjScnd->filterCombinationsByParamValue($objFirstLevelParam->getParamId(), $depInfo['value_id'], 'Complex');
					}
				}
			}
		}
	}
	
	/**
	 * @todo Метод проверяет 2 экземпляра комбинаций на совместимость (не конфликтуют ли параметры). Принцип проверки:
	 * по умолчанию значения параметров в комбинациях не конфликтуют. Но если при равенстве ключей параметров 
	 * обнаруживается неравентсво значений, это конфликт - такие комбинации нельзя соединить.
	 * @access private
	 * @param $comb1, $comb2 - пара комбинаций (экземпляров класса Combination), проверяемая на наличие конфликтующих значений. 
	 * @return $isCompartible - 1, если все проверяемая пара комбинаций совместима, 0 - в противном случае.
	 * */
	private function checkCompartible($comb1, $comb2){
		////p("вызов SelectionVariant->checkCompartible");
		
		$isCompartible = 1;		
		$keys = array_keys($comb1);
		
		foreach ($keys as $k){			
			
			if($comb2[$k] != null && $comb1[$k] != null && $comb1[$k] != $comb2[$k]){
				
				$isCompartible = 0;//для несовметсимости комбинаций достаточно одного несовпадения значений при совпадении ключа
				break;//при первом же несовпадении прекращаем выполнение цикла и возвращаем результат (0 = false)
			}
		}
		
		return $isCompartible;
	}
	
	
	/**
	 * @todo метод отбирает среди автономных параметров только участвующие в формировании строки заказа (codeorder != null)
	 * @access private
	 * @param void
	 * @return $result - подмножество массива $this->autonomicParams
	 * */
	private function filterAutonomicParams(){
		////p("вызов SelectionVariant->filterAutonomicParams");
		
		$result = array();
		//d($this->autonomicParams);
		foreach ($this->autonomicParams as $ap){
			/* в исходный массив значений автономных параметров должны попадать только те, которые пользователь не указал 
			 * явно (явно указанные значения параметров, как автономных, так и определяемых, и определяющих, хранятся в
			 * переменной $_SESSION)
			 *  */
			if($ap->getCodeOrder() != null){
				$result[$ap->getParamId()] = $ap;
			}
		}
		//d($_SESSION);
		return $result;
	}
	
	/**
	 * @todo метод возвращает для всех автономных параметров все комбинации пар (позиция-значение)
	 * значения по умолчанию автономным параметрам присваиваются внутри этого метода,
	 * для чего каждая автономная характеристика прибора снабжается ещё одним полем - значением по умолчанию
	 * @access private
	 * @param void
	 * @return $result - массив всех автономных параметров, участвующих в формировании строки заказа
	 * */
	private function getAutonomParamComb(){
		
		$result = array();
		$arrCodedAutoParams = array();
		$arrCodedAutoParams = $this->filterAutonomicParams();
		//d($arrCodedAutoParams);
		$pair = array();		
		
		foreach ($arrCodedAutoParams as $codedAutoParam){
			//d($cap);
			if($codedAutoParam->values != null){
				
				if(is_array($codedAutoParam->values)){
					
					$pairArr = array();
					
					foreach ($codedAutoParam->values as $objValue){
						//d($vals);
						if($objValue->getCode() != null){
							$pair= array($objValue->getCode());
						}
					}
				}
			} else{
				$pair = array($codedAutoParam->getDefaultValue()->getCode());
			}
			
			$result[$codedAutoParam->getCodeOrder()] = $pair;
			//p($result);
		}
		//d($result);
		//d($result);//это промежуточный результат - все возможные пары значений
		return $result;
	}
	
	/**
	 * @access private
	 * @todo метод выполняет слияние всех пар(позиция-значение) для всех автономных параметров прибора во всех возможных комбинациях
	 * @param void
	 * @return $result - массив всех возможных комбинаций позиция=>значение для автономных параметров в пределах одной модели
	 * */
	private function mergeAutonomicParamCombinations(){
		////p("вызов SelectionVariant->mergeAutonomicParamCombinations");
		
		$sourceAutonomParamComb = $this->getAutonomParamComb();		
		$result = array();
		$comb = array();
		$keys = array_keys($sourceAutonomParamComb);				
		
		if(is_array($sourceAutonomParamComb)){
			
			foreach ($keys as $k){
				
				$currentSourceComb = $sourceAutonomParamComb[$k];		
				//d($currentSourceComb);		
				$currentSourceKeys = array_keys($currentSourceComb);
				
				foreach ($currentSourceKeys as $csk){
					$comb[$k] = $currentSourceComb[$csk];
				}
			}
			
			$result[]= $comb;
		}
		//d($result);
		return $result;
	}
	
	/**
	 * @access private
	 * @todo метод обеспечивает слияние двух ЭКЗЕМПЛЯРОВ комбинаций
	 * @param $comb1, $comb2 - 2 экземпляра комбинаций (комбинация - массив, в котором ключ - позиция в строке заказа, а значение по этому 
	 * ключу - символ в карте заказа на соответствующей позиции). * перед слиянием делает проверку на совместимость комбинаций. если 
	 * комбинации несовместимы, перкращает выполенение, возвращая null. 
	 * @return $result - массив символов в карте заказа, с ключами, соответствующими позициям этих символов в строке заказа
	 * */
	private function mergeCombination($comb1, $comb2){
		////p("вызов SelectionVariant->mergeCombination");
		
		$result = array();
		/**
		 * комментарий к алгоритму слияния двух экземпляров комбинаций
		 * 
		 * 1. сливаются только совместимые комбинации, следовательно не может быть так, что
		 * при равенстве ключей комбинаций $comb1 и $comb2 оказалось неравенство значений
		 * 
		 * 2. в случае, если в $result уже имеется ключ, равный текущему значению из $comb2, то значение по этому ключу
		 * перезапишется само на себя (вследствие 1.)
		 * 
		 * 3. ключи, содержащиеся только в $comb1 или только в $comb2 никак не повлияют друг на друга - они полностью независимы
		 * */
		//p($comb1);
		//p($comb2);
		//p($this->checkCompartible($comb1, $comb2));
		//p("=====================");
		if($this->checkCompartible($comb1, $comb2) == true){
			
			$result = $comb1;
			$keys = array_keys($comb2);
			foreach ($keys as $k){
				$result[$k] = $comb2[$k];
			}
			//p($result);			
		} else{
			//echo('mergeCombination null');			
			$result = null;
		}
		
		return $result;//$result - экземпляр комбинации символов строки заказа, включающий символ из первого и из второго экземпля комбинаций
	}
	
	/**
	 * @access private
	 * @todo метод реализует слияние двух массивов комбинаций (комбинация - массив, в котором ключ - позиция в строке 
	 * заказа, значение - символ в этой позиции строки заказа) не забывать: комбинация - тоже МАССИВ!!!, в котором позиция в строке заказа - ключ, а значение по этому ключу-
	 * символ в позиции строки заказа.
	 * @param $arr1, $arr2 - массивы сливаемых комбинаций параметров
	 * @return результирующий массив, полученный слиянием совместимых (неконфликтующих) элементов исходных массивов
	 * */
	private function mergeCombinationArr($arr1, $arr2){
		////p("вызов SelectionVariant->mergeCombinationArr");
		
		$result = array();
		if((count($arr1) > 0) && (count($arr2) > 0)){		

			$keys1 = array_keys($arr1);
			$keys2 = array_keys($arr2);

			foreach ($keys1 as $k1){
				foreach ($keys2 as $k2){				
					$pair = $this->mergeCombination($arr1[$k1], $arr2[$k2]);
					if(count($pair) > 0){
						$result[] = $pair;
					}
					else{
						continue;
					}
				}
			}
			
			if(count($result) < 1){
				$result = $arr1;
			}
		} else{			
			$result = $arr1;
		}
		//использование этого метода исключает повторение комбинаций, полученных в результате слияния элементарных комбинаций
		$result = $this->excludeRepeatings($result);
		return $result;
	}
	
	/**
	 * @access private
	 * @todo метод удаляет из исхдного массива повторяющиеся элементы путём помещения в 
	 * новый массив только тех элементов, которые в нём ещё не содержатся
	 * @param $arr - исходный массив, из которого необходимо удалить повторяющиеся элементы
	 * @return $result - массив с теми же элементами, что и исходный, но без повторений
	 * */
	private function excludeRepeatings($arr){
		////p("вызов SelectionVariant->excludeRepeatings");
		
		$result = array();
		
		if(is_array($arr)){
			
			foreach ($arr as $a){
				if(!in_array($a, $result)){
					$result[] = $a;
				}
			}
		}
		
		return $result;
	}

	/**
	 * @access private
	 * @todo метод реализует слияние всех частичных комбинаций строк заказа в строку заказа, содержащую все позиции
	 * (полную строку заказа)
	 * @param void
	 * @return все допустимые строки заказа на модель прибора
	 * */
	private function combMerge($useBestWay = true){
		//
		$arrSource = array();

		foreach ($this->depends as $dep){
			
			if(count($dep->generateString($useBestWay)) > 0){
				$arrSource[$dep->getDependParam()->getModelParamId()] = $dep->generateString($useBestWay);
			}
		}
		
		sort($arrSource); //предварительно массив отсортировать
		$result = $this->mergeCombinationsFromArrSource($arrSource); 
		
		return $result;
	}
	
	/**
	 * 
	 * */
	private function mergeCombinationsFromArrSource($aSourceCombinationsArray){
		
		$mergedCombinations = array();
		
		if(is_array($aSourceCombinationsArray) && count($aSourceCombinationsArray) > 0){
		
			$mergedCombinations = array_pop($aSourceCombinationsArray);//инициализировать $result первым элементом $arrays
			$keys = array_keys($aSourceCombinationsArray);//получить все ключи исходного массива
				
			foreach ($keys as $k){
				$mergedCombinations = $this->mergeCombinationArr($mergedCombinations, $aSourceCombinationsArray[$k]);
			}
		}
		
		return $mergedCombinations;
	}
	
	
	/**
	 * @access public
	 * @todo метод проверяет, характеризуется ли подбираемая модель прибора параметром с заданным ID
	 * @param $param_id - ID параметра, проверяемого на наличие в множестве характеристик модели
	 * @return количество позиций параметризации прибора, соответствующихзаданному ID параметра (0 или 1)
	 * */
	public function hasParam($param_id){
		//p("вызов SelectionVariant->hasParam");
		
		$sql = 'SELECT count(mp.`id`) as cnt
				from `modelparametrization` mp
				where
				`mp`.`param_id` = '.$param_id.'
				and
				mp.`model_id` = '.$this->model_id;	
		
		//d($sql);
		$rs = Db::getInstance()->executeS($sql);
		
		return $rs[0]['cnt'];
	}	
		
	/**
	 * @access public
	 * @todo метод генерирует массив строк заказа для всех возможных исполнений
	 * в результате работы алгоритма каждый символ, определяемый значением зависимого или автономного парамтера,
	 * должен оказаться в массиве, из которого формируется строка, на своём месте (на своей позиции в карте заказа) 
	 * @param void
	 * @return массив всех строк заказа для данной модели, удовлетворяющих пользовательским значениям параметров
	 * */
	public function getOrderStrings($useBestWay = true){

		$arrDependCombs = $this->combMerge($useBestWay);
		
		sort($arrDependCombs); // предварительно массив отсортировать
		$resultArr = array();
		$arrAutonomicCombs = $this->mergeAutonomicParamCombinations();
		sort($arrAutonomicCombs);
		
		//d($arrDependCombs);
		//p($arrAutonomicCombs);
		
		$keysAutonomicCombs = array_keys($arrAutonomicCombs);
		$keysDependCombs = array_keys($arrDependCombs);
		$totalCombination = array();
		
		foreach ($keysAutonomicCombs as $kAutonomicComb){//все автономные комбинации
		
			$keysAutoComb = array_keys($arrAutonomicCombs[$kAutonomicComb]);
		
			foreach ($keysDependCombs as $kDependComb){//сливаются со всеми зависимыми
				/**
				 * ключи (позиции в строке заказа ) у зависимых  и независимых комбинаций не пересекаются,
				 * следовательно и конфликтовать их значения не могут - не нужна дополнительная проверка значений
				 * на совметсимость
				 **/
				foreach ($keysAutoComb as $key){
					//в качестве результирующего массива может быть любой - в данном случае выбран
					//массив зависимых комбинаций $arrDependCombs
					if(!array_key_exists($key, $arrDependCombs[$kDependComb])){
						$arrDependCombs[$kDependComb][$key] = $arrAutonomicCombs[$kAutonomicComb][$key];
					}
					else{
		
					}
				}
				ksort($arrDependCombs[$kDependComb]);
				$resultArr[] = $arrDependCombs[$kDependComb];
				//p($resultArr);
			}
		}
		
		$result = $this->produceOrderString($resultArr);
		$this->resultOrderStringsArr = $resultArr;
		
		//d($result);
		
		return $result;	
	}
	
	/**
	 * метод генерирует все строки заказа для конктретного варианта отображения 
	 * (bestWayOnly <- $useBestWay === true или all <- $useBestWay === false)
	 * */
	private function getOrderStringsForViewWay($useBestWay = true){

	}
	
	/**
	 * @todo Метод генерирует строку заказа из массива символов заказа, переданного ему в качестве параметра
	 * @param $resultArr - массив символов заказа.
	 * */
	private function produceOrderString($resultArr = null){
		
		if($resultArr == null){
			$resultArr = $this->resultOrderStringsArr;
		}
		
		$result = array();
		
		foreach ($resultArr as $rendering){
			
			ksort($rendering);
			$resultStr = $this->getModelName();
			
			foreach($rendering as $codeVal){
				$resultStr .= ' - '.$codeVal;
			}
			
			$result[] = $resultStr;
		}
		return $result;
	}
	
	/**
	 * @access private
	 * @todo метод генерирует и добавляет в массив $signArr все возможные коды автономных параметров 
	 * @param unknown $signArr - массив вида [позиция в строке заказа] => знак
	 * @return $signArr - массив вида [позиция в строке заказа] => знак
	 * */
	private function getAllSignsForIndepended(){
		//p("вызов SelectionVariant->getAllSignsForIndepended");
		
		$signArr = array();
		$orderStr = $this->getOrderStringFormatModel();
		//d($orderStr);
		foreach($orderStr as $pos=>&$value){
			
			$parId = $value['param_id'];
			$pos++;
			/* если позиция уже определена при разборе зависимостей */
			$defined = false;
			
			foreach($this->depends as $dep){
				
				if($dep->isDependOfParam($value['modelparam_id'])){
					$defined = true;
					break;				
				}				
			}
			
			if ($defined == true) continue;			
			/* если значение параметра введено пользователем */
			if(array_key_exists($parId, $this->autonomicParams)){		
				
				if($this->autonomicParams[$parId]['value']){					
					$signArr[$pos][] = $this->autonomicParams[$parId]['value']['codeval'];
				} else{
					
					//$avaibleValues = ;//new - используя обращение к автономному пакарметру, как к объекту
					//p($avaibleValues);
					$avaibleValues = $this->autonomicParams[$parId]->getAllValueForModelParam();//old
					//p($avaibleValues);
					
					if(count($avaibleValues)){
						
						$signArr[$pos]=array();
						
						foreach($avaibleValues as $o_val){
							$signArr[$pos][]=$o_val['codeval'];
						}
					}
				}
			}
			/* если значение параметра неизвестно получим все возможные значения для него */
		}		
	}
	
	/**
	 * @access private
	 * @todo метод отвечает за выборку параметров, ещё незапрошенных на предыдущей итерации
	 * в алгоритме выбора параметров, запрашиваемых на новой итерации, необходимо предусмотреть учёт
	 * $bestWay - отвечает за выборку параметров для определения единственности исполнения (рассматриваются только наиболее подходящие значения)
	 * @param $params - по умолчанию = null
	 * @return $needPar - массив параметров, которые должны быть запрошены на следующем шаге для продолжения процесса подбора
	 * */
	public function checkForNeedParams($params = null, $minReturnValues = 2) {
		
		$dependParams = array();
		$needParams = array();		
		$definedParams = array();
		
		foreach($this->depends as $objDepend){//для всех зависимостей
			
			$dependParams[] = $objDepend->getDependParam()->getParamId();
			$diffs = $objDepend->getNeedParams();
			
			//p($diffs);
			//$diffs = {countDiffs, paramsVars={paramId => {ValueIds}}}
			foreach ($diffs['paramsVars'] as $paramId=>$values){			
				
				$dependParams[] = $paramId;
				
				if(($diffs['countDiffs'][$paramId] >= $minReturnValues) && (!Parameter::getUserParamById($paramId))){
					
					foreach($values as &$objValue){						
						
						$paramOfValue = $objDepend->findParamByParamId($objValue->getParamId());
						//p($paramOfValue);
						if(($objValue->getCode() == "null") && ($paramOfValue->getCodeOrder() != "")){
							$isCodeValNull = true;
						}
					}
					
					if($isCodeValNull){
						continue;
					}
					
					$paramOfValue->values = (is_array($values))? $values : null;
					$needParams[$paramId] = $paramOfValue;
				}
			}
		}
		$orderString = $this->getOrderStringFormatModel();
		// чтобы строка закакза была сформирована полностью
		foreach($orderString as $orderPos){
			
			$paramId = $orderPos['param_id'];
			
			// проверим присутствует параметр строки заказа в зависимостях
			if(in_array($paramId, $dependParams)){
				continue;
			}			
			// проверим присутствует ли значение параметра строки заказа в автономных параметрах
			elseif((array_key_exists($paramId, $this->autonomicParams) 
					and	($this->autonomicParams[$paramId]->values != null) 
							or (is_object($this->autonomicParams[$paramId]) 
									and $this->autonomicParams[$paramId]->getParamWeight() <= 0))){
				continue;
			}
			// если значения для параметра нет добавим его для запроса
			$param = Parameter::createParamObj(new Parameter($paramId));
			$param->setModelParam($orderPos['modelparam_id']);
			$param->setCodeOrder($orderPos['codeorder']);
			$param->values = $param->getAllValueForModelParam();
			$needParams[$paramId] = $param;
		}
		
		/**
		 *из списка параметров, которые надо запрашивать, исключаем те, которые имеют неположительный вес. 
		 *они не должны запрашиваться никогда. 
		**/ 
		$needParamsKeys = array_keys($needParams);		
		$str_IN = '0';
		
		foreach ($needParamsKeys as $key){
			$str_IN = $str_IN.', '.$key;
		}
		
		$sql = 'select distinct mp.`param_id`
				from modelparametrization mp
				where
				mp.`weight` > 0
				and
				mp.model_id = '.$this->model_id.'
				and
				mp.`param_id` in ('.$str_IN.')';
		
		$result = Db::getInstance()->executeS($sql);
		$needPar = array();
		
		foreach ($result as $res){
			if(in_array($res['param_id'], $needParamsKeys)){
				$needPar[$res['param_id']] = $needParams[$res['param_id']];
			}
		}
		//d($needPar);
		return $needPar;
	}
	

	/**
	 * 
	 * @access private
	 * @todo метод получает формат строки заказа. будет полезен на конечном этапе подбора - для формирования строки карты заказа.
	 * @param void
	 * @return $result - формат строки заказа
	 * */
		////p("вызов SelectionVariant->getOrderStringFormatModel");
	private function getOrderStringFormatModel(){
		
		$sql = 'select
				`modelparametrization`.`id` as modelparam_id,
				`modelparametrization`.`param_id` as param_id,
			    `modelparametrization`.`codeorder` as codeorder,
			    `modelparametrization`.`independent` as independent
			from
				`modelparametrization`
			where
				`modelparametrization`.`model_id` = '.$this->model_id.'
			    and
			    `modelparametrization`.`codeorder` is not null
			order by codeorder ASC';		
		$result = Db::getInstance()->executeS($sql);
		
		return $result;
	}
	

	/**
	 * @access private
	 * @todo метод выбирает все значения указанного строкового параметра модели
	 * @param $model_param - ID параметризации модели
	 * @return $result - результат выоплнения SQL - запроса
	 */
	private function getAllValuesForString($model_param){
		
		$sql = '
				select
			    `stringparams`.`value_id` as value_id, 
			    `predefinedvalues`.`value` as value,
				`predefinedvalues`.`description` as descr,
			    `stringparams`.`codeval` as codeval
			from
				`modelparametrization`, `stringparams`, `predefinedvalues`
			where
			    `modelparametrization`.`id` = '.$model_param.'
				and
				`stringparams`.`modelparametrization_id` = `modelparametrization`.`id`
				and 
			    `stringparams`.`value_id` = `predefinedvalues`.`id`';

		$result = Db::getInstance()->executeS($sql);

		if($this->model_id == 15){
			d($result);
		}
		
		return $result;
	}
	
	/**
	 * @access private
	 * @todo метод выбирает все значения указанного параметра с плавающей точкой модели
	 * @param $model_param - ID параметризации модели
	 * @return $result - результат выоплнения SQL - запроса
	 * */
	private function getAllValuesForFloat($model_param){
		
		$sql = '
				select
				`modelparametrization`.`id` as modelparamid, 
			    `floatparams`.`id` as value_id, 
			    `floatparams`.`minval` as minval, 
			    `floatparams`.`maxval` as maxval, 
			    `floatparams`.`codeval` as codeval
			from
				`modelparametrization`, `floatparams`
			where
			    `modelparametrization`.`id` = '.$model_param.'
				and
				`floatparams`.`modelparametrization_id` = `modelparametrization`.`id`';		
		
		$result = Db::getInstance()->executeS($sql);
		
		return $result;
	}
		
	/**
	 * @access private
	 * @todo метод выбирает все значения указанного целочисленного параметра модели
	 * @param $model_param - ID параметризации модели
	 * @return $result - результат выоплнения SQL - запроса
	 * */
	private function getAllValuesForInt($model_param){
		////p("вызов SelectionVariant->getAllValuesForInt");
		
		$sql = '
				select
				`modelparametrization`.`id` as modelparamid, 
			    `intparams`.`id` as value_id, 
			    `intparams`.`value` as nomval, 
			    `intparams`.`minval` as minval, 
			    `intparams`.`maxval` as maxval, 
			    `intparams`.`codeval` as codeval
			from
				`modelparametrization`, `intparams`
			where
			    `modelparametrization`.`id` = '.$model_param.'
				and
				`intparams`.`modelparametrization_id` = `modelparametrization`.`id`';
		
		$result = Db::getInstance()->executeS($sql);
		
		return $result;
	}
	
	/**
	 * 
	 * @todo из всех зависимостей в варианте подбора удалить из списка параметров те, 
	 * которые передаются в $paramIdsForDeletion
	 * @param $paramIdsForDeletion - массив Id параметров для удаления из списка проверенных (проверенные - 
	 * те, по которым хотя бы одна зависимость проанализирована)
	 * */
	public function deleteFromAnalyzedDepends($paramIdsForDeletion){
		//p($paramIdsForDeletion);
		foreach ($paramIdsForDeletion as $oneParamId){
			// заменить на ввбор парамметров из Depend
			//$modelParamId = Param::getModelParamIdByModelIdParamId($this->model_id, $oneParamId);
			
			foreach ($this->unmatchParams as $key => $unmatchParam){
				if($unmatchParam == $oneParamId){
					unset($this->unmatchParams[$key]);
				}
			}			
			
			foreach ($this->depends as $oneDepend){
				
				$modelParamId = $oneDepend->findParamByParamId($oneParamId);
				
				if($modelParamId != false){
					$oneDepend->deleteFromAnalyzed($modelParamId);
				}
			}
		}
	}

}