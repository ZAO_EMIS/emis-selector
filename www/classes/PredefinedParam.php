<?php
class PredefinedParam extends Parameter{
	
	private static $path = "preddef.php";
	/**
	 * 
	 * конструктор 
	 */
	public function __construct($aParamObj){
		$this->initPredefinedByExempair($aParamObj);
	}
	
	/**
	 * метод выполняет инициализацию экземпляра простым окпированием значений свойств экзенмпляра класса
	 * */
	 private function initPredefinedByExempair($aParamObj){
	 	
	 	$commonParamInfo = $aParamObj->getCommonParamInfo();
	 	$this->setCommonParamInfo($commonParamInfo);
	 }
	 
	/**
	* @todo метод инициализирует предопределённый параметр для запроса значения, предоставляя 
	* множество допустимых значений для предопределёного параметра, или все возможные, если аргумент $aModelParamId неизвестен, 
	*	или только для определённой модели, если аргумент $aModelParamId задан. 
	* @param	$aModelParamId = null
	*/
	public function initForInquire(){
		
		parent::initForInquire();
	 	
	 	if($this->modelParamId > 0){
	 		$result = $this->getPreddefinedValuesForModel($this->modelParamId); /* модель известна (выбрана) */
	 	} else{
	 		$result = $this->getAllPredefVals(); /* модель неизвестна (вернуть все значения независимо от моделей прибора) */
	 	}
	 	
	 	$this->values = $result;
	}
	
	/**
	 *
	 **/
	//public function extendedInit($aModelId){
		//parent::extendedInit($aModelId);
	//}
	
	/**
	 задача метода - предоставить все возможные значения для параметра с предопределёнными значениями для 
	 инициализации запроса (безотносительно к модели прибора - modelParamId неизвестен, но известен paramId)
	 */
	private function getAllPredefVals(){

		$predDefValues_sql = 'select `predefinedvalues`.`value` as value, `predefinedvalues`.`id` as value_id,
										`predefinedvalues`.`description` as descr
									   from (`parameters` join `predefinedvalues`)
									   WHERE
									   `predefinedvalues`.`param_id` = `parameters`.`id`
										and
										`parameters`.`id` = ' . $this->getParamId();
		
		$predDefValues_result = Db::getInstance()->executeS ( $predDefValues_sql );
		
		return $predDefValues_result;
	}
	
	/**
	 * задача метода - предоставить все возможные значения для параметра с предопределёнными значениями для 
	 инициализации запроса для указанной модели прибора (modelParamId - известен)
	 * */
	
	private function getPreddefinedValuesForModel($aModelParamId){
		
		$predDefValues_sql = 'select `predefinedvalues`.`value` as value, `predefinedvalues`.`id` as value_id,
							`predefinedvalues`.`description` as descr
							from ((`modelparametrization` join `stringparams`) join `predefinedvalues`)
							WHERE
							`modelparametrization`.`id` = `stringparams`.`modelparametrization_id`
							and
							`predefinedvalues`.`id` = `stringparams`.`value_id`
							and
							`modelparametrization`.`id` = '. $aModelParamId;
		
		$predDefValues_result = Db::getInstance()->executeS ( $predDefValues_sql );
		
		return $predDefValues_result;
	} 
	
	/**
	 сравнение параметра с предопределёнными значениями, со значением, указанным пользователем
	 */
	public function compareParamWithUserVal($anEthalonPreddefValue){
		
		//$ethalValId = $anEthalonPreddefValue->getValue();		
		$userValue = Parameter::getUserParamById($this->getParamId());
		//$userValId = $userValue['min'];
		
		return $this->compareValues($anEthalonPreddefValue, $userValue);
	}
	
	/**
	 * 
	 * */
	public function compareValues($aUserValue, $aEthalValue){
		
		if(is_object($aUserValue)){
			$aUserValue = $aUserValue->getValue();
		}
		
		//$aUserValue = $aUserValue['min'];
		
		if(is_object($aEthalValue)){
			$aEthalValue = $aEthalValue->getValue();
		}
		
		//$aEthalValue = $aEthalValue['min'];
		
		if(($aUserValue != null) and ($aEthalValue != null)){
			return ($aUserValue['min'] == $aEthalValue['min']);
		} else{
			return true;
		}
	}
	
	/**
	 *@todo генерирует HTML-код для запроса параметра
	 * @see Parameter
	 * */
	public function getHTML(){
		
		if($this->checkForNeedInquire()){
			
			parent::getHTML();
			
			include(__ES__ROOT_DIR . '\\views\\paramView\\' . self::$path);
			
			//parent::includeUserNotesView();
		}
	}
	

	/**
	 * 
	 * */
	public function getParamType(){
		return Parameter::_P_PREDDEF_VALUE;
	}
	
	public function getParamCompareType(){
		return Parameter::_P_EXACT_COMPARATION;
	}
	
	/**
	 * @see Parameter
	 * */
	public function filterObjValue($anEthalValue){
		return $this->compareValues($this->value, $anEthalValue);
	}

	/**
	 * @see Parameter
	 * */
	public function selectDefaultValueIdsFromDb(){
	
		$sql = 'select sp.`id` as value_id
				from `stringparams` sp
				where
				`sp`.`modelparametrization_id` = '.$this->modelParamId.'
				and
				sp.`priority` = (select MAX(sp.`priority`)
					from `stringparams` sp
					where `sp`.`modelparametrization_id` = '.$this->modelParamId.')';
		
		$resultSql = Db::getInstance()->executeS($sql);
		$result = array();
			
		foreach($resultSql as $one_val){
			$result[] = $one_val['value_id'];
		}
			
		return $result;
	}
	
	/**
	 * @see Parameter
	 * */
	public function getAllValueForModelParam(){
		
		$sql = '
				select
			    `stringparams`.`value_id` as value_id,
			    `predefinedvalues`.`value` as value,
				`predefinedvalues`.`description` as descr,
			    `stringparams`.`codeval` as codeval
			from
				`modelparametrization`, `stringparams`, `predefinedvalues`
			where
			    `modelparametrization`.`id` = '.$this->modelParamId.'
				and
				`stringparams`.`modelparametrization_id` = `modelparametrization`.`id`
				and
			    `stringparams`.`value_id` = `predefinedvalues`.`id`';
		
		$result = Db::getInstance()->executeS($sql);

		$arrValues = array();
		
		foreach ($result as $recValue){
				
			$objValue = Value::createInstance($this->paramId);
			$objValue->init($recValue['value_id'], $this->modelParamId);
			$arrValues[$this->modelParamId] = $objValue;
		}
		
		return $arrValues;
	}
	
	/**
	 * @access private
	 * @todo метод, определяя тип параметра, по типу параметра выбирает запрос, выполняемый к БД
	 * (в зависимости от типа параметра запрос осуществляется к соответствующей таблице БД)
	 * @param $paramId - ID параметра, по которому производится выбор модели из множества, задаваемого параметром $models
	 * @param $value - наименьшее значение параметра
	 * @param $valueMax - наибольшее значение параметра
	 * @param $models - множество (массив) моделей, в котором ведётся поиск; структура элемента этого массива
	 * (
	 [modelparam_id] - ID характеристики (параметризации модели), по которому модель прошла проверку
	 [param_id] - ID параметра (параметризации модели), по которому модель прошла проверку
	 [codeorder] - код к строке заказа модели, соответствующий этому параметру
	 [model_id] - ID модели
	 [modelname] - наименование модели
	 )
	 * @return $sql - sql - запрос (все значения параметров в запросе уже проставлены, его нужно просто выполнитьЫ)
	 * */
	public function getSearchQuery($paramId, $value, $valueMax, $modelsTerm = null){

		$sql = 'select
			`vmp`.`mpid` as modelparam_id,
			`vmp`.`paramID` as param_id,
			ifnull(`vmp`.`codeorder`, 0) as codeorder,
            vmp.`modelID` as model_id,
            vmp.`modelname` as modelname
		from
			`vmodelparametrization` vmp, `stringparams` sp
		where
			`vmp`.`devtype` = 11
            '.$modelsTerm.'
			and
			`vmp`.`paramID` =  '.$paramId.'
			and
					vmp.`cmptype` = 0
						and `sp`.`modelparametrization_id` = `vmp`.`mpid`
						and '.$value.' = `sp`.`value_id`
		group by
			vmp.`modelID`';
		
		return $sql;
	}
	
	/**
	 * @see Parameter
	 * */
	public function getDefaultValue(){
		
		$sql = 'select sp.`id` as defaultval_id
				from
					`stringparams` sp, `vmodelparametrization` vmp
				where
					vmp.`paramtype_id` = '.Parameter::_P_PREDDEF_VALUE.'
					and
					vmp.`mpid` = sp.`modelparametrization_id`
					and
					vmp.`mpid` = '.$this->modelParamId.'
					and
					sp.`priority` = (select MAX(_sp.priority)
									from `stringparams` _sp, `vmodelparametrization` _vmp
                    				where _sp.`modelparametrization_id` = `_vmp`.`mpid`
                    				and `_vmp`.`mpid` = '.$this->modelParamId.')';
		
		$result = Db::getInstance()->executeS($sql);
		
		$defaultValue = Value::createInstance($this->paramId, $this->paramType);
		$defaultValue->init($result[0]['defaultval_id'], $this->modelParamId);
		
		return $defaultValue;
	}
		
	/**
	 * @see Parameter
	 * */
	public function searchSatisfyValues($aUserValue){

		$sql = 'select
				`stringparams`.`id` as value_id,
			    `modelparametrization`.`id` as modelparam_id,
			    ifnull(`modelparametrization`.`codeorder`, 0) as codeorder,
			    ifnull(`stringparams`.`codeval`, "-")  as codeval
			    from
			    `parameters`, `modelparametrization`, `stringparams`
			    where(
			    `stringparams`.`value_id` = '.$aUserValue['min'].'
			    and
			    `modelparametrization`.`id` = '.$this->modelParamId.'
			    and
			    `parameters`.`id` = `modelparametrization`.`param_id`
			    and
			    `modelparametrization`.`id` = `stringparams`.`modelparametrization_id`
			    );';
		//d();
		$arrValuesInfo = Db::getInstance()->executeS($sql);

		$objValues = array();
		
		foreach ($arrValuesInfo as $oneValue){

			$oneObjValue = Value::createInstance($this->paramId, $this->paramType);
			$oneObjValue->init($oneValue['value_id'], $this->modelParamId);
			$objValues[] = $oneObjValue;
		}
		
		return $objValues;
	}
	
	/**
	 * 
	 * */
	public function correctToStandartVariant($aUserValue){
		return $aUserValue;
	}
}
?>