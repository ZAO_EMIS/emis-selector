<?php
/**
 * @todo Клас отвечает за представление различных типов учёта расхода, и все операции с ними.
 * */
class FlowParam extends FloatParam{
	 
	const __MASS_FLOW_TYPE__ = 1;/**	 тип задания массовый расход	 */
	const __VOLUME_FLOW_TYPE__ = 0;/**	 тип задания объёмный расход	 */
	
	const __FLOW_ID__ = 98;
	const __FLOWTYPE_ID__ = 142; /** ID типа измерения расхода	*/
	const __DENSITY_ID__ = 114;/**	 константа, определяющая плотность	 */
	
	private static $pathFlowType = "flowType.php";
	//private $flowType = self::__VOLUME_FLOW_TYPE__;
	/**
	 * @see Parameter
	 * */
	public function getHTML(){
		
		if(!$this->checkForNeedInquire()){

			$flowVolumeParam = new FloatParam(new Parameter(self::__FLOW_ID__));
			$flowVolumeParam->initForInquire();
			$flowVolumeParam->setCommonParamInfo(Parameter::paramInfoFromDb(self::__FLOW_ID__));
			
			$densityParam = new FloatParam(new Parameter(self::__DENSITY_ID__));
			$densityParam->initForInquire();
			$densityParam->setCommonParamInfo(Parameter::paramInfoFromDb(self::__DENSITY_ID__));
			
			include(__ES__ROOT_DIR . '\\views\\paramView\\' . self::$pathFlowType);
		}
	}
	
	/**
	 * @see Parameter
	 * */
	protected function checkForNeedInquire(){
		
		$inquiredParamsIds = ObjectModel::getFromSessionByName('inquiredParamsIds');
		
		if(in_array(self::__FLOW_ID__, array_keys($inquiredParamsIds))){
			return true;
		} else{
			return false;
		}
	} 
	
	/**
	 *@see Parameter
	 * */
	public function getSearchQuery($paramId, $value, $valueMax, $modelsTerm = null){
		
		if($paramId != self::__DENSITY_ID__){
			$sql = parent::getSearchQuery($paramId, $value, $valueMax, $modelsTerm);
		} else{
			$sql = false;
		}
		//d($sql);
		return $sql;
	}
}
?>