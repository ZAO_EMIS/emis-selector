<?php
/**
 * Абстрактный класс, представляющий все операции со значениями параметров.
 */
abstract class Value{
	
	protected $valueId;
	protected $code;
	protected $paramId;
	protected $priority;
	protected $modelParamId;
	private $inited = false;
	
	/**
	 * конструктор без параметров
	 * */
	public function __construct(){}
	
	/**
	 * 
	 * */
	public function getCode(){
		return $this->code;
	}
	
	/**
	 * @todo Инициализация полей значения.
	 * @param $aValueId - идентификатор значения 
	 * @param $aModelParamId - параметризация модели, для которой создано значение
	 * */
	public function init($aValueId, $aModelParamId){
		
		if($aValueId != null){
			$this->valueId = $aValueId;
		}
		
		if($aModelParamId != null){
			$this->modelParamId = $aModelParamId;
		}
		
		if(($this->inited == false) and ($this->valueId > 0)){
			
			$this->initValueFromDb();
			$this->inited = true;
		}
	}
	
	/**
	 * @todo Инициализирует экземпляр класса информацией, полученной из БД. Реализуется только в классах потомках. 
	 * Для каждого типа значений информация, необходимая для инициализации, выбирается из различных таблиц в БД.
	 * */
	protected abstract function initValueFromDb();
	
	/**
	 * @todo Фабричный метод. Поскольку к моменту создания экземпляра тип параметра уже известен, то его логично и передать 
	 * конструктору для того, чтобы определить, какого именно потомка создать FloatValue или PreddefinedValue 
	 */
	public static function createInstance($aParamId, $aParamType = null){
		
		if($aParamType == null){
			$aParamInfo = Parameter::paramInfoFromDb($aParamId);
			$paramType = $aParamInfo['paramtype'];
		} else{
			$paramType = $aParamType;
		}
		
		if(($paramType == Parameter::_P_FLOAT_VALUE_TYPE) 
				or ($paramType == Parameter::_P_LOGIC_VALUE) 
				or($paramType == Parameter::_P_FLOW_VALUE_TYPE)
				or($paramType == Parameter::_P_VISCOSITY_VALUE)
				or($paramType == Parameter::_P_ACCURACY_VALUE)){
			 $valueInstance = new FloatValue($aParamId);
		} else if($paramType == Parameter::_P_PREDDEF_VALUE){
			$valueInstance = new PreddefinedValue($aParamId);
		} else{
			$valueInstance = false; // экземпляр неподходящего типа созадть нельзя!!!
		}
		
		return $valueInstance;
	}
	
	public function getModelParamId(){
		return $this->modelParamId;
	}
	 
	/**
	 * @todo Предоставляет общую информацию об экземпляре класса.
	 * */
	public function getInfo(){
		
		$result['id'] = $this->id;
		$result['code'] = $this->code;
		$result['paramId'] = $this->paramId;
		$result['priority'] = $this->priority;
		$result['modelParamId'] = $this->modelParamId;
		
		return $result;
	}
	
	public function getValueId(){
		return $this->valueId;		
	}
	
	public function getParamId(){
		return $this->paramId;
	}
	
	/**
	 * @todo Предоставляет информацию о значении в виде массива val('min'=>minValue, 'max'=>maxValue)
	 */
	public abstract function getValue();
	 
	public function getPriority(){
		return $this->priority;
	}
}

?>