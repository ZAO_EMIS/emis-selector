<?php
/**
 * @filesource Parameter.php
 * @author Velikiy
 * @todo
 * @propertyconst MAIN_DEVICE_TYPE = 11;
 * @propertyconst MAX_WEIGHT = 10;
 * @propertyconst MIN_WEIGHT_FOR_COMMON_PARAMS = 5;
 * @propertyconst _P_PREDDEF_VALUE = 7;
 * @propertyconst _P_FLOAT_VALUE_TYPE = 6;
 * @propertyconst _P_FLOW_VALUE_TYPE = 5;
 * @propertyconst _P_LOGIC_VALUE = 8;
 * @propertyconst _P_ACCURACY_VALUE = 9;
 * @propertyconst _P_VISCOSITY_VALUE = 10;
 * @propertyconst _P_OR_TYPE_COMPARATION = 1;
 * @propertyconst _P_EXACT_COMPARATION = 0;
 * @propertyconst _P_XOR_TYPE_COMPARATION = 2;
 * @propertyprotected $paramId = null;
 * @propertyprotected $caption = null;
 * @propertyprotected  $paramType = null;
 * @propertypublic $groupId = null;
 * @propertyprotected $groupName = 0;
 * @propertyprotected $groupCount = 0;
 * @propertyprotected $weight = null;
 * @propertyprotected $required = null;
 * @propertypublic $value = null;
 * @propertyprotected $modelParamId;
 * @propertyprotected $codeOrder = null;
 * @propertypublic $doCheckForInquiring = true; 
 */
class Parameter extends ObjectModel{
	//константа, определяющая основной тип подбираемого оборудования
	const MAIN_DEVICE_TYPE = 11;
	
	//константы, определяющие характерные веса параметров
	const MAX_WEIGHT = 10;
	const MIN_WEIGHT_FOR_COMMON_PARAMS = 5;
	// константы, определяющие тип параметров
	const _P_PREDDEF_VALUE = 7;
	const _P_FLOAT_VALUE_TYPE = 6;
	const _P_FLOW_VALUE_TYPE = 5;
	const _P_LOGIC_VALUE = 8;
	const _P_ACCURACY_VALUE = 9;
	const _P_VISCOSITY_VALUE = 10;
	// константы, определяющие способ сравнения параметров
	const _P_OR_TYPE_COMPARATION = 1; // прямой _P_RANGE_COMPARATION тип сравнения (значения 
									//пользовательского диапазона должны попадать в определённый диапазон)
	const _P_EXACT_COMPARATION = 0; // точное совпедение (значение должно точно совпадать с заданным)
	const _P_XOR_TYPE_COMPARATION = 2; // обратный _P_NON_EXCEED_COMPARATION тип сравнения (значение не должно превышать 
										//заданного)
	const _PATH_TO_PARAM_VIEWS_ = '\\views\\paramView\\';
	private static $pathUserNotes = 'userNotesView.php';
	/**
	 идентификатор параметра; основная характеристика, на её основе создаётся
	 экземпляр
	 */
	protected $paramId = null;
	/**
	 имя параметра
	 важно для представления параметра при запросе пользовательского значения
	*/
	protected $caption = null;
	/**
	 тип параметра
	 при запросе пользовательского значения определяет содержание запроса (список
	 выбора, ввод минимального и максимального значений, флажок, ...)
	*/
	protected  $paramType = null;
	public $groupId = null;
	protected $groupName = 0;
	protected $groupCount = 0;
	protected $weight = null;
	protected $required = null;
	public $value = null;
	protected $modelParamId;
	protected $codeOrder = null;
	public $doCheckForInquiring = true;
	protected $calcTemplate = false;

	/**
	 * */
	public function getCalcTemplate(){
		return $this->calcTemplate;
	}
	
	/**
	 * */
	public function setCalcTemplate($aCalcTemplate){
	
		if($aCalcTemplate != null){
			$this->calcTemplate = $aCalcTemplate;
		}
	}
	
	public function setWeight($aWeight){
		$this->weight = $aWeight;
	}
	
	/**
	 конструктор;
	  
	 назначение:
	 создаёт экземпляр класса, используя в качестве исходной информации ID параметра;
	  
	 реализация:
	 реализуется в классе Param, и в классах - наследниках не перекрывается: метод
	 всюду реализует одну и ту же работу
	  
	 полиморфизм (правильная инициализация наследников) достигается тем, что для
	 своей работы конструктор использует метод init(), который является абстрактным,
	 и реализуется только в классах - наследниках (см описание init())
	 @param aParamID
	 @roseuid 552661ED00A4
	 */
	public function __construct($aParamId, $aParamTypeId = null){
		
		$this->paramId = $aParamId;
		$this->paramType = $aParamTypeId;
	}
	
	public static function createParamObj($aParamObj){
		
		if(!($aParamObj->paramType > 0)){
			
			$paramInfo = self::paramInfoFromDb($aParamObj->paramId);
			$aParamObj->paramType = $paramInfo['paramtype'];
		}
		
		if(($aParamObj->paramType == self::_P_FLOAT_VALUE_TYPE) or ($aParamObj->paramType == self::_P_LOGIC_VALUE)){
			// логические параметры, или числовые параметры, задаваемые только в одной форме
			$instance = new FloatParam($aParamObj, $paramTypeId);
			$instance->setIsRange($paramInfo['isRange']);
			$instance->setMeasUnitId(ObjectModel::getFromSessionByName("params")[$aParamObj->getParamId()]["measUnitId"]);
		} elseif($aParamObj->paramType == self::_P_PREDDEF_VALUE){
			// параметры с предопределёнными значениями
			$instance = new PredefinedParam($aParamObj);			
		} elseif ($aParamObj->paramType == self::_P_FLOW_VALUE_TYPE){
			// параметры, задающие расход
			$instance = new FlowParam($aParamObj);
			$instance->setIsRange($paramInfo['isRange']);
			$instance->setMeasUnitId(ObjectModel::getFromSessionByName("params")[$aParamObj->getParamId()]["measUnitId"]);
		} elseif ($aParamObj->paramType == self::_P_VISCOSITY_VALUE){
			// вязкость измеряемой среды (может быть задана как динамическая или кинематическая)
			$instance = new ViscosityParam($aParamObj);
			$instance->setIsRange($paramInfo['isRange']);
			$instance->setMeasUnitId(ObjectModel::getFromSessionByName("params")[$aParamObj->getParamId()]["measUnitId"]);
		} elseif ($aParamObj->paramType == self::_P_ACCURACY_VALUE){
			// допустимая погрешность измерения (может быть задана как абсолютная или относительная)
			$instance = new AccuracyParam($aParamObj);
			$instance->setIsRange($paramInfo['isRange']);
			$instance->setMeasUnitId(ObjectModel::getFromSessionByName("params")[$aParamObj->getParamId()]["measUnitId"]);
		}
		
		$instance->setCommonParamInfo($paramInfo);
		//p($instance);
		return $instance;
	}
	
	/**
	 * устанавливает ID параметризации модели
	 * */
	public function setModelParam($aModelParamId){
		$this->modelParamId = $aModelParamId;
	}
	
	/**
	 * устанавливает номер позиции, определяемой параметром в карте заказа
	 * */
	public function setCodeOrder($aCodeOrder){
		$this->codeOrder = $aCodeOrder;
	}
	
	/**
	 * возвращает номер позиции, определяемой параметром в карте заказа
	 * */
	public function getCodeOrder(){
		return $this->codeOrder;
	}
	
	/**
	 * @access: private
	 *
	 * @todo для указанного типа оборудования метод получает массив ID параметров (parameter_id), отображаемых на начальном экране.
	 * этот набор параметров анализируется на первой итерации (шаге) процесса подбора.
	 * для каждого ID параметра создаётся объект-параметр (экземпляр класса Param @see Param.php).
	 *
	 * @param $deviceType_id - ID типа оборудования (11 - расходомер). в дальнейшем система должна подбирать не только
	 * сам расходомер, но и сопутствующее (дополнительное) оборудование. данный параметр предусматривает именно эту возможность.
	 *
	 * @return $startParams_result - массив параметров, запрашиваемых на первом шаге процесса подбора
	 * формат: $startParams_result[i]['parameter_id'], где 'parameter_id' - ID параметра, i - порядковый номер
	 * элемента (параметра) в результате запроса
	 * */
	public static function getStartStepForDeviceType($deviceType_id) {
		
		$startParams = array();
		/* для первой итерации подбора берём только параметры с начального экрана */
		$startParams_sql = 'select parameter_id
				from `start_page_choose`
				where
				`start_page_choose`.`devicetype_id` = '. $deviceType_id;
	
		$db = Db::getInstance();
		$startParams_result = $db->executeS($startParams_sql);
		
		foreach ($startParams_result as $oneParamId){
			$startParams[$oneParamId['parameter_id']] = self::createParamObj(new Parameter($oneParamId['parameter_id']));
		}
		
		return $startParams;
	}
	
	/**
	 * @todo установка значений общих полей для всех классов Parameter
	 *
	 * для установки общих полей класса и для реализации копирования значений из экземпляра предка в экземпляр потомка
	 * обе операции выполняются единообразно
	 *
	 ***/
	 public function setCommonParamInfo($paramInfo){
		
		if($paramInfo['paramtype'] != null){
			$this->paramType = $paramInfo['paramtype'];
		}
	 	if($paramInfo['caption'] != null){
	 		$this->caption = $paramInfo['caption'];
		}
	 	if($paramInfo['paramGroup'] != null){
	 		$this->groupId = $paramInfo['paramGroup'];
		}
	 	if($paramInfo['required'] != null){
	 		$this->required = $paramInfo['required'];
		}
	 	if($paramInfo['modelParamId'] != null){
	 		$this->modelParamId = $paramInfo['modelParamId'];
		}
	 	if($paramInfo['paramId'] != null){
	 		$this->paramId = $paramInfo['paramId'];
		}
	 	if($paramInfo['values'] != null){
	 		
	 		if(is_object($this->values[0])){
	 			$this->values = $paramInfo['values'];
	 		}else{
	 			p("Parameter->setCommonParamInfo()");
	 			p(debug_backtrace());
	 		}
	 		
		}
	 	if($paramInfo['weight'] != null){
	 		$this->weight = $paramInfo['weight'];
		}
		
	}
	
	 
	 
	/**
	 * @todo Rопирует значения полей класса-предка в поля классов потомнков
	 *
	 ***/
	 public function getCommonParamInfo(){
		
		$paramInfo['caption'] = $this->caption;
		$paramInfo['paramGroup'] = $this->groupId;
		$paramInfo['modelParamId'] = $this->modelParamId;
		$paramInfo['paramId'] = $this->paramId;
		$paramInfo['paramtype'] = $this->paramType;
		$paramInfo['required'] = $this->required;
		$paramInfo['values'] = $this->values;
		$paramInfo['weight'] = $this->weight;
		
		return $paramInfo;
	}
	
	/**
	 * @todo
	 ***/
	 public static function paramInfoFromDb($aParamID){
		
		$sql = 'select param.`paramtype_id` as paramtype
				,param.`name` as caption
				,param.`parametergroups` as paramGroupId
				,param.`required` as required
				,param.`isRange` as isRange
				from parameters param where param.`id` = '.$aParamID;
		
		$result = Db::getInstance()->getRow($sql);
		
		return $result;
	}
	 
	/** 
	 * @todo Инициализирует экземпляр класса информацией, необходимой для сравнения с эталоном; 
	 * перекрывается во всех классах - потомках
	 * */
	public function initForCompare(){}
	
	/**
	 * @todo устанавливает тип парамтера, id, isRange, caption, measUnits, defaultMeasUnit - 
	 * всё, что необходимо для запроса парамтера; перекрывается во всех потомках
	 * 
	 * */
	public function initForInquire(){}
	
	/**
	 * @todo Получает из БД информацию о группе параметра.
	 * @return $result = array("name"=>$this->groupName, "id" => $this->groupId, "count" => $this->groupCount);
	 * */
	public function getGroupInfo(){
		
		$sql = 'select pg.`name` as name, pg.`id` as id from `parametergroups` pg where pg.`id` = ' .$this->groupId;
		$result = Db::getInstance()->getRow($sql);
		$this->groupName = $result['name'];
		$sqlcount = 'select count(*) as count from `parameters` p where p.`parametergroups` = '. $this->groupId;
		$resultcount = Db::getInstance()->getRow($sqlcount);
		$this->groupCount = $resultcount['count'];
		
		$result = array("name"=>$this->groupName, "id" => $this->groupId, "count" => $this->groupCount);
		//d($result);
		return $result;		
	}
	
	
	/**
	 * @todo инициализация дополнительных полей класса (тех, которые определяются не только параметром, но и моделью прибора)
	 *  $this->modelParamId = $extendedInitFields[0]['modelParamId'];
	 *	$this->weight = $extendedInitFields[0]['weight'];
	 *	$this->codeOrder = $extendedInitFields[0]['codeorder'];
	 * */
	public function extendedInit($aModelId){
		
		$sql = 'select
				modelparametrization.id as modelParamId,
				modelparametrization.weight as weight,
				modelparametrization.codeorder as codeorder
				from modelparametrization where modelparametrization.param_id = '.$this->paramId.
				' and modelparametrization.model_id = '.$aModelId;
		
		$extendedInitFields = Db::getInstance()->executeS($sql);
		
		$this->modelParamId = $extendedInitFields[0]['modelParamId'];
		$this->weight = $extendedInitFields[0]['weight'];
		$this->codeOrder = $extendedInitFields[0]['codeorder'];
	}
	
	/**
	 * возвращает пользовательское значение для параметра по его ID
	 ***/
	 public static function getUserParamById($id){
		
		$temp_params = ObjectModel::getFromSessionByName('params');
				
		foreach ( $temp_params as $paramId => $value ) {
				
			if ($id == $paramId) {
				return $value;
			}
		}
		
		return false;
	}
	
	/**
	 открытый, статический
	  
	 @todo получает из поста ($_POST) пользовательские значения параметра, и сохраняет их
	 в $_SESSION['params'] как запрошенные на текущем шаге процесса подбора
	  
	 реализация:
	 реализуется в классе Param;  метод всегда делает одну и ту же работу, которая к
	 особенностям структуры классов-потомков не имеет никакого отношения
	 @roseuid 552661ED017B
	*/
	public static function parseUserValuesFromPost($aSelectionId = 0, $step = null){
		//d($_POST);
		$params = array ();
		
		if (($step == null) or ($step < 0)) {
			$step = 0;
		}
		
		if(Dispatcher_Single::$post['ajax'] == 1){
			$paramsFromPost = Dispatcher_Single::$post['params'];
		} else{
			$paramsFromPost = Dispatcher_Single::$post;
		}
		
		if (! empty( $paramsFromPost )) {
			//
			foreach ( $paramsFromPost as $paramId => $value ) {
				/* префикс param - для значения параметра, префикс parNote - для замечаний пользователей относительно параметра */
				if (substr_count( $paramId, "param" ) && ($value !== null) && ($value !== '')) {
					
					$key = str_replace( "param", '', $paramId );
						
					if (substr_count( $key, '_min' )) {
		
						$id = str_replace( '_min', '', $key );
						$params[$id]['min'] = str_replace ( ',', '.', $value );
						$params[$id]['step'] = $step;
		
						if($params[$id]['max'] == null){
							$params[$id]['max'] = $params[$id]['min'];
						}
						
						if($paramsFromPost["mesunit".$id] != null){
							$params[$id]["measUnitId"] = $paramsFromPost["mesunit".$id];
						}
					} elseif (substr_count( $key, '_max' )) {
						
						$id = str_replace( '_max', '', $key );
						$params[$id]['max'] = str_replace( ',', '.', $value );
						$params[$id]['step'] = $step;
						
						if($params[$id]['min'] == null){
							$params[$id]['min'] = $params[$id]['max'];
						}
						
						if($params[$id]["measUnitId"] == null){
							
							if($paramsFromPost["mesunit".$id] != null){
								$params[$id]["measUnitId"] = $paramsFromPost["mesunit".$id];
							}
						}
					} else {
		
						$params[$key]['step'] = $step;
						$params[$key]['min'] = str_replace( ',', '.', $value );
						$params[$key]['max'] = str_replace( ',', '.', $value );
						
						if($params[$key]["measUnitId"] == null){
								
							if($paramsFromPost["mesunit".$key] != null){
								$params[$key]["measUnitId"] = $paramsFromPost["mesunit".$key];
							}
						}
					}
					
				}  elseif(substr_count($paramId, "parNote") && ($value != null) && ($value != '')){
					
					$key = str_replace( "parNote", '', $paramId );
					$notesForSave[$key] = $value;
				}
			}
			//d($params);
			$session_params = ObjectModel::getFromSessionByName('params');
				
			if ($session_params != null) {
				
				$paramsToSave = $params;
		
				foreach ( $session_params as $paramId => $value ) {
						
					if(array_key_exists($paramId, $params)){
						$paramsToSave[$paramId] = $params[$paramId];
					} elseif ((array_key_exists($paramId, $paramsFromPost)) &&
							(($paramsFromPost[$paramId] == '') || ($paramsFromPost[$paramId] == null))){
						continue;
					}
					else{
						$paramsToSave[$paramId] = $value;
					}
				}
			}
			else{
				$paramsToSave = $params;
			}
			
			ObjectModel::saveToSession('params', $paramsToSave);
			if ($aSelectionId > 0) {
				self::logUserParams($aSelectionId, $paramsToSave);
				self::logUserNotes($aSelectionId, $notesForSave);
			}

		} else {
			$params = null;
		}
		

		return $params;
	}
	
	/**
	 * 
	 * */
	public function correctToStandartVariant($aUserValue){}
	
	/**
	 * @todo Метод ведёт лог пользовательских значений параметров в БД.
	 * */
	private static function logUserParams($aSelectionId, $paramsForLogging){
		
		if(is_array($paramsForLogging)){
			
			foreach ($paramsForLogging as $key => $paramValue){
					
				if($paramValue['min'] == null){
					$paramMin = 'null';
				} else{
					$paramMin = $paramValue['min'];
				}
					
				if($paramValue['max'] == null){
					$paramMax = 'null';
				} else{
					$paramMax = $paramValue['max'];
				}
					
				if($paramValue['step'] == null){
					$paramStep = 'null';
				} else{
					$paramStep = $paramValue['step'];
				}
					
				$sql = 'insert into selectionprotocol(
					`selection_id`, `param_id`, `param_min_value`, `param_max_value`, `selection_step`)
					values('.$aSelectionId.', '.$key.', '.$paramMin.', '.$paramMax.', '.$paramStep.');';
					
				Db::getInstance()->executeS($sql);
			}
		}
	}
	
	/**
	 * метод ведёт лог замечаний пользователей в БД
	 * */
	private static function logUserNotes($aSelectionId, $notesForLogging){
		
		//$sql_commit = 'commit;';
		
		if(is_array($notesForLogging)){
				
			foreach ($notesForLogging as $key => $note){
					
				$sql = 'insert into userNotes(
					`selection_id`, `param_id`, `note`)
					values('.$aSelectionId.', "'.$key.'", "'.$note.'");';
				
				Db::getInstance()->executeS($sql);
				//Db::getInstance()->executeS($sql_commit);
			}
		}
	}
	
	/**
	 открытый, статический
	 @todo Метод удаляет из $_SESSION['params'] пользовательские значения тех параметров,
	 которые были запрошенны на шаге процесса подбора, указанном в качестве параметра
	 - формирует список параметров $forDeletionFromInquired, подлежащих удалению из списка запрошенных 
	 
	 реализация
	 реализуется в классе Param; метод никак не затрагивает особенностей структуры классов - потомков
	 */
	public static function deleteParamValuesByStep($step){
		
		$params = self::getParamUserValuesByStep();
		//d($params);
		$result = array();
		
		foreach ($params as $key => $par){
				
			if($par["step"] != $step){
				$result[$key] = $par;
			} else{
				$forDeletionFromInquired[] = $key;
			}
		}
		
		ObjectModel::saveToSession('params', $result);
		
		return $forDeletionFromInquired;
	}
	 
	/**
	 открытый, статический
	  
	 @todo Метод возвращает из $_SESSION['params'] пользовательские значения для параметров,
	 запрошенны на шаге процесса подбора, указанном в качестве параметра
	  
	 реализация
	 реализуется в классе Param; метод выполняет всегда одну и ту же работу,
	 безотносительно к особенностям структуры классов-потомков
	*/
	public static function getParamUserValuesByStep($cuurStep = NULL){
		
		if ($cuurStep == null) {
				
			$session_params = ObjectModel::getFromSessionByName('params'); // запрашиваем из сессии все параметры, сохранённые в ней
			//$_SESSION['params'] = $session_params;
			ObjectModel::saveToSession('params', $session_params);
		} else { // иначе
				
			$temp_params = ObjectModel::getFromSessionByName('params'); // читаем параметры из сесси во временную переменную
			//$_SESSION['params'] = $temp_params;
			ObjectModel::saveToSession('params', $temp_params);
			$temp_keys = array_keys ( $temp_params ); // получаем все ключи массива
				
			foreach ( $temp_keys as $key ) { // и выбираем из них в $session_params только те, у которых шаг = $currStep
		
				$sp_temp = $temp_params[$key];
		
				if ($sp_temp['step'] == $cuurStep) {
					$session_params[$key] = $sp_temp;
				}
			}
				
			//$_SESSION['params'] = $temp_params;
			ObjectModel::saveToSession('params', $temp_params);
		}
		//d($session_params);
		return $session_params;
	}
	 
	/**
	 открытый, конкретный
	 @todo возвращает имя параметра
	 реализация
	 как все не статические методы getXXX класса Param, возвращает информацию об
	 общих полях класса (которыми характеризуются все параметры), а потому в
	 классах-потомках его перекрывать не нужно
	*/ 
	public function getParamCaption(){
		return $this->caption;
	}
	
	public function getParamId(){
		return $this->paramId;
	}
	
	/**
	 @todo устанавливает значения полей экземпляра класса, общие для параметров всех типов, получив их значения из БД
	  
	 реализация
	 как все не статические методы setXXX класса Param, устанавливает информацию об
	 общих полях класса Param (которыми характеризуются все параметры), а потому в
	 классах-потомках его перекрывать не нужно
	 */
	private function setCommonParamInfoFromDb($id){
		
		$sql='select par.`name` as caption,
				par.`id` as parId,
				par.`paramtype_id` as paramTypeId,
				par.`parametergroups` as paramGroupId
			  from parameters par
			  where par.`id` = '.$id;
		
		$result = Db::getInstance()->getRow($sql);
		
		$this->caption = $result['caption'];
		$this->paramId = $result['parId'];
		$this->paramType = $result['paramTypeId'];
		$this->groupId = $result['paramGroupId'];
	}
	 
	/**
	 открытый, конкретный
	  
	 @todo возвращает тип параметра
	  
	 реализация
	 как все не статические методы getXXX класса Param, возвращает информацию об
	 общих полях класса (которыми характеризуются все параметры), а потому в
	 классах-потомках его перекрывать не нужно
	 @roseuid 552661EE0144
	*/ 
	public function getParamType(){
		return $this->param_type;
	}
	
	/**
	 открытый, конкретный
	  
	 @todo возвращает ID группы, в которой состоит параметр, по ID параметра, указанному в
	 качестве аргумента
	  
	 реализация
	 как все не статические методы getXXX класса Param, возвращает информацию об
	 общих полях класса (которыми характеризуются все параметры), а потому в
	 классах-потомках его перекрывать не нужно
	 @roseuid 552661EE02A5
	 */
	public function getParamGroupID(){
		return $this->groupId;
	}
	 
	 	 
	/**
	 открытый, статический
	  
	 @todo возвращает вес параметра, получая его из БД
	  
	 реализация
	 реализуется в классе Param
	 @roseuid 552661EF0386
	 */
	public function getParamWieght(){
		
		if(($this->weight == null) or (is_nan($this->weight) == true)){
			
			$sql = 'select weight from modelparametrization where id = '.$this->modelParamId;
			$result = Db::getInstance()->executeS($sql);
			$this->weight = $result[0]['weight'];
		}
		
		return $this->weight;
	}
	
	/**
	 * возвращает ID парамтеризации модели
	 * */
	public function getModelParamId(){
		return $this->modelParamId;
	}
	
	/**
	 * @todo Метод выполняет действия, общие для всех запрашиваемых параметров - получает информацию, 
	 * необходимую для того, чтобы сформировать интерфейс для запроса пользовательского значения.  
	 * В классах - потомках перекрывается.
	* */ 
	public function getHTML(){
		$this->initForInquire();
	}
	
	/**
	 * 
	 * */
	public function getPreviousUserValue(){
		
		$result = array();
		
		$result = ObjectModel::getFromSessionByName("_params")[$this->paramId];
		
		unset(ObjectModel::getFromSessionByName("_params")[$this->paramId]);
		
		if((($this->paramType == self::_P_LOGIC_VALUE) or ($this->paramType == self::_P_PREDDEF_VALUE)) and ($result["min"] == null)){
			$result["min"] = "'"."NOT_A_NUMBER"."'";
		}
		
		if(($this->paramType != self::_P_LOGIC_VALUE) and ($this->paramType != self::_P_PREDDEF_VALUE)){
			
			if($result["min"] == null){
				$result["min"] = "'"."NOT_A_NUMBER"."'";
			}
			
			if($result["max"] == null){
				$result["max"] = "'"."NOT_A_NUMBER"."'";
			}
		}
		
		if($result['measUnitId'] == null){
			$result["measUnitId"] = "'"."NOT_A_NUMBER"."'";
		}
		
		return $result;
	}
	
	/**
	 * @todo Метод выполняет действия, общие для всех запрашиваемых параметров - получает информацию, 
	 * необходимую для того, чтобы сформировать интерфейс для запроса пользовательского значения.  
	 * В классах - потомках перекрывается.
	* */ 
	protected function includeUserNotesView(){
		include(__ES__ROOT_DIR . '\\views\\paramView\\' . self::$pathUserNotes);
	}
	
	/**
	 * @todo Метод определяет, нужно ли запрашивать пользовательское значение данного параметра.
	 * */
	protected function checkForNeedInquire(){
		//p($this->paramId);
		//p($this->doCheckForInquiring);
		if($this->doCheckForInquiring == false){
			return true;
		}
		
		$inquiredParamsIds = ObjectModel::getFromSessionByName('inquiredParamsIds');
		
		if($inquiredParamsIds == null){
			$inquiredParamsIds = array();
		}
		
		if(!in_array($this->paramId, $inquiredParamsIds)){

			$inquiredParamsIds[$this->paramId] = $this->paramId;
			ObjectModel::saveToSession('inquiredParamsIds', $inquiredParamsIds);
				
			return true;
		} else{
			return false;
		}
	} 
	
	/**
	 * скорее всего надобность в этом методе отпадёт
	 * @access	public
	 * @todo метод возвращает массив характеристик прибора по id параметризации
	 * @param $model_par_id - ID параметризации модели
	 * @return $result - в виде 1 строки SQL - запроса (набор характеристик прибора всегда однозначен!)
	 * */
	public static function getParamInfoByModelParamIdFromDb($aModelParamId) {
	//public static function getParamIdByModel_parId($model_par_id) {
	
		$sql = ' select
	    `modelparametrization`.`id` AS `id`,
    	`parameters`.`id` AS `param_id`,
	    `parameters`.`paramtype_id` AS `ptype`,
    	`modelparametrization`.`codeorder` AS `codeorder`,
	    `modelparametrization`.`weight` AS `weight`,
    	`parameters`.`comparetype` AS `cmptype`
  		from
    	(`modelparametrization` join `parameters`)
		where
  		`modelparametrization`.`param_id` = `parameters`.`id`
    	and
    	modelparametrization.`id` =' . $aModelParamId;
	
		$result = Db::getInstance ()->getRow ( $sql );
		//p($result);
		return $result;
	}
	
	public function getValues(){
		
		//if(is_object($this->values[0])){
			return $this->values;
		//}else{
			//p("Parameter->getValues()");
			//p(debug_backtrace());
		//}
	}
	
	public function getParamWeight(){
		return $this->weight;
	}
	
	/**
	 *@todo Сравнивает эталонное значение с пользовательским для данного параметра посредством вызова 
	 * @seeParameter->compareValues($aUserValue, $aEthalValue). 
	 * Реализуется в классах потомках.
	 * */
	public function compareParamWithUserVal($anEthalonValue){}
	
	/**
	 *@todo  Сравнивает 2 значения параметра, одноиз которых пользовательское, второе - эталонное.
	 * */
	public function compareValues($aUserValue, $aEthalValue){}
	
	/**
	 * @todo Выполняет фильтрацию значения $this->value (значения, взятого из Depend->combinations['paramId'])
	 * Используется в filterCombinationsByParamValue($paramId, $values).
	 * @return Результат сравнения $this->value с $anEthalValue.
	 * */
	public function filterObjValue($anEthalValue){}

	/**
	 * @todo Возвращает массив значений параметра с максимальным приоритетом, выбирая их из БД.
	 * Реализуется в классах - потомках.
	 * */
	public function selectDefaultValueIdsFromDb(){}
	
	/**
	 * @todo Получает все значения параметра для параметризации модели ($this->modelParamId).
	 * Перекрывается в потомках.
	 * @return Массив значений параметра (экз потомков Value).
	 * */
	public function getAllValueForModelParam(){}
	
	/**
	 * @access private
	 * @todo метод по ID параметра получает ID типа параметра
	 * @param $paramId - ID параметра
	 * @return $rs[0]['paramtype_id'] - IDтипа параметра
	 **/
	private static function getParamTypeByParamID($paramId){
	
		$sql = 'select paramtype_id from parameters p where p.id = '.$paramId;
		$rs = Db::getInstance()->executeS($sql);
	
		return $rs[0]['paramtype_id'];
	}

	/**
	 * @access private
	 * @todo метод, определяя тип параметра, по типу параметра выбирает запрос, выполняемый к БД
	 * (в зависимости от типа параметра запрос осуществляется к соответствующей таблице БД)
	 * @param $paramId - ID параметра, по которому производится выбор модели из множества, задаваемого параметром $models
	 * @param $value - наименьшее значение параметра
	 * @param $valueMax - наибольшее значение параметра
	 * @param $models - множество (массив) моделей, в котором ведётся поиск; структура элемента этого массива
	 * (
	 [modelparam_id] - ID характеристики (параметризации модели), по которому модель прошла проверку
	 [param_id] - ID параметра (параметризации модели), по которому модель прошла проверку
	 [codeorder] - код к строке заказа модели, соответствующий этому параметру
	 [model_id] - ID модели
	 [modelname] - наименование модели
	 )
	 * @return $sql - sql - запрос (все значения параметров в запросе уже проставлены, его нужно просто выполнитьЫ)
	 * */
	public function getSearchQuery($paramId, $value, $valueMax, $modelsTerm = null){}
	
	/**
	 * @access	public
	 * @todo метод возвращает последний пройденный шаг процесса подбора из переменной $_SESSION
	 * @param void
	 * @return $maxStep - последний пройденный шаг процесса подбора
	 * */
	public static function getLastIterationStep() {
	
		if (is_array ( $_SESSION ) && count ( $_SESSION ) > 0) {
				
			$params = ObjectModel::getFromSessionByName('params');
			$keys = array_keys ( $params );
			$maxStep = 0;
				
			foreach ( $keys as $key ) {
	
				if ($maxStep < $params [$key] ['step']) {
					$maxStep = $params [$key] ['step'];
				}
			}
		}
		// d($maxStep);// отладочная информация
		return $maxStep;
	}
	
	/**
	 @todo Получает из БД значение параметра с максимальным приоритетом. Реализуется в классах - потомках.
	  
	 @return возвращает значение по умолчанию для параметра
	 */
	public function getDefaultValue(){}
	
	/**
	 * @todo Возвращает массив эталонных значений параметров, удовлетворяющих пользовательскому значению, получая их из БД. 
	 * */
	public function searchSatisfyValues(){}
}
?>