<?php
/**
 * @category FloatParam
 * @filesource AccuracyParam
 * @todo
 * @property __ACCURACY_ID__ = 113; константа, определяющая ID параметра "допустимая погрешность измерения"
 * @property константа, определяющая тип задания погрешности измерения - АБСОЛЮТНАЯ
 * @property константа, определяющая тип задания погрешности измерения - ОТНОСИТЕЛЬНАЯ
 * @property константа, определяющая тип задания расхода МАССОВЫЙ
 * @property константа, определяющая тип задания расхода ОБЪЁМНЫЙ
 * @property $accuracyDefinitionType = self::__RELATIVE_VALUE__; 
 * Текущий тип указания допустимой погрешности измерений. По умолчанию - ОТНОСИТЕЛЬНЫЙ.
 * @property $pathAccuracy = "accuracy.php"; 
 * Файл, содержащий html-код для запроса значения параметра. В данном случае - accuracy.php.
 * 
 * @method public function __construct($aParamObj, $aParamTypeId = null) - конструктор 
 * @method public function getAccuracyType() - Возвращает текущее значение типа задания допустимой погрешности измерений.
 * @method public function setAccuracyType($aAccuracyType = null) - Устанавливает тип задания допустимой погрешности измерений.
 * @method private static function getFlow() - Получает численное значение для расхода.
 * @method public function compareValues($aUserValue, $aEthalValue) - Сравнение пользовательского значения допустимой 
 * погрешности измерения с эталонным.
 * @method private function recalcAccuracyUserValue($aUserValue) - Если задана относительная допустимая погрешность, 
 * то метод её и возвращает. Если задана абсолютная допустимая погрешность, метод вычисляет относительную допустимую 
 * погрешность измерения, используя данные о расходе и абсолютной допустимой погрешности измерения.
 * @method public function getHTML() - Включает html - код, запрашивающий значение параметра, в тело html - страницы, 
 * на которой вызывается метод, если значение параметра должно быть запрошено у пользователя (см checkForNeedInquire()).
 * @method public function searchSatisfyValues($aUserValue) - Для параметризации модели, заданной $modelParamId, 
 * выполняет поиск в БД значений, удовлетворяющих пользовательскому значению параметра (допускаемой основной погрешности).
 * */
class AccuracyParam extends FloatParam{
	
	/**
	 * @access public
	 * @static
	 * @property константа, определяющая ID параметра "допустимая погрешность измерения" 
	 * */
	const __ACCURACY_ID__ = 113;
	
	/**
	 * @access public
	 * @static
	 * @property константа, определяющая тип задания погрешности измерения - АБСОЛЮТНАЯ
	 * */
	const __ABSOLUTE_VALUE__ = 0;/*	констаната, определяющая задание необходимой точности измерения абсолютным значением */

	/**
	 * @access public
	 * @static
	 * @property константа, определяющая тип задания погрешности измерения - ОТНОСИТЕЛЬНАЯ
	 * */
	const __RELATIVE_VALUE__ = 1;/* константа, определяющая задание необходимой тчности измерений относительным значением (в %) */
	
	/**
	 * @access public
	 * @static
	 * @property константа, определяющая тип задания расхода МАССОВЫЙ
	 * */
	const __MASS_FLOW_ID__ = 100;/*	константа, определяющая массовый расход	 */
	
	/**
	 * @access public
	 * @static
	 * @property константа, определяющая тип задания расхода ОБЪЁМНЫЙ
	 * */
	const __VOLUME_FLOW_ID__ = 98;/* константа, определяющая объёмный расход	 */
	
	/**
	 * @access private
	 * @property Текущий тип указания допустимой погрешности измерений. По умолчанию - ОТНОСИТЕЛЬНЫЙ.
	 * */
	private $accuracyDefinitionType = self::__RELATIVE_VALUE__;/* тип задания точности - целое число, 0 (абсолютное значение) или 1
															(относительное значение) */
	/**
	 * @static
	 * @access private
	 * @property Файл, содержащий html-код для запроса значения параметра. В данном случае - accuracy.php.   
	 * */
	private static $pathAccuracy = "accuracy.php";
	private static $pathAccuracyList = "accuracyList.php";
	 
	/**
	* конструктор
	*/
	public function __construct($aParamObj, $aParamTypeId = null) {
		parent::__construct($aParamObj, $aParamTypeId = null);
	}

	/**
	 * @todo Возвращает текущее значение типа задания допустимой погрешности измерений.
	 * @return Текущее значение типа задания допустимой погрешности измерений.
	 * */
	public function getAccuracyType(){
		return $this->accuracyDefinitionType;
	}
	
	/**
	 * @todo Устанавливает тип задания допустимой погрешности измерений.
	 * Если значение не задано - по умолчанию точность считается ОТНОСИТЕЛЬНОЙ.
	 * */
	public function setAccuracyType($aAccuracyType = null){
	
		if(!$aAccuracyType){
			$this->accuracyDefinitionType = self::__RELATIVE_VALUE__;
		}else{
			$this->accuracyDefinitionType = $aAccuracyType;
		}
	}
	
	/**
	 * @todo Получает численное значение для расхода (нам не важно сейчас, какого типа расход - массовый или объёмный, - 
	 * какой есть - тот и получаем; расход одновременно 2 типов не может быть задан: это блокируется в классе FlowParam)
	 * @return Значение расхода того типа, который указан пользователем. В случае, если пльзователь расход не указывал,
	 * возвращает false.
	 * */
	private static function getFlow(){
		
		if(ObjectModel::getFromSessionByName('params')[FlowParam::__FLOW_ID__]){
			return ObjectModel::getFromSessionByName('params')[FlowParam::__FLOW_ID__];
		} else{
			return false;
		}
	}

	/**
	 * @param $aUserValue - пользовательское значение допускаемой основной погрешности
	 * @param $aEthalValue - эталонное значение допускаемой основной погрешности.
	 * @todo Сравнение пользовательского значения допустимой погрешности измерения с эталонным (автономным или из 
	 * текущей комбинации) для прибора. Для расчёта всегда надо получать относительную допустимую погрешность (именно 
	 * она всегда присутствует в качестве эталонной). Если пользователь ввёл абсолютную допустимую погрешность, то 
	 * перед сравнением выполняется вычисление относительной допустимой погрешности.
	 * @return Результат сравнения пользовательского параметра с эталонным (true или false). Перед сравнением 
	 * выполняет вычисление относительной допустимой погрешности, если задана была абсолютная.
	 * @see  Parameter->compareValues($aUserValue, $aEthalValue); 
	 * @see  $this->recalcAccuracyUserValue($aUserValue);
	 * */
	public function compareValues($aUserValue, $aEthalValue){
		// делаем метод независимым от того, что передаётся в качестве пользовательского значения - массив или объект подкласса Value
		if(is_object($aUserValue)){
			$aUserValue = $aUserValue->getValue();
		}
		// вычисление относительной допустимой погрешности производим только в том случае, если была задана абсолютная погрешность,
		// иначе - производим процесс сравнения с эталоным значением немедленно
		return parent::compareValues($aUserValue, $aEthalValue);
	}

	/**
	 * @param $aUserValue - пользовательское значение допускаемой основной погрешности (относительное или 
	 * абсолютное - определяется значением поля $this->accuracyDefinitionType)
	 * @todo Если задана относительная допустимая погрешность, то метод её и возвращает. Если задана абсолютная 
	 * допустимая погрешность, метод вычисляет относительную допустимую погрешность измерения, используя данные 
	 * о расходе и абсолютной допустимой погрешности измерения.
	 * @return $normalizedUserValue - значение относительной допускаемой основной погрешности
	 * */
	private function recalcAccuracyUserValue($aUserValue){
		// вычисление относительной допустимой погрешности производим только в том случае, если была задана абсолютная погрешность,
		// иначе - производим процесс сравнения с эталоным значением немедленно
		if($this->accuracyDefinitionType == self::__ABSOLUTE_VALUE__){
	
			$flow = self::getFlow();
			// во избежание попыток задания отрицательного или нулевого расхода (если расход не указан, или случайно задан равным 0,
			// измерить его нельзя) блокируем их - метод должен сразу же вернуть true - нельзя отсеивать модели по параметру,
			// значение которого пользователь не указал, или указал неправильно
			//if($flow['min'] > 0){ 
				//$normalizedUserValue['min'] = 100 * $aUserValue['min']/$flow['min'];
			//}
			
			if($flow['max'] > 0){
				$normalizedUserValue['max'] = 100 * $aUserValue['max']/$flow['max'];
				$normalizedUserValue['min'] = $normalizedUserValue['max'];
			}
			// после этого у нас погрешность - относительная
		}elseif ($this->accuracyDefinitionType == self::__RELATIVE_VALUE__){
			$normalizedUserValue = $aUserValue;
		}
	
		return $normalizedUserValue;
	}
	
	/**
	 * @todo Включает html - код, запрашивающий значение параметра, в тело html - страницы, на которой вызывается метод, 
	 * если значение параметра должно быть запрошено у пользователя (см checkForNeedInquire()).
	 * @see Parameter->checkForNeedInquire()
	 * */
	public function getHTML(){
		
		if($this->checkForNeedInquire()){

			if(count($this->values) > 0){
				include(__ES__ROOT_DIR . '\\views\paramView\\' . self::$pathAccuracyList);
			}else{
				include (__ES__ROOT_DIR . '\\views\\paramView\\' . self::$pathAccuracy);
			}
			
		}
	}
	
	/**
	 * @todo Для параметризации модели, заданной $modelParamId, выполняет поиск в БД значений, 
	 * удовлетворяющих пользовательскому значению параметра (допускаемой основной погрешности). 
	 * @return Массив значений, удовлетворяющих пользовательскому значению допускаемой основной погрешности. 
	 * Перед вызовом родительского метода выполняет вычисление относительной допускаемой погрешности, если была задана абсолютная.
	 * @see FloatParam->searchSatisfyValues($aUserValue);
	 * */
	public function searchSatisfyValues($aUserValue){
		return parent::searchSatisfyValues($this->recalcAccuracyUserValue($aUserValue));
	}
}

?>