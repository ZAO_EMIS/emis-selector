<?php
class PreddefinedValue extends Value{
	
	private $preddefValueId;
	
	public function __construct($aParamId, $aValueId = null, $aModelParamId = null){
		$this->paramId = $aParamId;
	}
	
	public static function getPreddefValueFromDb($aStringParamValueId){
		
		$sql = "select `predefinedvalues`.`value` as preddefvalue
				from `predefinedvalues`
				where
				`predefinedvalues`.`id` = ".$aStringParamValueId;
		
		$result = Db::getInstance()->executeS($sql);
		
		return $result[0]["preddefvalue"];
	}

	/**
	 * @see Value
	 * */
	protected function initValueFromDb(){
		
		if($this->valueId > 0){
				
			$sql = 'select 
					sp.codeval as code
					, sp.priority as priority
					, sp.value_id as preddefValueId 
					from stringparams sp where sp.id = '.$this->valueId;
			
			$result = Db::getInstance()->executeS($sql);
			
			$this->code = $result[0]['code'];
			$this->priority = $result[0]['priority'];
			$this->preddefValueId = $result[0]['preddefValueId'];
		}
	}
	
	public function getCode(){
		return $this->code;
	}
	
	/**
	 * @see Value::getValue()
	 */
	public function getValue(){
	
		$value = array();
		
		$value['min'] = $this->preddefValueId;
		$value['max'] = $this->preddefValueId;
		
		return $value;
	}
	
	public function getParamId(){
		return $this->paramId;
	}
}
?>