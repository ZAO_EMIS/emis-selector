<?php
/**
 * Класс - входная точка,синглтон
 */
class Dispatcher_Single {
	// dispathcer
	protected static $instance = null;		// сам объект
	// 
	private $root_dir = null;				// корневая директория
	private $controller = null;				// текущий контроллер
	private $default_controller = null;		// контроллер по умолчанию (обычно это главная страница)
	private $pagenotfound_controller = 'PageNotFoundController'; // контроллер 404
	
	private $current_url = null;			// относительный URL (без домена)
	private $full_url;
	
	public static $post;
	
	protected function __construct()
	{		
		$this->current_url = $_SERVER['REQUEST_URI'];		
		//$this->current_url = Tools::getHttpHost();
		$this->root_dir = dirname(__FILE__);
		$this->default_controller = 'IndexController';				
		//$this->default_controller = 'AuthController';
	}
	
	private static function parsePost(){
		/* ПРИВЕСТИ В ПОРЯДОК
		if(Dispatcher_Single::$post['ajax'] == 1){
			$paramsFromPost = Dispatcher_Single::$post['params'];
		} else{
			$paramsFromPost = Dispatcher_Single::$post;
		}
		*/
		self::$post = $_POST;
	}
	
	public static function getInstance()
	{
		if (!Dispatcher_Single::$instance)
		{
			Dispatcher_Single::$instance = new Dispatcher_Single();	
		}

		return Dispatcher_Single::$instance;
	}
	
	
/**
 * Входной метод. в файле index.php  вызывается именно этот метод.
 *
*/
	public function dispatch() {
				
		if ( true) {//для норамльной авторизации этоусловие надобудет изменить (isAutorized == true ...)
			$this->getController();
		} else{
			$this->controller = "AuthController";
		}			
		
		self::parsePost();
		$controller = Controller::getController($this->controller);
		
		if ($controller != null){			
			$controller->run();
			//Db::getInstance()->__destruct();
		}
	}
//==============================================================================	
/**
* Определяет запрашиваемый контроллер или задает контроллер по умолчанию 
*/	
	protected function getController() {		
		
		if(self::getFrom_GETByName('debug') == 1){
			ObjectModel::$debug = 1;			
		}
		
		if (self::getFrom_GETByName('fc')){
			;
		} elseif(self::getFrom_GETByName('controller')){// classic controller
			$this->controller = self::getFrom_GETByName('controller');		
		} else{// default controller					
			
			if ((trim($this->current_url) == _ES_ROOT_URL) || substr_count($this->current_url, 'index.php')){
				$this->controller = $this->default_controller;
			} else {
				$this->controller = $this->pagenotfound_controller;
			}
		}
	}
//==============================================================================
	public static function checkAccess(){
		if (ObjectModel::getFromSessionByName('authorized') == true) {
			return true;
		}
		else{
			return false;
		}
	}

	public static function getFrom_GETByName($aName){
		return $_GET[$aName];
	}
}