<?php
/**
 * @access public
 * @todo Класс отвечает за формирование начальной сраницы программы. Макет страницы - startPage.php.
 * Макет страницы в процессе работы не меняется (всегда только startPage.php).
 * */
 class IndexController extends FrontController{
	
	/**
	 * @access
	 * @todo Конструктор.
	 * @return Экземпляр класса.
	 * */
	public function __construct(){
		$this->template = 'startPage.php';
	}
	
	/**
	 * @access
	 * @todo
	 * @param
	 * @return
	 * */
	public function initContent($params = null) {
		
		$this->reInitSession();
		
		$result['isModelRequest'] = 1;
		$result['models'] = Selection::getAllModelsInfo();
		
		//OLD
		//$selection = new Selection();
		//$selection->init('Selection');
		$selection = parent::initContent();// NEW
		
		$result['params'] = $selection->getParamsToInquire(null, 11);
		
		//d(_ES_ROOT_URL);
		$result['link'] = _ES_ROOT_URL . "index.php?controller=SelectController&step=1";		
		
		ObjectModel::saveToSession('Selection', $selection);
		
		return $result; 
	}
	
	/**
	 * @access
	 * @todo
	 * @param
	 * @return
	 * */
	 private function reInitSession(){
		
		if(count(ObjectModel::getFromSessionByName('params'))){
				
			$name = ObjectModel::getFromSessionByName('fio');
			$userValues = ObjectModel::getFromSessionByName('params');
			$currentSelectedModelId = ObjectModel::getFromSessionByName('model_id');
		}
		
		session_unset();
		
		ObjectModel::saveToSession('_fio', $name);
		ObjectModel::saveToSession('_params', $userValues);
		ObjectModel::saveToSession('_model_id', $currentSelectedModelId);
	}
	
	public function getPreviousUserName(){
		
		if(ObjectModel::getFromSessionByName('_fio') != null){
			$result = ObjectModel::getFromSessionByName('_fio');
		} else{
			$result = 0;
		}
		
		return $result;
	}
	
	public function getPreviousModelId(){

		if(ObjectModel::getFromSessionByName('_model_id') != null){
			$result = ObjectModel::getFromSessionByName('_model_id');
		} else{
			$result  = "'"."NOT_A_NUMBER"."'";
		}
		
		return $result;
	}
}