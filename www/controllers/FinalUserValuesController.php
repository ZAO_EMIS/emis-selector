<?php
class FinalUserValuesController extends FrontController{
	
	private $selectedModelId;
	
	public function __construct(){
		$this->template = "finalUserValuesView.php";
	}
	
	/**
	 * структура возвращаемого результата:
	 * 
	 * $result["caption"] -
	 * $result["selectedModel"] - 
	 * $result["userValues"] - 
	 * $result["orderString"] - 
	 * */
	protected function initContent(){
		
		$result = array();
		$this->selectedModelId = Dispatcher_Single::getFrom_GETByName("model_id");
		
		if(!$this->selectedModelId){
			die;
		} else{
			
			$selection = new Selection();
			$selection->init("Selection");
			
			$otherMatchModels = array();
			$completeMatch = $selection->getCompleteMatch();

			foreach ($completeMatch as $oneModel){
				
				if($oneModel->getModelId() == $this->selectedModelId){
					
					$result["selectedModel"]["userName"] = $this->getUserName();
					$result["selectedModel"]["selectionId"] = $this->getSelectionId();
					$result["selectedModel"]["selectedModelName"] = $this->getModelName();
					$result["selectedModel"]["userValues"] = $this->getUserValues();
					$result["selectedModel"]["orderString"] = $this->getOrderString();
				} else{
					
					$oneModelInfo = $oneModel->getSelectVariantInfo();
					$oneModelInfo['href'] =  _ES_ROOT_URL . "index.php?controller=SelectModelController&model_id=" . $oneModelInfo['id'];
					
					$otherMatchModels[$oneModelInfo['id']] = $oneModelInfo;
				}
			}
			
			$result["otherModels"] = $otherMatchModels;
		}
		
		return $result;
	}
	
	/**
	 * кто выполнял подбор
	 * */
	private function getUserName(){
		return ObjectModel::getFromSessionByName("fio");
	}
	
	/**
	 * выбранная модель прибора
	 * */
	private function getModelName(){
		
		$selectionObject = ObjectModel::getFromSessionByName("Selection");
		$selectedModel = $selectionObject->getCompleteMatch()[Dispatcher_Single::getFrom_GETByName("model_id")];
		
		return $selectedModel->getModelName();
	}
	
	/**
	 * 
	 * */
	private function getOrderString(){
		
		$selectionObject = ObjectModel::getFromSessionByName("Selection");
		$selectedModel = $selectionObject->getCompleteMatch()[Dispatcher_Single::getFrom_GETByName("model_id")];
		
		return $selectedModel->getOrderStrings();
	}
	
	
	/**
	 * ID процесса подбора
	 * */
	private function getSelectionId(){
		
		$selectionObj = ObjectModel::getFromSessionByName("Selection");
		
		return $selectionObj->getSelectionId();
	}
	
	/**
	 * значения пользовательских параметров
	 * */
	private function getUserValues(){
		
		$userValues = array();
		$userValuesFromSession = ObjectModel::getFromSessionByName("params");
		$selectionObject = ObjectModel::getFromSessionByName("Selection");
		 
		$selectedModel = $selectionObject->getCompleteMatch()[Dispatcher_Single::getFrom_GETByName("model_id")];
		
		// каждый из параметров, введённых пользователем, нужно найти и получить его 
		// значение (не id значения, это пользователю ни о чём не говорит)
		foreach ($userValuesFromSession as $paramId => $userVal){
			
			$oneParameter = $this->findParamInSelectionVariant($selectedModel, $paramId);
			
			$userValues[$paramId]["caption"] = $oneParameter->getParamCaption();
			$userValues[$paramId]["codeorder"] = $oneParameter->getCodeOrder();
			$userValues[$paramId]["paramtype"] = $oneParameter->getParamType();
			
			if($userValues[$paramId]["paramtype"] == Parameter::_P_PREDDEF_VALUE){
				
				$userValues[$paramId]["value"] = PreddefinedValue::getPreddefValueFromDb($userVal["min"]);
			} elseif($userValues[$paramId]["paramtype"] == Parameter::_P_LOGIC_VALUE){
				
				if($userVal["min"] > 0){
					$userValues[$paramId]["value"] = "есть";
				} else{
					$userValues[$paramId]["value"] = "нет";
				}
			} else{
				
				// в зависимости от того, диапазонный параметр или нет, отображаем значение по разному
				if($oneParameter->getIsRange()){
					$userValues[$paramId]["value"] = $userVal["min"]." - ".$userVal["max"];
				} else{
					$userValues[$paramId]["value"] = $userVal["min"];
				}
				// при необходимости указываем единицы измерения, в которых указывались пользовательские значения
				if($userVal["measUnitId"] > 0){
					$userValues[$paramId]["measunit"] = $oneParameter->findMeasUnitById($userVal["measUnitId"]);
				}
			}
		}
		
		//d($userValues);
		
		return $userValues;
	}
	
	private function findParamInSelectionVariant($aSelectionVariant, $aParamId){
		
		if(($aSelectionVariant != null) and ($aParamId > 0)){
			
			if($this->findParamInSelectionVariantAutonomicParams($aSelectionVariant, $aParamId)){
				return $this->findParamInSelectionVariantAutonomicParams($aSelectionVariant, $aParamId);
			} else{
				return $this->findParamInSelectionVariantDepends($aSelectionVariant, $aParamId);
			}
		} else{
			return false;
		}
	}
	
	/**
	 * поиск параметра в зависимостях выбранной модели
	 * */
	private function findParamInSelectionVariantDepends($aSelectionVariant, $aParamId){
		
		foreach ($aSelectionVariant->getDepends() as $oneDepend){
			
			if($oneDepend->findParamByParamId($aParamId) != false){
				return $oneDepend->findParamByParamId($aParamId);
			}
		}
		
		return false;
	}
	
	/**
	 * поиск параметра среди автономных параметров выбранной модели
	 * */
	private function findParamInSelectionVariantAutonomicParams($aSelectionVariant, $aParamId){
			
		if($aSelectionVariant->getAutonomicParams()[$aParamId] != null){
			return $aSelectionVariant->getAutonomicParams()[$aParamId];
		} else{
			return false;
		}
	}
}
?>