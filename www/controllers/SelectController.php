<?php
/**
 * @access
 * @todo
 * @property
 * @property
 * @property
 * @property
 * @property
 * @return
 * */
class SelectController extends FrontController{
	
	private $selectionId;
	
	public function __construct(){
		$this->template = 'startPage.php';		
	}
	
	/**
	 * @todo 
	 * @access protected 
	 * @param $param = null - 
	 * @return
	 * */
	protected function initContent($params = null) {					
		//d($_POST);
		//OLD
		//$selection = new Selection();
		//$selection->init('Selection');
		$selection = parent::initContent();// NEW
		$selection->setSelectionId($this->logSelection());
		//d($selection);
		$previosStep = $selection->getCurrentStep();
		
		$params = Parameter::parseUserValuesFromPost($selection->getSelectionId(), $previosStep);
		// если ни одного параметра не введено, и текущий шаг подбора > 1
		if((count($params)  == 0) && ($previosStep != 1)){
			
			if((Dispatcher_Single::getFrom_GETByName("step") == $previosStep) and (Dispatcher_Single::getFrom_GETByName("back") != "1")){
				//d("redirect 0")
				Tools::redirect(_ES_ROOT_URL."index.php?controller=SelectModelController&step=".$previosStep);			
			} else{
				// подбор заново будет выполнен в любом случае (str. 67: $afterSelect = $selection->select();)
				ObjectModel::unsetInSessionByName("model_id");
			}
		}
		
		$afterSelect = $selection->select();
		
		$result = array();	
		$paramsToInquire = $selection->getParamsToInquire();
		
		ObjectModel::saveToSession('Selection', $selection);
		
		if(count($selection->getCompleteMatch()) == 1){
			
			$currentMatch = $selection->getCompleteMatch();
			
			$model_id = "&model_id=";
			
			if(is_object($currentMatch)){
				$model_id = $model_id.$currentMatch->getModelId();
			} elseif(is_array($currentMatch)){
				
				// выдернуть первую модель из массива, независимо от ключа
				foreach ($currentMatch as $oneMatch){
					
					$currentMatch = $oneMatch;
					break;
				}
				
				//$currentMatch = $currentMatch[0]; // а этого, сответственно, более не нужно
				$model_id = $model_id.$currentMatch->getModelId();
			} else{
				$model_id = '';
			}
			//p($currentMatch);
			//d("redirect 1");
			Tools::redirect(_ES_ROOT_URL."index.php?controller=SelectModelController".$model_id);
		}elseif(($currentStep >= Parameter::MIN_WEIGHT_FOR_COMMON_PARAMS) or (count($paramsToInquire) < 2)){
			//Редирект на выбор модели
			//p(Dispatcher_Single::getFrom_GETByName('step'));
			//d($previosStep);
			if(Dispatcher_Single::getFrom_GETByName('step') >= $previosStep){
				Tools::redirect(_ES_ROOT_URL."index.php?controller=SelectModelController&step=".$previosStep);
			}
		}else{
			$link = _ES_ROOT_URL . "index.php?controller=SelectController&step=".($previosStep+1);			
		}		
		//print_r($paramsToInquire);
		
		$result['link'] = $link;
		$result['params'] = $paramsToInquire;
		//d($result);
		//d($_POST);		
		return $result;
	}

	/**
	 * @todo Метод выполняет логгирование процесса подбора, сохраняя его в БД
	 * @access protected
	 * @param $param = null -
	 * @return
	 * */
	private function logSelection(){
		
		$ip = "'".$_SERVER['REMOTE_ADDR']."'";
		
		if(!ObjectModel::getFromSessionByName('logged')){
			
			$name = "'".Dispatcher_Single::$post['family']."'";
			ObjectModel::saveToSession('fio', $name);
			
			$selectedModelId = "'".Dispatcher_Single::$post['model_id']."'";
			ObjectModel::saveToSession('model_id', $selectedModelId);
			
			$sql = 'insert into tmp_ip(`date_time`, `ip`, `fio`) values(now(),'.$ip.', '.$name.');';
			//d($sql);
			Db::getInstance()->executeS($sql);
			ObjectModel::saveToSession('logged', 1);
		}
		
		$sql = 'select max(id) as max_id from tmp_ip where ip = '.$ip;
		
		$sqlResult = Db::getInstance()->executeS($sql);
		
		return $sqlResult[0]['max_id'];
	}
}