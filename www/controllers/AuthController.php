<?php
class AuthController extends FrontController{
	// флаг попытки авторизации (если пользователь ещё не пытался авторизоваться, то и сообщений 
	// о неуспешной авторизаци он получать не должен)
	private $tryAuthorization = null;
	/**
	 * @access public
	 * @todo создание экземпляра класса
	 * @param void
	 * @return class instance
	 * */
	public function __construct(){
		$this->template = 'authPage.php';
	}
	
	/**
	 * @access public
	 * @todo проверка авторизации
	 * @param void
	 * @return
	 * */
	public function initContent($params = null){
		
		parent::initContent($params);
		
		if(ObjectModel::getFromSessionByName('authorized') == true){
			Tools::redirect("index.php?controller=IndexController");
		} else{
			
			if(Dispatcher_Single::$post['submit']){
					
				$this-> authorize();
					
				if($this->isAuthorized == true){
					//перейти к начальной странице запроса параметров
					//p("/index.php?controller=IndexController");
					$ip = "'".$_SERVER['REMOTE_ADDR']."'";
					$name = "'".Dispatcher_Single::$post['login']."'";
					$sql = 'insert into tmp_ip(`date_time`, `ip`, `fio`) values(now(), '.$ip.', '.$name.');';
					Db::getInstance()->executeS($sql);
					//Db::getInstance()->executeS("commit;");
					Tools::redirect("/index.php?controller=IndexController");
				} else{
					//оставаться на странице авторизации, и выдать сообщение о сбое авторизации с просьбой повторить процедуру
					// авторизации с другими именем пользователя или паролем.
					if($this->tryAuthorization == true){
							
						$result['inform'] = "Вы не можете авторизоваться с предоставленными учётными данными.
							Измените учётные данные и попробуйте авторизоваться снова.";
					}
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * @access public
	 * @todo проверка авторизации
	 * @param void
	 * @return void
	 * */
	private function authorize(){
		
		$login = isset($Dispatcher_Single::$post['login'])? Dispatcher_Single::$post['login'] : '';
		$passwd = isset(Dispatcher_Single::$post['passwd'])? Dispatcher_Single::$post['passwd'] : '';
		//p($_POST);
		if(($login != '') || ($passwd != '')){
			$this->tryAuthorization = true;
		} else{
			$this->tryAuthorization = false;
		}
		
		$loginCondition = "login = "."'".$login."'";
		$passwdCondition = "password = "."'".$passwd."'";
		
		$sql = "select count(*) as `count` from users where ".$loginCondition." and ".$passwdCondition;
		
		$result = Db::getInstance()->executeS($sql);		
		
		if($result[0]['count'] > 0){
			$this->isAuthorized = true;
		} else{
			$this->isAuthorized = false;
		}		
		
		ObjectModel::saveToSession('authorized', $this->isAuthorized);
	}	
}
?>