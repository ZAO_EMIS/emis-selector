<?php
/**
 * @access
 * @todo
 * @property
 * @property
 * @property
 * @property
 * @property
 * @return
 * */
class DevicePageController extends FrontController{
	
	/**
	 * ID прибора, для которго формируется страница описания
	 * */
	private $device_id;
	
	/**
	 * @access
	 * @todo конструктор задаёт шаблон страницы описания прибора - DevicePage.php
	 * @param
	 * @return
	 * */
	public function __construct(){
		//d($_SESSION);
		$this->template = 'DevicePage.php';
	}
	
	/** 
	 * @todo метод возвращает отностиельный URL фото прибра, есл итаковой указан, иначе - пустую сторку 
	 * @access
	 * @param
	 * @return
	 * */
	private function getPhoto(){
		
		$sql = 'select imagepath from modelline where id = '.$this->device_id;
		$rs = Db::getInstance()->executeS($sql);
		//d($rs);

		if($rs[0]['imagepath'] == null || $rs[0]['imagepath'] == ''){
			$res = '';
		} else{
			$res = $rs[0]['imagepath']; 
		}
		
		return $res;
	}
	
	/** 
	 * @todo метод возвращает полный набор информации текстового вида о прборе 
	 * @access
	 * @param
	 * @return
	 * */
	public function getTextInfo(){
		
		$sql = 'select title, content from descriptions where model_id = '.$this->device_id;
		$rs = Db::getInstance()->executeS($sql);
		//d($rs);
		
		$res = array();
		
		if(count($rs)){
			
			foreach ($rs as $_rs){
					
				$idx = $_rs['title'];
				$res[$idx] = $_rs['content'];
			}
		}
		
		return $res;
	}
	
	/** 
	 * @todo метод возвращает техницеские характреистики прибора в виде одномернго массива
	 * где наименование параметра - ключ, а его значения или диапахон значений - значение 
	 * @access
	 * @param
	 * @return
	 * */
	private function getTechnoCharacteristics(){
		/* на выходе - массив пар $tc[key][val] */
		$res = array();
		
		$sql = 'select 
						`vmp`.`paramID` as id, 
						`vmp`.`paramname` as name,
						`vmp`.`paramtype_id` as pt
					from `vmodelparametrization` `vmp` 
					where vmp.`modelID` = '.$this->device_id;
		/* получив все параметры, которыми характеризуется прибор, */
		$tc_params = Db::getInstance()->executeS($sql);
		//d($tc_params);
		/* формируем пары ключ-значение */
		foreach ($tc_params as $_tc){
			/* ключ */
			$idx = $_tc['name'];
			//d($idx);
			/* значение - для параметров различных типов имеет разлимчный вид  и по разному определяется */
			$res[$idx] = $this->getParamValue($_tc['id'], $_tc['pt']);
		}
		//d($res);
		return $res;
	}
	
	/**
	 * @todo 
	 * @access
	 * @param
	 * @return
	 * */
	private function getParamValue($param_id, $paramtype_id){
		
		$res = null;

		if($paramtype_id == 5 || $paramtype_id == 8){
			$res = $this->getIntMinMaxVal($param_id);
		}

		if($paramtype_id == 6){
			$res = $this->getFloatMinMaxVal($param_id);
		}
		
		if($paramtype_id == 7){
			$res = $this->getStrValSet($param_id);
		}
		
		return $res;
	}
	
	/**
	 * @access
	 * @todo 
	 * @param
	 * @return
	 * */
	private function getIntMinMaxVal($param_id){
		
		$sql = 'select min(minval) as mn, max(maxval) as mx, `mb`.`name` as nm
				from `intparams` ip, `modelparametrization` mp, `measurebase` `mb`  where
				`mp`.`id` = `ip`.`modelparametrization_id`
				and
				`mb`.`id` = `mp`.`measurebase_id`
				and
				mp.`model_id` = '.$this->device_id
				.'and'.
				'mp.`param_id` = '.$param_id;
		$rs = Db::getInstance()->executeS($sql);
		
		$res = $rs['mn'].' - '.$rs['mx'].' '.$rs['nm']; 
		
		return $res;
	}
	
	/**
	 * @access
	 * @todo 
	 * @param
	 * @return
	 * */
	private function getFloatMinMaxVal($param_id){
		
		$sql = 'select min(minval) as mn, max(maxval) as mx, `mb`.`name` as nm 
				from `floatparams` fp, `modelparametrization` mp, `measurebase` `mb`  where 
				`mp`.`id` = `fp`.`modelparametrization_id`
				and
				((`mb`.`id` = `mp`.`measurebase_id`) or (`mb`.`id` is null)) 
				and
				mp.`model_id` = '.$this->device_id
				.' and '.
				'mp.`param_id` = '.$param_id;
		$rs = Db::getInstance()->executeS($sql);
		$res = $rs[0]['mn'].' - '.$rs[0]['mx'].' '.$rs[0]['nm'];
		//d($res);
		return $res;
	}
	
	/**
	 * @access
	 * @todo 
	 * @param
	 * @return
	 * */
	private function getStrValSet($param_id){
		
		$sql = 'select
				`pv`.`value` as val
				from
				`vmodelparametrization` `vmp`, `stringparams` `sp`, `predefinedvalues` `pv`
				where
				`vmp`.`mpid` = `sp`.`modelparametrization_id`
				and
				`sp`.`value_id` = `pv`.`id`
				and
				`vmp`.`modelID` = '.$this->device_id
				.' and 
				`vmp`.`paramID` = '.$param_id;
		$rs = Db::getInstance()->executeS($sql);
		$res = '< ';
		foreach ($rs as $_rs){
			$res = $res.'<p>'.$_rs['val'].'</p>';
		}
		$res = substr($res, 2);
		//d($res);
		return $res;
	}
	
	/**
	 * @access
	 * @todo 
	 * @param
	 * @return
	 * */
	protected function initContent($params = null){
		
		$this->device_id = Dispatcher_Single::getFrom_GETByName('model_id');			
		
		if (!$this->device_id){
			die;
		} else{					
			
			$selection = new Selection();
			$selection->init("Selection");
			//d($selection);
			$otherMatchMods = array();
			$previosStep = $selection->getCurrentStep();			
			//d($selection->getSelectionId());
			$params = Parameter::parseUserValuesFromPost($selection->getSelectionId(), $previosStep);	
			$completeMatch = $selection->getCompleteMatch();
			
			foreach($completeMatch as $variant){
											
				$selectVar = new SelectionVariant();			
				$selectVar->init($variant);
				
				if($this->device_id  == $variant->getModelId()){
					
					if (count($params)>0) {
						
						//p($backupSelection);
						// проверяем, удовлетворяет ли вариант подбора комбинации пользовательских значений,
						// введённых на текущем шаге; если да, то всё нормально, продолжаем работать
						$match = $selectVar->checkThis($params, $previosStep);
						
						if(!$match){
							// если не удовлетворяет, то 
							//d($previosStep);
							// выполняем для них подбор
							$params = Parameter::getParamUserValuesByStep();
							$selectVar->checkThis($params, 1);
							//ObjectModel::saveToSession("Selection", $selection);
							Tools::redirect(_ES_ROOT_URL . "index.php?controller=SelectModelController&error=1&model_id=" . $selectVar->getModelId());
						}
					}					
					
					$result['tc'] = $this->getTechnoCharacteristics();;
					$result['txt'] = $this->getTextInfo();
					$result['imagepath'] = $img = $this->getPhoto();
					
					// если в $selectVar->getOrderStrings() передавать параметр, указывающий, на основе чего ($combinations или $bestCombinations)
					// составляются строки заказа, тогда есть возможность отделить реакцию на процесс подбора после ввода уточняющих параметров,
					// и реакцию на "Показать результат". Кроме того, появится возможность после ввода уточняющих параметров получить 
					// несколько вариантов подбора, из которых один будет позиционироваться как наилучший, другие - как альтернативные.
					$filterParams = $selectVar->checkForNeedParams();
					$result['filterParams'] = array();
					
					foreach ($filterParams as $oneParam){						
						
						if(count($oneParam->value) > 0){
							$result['filterParams'][] = $oneParam;
						}
					}
					
					$result['orderString'] = $selectVar->getOrderStrings(/* false */);
				} else{
					
					$o_modInfo = $variant->getSelectVariantInfo();
					$o_modInfo['href'] =  _ES_ROOT_URL . "index.php?controller=SelectModelController&model_id=" . $o_modInfo ['id'];
					$otherMatchMods[] = $o_modInfo;
				}
			}
		}		
		
		$result['otherModels'] = $otherMatchMods;
		//d($selection);
		ObjectModel::saveToSession("Selection", $selection);
		
		return $result;
	}
	
	protected function runAjax($params){
		//d($_SESSION);
		$this->device_id = Dispatcher_Single::getFrom_GETByName('model_id');
		
		$step = 100;
		$params = Parameter::parseUserValuesFromPost(0, $step);
		$selectionVar = new SelectionVariant();
		$selectionVar->init(array('model_id' =>$this->device_id));
		$selectionVar->checkThis(Parameter::getParamUserValuesByStep(), 1);
		Parameter::deleteParamValuesByStep($step);
		$strings = $selectionVar->getOrderStrings();
		$html = '<ul class="list-group">';
		
		foreach($strings as $oneStr){
			$html .= '<li class="list-group-item">' . $oneStr . '</li>';
		}
		
		$html .= '</ul>';
		$html .= '<a class="btn btn-info" href="#" id="showAll"><span class="glyphicon glyphicon-list-alt"></span> Показать все</a>';
		$stringsAll = $selectionVar->getOrderStrings(false);
		$html .= '<h3 class="showAll hd">Все возможные исполнения</h3>';
		$html .= '<ul class="list-group hd showAll">';
		foreach($stringsAll as $oneStr){
			$html .= '<li class="list-group-item">' . $oneStr . '</li>';
		}
		$html .= '</ul>';	
		echo($html);
	}
}
?>