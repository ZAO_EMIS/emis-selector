<?php
/**
 * @access
 * @todo
 * @property
 * @property
 * @property
 * @property
 * @property
 * @return
 * */
class SelectModelController extends FrontController {
	
	/**
	 * @todo
	 * @access
	 * @param
	 * @return
	 * */
	public function __construct() {
		$this->template = 'selectFirstStep.php';
		//$this->js_file = "js/getSuitableParamValues.js";
	}
	
	/**
	 * @todo
	 * @access
	 * @param
	 * @return
	 * */
	public function initContent($params = null) {			
		
		parent::addJS($this->js_file);		
		parent::initContent($params);
		
		//OLD
		//$selection = new Selection();
		//$selection->init('Selection');
		$selection = parent::initContent();// NEW
		$countModels = count( $selection->getCompleteMatch() );
		$result = array ();
		
		// если модель неизвестна и подходящих моделей несколько, подготовим $result и вернем view пользователю
		if ((Dispatcher_Single::getFrom_GETByName('model_id') == null) && ($countModels > 1)) {
			//d("модель неизвестна и подходящих моделей несколько");
			$matchModels = $selection->getCompleteMatchInfo();

			foreach ( $matchModels as &$o_model ) {
				$o_model ['href'] = /*_ES_ROOT_URL .*/ "index.php?controller=SelectModelController&model_id=" . $o_model ['id'];
			}
			
			$result ['models'] = $matchModels;
			ObjectModel::saveToSession('Selection', $selection);
			
			return $result;
		} elseif ((Dispatcher_Single::getFrom_GETByName("model_id") != null) or ($countModels == 1)) {
			// если модель известна
			//d("модель известна");

			$model_id_user = Dispatcher_Single::getFrom_GETByName("model_id");
			$allModels = $selection->getCompleteMatch();
			$otherMatchMods = array();
			$modelObjUser = new SelectionVariant();
				
			if(Dispatcher_Single::getFrom_GETByName("error") != null){
				
				$result["error"] = "Не удалось найти исполнение под введеные параметры";
				
				$selection->restoreToPreviousStep();
				//$paramIdsForDel = Parameter::deleteParamValuesByStep($selection->getCurrentStep());
				$modelObjUser->init($allModels[$model_id_user]);
				$modelObjUser->checkThis(Parameter::getParamUserValuesByStep(), 1);
			}
			
			foreach ( $allModels as $o_mod ) {
				
				if ($o_mod->getModelId() == $model_id_user) {
					$modelObjUser->init( $o_mod );
					
					// если имел место переход на шаг назад (текущий шаг в $_GET меньше текущего шага в $selection), то 
					// откатываем состояние $selection к предыдущему шагу, и выполняем подбор заново
					// это теперь возложено на FrontContrller
					/**
					if(Dispatcher_Single::getFrom_GETByName("step") < $selection->getCurrentStep()){
					
						$selection->restoreToPreviousStep();
						$paramIdsForDel = Parameter::deleteParamValuesByStep($selection->getCurrentStep());
						$modelObjUser->deleteFromAnalyzedDepends($paramIdsForDel);
						$params = Parameter::getParamUserValuesByStep();
						$modelObjUser->checkThis($params, 1);
					}
					*/
				} else{
					
					$o_modelInfo = $o_mod->getSelectVariantInfo(); 
					$o_modelInfo['href'] =  /*_ES_ROOT_URL .*/ "index.php?controller=SelectModelController&model_id=".$o_modelInfo ['id']."&step=".$selection->getCurrentStep();
					$otherMatchMods[] = $o_modelInfo;
				}				
			}			
			
			$needParams = $selection->getParamsToInquire(null, Selection::_DEVICE_TYPE_FLOWMETER, $model_id_user, false);
			//d($needParams);
		} elseif ($countModels == 1) {
			// или подходящая модель только одна
			//d("подходящая модель только одна");
			$allModels = $selection->getCompleteMatch();
			$modelObjUser = new SelectionVariant();
			
			// выдернуть первую модель из массива, независимо от ключа
			foreach($allModels as $oneModel){
				
				$theFirstModel = $oneModel;
				break;
			}
			
			$modelObjUser->init($theFirstModel);
		} else{
			// если ни то ни другое, выведем ошибку
			//d("модель не определена");
			if(parent::$debug){
				parent::$debugList = "Ошибка! Модель неопределена SelectModelController";
			} else {
				return;
			}			
		}
		
		$this->template = 'selectModelClick.php';
		//
		Parameter::parseUserValuesFromPost($selection->getSelectionId(), $selection->getCurrentStep());
		//p($params);		
		//d($needParams);
		if ($needParams) {
			$result ['needParams'] = $needParams;
		} else {
			ObjectModel::saveToSession('Selection', $selection);
			Tools::redirect(/*_ES_ROOT_URL.*/"index.php?controller=DevicePageController&model_id=".$modelObjUser->getModelId()."&step=".$selection->getCurrentStep());	
		}
		
		if(count($otherMatchMods) > 0){
			$result["otherModels"] = $otherMatchMods;
		}
					
		$result['name'] = $modelObjUser->model_name;
		$result['id'] = $modelObjUser->getModelId();
		
		ObjectModel::saveToSession('Selection', $selection);
		
		return $result;
	}
	
	/**
	 * @todo цель метода - по выбранному пользователем значению параметра вернуть массив зависящих от него параметров
	 * со списками (массивами) значений, совместимых с выбранным значением пользовательского параметра
	 * @access public
	 * @param $params - массив параметров, содержащий ниформацию вида (пример + описание):
	 * {$model_id=>31, - ID модели прибора
	 * $param_id=>98 (или лучше $param_id=>"paramId_98"), - ID параметра, для которого пользователь указал (выбрал) значение
	 * $val_id=>913 - ID выбранного пользователем значения
	 * }
	 * @return $result - массив параметров, зависящих от того параметра, для которого пользователь указал значение
	 * пример результирующего массива:
	 * {
	 * 		arr={77=>arr{
	 * 				[944]=>"2-5",
	 * 				[952]=>"5-33",
	 * 				[996]=>"400-1600",
	 * 				...
	 * 				[val_id]=>[val_str],
	 * 				...
	 * 			}
	 * 		},
	 * 		arr={127=>{
	 * 				[713]=>"1.33",
	 * 				[714]=>"1.65",
	 * 				[803]=>"4.0",
	 * 				...
	 * 				[val_id]=>[val_str],
	 * 				...
	 * 			}
	 * 		},
	 * 		.....
	 * }
	 * */
	public function runAjax($params = null){
		/*дополнения к методу runAjax, класс SelectModelController:
		 * 
		 * - при каждом изменении пользовательского выбора в сессии должно быть старое значение заменено на новое,
		 * при этом, если пользователь выбрал неопределённое значение, а сохранённое в сессии значение было не пустым,
		 * то значение параметра из сессии должно быть удалено.
		 * 
		 * - процесс подбора должен быть произведён по всем пользовательским значениям параметров, сохранённым в сессии на
		 * момент отправки данных на сервер
		 *  
		 * */
		
		$result = array();
		
		//OLD
		//$selection = new Selection();
		//$selection->init('Selection');
		$selection = parent::initContent();// NEW
		
		$currentStep = $selection->getCurrentStep();
		$selection->setCurrentStep($currentStep++);
		$params = Parameter::parseUserValuesFromPost($selection->getSelectionId(), $currentStep++);			
		
		$currentStep = $selection->getCurrentStep();
		
		$matches = $selection->getCompleteMatch();
		// из всех подходящих вариантов нас инетересует только один - с ID модели = $_GET['model_id']
		foreach ( $matches as $one_match ) {
			
			if ($one_match->getModelId() == $_GET['model_id']) {
				
				$one_match->checkThis($params, 1);				
				$result = $one_match->checkForNeedParams(null, 1);
				// цикл продолжать нет нужды - единственный подходящий match найден
				break;
			}
		}
		
		echo json_encode($result);
	}
}