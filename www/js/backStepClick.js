function parseQueryString(strQuery) {
	
	var i,
		tmp     = [],
		tmp2    = [],
		objRes   = {};
		
	if (strQuery != '') {
	
		tmp = (strQuery.substr(1)).split('&');

		for (i = 0; i < tmp.length; i += 1) {
		
			tmp2 = tmp[i].split('=');
		    
			if (tmp2[0]) {
				objRes[tmp2[0]] = tmp2[1];
		    }
		}
	}
	
	return objRes;
};
	
$(document).ready(
	
	function(){
		$(".backStep").click(
			
			function(){
					
				aParamArrayFromGET = parseQueryString(window.location.search);
				// вызывать history.back() можно не всегда - документ будет просрочен
				// форма может быть просрочена, если на предыдущей странице тоже вводились какие-то параметры (браузер запоминает только данные из последней формы, предыдущие теряются)
				if(((aParamArrayFromGET["controller"] == "SelectModelController") && (aParamArrayFromGET["model_id"] == null))
					|| ((aParamArrayFromGET["controller"] == "SelectController") && (aParamArrayFromGET["step"] > 1))){
						
					// если действующий контроллер SelectModelController и модель не выбрана (отображён список моделей), 
					//устанавливаем минимальный шаг и возвращаемся к SelectController
					location.href="index.php?controller=SelectController&back=1&step=" + (aParamArrayFromGET["step"] - 1);
				} else if((aParamArrayFromGET["controller"] != "FinalUserValuesController")
						&& (aParamArrayFromGET["controller"] != "DevicePageController")){
						
					if(aParamArrayFromGET["step"] < 2){
						location.href="index.php";
					} else{
						history.go(-1);
					}
				} else if(aParamArrayFromGET["controller"] == "FinalUserValuesController"){
					location.href = window.location.search.replace("FinalUserValuesController", "DevicePageController");
				} else if(aParamArrayFromGET["controller"] == "DevicePageController"){
					location.href = window.location.search.replace("DevicePageController", "SelectModelController");
				}
			}
		);
	}
);