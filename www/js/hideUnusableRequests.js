/**
 * 
 */
$(
		function() {
			$('[name = flowType]').click(
						function(){
							
							// изначально всё запрашиваемые варианты скрыты, чтобы пользователь не имел возможности ввести
							// одновременно и массовый расход, и объёмный, и плотность среды
							// при выборе одного из вариантов элементы можно разрешить для отображения, но может быть и на начальном этапе
							// это можно сделать классом?
							$('#paramId_98').removeAttr("hidden");
							$('#paramId_100').removeAttr("hidden");
							$('#paramId_114').removeAttr("hidden");
							// установкой класса hd все скрываем,
							$('#paramId_98').addClass("hd");
							$('#paramId_100').addClass("hd");
							$('#paramId_114').addClass("hd");
							// и открываем только нужные
							if($(this).attr("value") == 98){
								$('#paramId_98').removeClass("hd");//показать только объёмный расход
							}else{
								$('#paramId_100').removeClass("hd");// показать массовый расход и плотность среды
								$('#paramId_114').removeClass("hd");
							}
						}
					);
		}
);