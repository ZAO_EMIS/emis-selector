/**
 * 
 */
var dataForSend = {
		ajax:1,
		method: null,
		params: null
	};
var get = getUrlVars();
$(function() {
	$('.input_params #paramForm').toggle(function() {
		$(this).parent().find('form').eq(0).slideDown();
	}, function() {
		$(this).parent().find('form').eq(0).slideUp();
	});
	$('#paramListForm').submit(function(event) {

		var isAllRight = checkRightValues();
		if (!isAllRight){
			event.preventDefault();
			
		}
		else{
			prepareChecksAndNotPreddefSelects();
			changeMeasurment(event, this);
		}
		
	});
	$('.paramgroup .drop ').hide();
	$('.paramgroup a').click(function(){
		var drop  = $(this).parent().find(".drop").eq(0);
		if($(drop).css("display") == 'none'){
			$(drop).slideDown(300);
		}
		else{
			$(drop).slideUp(300);
		}
	});
	var templatePopover = '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>';
	var options = {
				'delay' : {"show" : 200, "hide" : 100},
				'title' : "Обратите внимание!",
				'template' : templatePopover
				};
	
	$('[data-toggle="popover"]').popover(options);
	$('.range').focusout(function(){		
		var id = $(this).parent().attr('id');
		var childMin = $(this).parent().find( $('[name="'+id+'_min"]') ).eq(0);
		var childMax = $(this).parent().find( $('[name="'+id+'_max"]') ).eq(0);		
		if( $(childMin))
		{				
			if($(childMin).val() == "" && $(childMax).val() != "" ){
				childMax.popover("show");
			}
			else if($(childMin).val() != "" && $(childMax).val() == "" ){
				childMin.popover("show");
			}
				//var newDiv = '<div class="alert alert-info tip">Подбор будет осуществлятся только по одному значению.</div><div class="arr"></div>';
				
			window.setTimeout('$("input").popover("hide")', 2000);
		}
	});
	$('.range').focusin(function(){
		$(this).parent().find('input').popover("hide");
	});

});

function checkRightValues() {
	
	var isAllRight = true;
	var isAllNull = true;
	
	$('input.text:text').each(function( index ) {
		var value = $(this).val();		
		value = value.replace(",",".");
		if (isNaN( value )) {
			$(this).css("background-color", "#f2dede");
			isAllRight = false;
		} 
		else if (value != '') {
			isAllNull = false;
			$(this).val(value);
			$(this).css("background-color", "#fff");
		}
		else{
			$(this).removeClass(("background-color", "#fff"));
		}
		isMeasurmentSetted = checkMeasurmentUnit($(this).parent());
		isAllRight = (isMeasurmentSetted) ? isAllRight : false;

	});
	
	if ((!isAllRight) | ((isAllNull) && get['step'] == 0)) {
		alert("Введите значения для параметров");
		return false;
	}
	if ($('.fio').val() == ""){
		$('.fio').addClass("redBorder");
		return false;
	}
	else{
		$('.fio').removeClass("redBorder");
	}
	
	return true;
}

function checkMeasurmentUnit(parentObject){
	var selectedEl = $(parentObject).find("select.measurUnit option:selected");
	// проверим есть ли еденицы измерения для данного поля
	if(selectedEl.length > 0){
		selectedEl = selectedEl.eq(0);
	}
	else{
		return true;
	}
	var haveInputValue = false;
	$(parentObject).find('input:text').each(function(){
		haveInputValue = (($(this).val() != '') || (haveInputValue)) ? true : false;
	});
	var measVal = $(selectedEl).val();
	// проверим выбрана ли еденица измерения
	if ((measVal == "") && (haveInputValue)) {
		$(selectedEl).parent().addClass("redBorder");
		return false;
	}
	else{
		$(selectedEl).parent().removeClass("redBorder");
		return true;
	}
}

function changeMeasurment(eventObject, form) {
	/*
	$(form).find("input:text").each(
			function(index) {

				var selectedEl = $(this).parent().find(
						"select.measurUnit option:selected");			
				if(selectedEl.length > 0){
					selectedEl = $(selectedEl).eq(0);
					var coeff = parseFloat($(selectedEl).attr("coeff"));
					var shift = parseFloat($(selectedEl).attr("shift"));
					if ($(this).val() != '') {
						var newValue = $(this).val() * coeff + shift;
						$(this).val(newValue);
					};
				};

			});
	*/
};

function prepareChecksAndNotPreddefSelects(){
	$(".notPreddef").each(function(){
		var realValue = $(this).find("select option:selected").eq(0).html();		
		var isRange = (realValue.indexOf(' ... ')) ? realValue.indexOf(' ... ') : false;
		var minVal = 0;
		var maxVal = 0;
		if(isRange > -1){
			minVal = realValue.substring(0,isRange);
			maxVal = realValue.substring(isRange + 5);
		}
		else if((isRange < 0) && isNaN(realValue)){
			minVal = maxVal = '';
		}
		else{
			minVal = maxVal = realValue;
		}
		$(this).find('.max').eq(0).val(maxVal);
		$(this).find('.min').eq(0).val(minVal);
	});
	/*$("input[type=checkbox]:checked").each(function(){
		//var val = $(this).val();
		$(this).parent().find("input:hidden").val(1);
	});*/
}

//Считывает GET переменные из URL страницы и возвращает их как ассоциативный массив.
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

