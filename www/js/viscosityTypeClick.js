$(document).ready(function(){

	$('.viscosityRadio').click(function(){

		var idDiv = $(this).attr('id').replace('Radio','Div');	
		const _DENSITY_ID_ = "#param114";
	
		//$('.paramViscosityWrap').slideUp();
		$('.paramViscosityWrap input[type=text]').val('');
	
		$('#' + idDiv).slideDown(200);
	
		var isValueEmptyDetector = $('input[name=' + _DENSITY_ID_.replace("#", "") + ']');
		var movedElement = $(_DENSITY_ID_);
		var placeToMove = $('.paramViscosityWrap');
	
		if(!(isValueEmptyDetector.val() > 0) && ($('#cinematicRadio').attr('checked') == 'checked')){
			movedElement.appendTo(placeToMove);
		};
	}
	);
}
);