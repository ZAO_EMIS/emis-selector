/**
 * 
 */
$(document).ready(function(){
	
	$("input#reset").click(function(){
		
		$("input[type='text']:not('.fio')").attr("value", "");
		$("input[type='checkbox']").removeAttr("checked");
		
		$(".param_values > option").removeAttr("selected");
	});
});