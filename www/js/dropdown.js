/**
 * 
 */
$(function(){
 
	$('.modelDescription').click(function(){
		
		$("#modelList_id").slideToggle('slow', function(){});
	});
 
	$('ul.modelList li').click(function(){
		
		var selectedModelId = $(this).attr("id").substr(4);
		var deviceName = $(this).html();
		var deviceDescripton = $(this).attr('title');
		
		$("#modelList_id").slideUp();
		$("#selectedModelID").attr("value", selectedModelId);
		$(".selectedModelName").html(deviceName);
		$(".modelDescriptionText").html(deviceDescripton);
	});
 });