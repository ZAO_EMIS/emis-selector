$(document).ready(function(){
	
	$('.flowRadio').click(function(){
		
		var idDiv = $(this).attr('id').replace('Radio','Div');
		const _DENSITY_ID_ = "#param114";

		$('.paramFlowWrap input[type=text]').val('');
		//$('.paramFlowWrap').slideUp();

		$('#' + idDiv).slideDown(200);

		var isValueEmptyDetector = $('input[name=' + _DENSITY_ID_.replace("#", "") + ']');
		var movedElement = $(_DENSITY_ID_);
		var placeToMove = $('.paramFlowWrap');

		if(!(isValueEmptyDetector.val() > 0)/* && ($('#massRadio').attr('checked') == 'checked')*/){
			movedElement.appendTo(placeToMove);
		};
	}
	);
}
);