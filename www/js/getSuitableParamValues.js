/**
 * 
 */


var responce = null;
function sendAjax(aData){
	
	$.ajax({
		type: "POST",
		dataType: "json",
		url: window.location.href,
		data: aData,
		beforeSend: function(){
			 	            $('body').append('<div class="ajaxLoader"></div>');
		},
		success: handlerResponce,
		error: handlerError,
		complete: handlerComplete
	});
}

$(function() {
	$('.param_values').change(
			function(){
				prepareChecksAndNotPreddefSelects();				// from main.js
				var paramArr = new Object();
				//
				if($(this).attr("type") == "checkbox"){
					var checkVal = ($(this).attr("checked") == "checked") ? 1 : 0;
					paramArr[$(this).attr("name") + '_min'] = checkVal; // 0 ... 30
					paramArr[$(this).attr("name") + '_max'] = checkVal;
				} else{
					paramArr[$(this).attr("name") + '_min'] = $(this).attr("value"); // 0 ... 30
					paramArr[$(this).attr("name") + '_max'] = $(this).attr("value");
				}
				// 
				dataForSend.params = paramArr;
				sendAjax(dataForSend);
			}
	);
});


/**
 * 
 * */
function handlerComplete(){
	//alert("Calling is complete!");
	$('.ajaxLoader').remove();
}

/**
 * 
 * */
function handlerError(){
	//alert("An error occured!");
}

/**
 * 
 * */
function handlerResponce(data){
	
	for(var key in data){
		
		var searchParam = "param" + key;
		var oneParam = data[key];
		//alert(oneParam.type);
		var paramType = oneParam.type;
		var searchParamValues = oneParam.values;
		
		//сначала запретим выбирать все элементы списков
		var disableSelector = "[name=" + searchParam + "] " + "option, #paramId_" + key + " input.param_values";
		$(disableSelector).attr("disabled", "disabled");
		// разблокируем неопределённые значения (value="")
		$(".undefined_value").removeAttr("disabled");
		
		var searchParamValue;
		var selector;
		
		switch(paramType){
			
			case '6':
				// числовые параметры выбираются по селектору вида [name=searchParam] [id=searchParamValue]
				var x = 0;
				for(x in searchParamValues){
					
					searchParamValue = "value_id_" + searchParamValues[x].id;
					selector = "[name=" + searchParam + "] " + "#" + searchParamValue;
					//alert(selector);
					$(selector).removeAttr("disabled");
					
				}
			break;
			
			case '8':
				
				selector = "[name=" + searchParam + "]";
				//alert(selector);
				$(selector).removeAttr("disabled");
				
				if(searchParamValues.length > 1){
					//alert("допустимо больше одного значения");
					$(selector).removeAttr("checked");
				} else{
					
					if(searchParamValues[0].minval == 1){
						$(selector).attr("checked", "checked");
					} else{
						$(selector).removeAttr("checked");
					}
				}
			break;
			
			case '7':
				
				var y = 0;
				for(y in searchParamValues){
					searchParamValue = searchParamValues[y].value_id;
					selector = "[name=" + searchParam + "] " + "[value=" + searchParamValue + "]";
					
					//alert(selector);
					$(selector).removeAttr("disabled");
				}
			break;
		}
	}
}