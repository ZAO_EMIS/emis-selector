$(document).ready(function(){
	
	$(".callback").click(function(){
		
		selectedParam = $(this).attr("for");
		paramCallbackSelector = "#param_note" + selectedParam;
		
		if($(paramCallbackSelector).css("display") == "none"){
			
			$(paramCallbackSelector).slideDown();
			userNoteSelector = paramCallbackSelector + ' > .userNote';
			$(userNoteSelector).css("width", $(this).css("width"));
		} else{
			$(paramCallbackSelector).slideUp();
		}
	});
});