/**
 * 
*/ 
$(document).ready(function(){
	
		const _PARAM_ENVIRONMENT_NAME_ = "param51";
		const _GAS_ID_ = "130";
	
	$(".param_values[name='" + _PARAM_ENVIRONMENT_NAME_ + "']").change(function(){
		
		var isGasSelected;
		
            isGasSelected = $(".param_values[name='" + _PARAM_ENVIRONMENT_NAME_ + "'] > option[value='" + _GAS_ID_ + "']").attr("selected");

			massRadio = $("#massRadio");
            disabledElementDiv = $("#massDiv");
            disabledElementMassInput = $("#massDiv input[type=text]");

            massRadio.removeAttr("disabled");

            checkedElementRadio = $("#volRadio");
            checkedElementDiv = $("#volDiv");

            if((isGasSelected == "selected")){

                checkedElementRadio.attr("checked", "checked");
                checkedElementDiv.slideDown();

                massRadio.removeAttr("checked");
                disabledElementDiv.slideUp();
                massRadio.attr("disabled", "disabled");
			}
	});
});
