<?php
	class OrderCard extends ObjectModel{
		
		private $orderCardCodes = null;
		
		public function __construct($modelId){
			
			if($modelId > 0){
			
				$sql = '
					select
						mp.`param_id` as p_id, `mp`.`codeorder` as codeorder, p.`name` as p_name
					from
						`modelparametrization` mp, `parameters` p
					where
						p.`id` = mp.`param_id`
	    				and
						`mp`.`codeorder` is not null
    					and
						mp.`model_id` = '.$modelId.' order by `mp`.`codeorder`';
			} else {
				
				$sql = '
					select
						mp.`param_id` as p_id, `mp`.`codeorder` as codeorder, p.`name` as p_name
					from
						`modelparametrization` mp, `parameters` p
					where
						p.`id` = mp.`param_id`
	    				and
						`mp`.`codeorder` is not null
    					and
						mp.`model_id` = 32 order by `mp`.`codeorder`';
			}
				
			$db = Db::getInstance();
			$this->orderCardCodes = $db->executeS($sql);
			//d($this->orderCardCodes);
		}
		
		public function getOrderCard(){
			return $this->orderCardCodes;
		}
	}
?>