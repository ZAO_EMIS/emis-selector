<body>
	<div class="wrap">
		<div class="head">
			<h1>
				<a href="/" class="mainPage">
					ЭМИС-СЕЛЕКТОР
				</a>
				<a href="http://emis-kip.ru/ru/">
					<img src="images/small_logo.png" width="30" height="30" />
				</a>
			</h1>
			<h2>Программа автоматического подбора расходомеров ЗАО ЭМИС</h2>
			<p>Версия:<strong> <?php echo _ES_VERSION_?></strong></p>
			<address>
				<strong>E-mail:</strong> 
					<a href="mailto:daineko@emis-kip.ru">Дайнеко Никита <b>(daineko@emis-kip.ru)</b></a> - руководитель проекта, 
					<a href="mailto:velikiy@emis-kip.ru"> Великий Алексей <b>(velikiy@emis-kip.ru)</b></a> - разработчик<br /> 
				<strong>Телефон:</strong>  206, 209 <br />
			</address>
		</div>
		<div>
			<?php						
				include(__ES__ROOT_DIR.'/views/'.$this->template);
			?>
 			<div class="br"></div>
		</div>
 	</div>
</body>
</html>
				