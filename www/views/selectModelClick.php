<div class="mainColumn">
<?php
		
		if($result['needParams'])
		{
			?>
			<div class="deviceCaption">
				<h2><?php echo($result['name'])?></h2>		
				<?php if($result["error"]){
				?>
					<div class="alert alert-danger" role="alert"><?php  echo($result["error"])?> </div>
				<?php 
				}?>	
				<p>
					<strong>
						Однозначный выбор прибора невозможен.
					</strong><br />
					Пожалуйста введите дополнительные параметры.
				</p>
			</div>
			
			<div class="input_params">
				<div class="divMargins">
				<!-- 
					<a class="hrefAsBtn hd" 
						href="<?php //echo(_ES_ROOT_URL.'index.php?controller=setParams');?>" id='paramForm'
					>
						Ввести параметры
					</a>
					 -->
					<a class="btn btn-info"
						href="<?php echo(_ES_ROOT_URL);?>index.php?controller=DevicePageController&model_id=<?php echo($result['id']);?>">
						<span class="glyphicon glyphicon-list-alt"></span> Показать результат
					</a>
				</div>
				<form class="positionForParent modelClick" method="post" id="paramListForm" action="<?php echo _ES_ROOT_URL;?>index.php?controller=DevicePageController&model_id=<?php echo($result['id']);?>">
					<?php
						$params = $result['needParams'];
						include (__ES__ROOT_DIR . '/views/includes/paramList.php');
					?>
				</form>
			</div>
			<ul class="hd">
			<?php					
				foreach($result['orderStrings'] as $string){
					echo('<li>'.$string.'</li>'); 
				}
			?>
			</ul>
			
		<?php }
		elseif ($result['orderStrings']){			
		?>
			<div>
			<?php					
				foreach($result['orderStrings'] as $string){
					echo($string); 
				}
			?>
			</div>
		<?php }
		else {?>				
			<h1>По Вашему запросу ничего не найдено</h1>
			<p>К сожалению в базей данных не нашлось прибора, удовлетворяющего Вашим параметрам, попробуйте изменить параметры, либо отправить опросный лист менеджеру</p>
			<a class="hrefAsBtn" href="/">изменить параметры</a>
			<a href="/index.php?controller=SendQuestionnaire">Отправить заявку менеджеру</a>
		<?php 
		}
		
		include (__ES__ROOT_DIR . '/views/includes/backAndResetButtons.php');
		?>
</div>

<?php $otherModels = $result['otherModels'];
	if(count($otherModels) > 0){ ?>
	<div class="scndCol">
	<?php 
		include (__ES__ROOT_DIR . '/views/includes/otherModels.php');	
	?>
	
	</div>
<?php } ?>		

				