<div>
	<h3>
		<b>
			Введите значения для следующих параметров:
		</b>
	</h3>
	<div class="alert alert-warning" role="alert"><strong>Внимание!</strong> Если какой-либо из параметров вызывает трудности, просто не вводите его. </div>
<!-- 	<p class="red">
		<b>
			
		</b>
	</p> -->
</div>
			
<div class="paramList container-fluid">
	<?php
		if($isModelRequest == 1){ 
	?>
	<div class="modelSelection  form-inline form-group row">
		<p><b>Модель расходомера:</b></p>
		<div class="selectModelBlock"  name="model_name" title="model_id">
			<ul class="modelList"  id="modelList_id">
				<li id="" title="">Выбрать модель прибора</li>
	<?php
		foreach ($models as $model){
				
			$model_id = $model['id'];
			$model_name = $model['basename'];
			$model_description = $model['description'];
	?>
				<li id="mod_<?php echo($model_id)?>" title="<?php echo($model_description)?>">
					<?php echo($model_name)?>
				</li>
	<?php
		} 
	?>
			</ul>
		</div>
			
		<div class="modelDescription">
			<div class="selectedModelName">Выбрать модель прибора</div>
			<div id="btn"></div>
		</div>
		<div class="modelDescriptionText"></div>
		<input type = "hidden" name="model_id" id="selectedModelID" value=""></input>
	</div>
	<?php
		} 
	?>
	
	<?php
	
		$i = 0;
		
		foreach($params as $one){		
			
			$i++;
			
			$groupInfo = $one->getGroupInfo();		
			$groupId = $groupInfo['id']; 
			
			if($groupId > 0 && $groupId != $labelCreatedFor){				
				
	?>
	<div class="paramgroup">
		<a href="#"><?php  echo ($groupInfo['name']); $labelCreatedFor = $groupId; ?></a>
		<div class="drop">
		<?php 
			}
			
			if($groupId != $labelCreatedFor){ 
				$labelCreatedFor = 0;
		?>
		</div>
	</div>
	<?php 
			}
 			
			$one->getHtml();
		}
	
		if($labelCreatedFor != 0){ ?>
	</div>
	<?php 
		}
	?>
</div>

<div>
	<input type="submit" class="btn btn-success"id="sbmt" value="Отправить"> </input>
</div>

