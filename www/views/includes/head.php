<html>
<head>
    
	<title>ЭМИС-СЕЛЕКТОР</title>
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700italic,400italic,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<?php foreach ($css_files as $path => $media) {?>	
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	
	
	<link rel="stylesheet" href="<?php echo $path?>" type="text/css" media="<?php echo $media ?>" />
	
	<script type="text/javascript" src="<?php echo(_ES_ROOT_URL);?>js/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="<?php echo(_ES_ROOT_URL);?>js/jquery/jquery-migrate-1.2.1.min.js"></script>
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(_ES_ROOT_URL);?>js/main.js"></script>  
	<script type="text/javascript" src="<?php echo(_ES_ROOT_URL);?>js/dropdown.js"></script>
	<script type="text/javascript" src="<?php echo(_ES_ROOT_URL);?>js/callback.js"></script>
	<script type="text/javascript" src="<?php echo(_ES_ROOT_URL);?>js/clearFormData.js"></script>
	<script type="text/javascript" src="<?php echo(_ES_ROOT_URL);?>js/flowTypeClick.js"></script>
	<script type="text/javascript" src="<?php echo(_ES_ROOT_URL);?>js/viscosityTypeClick.js"></script>
	<script type="text/javascript" src="<?php echo(_ES_ROOT_URL);?>js/onScrollWindow.js"></script>
	<script type="text/javascript" src="<?php echo(_ES_ROOT_URL);?>js/backStepClick.js"></script>

	<?php }
		//if(count($js_file) > 0){<script type="text/javascript" src="<?php echo $js_file "></script>
	?>
	<?php //}
		//if(count($js_file) > 0){echo $js_file;}
	?>
</head>
