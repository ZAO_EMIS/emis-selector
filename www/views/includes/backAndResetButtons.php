<div class="buttonsCol additionalButtonsPosition btn-group-vertical" role="group">
		<button type="reset"	id="reset" class="btn btn-default" >
			<span class="glyphicon glyphicon-refresh"></span>
			Очистить форму
		</button>
		<button 	class="backStep btn btn-default" type="button" 	value="" >
			<span class="glyphicon glyphicon-step-backward"></span>
			Шаг назад
		</button>
		<button 	class="backToStart btn btn-default"  type="button" onclick="location.href='<?php echo(_ES_ROOT_URL);?>'">
			<span class="glyphicon glyphicon-repeat"></span>
			Новый подбор
		</button>
</div>