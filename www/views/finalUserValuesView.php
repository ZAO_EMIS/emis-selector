<body>
<div class="wrap">
	<div class="mainColumn">
		<h3><b>Заключение по процессу подбора №</b><?php echo($result["selectedModel"]["selectionId"]);?></h3>
		<h4><b>Подбор выполнил пользователь</b> <?php echo($result["selectedModel"]["userName"]);?></h4>
		<h4><b>Пользовательские значения параметров</b></h4>
		
		<div class="list-group">
		<?php
		foreach ($result["selectedModel"]["userValues"] as $oneUserValue){
		?>
			<a class="list-group-item normalFontSize">
				<b class="text-uppercase">
					<?php echo($oneUserValue["caption"].": ");?>
				</b>
				<span class="encreasedFontSize">
				<?php
						
					$measUnit = "";
				
					if(!is_null($oneUserValue["measunit"])){
						$measUnit = "(".$oneUserValue["measunit"].")";
					}
					
					echo($oneUserValue["value"].$measUnit);
				?>
				</span>
			</a>
		<?php
		} 
		?>
		</div>
		
		<div>
			<h4><b>Результат подбора</b></h4>
			<?php
			foreach ($result["selectedModel"]["orderString"] as $oneResult){ 
			?>
				<p class="small"><?php echo($oneResult);?></p>
			<?php 
			}
			?>
		</div>
		
		<div>
		<?php
		//d(__ES__ROOT_DIR);
			include (__ES__ROOT_DIR . '/views/includes/backAndResetButtons.php');
		?>
		</div>
	</div>
	
	<?php 
	$otherModels = $result["otherModels"];
	
	if(count($otherModels) > 0){
	?>
	<div class="scndCol">
		<?php	
			include (__ES__ROOT_DIR . '/views/includes/otherModels.php');
		?>
	</div>
	<?php 
	} 
	?>
	
</div>
</body>
</html>