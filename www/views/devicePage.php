<?php
/* - фото прибора (получить из БД путь к image)
 * - краткое описание (получить из БД)
 * -- назначение (тже из БД)
 * -- описание условий работы
 * - технические характеристики (только основные или все?)
 * -- описание исполений (пара текст + рисунок)
 * -- чертежи (если есть, тоже из БД получить путь)
 * -- описание схем подключения
echo('<>');
echo('</>');
 * */	
if(!$result['errorString']){
?>
	
	<div class="mainColumn">
		
		<h2>Вашим параметрам наиболее точно соответствует:</h2>
		<?php 
			/* прочитать и вывести фото прибора */
			if(count($result) > 0){
		?>
			<!--  
			<img src="<?php //echo($result['imagepath']);?>">
			 -->
			<ul>
				<?php 
					foreach($result['orderString'] as $bestStr){ 
				?>
						<li><strong><?php echo($bestStr)?></strong></li>
				<?php 
					}
				?>
			</ul>
			
			<div>
				<a href="<?php echo("http://localhost/emis-selector/index.php?controller=FinalUserValuesController&model_id=".Dispatcher_Single::getFrom_GETByName("model_id"));?>"	>
					Посмотреть введенные параметры
				</a>
			</div>

			<img src="<?php echo($result['imagepath']);?>">
			<form class="pull-right filterParams" id="filter">
				<?php foreach($result['filterParams'] as $oneParam) : ?>
					<?php $oneParam->getHtml(); ?>
				<?php endforeach;?>
			</form>
			<div class="anotherMods"></div>
		<?php
			$txt_idx = array_keys($result['txt']);
			//d($result);
				
			foreach($txt_idx as $_txt_idx){
		?>
				<p class="hd"><?php echo($_txt_idx);?></p>
				<p class="hd"><?php echo($result[$_txt_idx]);?></p>
		<?php 
			}
		?>
		<?php
			
			$tbl_idx = array_keys($result['tc']);
			//d($result);
		?>
			<table class="hd" border = "2">
			<tr><th> Наименование параметра </th><th> Возможные значения параметра <p>(или диапазоны значений)</p> </th></tr>
		<?php		
			foreach ($tbl_idx as $_tbl_idx){
				
				//$naming = '<tr><td>'.$_tbl_idx.'</td>';
				//d($result['tc'][$_tbl_idx]);
				//$values = '<td>'.$result['tc'][$_tbl_idx].'</td></tr>';
				//echo($naming.$values);
		?>
				<tr>
					<td>
						<?php echo($_tbl_idx);?>
					</td>
					<td>
						<?php echo($result['tc'][$_tbl_idx]);?>
					</td>
				</tr>
		<?php 		
			}
		?>
			</table>
		<?php 
		} else{
		?>
			<p>Информации о приборе не найдено.</p>
		<?php 
		}
		
		include (__ES__ROOT_DIR . '/views/includes/backAndResetButtons.php');
		?>
										
		
	</div>

	<?php 
	$otherModels = $result['otherModels'];
	if(count($otherModels) > 0){
	?>
	<div class="scndCol">
	
		<?php	
			include (__ES__ROOT_DIR . '/views/includes/otherModels.php');
		?>
	</div>
	<?php } ?>
	
	<div class="br"></div>
	<div class="footer">
		<div class="footerData">
			<h2>Сопутсвующее оборудование:</h2>
			<ul class="additEq">
				<li><a href="http://emis-kip.ru/ru/prod/es750/"><img src="http://emis-kip.ru/pics/gallery/100thumb2.gif" /><br> Конвертер RS-485 -> USB</a></li>
				<li><a href="http://emis-kip.ru/ru/prod/eb60_90/"><img src="http://emis-kip.ru/pics/gallery/40thumb2.gif" /><br> Блоки питания</a></li>
			</ul>
		</div>
	</div>
	<?php
} else{
?>
	<div>
	
	</div>
<?php
}
?>

<script>
var selectChanged;


$('.filterParams select').change(function(){
	$('.wrap').append('<div class="black"><img src="../images/ajax-loader.gif" /></div>');
	prepareChecksAndNotPreddefSelects();
	$('.black').show();	
	
	dataForSend.params = serializeFilterForm();
	selectChanged = this;
	$.post(window.href , dataForSend , function(data){
		$('.anotherMods').html(data);
		$('.black').detach();
		$('#showAll').click(function(event) {
			event.preventDefault();
			$('.showAll').slideDown();
		});
		//selectChanged.selectedIndex = 0;		
	});
});

function serializeFilterForm(){
	var paramArr = new Object();
	$('select').each(function(){
		if($(this).parent().hasClass("notPreddef")){
			paramArr[$(this).attr("name") + '_min'] = $(this).parent().find('.min').attr("value"); // 0 ... 30
			paramArr[$(this).attr("name") + '_max'] = $(this).parent().find('.max').attr("value");
		}
		else{
			paramArr[$(this).attr("name") + '_min'] = $(this).attr("value");
		}
	});
	return paramArr;
}

</script>
