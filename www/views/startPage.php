<div class="centerCol">
	<form class="htmlForm positionForParent" method="post" action="<?php echo $result['link'];?>" id="paramListForm">
		<div>
   			<?php
		    	if(ObjectModel::getFromSessionByName('logged') < 1){
		    ?>
		    <div class="input-group input-group-md">
			  <span class="input-group-addon" id="sizing-addon1">@</span>
			  <input type="text" class="form-control fio" id="fio" name="family" placeholder="Введите свои фамилию и инициалы" aria-describedby="sizing-addon1">
			</div>   			
   			<?php	
		    	} 
    		?>
 		</div>
		<?php
			$isModelRequest = $result['isModelRequest'];
			$models = $result['models'];
			$params = $result['params'];
			/* а здесь происходит непосредственное формирование страницы запроса */			
			include (__ES__ROOT_DIR . '/views/includes/paramList.php');
		?>
	</form>
	 
	<?php
	 	include (__ES__ROOT_DIR . '/views/includes/backAndResetButtons.php');
	?>
</div>
 
 <script type="text/javascript">

	previousUserName = <?php echo($this->getPreviousUserName());?>;
	previousSelectedModelId = <?php echo($this->getPreviousModelId());?>;

	// вернуть предыдущее имя пользователя
	if(isNaN(previousUserName)){
		$(".fio").attr("value", previousUserName);
	}

	// и модель прибора
	if(!isNaN(previousSelectedModelId)){

		$("#selectedModelID").attr("value", previousSelectedModelId);
		
		$(".selectedModelName").html($("ul.modelList li#mod_" + previousSelectedModelId).html());
		$(".modelDescriptionText").html($("ul.modelList li#mod_" + previousSelectedModelId).attr("title"));
	}
</script>