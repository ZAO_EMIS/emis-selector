<div class="param form-inline form-group row" id="<?php echo('param'.$this->paramId);?>">
<label class="bl" for="param<?php echo $this->paramId?>"><?php echo $this->caption?></label>
	<?php 
			if ($this->isRange){
	?>
			<input 
				type = "text"
				placeholder="мин." 
				
				class="text range form-control col-md-3" 
				name="param<?php echo $this->paramId?>_min" 
				autocomplete="on"				 
				data-toggle="popover" data-trigger="manual" data-placement="top" data-content="Подбор будет осуществлятся только по одному значению."
			></input>
			<input 
				type = "text" 
				placeholder="макс."
				 
				class="text range form-control col-md-3" 
				name="param<?php echo $this->paramId?>_max" 
				autocomplete="on"
				 data-toggle="popover" data-trigger="manual" data-placement="top" data-content="Подбор будет осуществлятся только по одному значению."
			></input>
	<?php 						
			} else{
	?>
			<input 
				type = "text" 
				title="<?php echo $this->paramId ?>" 
				class="text" 
				name="param<?php echo $this->paramId ?>" 
				autocomplete="on"
			></input>
	<?php 
			}
	?>
		<?php // получаем единицы измерения для текущего параметра и заполняем список выбора, если выборка не пуста 
			
			$measUnit = $this->getParamMeasurementUnits();
			$defaultMeasUnit = $this->getDefaultMeasurementUnit();
			$defaultMeasUnitId = $defaultMeasUnit['measureUnitId'];
			//$defaultMeasUnit = $this->getDefaultMeasurementUnit();
			// здесь добавить проверку на пустоту списка 
			if(count($measUnit) != 0){
				
				$param_mu = "mesunit".$this->paramId;
		?>
				<select class="measurUnit form-control" name="<?php echo $param_mu ?>" autocomplete="off">
			<?php 
				foreach ($measUnit as $measUnit_data){
					//d($o_val);
					$id = $measUnit_data['measureUnitId'];
					
					if($measUnit_data["measureUnitId"] == $defaultMeasUnit["measureUnitId"]){
						$isSelected = 'selected';
					} else{
						$isSelected = null;
					}
					
					$name = $measUnit_data['measureUnitName'];
					$coeff = $measUnit_data['coeff'];
					$shift = $measUnit_data['shift'];
			?>
					<option 
						<?php echo($isSelected);?> 
						value = "<?php echo ($id);?>" 
						coeff="<?php echo ($coeff); ?>" 
						shift = "<?php echo ($shift); ?>"
					>
						<?php echo $name ?>
					</option>
		<?php 
				}
		?>
				</select>
		<?php
			} 
		?>		

		<script type="text/javascript">

			previousValueMin = <?php echo($this->getPreviousUserValue()["min"]);?>;
			previousValueMax = <?php echo($this->getPreviousUserValue()["max"]);?>;
			selectedMeasUnitId = <?php echo($this->getPreviousUserValue()["measUnitId"]);?>;
		
			if(!isNaN(previousValueMin)){
				$("input[name=<?php echo("param".$this->paramId."_min");?>]").attr("value", previousValueMin);
			}

			if(!isNaN(previousValueMax)){
				$("input[name=<?php echo("param".$this->paramId."_max");?>]").attr("value", previousValueMax);
			}

			if(!isNaN(selectedMeasUnitId)){
				//alert('".measurUnit>option[value=' + selectedMeasUnitId + ']"');
				$(".measurUnit>option[value=<?php echo($this->getPreviousUserValue()["measUnitId"]);?>]").attr("selected", "true");
			}
			
		</script>
</div>