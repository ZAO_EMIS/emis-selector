<div class="param notPreddef form-group row form-inline" id="<?php echo('paramId_'.$this->paramId);?>" > 
	<label class="bl" for="param<?php echo $this->paramId?>"><?php echo $this->caption?></label>		
	<select class="param_values form-control" name="param<?php echo$this->paramId ?>">
		<option class="undefined_value" value="" title="неопределённое значение">выберите значение из списка</option>
	<?php			
			foreach($this->values as $valObj){
				//d($this->values);
				if(is_object($valObj)){
					$val = $valObj->getValue();
				}else{
				}
									
				if($val['min'] != $val['max']){
					
					// завершающие нули убрать 
					while (substr($val['min'], strlen($val['min']) - 1, 1) == "0") {
						$val['min'] = substr($val['min'], 0, strlen($val['min']) - 1);
					}
					
					while (substr($val['max'], strlen($val['max']) - 1, 1) == "0") {
						$val['max'] = substr($val['max'], 0, strlen($val['max']) - 1);
					}
					$val['min'] = rtrim($val['min'],'.');
					$val['max'] = rtrim($val['max'],'.');
					$realValString = $val['min'] . " ... " . $val['max'];
				} else{
					//$dptPos = strpos($val['min'], ".");
					$realValString = substr($val['min'], 0, strpos($val['min'], "."));								
				}										
	?>		
		<option id="value_id_<?php echo($valObj->getValueId());?>"><?php echo($realValString); ?></option>
	<?php 
			}
	?>					
	</select>
	<input type="hidden" name="param<?php echo($this->paramId)?>_min" class="min" value=""></input>
	<input type="hidden" name="param<?php echo($this->paramId)?>_max" class="max" value=""></input>
</div>