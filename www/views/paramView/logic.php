<div class="param  form-inline form-group row" id="<?php echo('paramId_'.$this->paramId);?>">
	
	<?php 
		if(($this->paramId == 144) or ($this->paramId == 120)){
		?>
			<label class="bl"><?php echo($this->caption); ?></label>
			<div class="radioGroup">
				<input
					class="param_values"
					type="radio"
					name="param<?php echo $this->paramId?>"
					value="0"
					checked="checked"
					autocomplete="on"
				>нет</input>
			
				<input
					class="param_values"
					type="radio"
					name="param<?php echo $this->paramId?>"
					value="1"
					autocomplete="on"
				>да</input>
			</div>
	<?php			
		} else{
	?>
			
			<input
				class="param_values"
				type="checkbox"
				name="param<?php echo $this->paramId?>"
				value="1"
				autocomplete="on"
			> <?php echo($this->caption); ?></input>
	<?php
		}
	?>
	
	
</div>
	
<script type="text/javascript">

	isPreviousChecked = <?php echo($this->getPreviousUserValue()["min"])?>;

	if(isPreviousChecked > 0){
		$("#paramId_<?php echo($this->paramId);?> > .param_values").attr("checked", 'true');
	} else{
		$("#paramId_<?php echo($this->paramId);?> > .param_values").removeAttr("checked");
	}
</script>
