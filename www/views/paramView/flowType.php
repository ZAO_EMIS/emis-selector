<div class="param">
	<div class="radioGroup  form-inline form-group row">
		<label class="bl">Способ измерения расхода</label>
		<input class="flowRadio" id="volRadio" type="radio" checked  name="<?php echo('param'.self::__FLOWTYPE_ID__);?>"  value=<?php echo(self::__VOLUME_FLOW_TYPE__);?>>
		<label for="volRadio">-объёмный</label>
		
		<input class="flowRadio" id="massRadio" type="radio" name="<?php echo('param'.self::__FLOWTYPE_ID__);?>" value=<?php echo(self::__MASS_FLOW_TYPE__);?>>
		<label for="massRadio">-массовый</label>
	</div>
	<div class="paramFlowWrap" id="volDiv">
		<?php $flowVolumeParam->getHtml();?>
		<?php $densityParam->getHtml();?>
	</div>
</div>

