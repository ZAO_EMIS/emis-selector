
<div class="param  form-inline form-group row" id="<?php echo('paramId_'.$this->paramId);?>">
<label class="bl" for="param<?php echo $this->paramId?>"><?php echo $this->caption?></label>
 	<?php 
				$paramname = $this->name;
				$paramid = $this->paramId;
	?>					
		<select class="param_values form-control" name="<?php echo('param'.$this->paramId);?>" title="<?php echo($this->paramId);?>">
			<option class="undefined_value" value="" title="неопределённое значение">выберите значение из списка</option>
	<?php
				foreach ($this->values as $o_val){
					$id = $o_val['value_id'];
					$val = $o_val['value'];
					$descr = $o_val['descr'];
	?>
			<option value="<?php echo($id);?>" 	title="<?php echo($descr);?>"><?php echo($val);?></option>
	<?php
				}
	?>
		</select>	
	<?php
		/* id текущего параметра нам известен - $paramid */
		//$currentParamValueId = ObjectModel::getFromSessionByName('_params')[$paramid]['min']; /* в массив помещаем значения id и value_id текущего параметра */
	?>
		
	<script type="text/javascript">

		previousValueId = <?php echo($this->getPreviousUserValue()["min"]);?>;

		if(previousValueId > 0){
			$("option[value=" + previousValueId + "]").attr("selected", "true");
		} else{
			//$(".undefined_value").attr("selected", "true");
		}
	</script>
</div>