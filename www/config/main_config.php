<?php
error_reporting(E_ERROR);
set_time_limit(180);
define('_ES_VERSION_',"1.0.0.b");
define('_ES_MODE_DEV_', true);
define('__ES__ROOT_DIR', realpath(dirname(__FILE__).'/..'));
//define('_ES_ROOT_URL', '/'.substr(strrchr(__ES__ROOT_DIR, '\\'), 1).'/');
define('_ES_ROOT_URL', '/');
define('__ES__CLASS_DIR', __ES__ROOT_DIR .'/classes/');
//autoloading undefined classes
require_once(__ES__CLASS_DIR.'autoload.php');
define('_ES_TOOL_DIR_', __ES__TOOL_DIR .'/tools/');
require_once(__ES__ROOT_DIR.'/config/alias.php');
spl_autoload_register(array(AutoLoad::getInstance(), 'load'));
session_start();
