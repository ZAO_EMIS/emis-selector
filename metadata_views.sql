﻿SET FOREIGN_KEY_CHECKS=0;

DROP VIEW IF EXISTS `vparamvariants`;
DROP VIEW IF EXISTS `vintvals`;
DROP VIEW IF EXISTS `vfloatvals`;
DROP VIEW IF EXISTS `vautonomicparams`;
DROP VIEW IF EXISTS `vrp`;
DROP VIEW IF EXISTS `vmodelparametrization`;
DROP VIEW IF EXISTS `dp`;
DROP VIEW IF EXISTS `devicemetertypes`;
DROP VIEW IF EXISTS `deps`;
DROP VIEW IF EXISTS `depparam`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `usernotes`;
DROP TABLE IF EXISTS `stringparams`;
DROP TABLE IF EXISTS `start_page_choose`;
DROP TABLE IF EXISTS `selectionprotocol`;
DROP TABLE IF EXISTS `tmp_ip`;
DROP TABLE IF EXISTS `selectionprocesses`;
DROP TABLE IF EXISTS `searchresults`;
DROP TABLE IF EXISTS `searchparamvalues`;
DROP TABLE IF EXISTS `searchhistory`;
DROP TABLE IF EXISTS `requestedvalues`;
DROP TABLE IF EXISTS `predefinedvalues`;
DROP TABLE IF EXISTS `paramvariants`;
DROP TABLE IF EXISTS `recalcs`;
DROP TABLE IF EXISTS `orderpositions`;
DROP TABLE IF EXISTS `orders`;
DROP TABLE IF EXISTS `ordercards`;
DROP TABLE IF EXISTS `intparams`;
DROP TABLE IF EXISTS `floatparams`;
DROP TABLE IF EXISTS `enviromentphases`;
DROP TABLE IF EXISTS `enviroments`;
DROP TABLE IF EXISTS `descriptions`;
DROP TABLE IF EXISTS `dependence_parametrization`;
DROP TABLE IF EXISTS `customers`;
DROP TABLE IF EXISTS `consistensyruleparameters`;
DROP TABLE IF EXISTS `consistencyrules`;
DROP TABLE IF EXISTS `combinations`;
DROP TABLE IF EXISTS `modelparametrization`;
DROP TABLE IF EXISTS `parameters`;
DROP TABLE IF EXISTS `parametertypes`;
DROP TABLE IF EXISTS `parametergroups`;
DROP TABLE IF EXISTS `measurebase`;
DROP TABLE IF EXISTS `phisquantity`;
DROP TABLE IF EXISTS `modelline`;
DROP TABLE IF EXISTS `devicetype`;
DROP TABLE IF EXISTS `aggrstates`;
DROP TABLE IF EXISTS `aggressiveytpe`;

CREATE TABLE `aggressiveytpe` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT 'среда химически инертна',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
)ENGINE=InnoDB
AUTO_INCREMENT=35 AVG_ROW_LENGTH=1170 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `aggrstates` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(40) COLLATE utf8_general_ci NOT NULL DEFAULT 'агрегатное состояние  не указано',
  `alienimpurity` VARCHAR(5) COLLATE utf8_general_ci NOT NULL DEFAULT 'нет',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=11 AVG_ROW_LENGTH=4096 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `devicetype` (
  `id` TINYINT(4) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT 'тип измерительного устройства',
  `assigment` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT 'назначение не указано',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
)ENGINE=InnoDB
AUTO_INCREMENT=12 AVG_ROW_LENGTH=8192 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `modelline` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `basename` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT 'наименование модели не определено',
  `devicetype_id` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `isexists` VARCHAR(20) COLLATE utf8_general_ci DEFAULT 'в наличии',
  `production` VARCHAR(20) COLLATE utf8_general_ci DEFAULT 'производится',
  `shipmentenable` VARCHAR(20) COLLATE utf8_general_ci DEFAULT 'поставки разрешены',
  `imagepath` VARCHAR(100) COLLATE utf8_general_ci DEFAULT 'null',
  `description` TEXT COLLATE utf8_general_ci,
  `comments` TEXT COLLATE utf8_general_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `modelline_fk_devicetype` (`devicetype_id`),
  CONSTRAINT `modelline_fk_devicetype` FOREIGN KEY (`devicetype_id`) REFERENCES devicetype (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=39 AVG_ROW_LENGTH=4096 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `phisquantity` (
  `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) COLLATE utf8_general_ci NOT NULL DEFAULT 'наименование физической величины',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
)ENGINE=InnoDB
AUTO_INCREMENT=25 AVG_ROW_LENGTH=1260 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `measurebase` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) COLLATE utf8_general_ci NOT NULL DEFAULT 'ед. измер.',
  `coeff` FLOAT UNSIGNED NOT NULL DEFAULT 1,
  `shift` FLOAT NOT NULL DEFAULT 0,
  `phisquantity_id` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `base_id` BIGINT(20) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `measurebase_fk1` (`phisquantity_id`),
  KEY `measurebase_fk2` (`base_id`),
  CONSTRAINT `measurebase_fk1` FOREIGN KEY (`phisquantity_id`) REFERENCES phisquantity (`id`),
  CONSTRAINT `measurebase_fk2` FOREIGN KEY (`base_id`) REFERENCES measurebase (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=64 AVG_ROW_LENGTH=1820 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `parametergroups` (
  `id` BIGINT(20) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT 'группа параметров устройтсва' COMMENT '/**************************************************\r\nнаименование группы параметров\r\n**************************************************/',
  `rang` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `parentgroup_id` BIGINT(20) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `parametergroups_fk2` (`parentgroup_id`),
  CONSTRAINT `parametergroups_fk2` FOREIGN KEY (`parentgroup_id`) REFERENCES parametergroups (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=32 AVG_ROW_LENGTH=1024 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `parametertypes` (
  `id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) COLLATE utf8_general_ci NOT NULL DEFAULT 'наименование типа параметра',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`)
)ENGINE=InnoDB
AUTO_INCREMENT=11 AVG_ROW_LENGTH=4096 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `parameters` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT 'укажите наименование параметра',
  `phisquantity_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
  `paramtype_id` TINYINT(3) UNSIGNED NOT NULL DEFAULT 7,
  `isRange` TINYINT(4) UNSIGNED DEFAULT NULL,
  `description` VARCHAR(200) COLLATE utf8_general_ci DEFAULT 'null',
  `recomendations` VARCHAR(200) COLLATE utf8_general_ci DEFAULT NULL,
  `comparetype` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0-точное сравнение 1-интервальное 2-ограничение сверху(не более, чем...)',
  `multiselection` TINYINT(4) UNSIGNED NOT NULL DEFAULT 0,
  `parametergroups` BIGINT(20) UNSIGNED DEFAULT NULL,
  `required` TINYINT(4) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `parameters_fk3` (`paramtype_id`),
  KEY `parameters_fk2` (`phisquantity_id`),
  KEY `parameters_fk1` (`parametergroups`),
  CONSTRAINT `parameters_fk1` FOREIGN KEY (`parametergroups`) REFERENCES parametergroups (`id`),
  CONSTRAINT `parameters_fk2` FOREIGN KEY (`phisquantity_id`) REFERENCES phisquantity (`id`),
  CONSTRAINT `parameters_fk3` FOREIGN KEY (`paramtype_id`) REFERENCES parametertypes (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=145 AVG_ROW_LENGTH=2340 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `modelparametrization` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `param_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `weight` TINYINT(3) NOT NULL DEFAULT 0,
  `measurebase_id` BIGINT(20) UNSIGNED DEFAULT NULL,
  `model_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `independent` TINYINT(1) NOT NULL DEFAULT 1,
  `codeorder` TINYINT(4) UNSIGNED DEFAULT NULL,
  `paramVariant_id` BIGINT(20) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `fk1` (`model_id`),
  KEY `modelparametrization_fk_parameters` (`param_id`),
  KEY `modelparametrization_fk_measurebase` (`measurebase_id`),
  KEY `modelparametrization_fk_paramvariant` (`paramVariant_id`),
  CONSTRAINT `modelparametrization_fk1` FOREIGN KEY (`model_id`) REFERENCES modelline (`id`),
  CONSTRAINT `modelparametrization_fk_measurebase` FOREIGN KEY (`measurebase_id`) REFERENCES measurebase (`id`),
  CONSTRAINT `modelparametrization_fk_parameters` FOREIGN KEY (`param_id`) REFERENCES parameters (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=484 AVG_ROW_LENGTH=68 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `combinations` (
  `id` BIGINT(20) UNSIGNED NOT NULL,
  `modelparam_id` BIGINT(20) UNSIGNED NOT NULL,
  `val_id` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `combinations_fk1` (`modelparam_id`),
  CONSTRAINT `combinations_fk1` FOREIGN KEY (`modelparam_id`) REFERENCES modelparametrization (`id`)
)ENGINE=InnoDB
AVG_ROW_LENGTH=66 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `consistencyrules` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `modelparametrization_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `name` VARCHAR(20) COLLATE utf8_general_ci NOT NULL DEFAULT 'правило целостности',
  `shortdescription` VARCHAR(500) COLLATE utf8_general_ci NOT NULL DEFAULT 'краткое описание правила целостности',
  `sqlstmt` VARCHAR(2000) COLLATE utf8_general_ci NOT NULL DEFAULT 'select\r\n...\r\nfrom\r\n...\r\nwhere\r\n...',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `consistencyrules_fk1` (`modelparametrization_id`),
  CONSTRAINT `consistencyrules_fk1` FOREIGN KEY (`modelparametrization_id`) REFERENCES modelparametrization (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `consistensyruleparameters` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parameter_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `rule_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `paramposition` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1,
  `sqlparamvalue` FLOAT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `rule_id` (`rule_id`),
  KEY `consistensyruleparameters_fk1` (`parameter_id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `customers` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `endcustomerflag` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  `ownershipform` VARCHAR(30) COLLATE utf8_general_ci NOT NULL DEFAULT 'форма собственности не указана',
  `firmname` VARCHAR(200) COLLATE utf8_general_ci NOT NULL DEFAULT 'наименование фирмы\\организации не указано',
  `endcustomername` VARCHAR(200) COLLATE utf8_general_ci NOT NULL DEFAULT 'наименование конечного заказчикане указано',
  `family` VARCHAR(30) COLLATE utf8_general_ci NOT NULL DEFAULT 'Фамилия',
  `name` VARCHAR(30) COLLATE utf8_general_ci NOT NULL DEFAULT 'Имя',
  `longname` VARCHAR(30) COLLATE utf8_general_ci NOT NULL DEFAULT 'Отчество',
  `post` VARCHAR(100) COLLATE utf8_general_ci NOT NULL DEFAULT 'занимемая должность не указана',
  `phone` VARCHAR(20) COLLATE utf8_general_ci NOT NULL DEFAULT '000-000-000',
  `mobphone` VARCHAR(20) COLLATE utf8_general_ci NOT NULL DEFAULT '+7-000-000-00-00',
  `fax` VARCHAR(20) COLLATE utf8_general_ci NOT NULL DEFAULT '000-000-0000-000',
  `email` VARCHAR(50) COLLATE utf8_general_ci NOT NULL DEFAULT 'e-mail не указан',
  `address_fk` BIGINT(20) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `customers_fk_address` (`address_fk`),
  CONSTRAINT `customers_fk_address` FOREIGN KEY (`address_fk`) REFERENCES addresses (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `dependence_parametrization` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dependence_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `modelparametrization_independent_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `value_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `dependence_parametrization_fk1` (`dependence_id`),
  KEY `dependence_parametrization_fk2` (`modelparametrization_independent_id`),
  CONSTRAINT `dependence_parametrization_fk2` FOREIGN KEY (`modelparametrization_independent_id`) REFERENCES modelparametrization (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=105370 AVG_ROW_LENGTH=317 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `descriptions` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `model_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 1,
  `title` VARCHAR(200) COLLATE utf8_general_ci DEFAULT 'null',
  `content` TINYTEXT COLLATE utf8_general_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `descriptions_fk1` (`model_id`),
  CONSTRAINT `descriptions_fk1` FOREIGN KEY (`model_id`) REFERENCES modelline (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=33 AVG_ROW_LENGTH=512 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `enviroments` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) COLLATE utf8_general_ci NOT NULL DEFAULT 'наименование среды не указано',
  `aggressivetype_fk` BIGINT(20) UNSIGNED NOT NULL DEFAULT 1,
  `minagressiveconc` FLOAT UNSIGNED NOT NULL DEFAULT 0,
  `maxaggressiveconc` FLOAT UNSIGNED NOT NULL DEFAULT 100,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `enviroments_fk1` (`aggressivetype_fk`),
  CONSTRAINT `enviroments_fk1` FOREIGN KEY (`aggressivetype_fk`) REFERENCES aggressiveytpe (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=40 AVG_ROW_LENGTH=744 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `enviromentphases` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `aggrstate_fk` BIGINT(20) UNSIGNED NOT NULL DEFAULT 1,
  `env_fk` BIGINT(20) UNSIGNED NOT NULL DEFAULT 1,
  `flammable` VARCHAR(5) COLLATE utf8_general_ci NOT NULL DEFAULT 'нет',
  `explosive` VARCHAR(5) COLLATE utf8_general_ci NOT NULL DEFAULT 'нет',
  `toxic` VARCHAR(5) COLLATE utf8_general_ci NOT NULL DEFAULT 'нет',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `enviromentphases_fk1` (`aggrstate_fk`),
  KEY `enviromentphases_fk2` (`env_fk`),
  CONSTRAINT `enviromentphases_fk1` FOREIGN KEY (`aggrstate_fk`) REFERENCES aggrstates (`id`),
  CONSTRAINT `enviromentphases_fk2` FOREIGN KEY (`env_fk`) REFERENCES enviroments (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `floatparams` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `value` FLOAT NOT NULL DEFAULT 0,
  `modelparametrization_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `codeval` VARCHAR(30) COLLATE utf8_general_ci DEFAULT 'null',
  `priority` TINYINT(4) NOT NULL DEFAULT 0,
  `minval` DOUBLE(15,6) NOT NULL DEFAULT 0.000000,
  `maxval` DOUBLE(15,6) NOT NULL DEFAULT 0.000000,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `floatparams_fk1` (`modelparametrization_id`),
  CONSTRAINT `floatparams_fk1` FOREIGN KEY (`modelparametrization_id`) REFERENCES modelparametrization (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=2135 AVG_ROW_LENGTH=137 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `intparams` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `value` INTEGER(11) NOT NULL DEFAULT 0,
  `minval` INTEGER(11) NOT NULL DEFAULT 0,
  `maxval` INTEGER(11) NOT NULL DEFAULT 0,
  `modelparametrization_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `codeval` VARCHAR(30) COLLATE utf8_general_ci DEFAULT 'null',
  `priority` TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `intparams_fk1` (`modelparametrization_id`),
  CONSTRAINT `intparams_fk1` FOREIGN KEY (`modelparametrization_id`) REFERENCES modelparametrization (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=4 AVG_ROW_LENGTH=5461 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `ordercards` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `orders` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `orderdate` DATE DEFAULT '2001-01-20',
  `shipdate` DATE DEFAULT '2001-01-20',
  `deliverydate` DATE DEFAULT '2001-01-20',
  `customers_fk` BIGINT(20) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `users_fk` (`customers_fk`),
  CONSTRAINT `orders_fk_customers` FOREIGN KEY (`customers_fk`) REFERENCES customers (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `orderpositions` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `equipmentexemplair_fk` BIGINT(20) NOT NULL DEFAULT 0,
  `exemplaircount` BIGINT(20) UNSIGNED NOT NULL DEFAULT 1,
  `order_fk` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `orderpositions_fk1` (`order_fk`),
  CONSTRAINT `orderpositions_fk1` FOREIGN KEY (`order_fk`) REFERENCES orders (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `recalcs` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `condition` TEXT COLLATE utf8_general_ci,
  `formulaTemplate` TEXT COLLATE utf8_general_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=19 AVG_ROW_LENGTH=4096 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `paramvariants` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `measurebase_id` BIGINT(20) UNSIGNED DEFAULT NULL,
  `recalc_id` BIGINT(20) UNSIGNED DEFAULT NULL,
  `modelparam_id` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `paramvariants_fk_measurebase` (`measurebase_id`),
  KEY `paramvariants_fk_recalc` (`recalc_id`),
  KEY `paramvariants_fk_modelparametrization` (`modelparam_id`),
  CONSTRAINT `paramvariants_fk_measurebase` FOREIGN KEY (`measurebase_id`) REFERENCES measurebase (`id`),
  CONSTRAINT `paramvariants_fk_modelparametrization` FOREIGN KEY (`modelparam_id`) REFERENCES modelparametrization (`id`),
  CONSTRAINT `paramvariants_fk_recalc` FOREIGN KEY (`recalc_id`) REFERENCES recalcs (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=154 AVG_ROW_LENGTH=819 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `predefinedvalues` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `value` VARCHAR(1000) COLLATE utf8_general_ci NOT NULL DEFAULT 'значение строкового паарметра',
  `param_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `description` VARCHAR(512) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `predefinedvalues_fk1` (`param_id`),
  CONSTRAINT `predefinedvalues_fk1` FOREIGN KEY (`param_id`) REFERENCES parameters (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1212 AVG_ROW_LENGTH=1820 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `requestedvalues` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `searchhistory` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `searchdt` DATETIME NOT NULL DEFAULT '2014-01-01 00:00:00',
  `customer_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `asethalon` TINYINT(4) UNSIGNED NOT NULL DEFAULT 0,
  `passtoexpert` VARCHAR(20) COLLATE utf8_general_ci DEFAULT 'в инженерный отдел',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `searchparamvalues` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parameter_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `search_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `wantedvalue` FLOAT DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `searchparamvalues_fk1` (`search_id`),
  KEY `searchparamvalues_fk2` (`parameter_id`),
  CONSTRAINT `searchparamvalues_fk1` FOREIGN KEY (`search_id`) REFERENCES searchhistory (`id`),
  CONSTRAINT `searchparamvalues_fk2` FOREIGN KEY (`parameter_id`) REFERENCES parameters (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `searchresults` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `search_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `modelline_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `rang` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `searchresults_fk1` (`search_id`),
  KEY `searchresults_fk2` (`modelline_id`),
  CONSTRAINT `searchresults_fk1` FOREIGN KEY (`search_id`) REFERENCES searchhistory (`id`),
  CONSTRAINT `searchresults_fk2` FOREIGN KEY (`modelline_id`) REFERENCES modelline (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `selectionprocesses` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `tmp_ip` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date_time` DATETIME NOT NULL,
  `ip` VARCHAR(20) COLLATE utf8_general_ci DEFAULT NULL,
  `fio` VARCHAR(20) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1701 AVG_ROW_LENGTH=16384 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `selectionprotocol` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `selection_id` BIGINT(20) UNSIGNED NOT NULL,
  `param_id` BIGINT(20) UNSIGNED NOT NULL,
  `param_min_value` DOUBLE(15,3) DEFAULT NULL,
  `param_max_value` DOUBLE(15,3) DEFAULT NULL,
  `selection_step` INTEGER(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `param_id` (`param_id`),
  KEY `selection_id` (`selection_id`),
  CONSTRAINT `selectionprotocol_fk1` FOREIGN KEY (`param_id`) REFERENCES parameters (`id`),
  CONSTRAINT `selectionprotocol_fk2` FOREIGN KEY (`selection_id`) REFERENCES tmp_ip (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=13113 AVG_ROW_LENGTH=125 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `start_page_choose` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `devicetype_id` TINYINT(4) UNSIGNED NOT NULL DEFAULT 0,
  `parameter_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `start_page_choose_fk1` (`devicetype_id`),
  KEY `start_page_choose_fk2` (`parameter_id`),
  CONSTRAINT `start_page_choose_fk1` FOREIGN KEY (`devicetype_id`) REFERENCES devicetype (`id`),
  CONSTRAINT `start_page_choose_fk2` FOREIGN KEY (`parameter_id`) REFERENCES parameters (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=121 AVG_ROW_LENGTH=248 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `stringparams` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `value_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `modelparametrization_id` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
  `codeval` VARCHAR(30) COLLATE utf8_general_ci DEFAULT 'null',
  `priority` TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `stringparams_fk3` (`value_id`),
  KEY `stringparams_fk1` (`modelparametrization_id`),
  CONSTRAINT `stringparams_fk1` FOREIGN KEY (`modelparametrization_id`) REFERENCES modelparametrization (`id`),
  CONSTRAINT `stringparams_fk3` FOREIGN KEY (`value_id`) REFERENCES predefinedvalues (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=457 AVG_ROW_LENGTH=66 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `usernotes` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `selection_id` BIGINT(20) UNSIGNED NOT NULL,
  `param_id` BIGINT(20) UNSIGNED DEFAULT NULL,
  `note` VARCHAR(1000) COLLATE utf8_general_ci NOT NULL DEFAULT 'без замечаний',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `usernotes_fk1` (`selection_id`),
  CONSTRAINT `usernotes_fk1` FOREIGN KEY (`selection_id`) REFERENCES tmp_ip (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=75 AVG_ROW_LENGTH=2340 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE TABLE `users` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `password` VARCHAR(20) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `role` VARCHAR(20) COLLATE utf8_general_ci NOT NULL DEFAULT 'пользователь',
  `login` VARCHAR(20) COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
)ENGINE=InnoDB
AUTO_INCREMENT=1 AVG_ROW_LENGTH=16384 CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT=''
;

CREATE ALGORITHM=TEMPTABLE DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `depparam`
AS
select 
    `dp`.`id` AS `id`,
    `dp`.`dependence_id` AS `dep_id`,
    `dp`.`modelparametrization_independent_id` AS `mp_id`,
    `dp`.`value_id` AS `val_id`,
    `ml`.`id` AS `m_id`,
    `ml`.`basename` AS `m_name`,
    `p`.`id` AS `p_id`,
    `p`.`name` AS `p_name`,
    `p`.`paramtype_id` AS `p_type`,
    `mp`.`independent` AS `indep` 
  from 
    (((`dependence_parametrization` `dp` join `modelparametrization` `mp`) join `parameters` `p`) join `modelline` `ml`) 
  where 
    ((`dp`.`modelparametrization_independent_id` = `mp`.`id`) and (`p`.`id` = `mp`.`param_id`) and (`ml`.`id` = `mp`.`model_id`)) 
  order by 
    `dp`.`id`;

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `deps`
AS
select 
    distinct `dp`.`dependence_id` AS `dependence_id` 
  from 
    `dependence_parametrization` `dp`;

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `devicemetertypes`
AS
select 
    `fp`.`modelparametrization_id` AS `modelparametrization_id`,
    `mp`.`model_id` AS `model_id`,
    `fp`.`minval` AS `isMassMeter` 
  from 
    (`floatparams` `fp` join `modelparametrization` `mp`) 
  where 
    ((`fp`.`modelparametrization_id` = `mp`.`id`) and (`mp`.`param_id` = 142));

CREATE ALGORITHM=TEMPTABLE DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `dp`
AS
select 
    `dependence_parametrization`.`id` AS `id`,
    `dependence_parametrization`.`dependence_id` AS `dependence_id`,
    `dependence_parametrization`.`modelparametrization_independent_id` AS `modelparametrization_independent_id`,
    `dependence_parametrization`.`value_id` AS `value_id` 
  from 
    `dependence_parametrization`;

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `vmodelparametrization`
AS
select 
    `modelparametrization`.`id` AS `mpid`,
    `modelparametrization`.`independent` AS `independent`,
    `modelline`.`devicetype_id` AS `devtype`,
    `modelline`.`id` AS `modelID`,
    `modelline`.`basename` AS `modelname`,
    `parameters`.`id` AS `paramID`,
    `parameters`.`paramtype_id` AS `paramtype_id`,
    `parameters`.`name` AS `paramname`,
    `modelparametrization`.`codeorder` AS `codeorder`,
    `modelparametrization`.`weight` AS `weight`,
    `parameters`.`description` AS `descr`,
    `parameters`.`comparetype` AS `cmptype` 
  from 
    ((`modelline` join `modelparametrization`) join `parameters`) 
  where 
    ((`modelparametrization`.`model_id` = `modelline`.`id`) and (`modelparametrization`.`param_id` = `parameters`.`id`) and (`modelline`.`shipmentenable` is not null));

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `vrp`
AS
select 
    `vmodelparametrization`.`mpid` AS `mpid`,
    `vmodelparametrization`.`independent` AS `independent`,
    `vmodelparametrization`.`modelID` AS `modelID`,
    `vmodelparametrization`.`modelname` AS `modelname`,
    `vmodelparametrization`.`paramID` AS `paramID`,
    `vmodelparametrization`.`paramtype_id` AS `paramtype_id`,
    `vmodelparametrization`.`paramname` AS `paramname`,
    `vmodelparametrization`.`codeorder` AS `codeorder`,
    `vmodelparametrization`.`weight` AS `weight`,
    `vmodelparametrization`.`descr` AS `descr`,
    `vmodelparametrization`.`cmptype` AS `cmptype` 
  from 
    `vmodelparametrization` 
  where 
    (`vmodelparametrization`.`devtype` = 11);

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `vautonomicparams`
AS
select 
    `vrp`.`mpid` AS `mpid`,
    `vrp`.`modelID` AS `modelID`,
    `vrp`.`paramID` AS `paramID`,
    `vrp`.`paramtype_id` AS `paramtype_id`,
    `vrp`.`paramname` AS `paramname`,
    `vrp`.`codeorder` AS `codeorder`,
    `vrp`.`weight` AS `weight`,
    `vrp`.`cmptype` AS `cmptype` 
  from 
    `vrp` 
  where 
    ((not(`vrp`.`mpid` in (
  select 
    distinct `combinations`.`modelparam_id` 
  from 
    `combinations`))) and (not(`vrp`.`mpid` in (
  select 
    distinct `dependence_parametrization`.`modelparametrization_independent_id` 
  from 
    `dependence_parametrization`))) and (`vrp`.`codeorder` > 0));

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `vfloatvals`
AS
select 
    `modelline`.`id` AS `id`,
    `modelline`.`basename` AS `basename`,
    `modelparametrization`.`id` AS `modelparamID`,
    `parameters`.`id` AS `paramID`,
    `parameters`.`name` AS `name`,
    ifnull(`floatparams`.`minval`,0) AS `minval`,
    ifnull(`floatparams`.`maxval`,0) AS `maxval` 
  from 
    ((((`parameters` join `modelparametrization`) join `modelline`) join `floatparams`) join `parametertypes`) 
  where 
    ((`modelparametrization`.`model_id` = `modelline`.`id`) and (`modelparametrization`.`param_id` = `parameters`.`id`) and (`parametertypes`.`id` = 6) and (`floatparams`.`modelparametrization_id` = `modelparametrization`.`id`));

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `vintvals`
AS
select 
    `modelparametrization`.`id` AS `modelparamID`,
    `parameters`.`id` AS `paramID`,
    `parameters`.`name` AS `name`,
    ifnull(`intparams`.`minval`,0) AS `minval`,
    ifnull(`intparams`.`maxval`,0) AS `maxval` 
  from 
    (((`parameters` join `modelparametrization`) join `intparams`) join `parametertypes`) 
  where 
    ((`modelparametrization`.`param_id` = `parameters`.`id`) and (`parametertypes`.`id` = 5) and (`intparams`.`modelparametrization_id` = `modelparametrization`.`id`));

CREATE ALGORITHM=UNDEFINED DEFINER='root'@'localhost' SQL SECURITY DEFINER VIEW `vparamvariants`
AS
select 
    `pv`.`modelparam_id` AS `modelparam_id`,
    count(`pv`.`id`) AS `variantcount` 
  from 
    (`paramvariants` `pv` join `vmodelparametrization` `vmp`) 
  where 
    ((`vmp`.`mpid` = `pv`.`modelparam_id`) and (`pv`.`recalc_id` is not null)) 
  group by 
    `pv`.`modelparam_id`;

