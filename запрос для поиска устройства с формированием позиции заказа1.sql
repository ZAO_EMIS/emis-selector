/* ��������� ���������: �� ��������� ������ - ������ ����������� ���������. 
���� ����������� ���� - ��� ��������������� �� ��������� ����������� ���������� */
/* 1: ��������� �������� ID ���������, ��� ������������ � ����������� �������� (���� ��� ��������), 
��� ���� � �� �� �������� � �������� ������������ � ������������� (���� �������� �� ������������ ����)
��� �������� ��� ���������� ���������, �������� ������ �������, ��������������� ����������� �� ��������� ���������.

�� ������ ��������� ���� (���������) ������������ ����� ������� �� ���� ������������� ������������ ����������� IN (`modelline`.`id` in (..., ..., ...))
 */
/* �� ����������� ID ��������� � ��� �������� ����������, ����� ������ ����� �������� ��������� �������������

�� ����: 
- ID ���������, 

- ����������� ��� ������������� (����� ���� null), 
- ������������ ��� ������������� (����� ���� null), 

- ����������� ��� float (����� ���� null), 
- ������������ ��� float (����� ���� null), 

- ID ���������� ��������� (����� ���� null)

������� isCoincidenceXXX(...) (����������) �������� ���, ��� ���������� ������ "����" �������� ����������, �� ������ ���������.
������� � ������ ��������� �������� �� ����, � ������ ����������� ���������� (������������ � null �� ����� ������������)


�� ������: 
- ������� �������������� ������, 
- ����� ������� � ����� ������ (���� ����, ���� ��� - ��������� ������ ������)
- ID ������
- ��� ������
 */

/* �������� �� ���� ���������� �� ������ � ������ ������ �� ��������� ������, ������� ����������� �� ������ �������, ������� ������������� �������� ������ 
�� ����������� ���������� */
/**********************************************************************************
//rs �������� �����, ��� ������� ���������� �� ��������� ������
....
JList<Integer> paranList = rs.getValues(paramID);
int i = 0;

while(i < paramList.count){
	...
	//��������� ������, �������� ������ ID �������
    rsModels.getValues(model_id).getString; //�������� ������ �������� ����� �������
	i++;
}
**********************************************************************************/
select
	`modelparametrization`.`id` as modelparam_id, 
    ifnull(`modelparametrization`.`codeorder`, 0) as codeorder, 
    `modelline`.`id` as model_id, 
    `modelline`.`basename` as modelname
from
	`modelline`, `modelparametrization`, `intparams`, `floatparams`, `stringparams`
where(
	`modelline`.`id` = `modelparametrization`.`model_id`
    and
    `modelparametrization`.`independent` = 1
    and
    `modelline`.`id` in (29, 31, 16) /* ��� ����� ������� ������ ������������� �����������, � ��� ������ ���������� ������ ������������� (��������� ������� ���� �����) */
    and
    `modelparametrization`.`param_id` = :param_id
    and(
    	(
        	(`typedAs`(`modelparametrization`.`id`) = 5 or `typedAs`(`modelparametrization`.`id`) = 8)
            and `intparams`.`modelparametrization_id` = `modelparametrization`.`id`
            and `isCoincidenceInt`(`modelparametrization`.`id`, :minintval, :maxintval)
        ) or (
        	(`typedAs`(`modelparametrization`.`id`) = 6)
            and `floatparams`.`modelparametrization_id` = `modelparametrization`.`id`
            and `isCoincidenceFloat`(`modelparametrization`.`id`, :minfloatval, :maxfloatval)
        ) or (
        	(`typedAs`(`modelparametrization`.`id`) = 7)
            and `stringparams`.`modelparametrization_id` = `modelparametrization`.`id`
            and `isCoincidenceStr`(`modelparametrization`.`id`, :strval)
        )
    )
)
group by
	`modelline`.`id`;
    
/* 2 ��� ������� ������������ ��������� � �������� ���������, ��� ������ �� �������, ��������� �� ���� 1, 
�������� ID �������� ���������, ��������������� ������� ������������, �, ���� ����,
���, ��������������� ����������� ���������� � ����� ������
*/
/* �. ���������� ��� ��������� */
/*
�� ����: :paramID - ������������� ��������� (��� ���������� ������ ��, ������� ���� �� ��������� ������, � ��� ������� ������� �������� ��������)
�� ������: ID ���� ���������: 5- �������������, 6- � ����.������, 7- ���������, 8- ����������
*/
select `parametertypes`.`id` as paramtype_id
from `parameters`, `parametertypes`
where `parametertypes`.`id` = `parameters`.`paramtype_id` and `parameters`.`id` = :paramID

/* �. �������� - �������������� ��� ����������� ���� */
/*
�� ����: 
- :modelID - ID ������, ����������� �� ���� 1
- :paramID - ID ������������ ���������
- :minVal - ���������� ��������, ��������� �������������
- :maxVal - ���������� ��������, ��������� �������������
�� ������: 
- ID �������������� ������
- ������� � ����� ������, ���� ���� (���� ��� - 0)
- ��� � ����� ������, ���� ���� (���� ��� - ������ ������)
*/
select 
	`modelparametrization`.`id` as modelparam_id,
    `intparams`.`id` as values_id,
    `parameters`.`id`,
    `modelparametrization`.`codeorder`,
	`getCodeValue`(`floatparams`.`codeval_id`) as codeval
from
	`modelparametrization`, `parameters`, `intparams`
where
`modelparametrization`.`param_id` = `parameters`.`id`
and
`floatparams`.`modelparametrization_id` = `modelparametrization`.`id`
and
`modelparametrization`.`model_id` = :modelID
and
`parameters`.`id` = :paramID
and
`intparams`.`minval` <= :minval and `intparams`.`maxval` >= :maxval

/* �. �������� - � ��������� ������ */
/*
�� ����: 
- :modelID - ID ������, ����������� �� ���� 1
- :paramID - ID ������������ ���������
- :minVal - ���������� ��������, ��������� �������������
- :maxVal - ���������� ��������, ��������� �������������
�� ������: 
- ID �������������� ������
- ������� � ����� ������, ���� ���� (���� ��� - 0)
- ��� � ����� ������, ���� ���� (���� ��� - ������ ������)
*/
select 
	`modelparametrization`.`id` as modelparam_id,
    `floatparams`.`id` as values_id,
    `parameters`.`id`,
    `modelparametrization`.`codeorder`,
	`getCodeValue`(`floatparams`.`codeval_id`) as codeval
from
	`modelparametrization`, `parameters`, `floatparams`
where
`modelparametrization`.`param_id` = `parameters`.`id`
and
`floatparams`.`modelparametrization_id` = `modelparametrization`.`id`
and
`modelparametrization`.`model_id` = :modelID
and
`parameters`.`id` = :paramID
and
`floatparams`.`minval` <= :minval and `floatparams`.`maxval` >= :maxval

/* �. �������� - ���������� ���� */
/*
�� ����: 
- :modelID - ID ������, ����������� �� ���� 1
- :paramID - ID ������������ ���������
- :Val_ID - ID ���������������� ��������, ���������� �������������
�� ������: 
- ID �������������� ������
- ������� � ����� ������, ���� ���� (���� ��� - 0)
- ��� � ����� ������, ���� ���� (���� ��� - ������ ������)
*/
select
	`modelparametrization`.`id` as modelparam_id,
	`stringparams`.`id` as values_id,
	`modelparametrization`.`codeorder` as codeorder,
	`getCodeValue`(`stringparams`.`codeval_id`) as codeval
from
	`parameters`, `modelparametrization`, `stringparams`
where(
    `parameters`.`id` = :paramID
    and
    `stringparams`.`value_id` = :Val_ID
    and
    `modelparametrization`.`model_id` = :modelID
    and
    `parameters`.`id` = `modelparametrization`.`param_id`
    and
    `modelparametrization`.`id` = `stringparams`.`modelparametrization_id`
);

/* ������������� ��������� 1: ��� �������, ��������������� ����������� �� ����������� ����������,
�������� ���������� ��� ������������ ������� ����� ������, ������������ ������������ ��� ���� ������� ����������� */

/* 3: ��� ������ �� ��������� � ���� 1 ������� �������� ������ ��������� ����������
 */
/* �������� �� ������ �������, ���������� �� ����������� ����������, */
/* ��� ��������� ������ ����� ��� � ����������� 
�� ����: id  ������
�� ������: id ������������
 */
select
	`dependences`.`id` as dependence_id
from
	`modelparametrization`, `dependences`
where
	`modelparametrization`.`id` = `dependences`.`modelparametrization_id`
    and
    `modelparametrization`.`model_id` = :model_id
group by
	`dependences`.`id`;
    
/* 4: ��� ������ �� ��������� �� ���� 2 ������������ ����� � ��������������
 */
/*��� ������ ����������� ����� ����� � �������������� (������ ����������� ����������, ������������ ��������� ��������)
�� ����: id �����������
�� ������: id ����������� ���������� ������ (� �������� ����� �� modelparametrization), ������������ ��������� �����������
*/
select
	`dependence_parametrization`.`modelparametrization_independent_id` as dep_param_id
from
	`dependences`, `dependence_parametrization`
where
	`dependence_parametrization`.`depnedence_id` = `dependences`.`id`
    and
    `dependences`.`id` = :dependence_id
group by
	`dependence_parametrization`.`modelparametrization_independent_id`;

/* 5: �������� �� ���� ��������� ������������, ��� ������ �� ��� ���������, ������������� �� ����������� �������� ���������� ��������� */
/* �� ��������� id �����������, id ������������ ��������� � id ��� ��������
�������� ���, ��������������� ���� ������������ � ����� ������

���� ��� ���������� �� ���� ���������� ��� ����������, ������ ������ ����� ������.
���� ���������� ���� (����������� ������������ � ��������� ���������������) - ������ ���, ��������� ������������ � ����� ������.

�� ����:
- :indepentent_modelparam_id - ID �������������� ������ (����������� ����������)
- :val_id - ID �������� ������������ ���������
- :dep_id - ID ����������� (�����������)
- 
�� ������:
- ID ������ �������������� ������ (��� ��������������� ������ ������������ �����������) - � �������� ������������ ���������
- ID �������������� ������ (��������� ����������)
- ID �����������
- ������� � ����� ������, ������������ ��������� ����������, ���� ���� (���� ��� - 0)
- ��� � ������� ����� ������, ������������ ������������, ���� ���� (���� ��� - ������ ������)
 */
 /*
��� ���������� ���������� ����� �������:

- ���� ������ �� ������ �� ����� ������, �� ����� ����������� ��� � ����. 
� ���� ��� ����������� - ��� �� �� �������� �� ��� � ����� ������, �� ������� � ����� ������.
� ��������� �������� ������ ����������� (:dep_id) ������ �� 
���� (:independent_modelparam_id, :val_id)=:(ID �������������� ����������� ����������, ID ������ � ���������� ���������),
�� ����� �������������� (:dep_id) - ����������� �� ����������� ������������.

- ���� ������ ������ 1 �������� (1 ������), ����� ������� � �������� ���� �� 
����������� (:dep_id) �� ���������� ��������� (:indepentent_modelparam_id, :val_id).

- ���� ������ ������ ����� 1 ������ - ��� ������ �� ������ ��� ���������� ��. 
������ ������� � ���, ��� ���� � �� �� ����������� ��� ���������� ��������������
������� ����� ������ �������� ����, ����������� � ����� ������, ���� ���� �� ������,
���� ��������� �������� ����� multiselection = 0.

���, ���������������� ������������ � ����� ������, ���� ������������ ���������� (� ����� ����������� 
�����, � ����������� ������� ����������� - ��������), ���� �� ������������ ������ (��������� ������� ����),
� ����� ����������� �� ������������ �����������, ��������� � ��������� �����������.
 */
select
    `vdependence_parametrization`.`dep_param_id` as depparam_id, /* ��� ���� - �����������. ���� � �������������� ����������� 
    ���� ������, ��������������� ������� �� ��������� ��������, �� ��� � ���� � ID */
    `vdependence_parametrization`.`dep_id` as dep_id,
    `getDependentCodePosition`(`vdependence_parametrization`.`dep_id`) as codeorder, 
    `getDependentCodeByDepID`(`vdependence_parametrization`.`dep_id`) as codeval
from
	`vdependence_parametrization`, `modelparametrization`, `intparams`, `floatparams`, `stringparams`
where
	`vdependence_parametrization`.`indepentent_modelparam_id` = :indepentent_modelparam_id
    and
	`vdependence_parametrization`.`val_id` = :val_id
    and
    `vdependence_parametrization`.`dep_id` = :dep_id
	and 
    `vdependence_parametrization`.`indepentent_modelparam_id` = `modelparametrization`.`id`
    and(
/*
*/
    	(`vdependence_parametrization`.`paramtype_id` = 5 or `vdependence_parametrization`.`paramtype_id` = 8
			and `vdependence_parametrization`.`indepentent_modelparam_id` = `modelparametrization`.`id`
            and `modelparametrization`.`id` = `intparams`.`modelparametrization_id`
            and `intparams`.`id` = :val_id
        ) 
        or
/*
*/    
    	(`vdependence_parametrization`.`paramtype_id` = 6
            and `modelparametrization`.`id` = `floatparams`.`modelparametrization_id`
			and `vdependence_parametrization`.`indepentent_modelparam_id` = `modelparametrization`.`id`
            and `floatparams`.`id` = :val_id
        ) 
        or
    	(`vdependence_parametrization`.`paramtype_id` = 7
            and `modelparametrization`.`id` = `stringparams`.`modelparametrization_id`
			and `vdependence_parametrization`.`indepentent_modelparam_id` = `modelparametrization`.`id`
            and `stringparams`.`id` = :val_id
        )
    )
group by
    `vdependence_parametrization`.`dep_id`;

/* ������������� ��������� 2: �������� ���������� ��� ������������ ������� ���� ������ �� ��������� ���������� */    

/* ������������: ��������� ����� ������ �� ����������� 
������ (���� ����� �� ������, ������� �� ����� ���� ���������) 
�� ������ ������� ������� ����� ������ � ������������ ��� ������ ����� � ���� ��������.
*/