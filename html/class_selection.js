var class_selection =
[
    [ "__construct", "class_selection.html#a837bf42789772a0d0f927ec5be790b49", null ],
    [ "deleteFromInquiredParams", "class_selection.html#a2cd6459a9f78dbbe7fd8d0da82d40f9e", null ],
    [ "deleteParamFromInquired", "class_selection.html#ad274afd4a990a9e559de5c810197cc11", null ],
    [ "getCompleteMatch", "class_selection.html#a95d59c5ea6035a6f40e3bf3176902336", null ],
    [ "getCompleteMatchInfo", "class_selection.html#a6bd9872e1cac29a3b2079814401d3bbc", null ],
    [ "getCurrentStep", "class_selection.html#a8c10d82ce4005a51c021f375175e117a", null ],
    [ "getParamsToInquire", "class_selection.html#a221aec4d13eba3c14be149c40adbaeb8", null ],
    [ "getSelectionId", "class_selection.html#a1e13a527be2c66cee1318d9fba658b43", null ],
    [ "init", "class_selection.html#a12582170000dc9326b614795cc2aeb2d", null ],
    [ "select", "class_selection.html#a701f78ac6d7ece9e18883fe7ee612e9f", null ],
    [ "setCompleteMatch", "class_selection.html#a411ccd97819eee952c2f307a9f407c16", null ],
    [ "setCurrentStep", "class_selection.html#a9fd40614b32e8d2be5c04a15d3c1a1ef", null ],
    [ "setSelectionId", "class_selection.html#a0181b44d2534ee6c23b5a10f96e9b83d", null ],
    [ "_DEVICE_TYPE_FLOWMETER", "class_selection.html#aaeaf96a77510251578d701f3b996f4cb", null ]
];