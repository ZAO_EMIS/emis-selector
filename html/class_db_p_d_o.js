var class_db_p_d_o =
[
    [ "_escape", "class_db_p_d_o.html#ac8a5f87620ff480ff6480432c856a5e8", null ],
    [ "_numRows", "class_db_p_d_o.html#a66ee15a3cadcf6b766b29bab19212bc6", null ],
    [ "_query", "class_db_p_d_o.html#a03b0ba73d6c78fafe419097367b8fa94", null ],
    [ "Affected_Rows", "class_db_p_d_o.html#acb9fafba996a1a97b2b1729f5ed8145f", null ],
    [ "connect", "class_db_p_d_o.html#a78572828d11dcdf2a498497d9001d557", null ],
    [ "disconnect", "class_db_p_d_o.html#abe175fcf658475bc56e9d6fa02bc88ec", null ],
    [ "getBestEngine", "class_db_p_d_o.html#aca886df78fa956b29997794a67265549", null ],
    [ "getMsgError", "class_db_p_d_o.html#aea0196c004309e9c4b65838ed873e925", null ],
    [ "getNumberError", "class_db_p_d_o.html#ac8db5c899c5e7141649bcb40c4912819", null ],
    [ "getVersion", "class_db_p_d_o.html#afa8e7a3a646144eab50188b7a805a389", null ],
    [ "Insert_ID", "class_db_p_d_o.html#a389ef959ad6468921b0303a474e60061", null ],
    [ "nextRow", "class_db_p_d_o.html#a2e1c7663c273dc1433d6f5bf1f956409", null ],
    [ "set_db", "class_db_p_d_o.html#a2bd85be1d65d99cbb383e25bc5303979", null ]
];