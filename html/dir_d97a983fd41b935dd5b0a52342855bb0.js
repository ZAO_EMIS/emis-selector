var dir_d97a983fd41b935dd5b0a52342855bb0 =
[
    [ "index.php", "classes_2exception_2index_8php.html", "classes_2exception_2index_8php" ],
    [ "PrestaShopDatabaseException.php", "_presta_shop_database_exception_8php.html", [
      [ "PrestaShopDatabaseException", "class_presta_shop_database_exception.html", "class_presta_shop_database_exception" ]
    ] ],
    [ "PrestaShopException.php", "_presta_shop_exception_8php.html", [
      [ "PrestaShopException", "class_presta_shop_exception.html", "class_presta_shop_exception" ]
    ] ],
    [ "PrestaShopModuleException.php", "_presta_shop_module_exception_8php.html", [
      [ "PrestaShopModuleException", "class_presta_shop_module_exception.html", null ]
    ] ],
    [ "PrestaShopPaymentException.php", "_presta_shop_payment_exception_8php.html", [
      [ "PrestaShopPaymentException", "class_presta_shop_payment_exception.html", null ]
    ] ]
];