var class_abstract_logger =
[
    [ "__construct", "class_abstract_logger.html#ae191e2b8226d69e767917a7d3b114cd3", null ],
    [ "log", "class_abstract_logger.html#a591b7bfc7902033889ea0b20b04af84f", null ],
    [ "logDebug", "class_abstract_logger.html#a363b67f5ae9142fa1390b3a22e47592d", null ],
    [ "logError", "class_abstract_logger.html#a5ad0269ceda9308c93504802a493a0bb", null ],
    [ "logInfo", "class_abstract_logger.html#a232cb571b538ad7b73d4c08206e8ff52", null ],
    [ "logMessage", "class_abstract_logger.html#ab86df6d8b9d60fd29af74b581abbd98f", null ],
    [ "logWarning", "class_abstract_logger.html#aca7fbd1fc2fccf76b07cf5c0a1e1c204", null ],
    [ "$level", "class_abstract_logger.html#abd32cc82c6a3f79491987de36ad580ca", null ],
    [ "$level_value", "class_abstract_logger.html#a3338ccba42085482a45e7d1850af4a5e", null ],
    [ "DEBUG", "class_abstract_logger.html#a758c150b67e476ecf77478f16b387c61", null ],
    [ "ERROR", "class_abstract_logger.html#a7f79d7b73cfb40bb7855d4260393cc0f", null ],
    [ "INFO", "class_abstract_logger.html#af2d1bd27ecbe33ecaadb558404e9c669", null ],
    [ "WARNING", "class_abstract_logger.html#ad0c7ccd2f8b92a760391d21d0ec7b339", null ]
];