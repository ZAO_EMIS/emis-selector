var hierarchy =
[
    [ "AbstractLogger", "class_abstract_logger.html", [
      [ "FileLogger", "class_file_logger.html", null ]
    ] ],
    [ "AutoLoad", "class_auto_load.html", null ],
    [ "Db", "class_db.html", [
      [ "DbPDO", "class_db_p_d_o.html", null ],
      [ "MySQL", "class_my_s_q_l.html", null ]
    ] ],
    [ "DbQuery", "class_db_query.html", null ],
    [ "DeviceFormInit", "class_device_form_init.html", null ],
    [ "Dispatcher_Single", "class_dispatcher___single.html", null ],
    [ "Exception", null, [
      [ "PrestaShopException", "class_presta_shop_exception.html", [
        [ "PrestaShopDatabaseException", "class_presta_shop_database_exception.html", null ],
        [ "PrestaShopModuleException", "class_presta_shop_module_exception.html", null ],
        [ "PrestaShopPaymentException", "class_presta_shop_payment_exception.html", null ]
      ] ]
    ] ],
    [ "ObjectModel", "class_object_model.html", [
      [ "Combination", "class_combination.html", null ],
      [ "Controller", "class_controller.html", [
        [ "FrontController", "class_front_controller.html", [
          [ "AuthController", "class_auth_controller.html", null ],
          [ "DevicePageController", "class_device_page_controller.html", null ],
          [ "IndexController", "class_index_controller.html", null ],
          [ "PageNotFoundController", "class_page_not_found_controller.html", null ],
          [ "SelectController", "class_select_controller.html", null ],
          [ "SelectModelController", "class_select_model_controller.html", null ]
        ] ]
      ] ],
      [ "Depend", "class_depend.html", null ],
      [ "DeviceController", "class_device_controller.html", null ],
      [ "OrderCard", "class_order_card.html", null ],
      [ "Param", "class_param.html", null ],
      [ "Parameter", "class_parameter.html", [
        [ "FloatParam", "class_float_param.html", [
          [ "AccuracyParam", "class_accuracy_param.html", null ],
          [ "FlowParam", "class_flow_param.html", null ],
          [ "ViscosityParam", "class_viscosity_param.html", null ]
        ] ],
        [ "PredefinedParam", "class_predefined_param.html", null ]
      ] ],
      [ "ParameterController", "class_parameter_controller.html", null ],
      [ "Selection", "class_selection.html", null ],
      [ "SelectionVariant", "class_selection_variant.html", null ],
      [ "Tools", "class_tools.html", null ]
    ] ],
    [ "ParamFormInit", "class_param_form_init.html", null ],
    [ "Value", "class_value.html", [
      [ "FloatValue", "class_float_value.html", null ],
      [ "PreddefinedValue", "class_preddefined_value.html", null ]
    ] ]
];