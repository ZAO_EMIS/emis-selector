var class_viscosity_param =
[
    [ "__construct", "class_viscosity_param.html#a4f46318f7632e5e6f46e8589f34fbaad", null ],
    [ "checkForNeedInquire", "class_viscosity_param.html#a8ebbe652f1e1c9e280757478c3788f02", null ],
    [ "compareValues", "class_viscosity_param.html#ac57e9be8bc26ae55423c9b494e8cf6cc", null ],
    [ "getHTML", "class_viscosity_param.html#a5fc878ede54118176f912b557031ddd6", null ],
    [ "getSearchQuery", "class_viscosity_param.html#a95e794cd653ef78e423994bc8deee351", null ],
    [ "searchSatisfyValues", "class_viscosity_param.html#a1b5d0ab4fe3b2da20ea27ee62ebe6196", null ],
    [ "setViscosityType", "class_viscosity_param.html#af9b10d6fecc6b37e67a77c353b465317", null ],
    [ "__CINEMATIC_VISCOSITY", "class_viscosity_param.html#abffadfc92b35d18dc268a9878776913e", null ],
    [ "__DENSITY_ID__", "class_viscosity_param.html#a8fbafc4e5a9afb42798e832cebdd0a95", null ],
    [ "__DYNAMIC_VISCOSITY", "class_viscosity_param.html#abf8c4195715812adbfa9430041a65cb2", null ]
];