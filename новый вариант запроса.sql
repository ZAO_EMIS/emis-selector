/*
��� ������ � ID :M ����� �����������, � ������� ��������� ����������� �������� c ID :P � ID �������� :N
*/
select
	`dependence_parametrization`.`id` as depparamid,
    `dependences`.`id` as dependence_id
from
	`modelparametrization`, `dependences`, `floatparams`, `dependence_parametrization`
where
    `modelparametrization`.`model_id` = :M
    and
    `modelparametrization`.`param_id` = :P
    and
    `floatparams`.`id` = :N
    and
    `modelparametrization`.`id` = `dependence_parametrization`.`modelparametrization_independent_id`
    and
    `dependence_parametrization`.`value_id` = `floatparams`.`id`
    and 
    `dependences`.`id` = `dependence_parametrization`.`depnedence_id`