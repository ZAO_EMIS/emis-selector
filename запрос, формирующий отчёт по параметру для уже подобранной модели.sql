/* � �������� ��������������� ������ ������ ������ �� �������� 
������ (��������, ����-���� 210) � � ���������� ���������������� (���� �� � ����, ������ ���� ������� ������������� ��� ������)
� �����
������:  ����-���� 210|                 

��������������:
|     ��������        |     ��������     | ������� ��������� |
|_____________|____________|________________|
|     ��                                |      25      |       ��                        |
|������� ����������     |      ����  |                                      |
|��� ��������� �������|      �        |                                     |
|����������, �������   |      ��     |                                     |
|      ...                                 |     ...        |       ...                           |
|__________________|_______|________________|
*/
/* �� ����: ������������� ��������� :param_id, �������� ��������� :val, ������������� ������ :mod_id */
/* ����� ������ ���������, � ��� ��� ���� � ID */
/* �� ������: ��� ���������, ����������� ��������, ������������ ��������, �������� (��� ���������� ���������) */
/*
*/
select `parameters`.`name`, `floatparams`.`minval`, `floatparams`.`maxval`, IFNULL(`measurebase`.`name`, ' ')
from
`modelline`, `modelparametrization`, `parameters`, `parametertypes`, `floatparams`, `measurebase`, `phisquantity`
where
`modelparametrization`.`model_id` = `modelline`.`id`
and
`modelparametrization`.`param_id` = `parameters`.`id`
and
`parameters`.`phisquantity_id` = `phisquantity`.`id`
and
`measurebase`.`phisquantity_id` = `phisquantity`.`id`
and
`modelline`.`id` = :model_id
and
`parameters`.`id` = :param_id
and(
		`parametertypes`.`id` = 6 and `parameters`.`paramtype_id` = `parametertypes`.`id` and `floatparams`.`modelparametrization_id` = `modelparametrization`.`id`
		and :val between `floatparams`.`minval` and `floatparams`.`maxval`
)
group by `modelparametrization`.`id`;

/* � ��� ����� �� ������ ��� ���������� ��������. */
/* �� ����� - �� �� ����� */
/* �� ������ - ������ ���� (��� ���������, ��������) */
select `parameters`.`name`, `predefinedvalues`.`value`
from
`modelline`, `modelparametrization`, `parameters`, `parametertypes`, `stringparams`, `predefinedvalues`
where
`modelparametrization`.`model_id` = `modelline`.`id`
and
`modelparametrization`.`param_id` = `parameters`.`id`
and
`modelline`.`id` = :model_id
and
`parameters`.`id` = :param_id
and(
		`parametertypes`.`id` = 7 and `parameters`.`paramtype_id` = `parametertypes`.`id` and `stringparams`.`modelparametrization_id` = `modelparametrization`.`id`
		and `stringparams`.`value_id` = `predefinedvalues`.`id` and :val = `predefinedvalues`.`value`
)
group by `modelparametrization`.`id`;

