/* �������� ������ ���� ����� ��������� (������ �������������� �� ������, ����� ����� � ����������) */
/*
*/
select `devicetype`.`id`, `devicetype`.`name`
from `devicetype`;



/* �������� ������ ���������� ��� ������������� ���������� ������ (������� ���� ����������)
������� ���� ���������� ���������������� ��������� �����. �� ������ ����� devicetype.id - ������������� ��������
(������ �����������) */
/* �� ����: ������������� ���� ����������, :devicetype_id = 11 (��� "����������") */
/* �� ������: ������ ���� ���������� ���������� ������ ��� ���������� ���������� (����������) */
/* �������������� ���� ���������: 5 - �������������, 6 - � ��������� ������, 7 - ��������� */
/*
*/
select `parameters`.`id`, `parameters`.`name`, `parametertypes`.`id`
from `devicetype`, `start_page_choose`, `parameters`, `parametertypes`
where
`devicetype`.`id` = :devicetype_id
and
`start_page_choose`.`devicetype_id` = `devicetype`.`id`
and
`start_page_choose`.`parameter_id` = `parameters`.`id`
and
`parameters`.`paramtype_id` = `parametertypes`.`id`;


/* �������� ������ ���� ��������� ���������� �� ���, ��� ���������� �� ������ ������ ������ */
/* ��� ����? ����� �����, ��� ����� ���������� ������������ � ���������� ������ ������ �������� (��� ������ - ���� �����) */
/* �� ����: ������������� ���� ���������� :devicetype_id */
/* �� ������: ������ ���� ��������� ��������� �� ��������� ������ ������ */
/*
*/
select `parameters`.`id`, `parameters`.`name`
from `parameters`, `parametertypes`, `start_page_choose`, `devicetype`
where
`start_page_choose`.`parameter_id` = `parameters`.`id`
and
`start_page_choose`.`devicetype_id` = `devicetype`.`id`
and
`parameters`.`paramtype_id` = `parametertypes`.`id`
and
`devicetype`.`id` = :devicetype_id
and
`parametertypes`.`id` = 7;



/* ������ ��� ���������� (�������������) ������� ������ ���������� ���������� ���� (������������, ���� ����������, ����������, ...) */
/*
*/
select `predefinedvalues`.`value`
from `predefinedvalues`, `parameters`, `parametertypes`
WHERE
`predefinedvalues`.`param_id` = `parameters`.`id`
and
`parameters`.`paramtype_id` = `parametertypes`.`id`
and
`parametertypes`.`id` = 7
and
`parameters`.`id` = :paramid;
