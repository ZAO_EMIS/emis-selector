/* ��������� ���������: �� ��������� ������ - ������ ����������� ���������. 
���� ����������� ���� - ��� ��������������� �� ��������� ����������� ���������� */
/* 1: ��������� �������� ID ���������, ��� ������������ � ����������� �������� (���� ��� ��������), 
��� ���� � �� �� �������� � �������� ������������ � ������������� (���� �������� �� ������������ ����)
��� �������� ��� ���������� ���������, �������� ������ �������, ��������������� ����������� �� ��������� ���������.

�� ������ ��������� ���� (���������) ������������ ����� ������� �� ���� ������������� ������������ ����������� IN (`modelline`.`id` in (..., ..., ...))
 */
/* �� ����������� ID ��������� � ��� �������� ����������, ����� ������ ����� �������� ��������� �������������

�� ����: 
- ID ���������, 

- ����������� ��� ������������� (����� ���� null), 
- ������������ ��� ������������� (����� ���� null), 

- ����������� ��� float (����� ���� null), 
- ������������ ��� float (����� ���� null), 

- �������� ���������� ��������� (����� ���� null)

������� isCoincidenceXXX(...) (����������) �������� ���, ��� ���������� ������ "����" �������� ����������, �� ������ ���������.
������� � ������ ��������� �������� �� ����, � ������ ����������� ���������� (������������ � null �� ����� ������������)


�� ������: 
- ������� �������������� ������, 
- ����� ������� � ����� ������ (���� ����, ���� ��� - ��������� ������ ������)
- ID ������
- ��� ������
 */

/* �������� �� ���� ���������� �� ������ � ������ ������ �� ��������� ������, ������� ����������� �� ������ �������, ������� ������������� �������� ������ 
�� ����������� ���������� */
select
	`modelparametrization`.`id` as modelparam_id, 
    ifnull(`modelparametrization`.`codeorder`, 0) as codeorder, 
    `modelline`.`id` as model_id, 
    `modelline`.`basename` as modelname
from
	`modelline`, `modelparametrization`, `intparams`, `floatparams`, `stringparams`
where(
	`modelline`.`id` = `modelparametrization`.`model_id`
    and
    `modelparametrization`.`independent` = 1
    and
    `modelline`.`id` in (29, 31, 16) /* ��� ����� ������� ������ ������������� �����������, � ��� ������ ���������� ������ ������������� (��������� ������� ���� �����) */
    and
    `modelparametrization`.`param_id` = :param_id
    and(
    	(
        	(`typedAs`(`modelparametrization`.`id`) = 5 or `typedAs`(`modelparametrization`.`id`) = 8)
            and `intparams`.`modelparametrization_id` = `modelparametrization`.`id`
            and `isCoincidenceInt`(`modelparametrization`.`id`, :minintval, :maxintval)
        ) or (
        	(`typedAs`(`modelparametrization`.`id`) = 6)
            and `floatparams`.`modelparametrization_id` = `modelparametrization`.`id`
            and `isCoincidenceFloat`(`modelparametrization`.`id`, :minfloatval, :maxfloatval)
        ) or (
        	(`typedAs`(`modelparametrization`.`id`) = 7)
            and `stringparams`.`modelparametrization_id` = `modelparametrization`.`id`
            and `isCoincidenceStr`(`modelparametrization`.`id`, :strval)
        )
    )
)
group by
	`modelline`.`id`;
    
/* 2 ��� ������� ������������ ��������� ������ �� �������, ��������� �� ���� 1, 
�������� ID �������� ���������, ��������������� ������� ������������, �, ���� ����,
���, ��������������� ����������� ���������� � ����� ������
*/
/* �. ���������� ��� ��������� */
/*
�� ����: :paramID - ������������� ���������
�� ������: ID ���� ���������: 5- �������������, 6- � ����.������, 7- ���������, 8- ����������
*/
select `parametertypes`.`id`
from `parameters`, `parametertypes`
where `parametertypes`.`id` = `parameters`.`paramtype_id` and `parameters`.`id` = :paramID

/* �. �������� - �������������� ��� ����������� ���� */
/*
�� ����: 
- :modelID - ID ������, ����������� �� ���� 1
- :paramID - ID ������������ ���������
- :minVal - ���������� ��������, ��������� �������������
- :maxVal - ���������� ��������, ��������� �������������
�� ������: 
- ID �������������� ������
- ������� � ����� ������, ���� ���� (���� ��� - 0)
- ��� � ����� ������, ���� ���� (���� ��� - ������ ������)
*/
select
`modelparametrization`.`id`,
ifnull(`modelparametrization`.`codeorder`, 0),
ifnull(`codevalue`.`codeval`, '')
from
`parameters`, `modelparametrization`, `intparams`, `codevalue`
where(
    `parameters`.`id` = :paramID
    and
    `intparams`.`minval` <= :minVal and `intparams`.`maxval` >= :maxVal
    and
    `modelparametrization`.`model_id` = :modelID
    and
    `parameters`.`id` = `modelparametrization`.`param_id`
    and
    `modelparametrization`.`id` = `intparams`.`modelparametrization_id`
    and
    `intparams`.`codeval_id` = `codevalue`.`id`
);

/* �. �������� - � ��������� ������ */
/*
�� ����: 
- :modelID - ID ������, ����������� �� ���� 1
- :paramID - ID ������������ ���������
- :minVal - ���������� ��������, ��������� �������������
- :maxVal - ���������� ��������, ��������� �������������
�� ������: 
- ID �������������� ������
- ������� � ����� ������, ���� ���� (���� ��� - 0)
- ��� � ����� ������, ���� ���� (���� ��� - ������ ������)
*/
select
	`modelparametrization`.`id` asmodelparam_id,
	ifnull(`modelparametrization`.`codeorder`, 0) as codeorder,
	ifnull(`codevalue`.`codeval`, '') as codeval
from
	`parameters`, `modelparametrization`, `floatparams`, `codevalue`
where(
    `parameters`.`id` = :paramID
    and
    `floatparams`.`minval` <= :minVal and `floatparams`.`maxval` >= :maxVal
    and
    `modelparametrization`.`model_id` = :modelID
    and
    `parameters`.`id` = `modelparametrization`.`param_id`
    and
    `modelparametrization`.`id` = `floatparams`.`modelparametrization_id`
    and
    `floatparams`.`codeval_id` = `codevalue`.`id`
);

/* �. �������� - ���������� ���� */
/*
�� ����: 
- :modelID - ID ������, ����������� �� ���� 1
- :paramID - ID ������������ ���������
- :Val_ID - ID ���������������� ��������, ���������� �������������
�� ������: 
- ID �������������� ������
- ������� � ����� ������, ���� ���� (���� ��� - 0)
- ��� � ����� ������, ���� ���� (���� ��� - ������ ������)
*/
select
`modelparametrization`.`id`,
ifnull(`modelparametrization`.`codeorder`, 0),
ifnull(`codevalue`.`codeval`, '')
from
`parameters`, `modelparametrization`, `stringparams`, `codevalue`
where(
    `parameters`.`id` = :paramID
    and
    `stringparams`.`value_id` = :Val_ID
    and
    `modelparametrization`.`model_id` = :modelID
    and
    `parameters`.`id` = `modelparametrization`.`param_id`
    and
    `modelparametrization`.`id` = `stringparams`.`modelparametrization_id`
    and
    `stringparams`.`codeval_id` = `codevalue`.`id`
);

/* ������������� ��������� 1: ��� �������, ��������������� ����������� �� ����������� ����������,
�������� ���������� ��� ������������ ������� ����� ������, ������������ ������������ ��� ���� ������� ����������� */

/* 3: ��� ������ �� ��������� � ���� 1 ������� �������� ������ ��������� ����������
 */
/* �������� �� ������ �������, ���������� �� ����������� ����������, */
/* ��� ��������� ������ ����� ��� � ����������� 
�� ����: id  ������
�� ������: id ������������
 */
select
	`dependences`.`id` as dependence_id
from
	`modelparametrization`, `dependences`
where
	`modelparametrization`.`id` = `dependences`.`modelparametrization_id`
    and
    `modelparametrization`.`model_id` = :model_id
group by
	`dependences`.`id`;
    
/* 4: ��� ������ �� ��������� �� ���� 2 ������������ ����� � ��������������
 */
/*��� ������ ����������� ����� ����� � �������������� (������ ����������� ����������, ������������ ��������� ��������)
�� ����: id �����������
�� ������: id ����������� ���������� ������ (� �������� ����� �� modelparametrization), ������������ ��������� �����������
*/
select
	`dependence_parametrization`.`modelparametrization_independent_id` as dep_param_id
from
	`dependences`, `dependence_parametrization`
where
	`dependence_parametrization`.`depnedence_id` = `dependences`.`id`
    and
    `dependences`.`id` = :dependence_id
group by
	`dependence_parametrization`.`modelparametrization_independent_id`;

/* 5: �������� �� ���� ��������� ������������, ��� ������ �� ��� ���������, ������������� �� ����������� �������� ���������� ��������� */
/* �� ��������� id �����������, id ������������ ��������� � id ��� ��������
�������� ���, ��������������� ���� ������������ � ����� ������

���� ��� ���������� �� ���� ���������� ��� ����������, ������ ������ ����� ������.
���� ���������� ���� (����������� ������������ � ��������� ���������������) - ������ ���, ��������� ������������ � ����� ������.

�� ����:
- :indepentent_modelparam_id - ID �������������� ������ (����������� ����������)
- :val_id - ID �������� ������������ ���������
- :dep_id - ID ����������� (�����������)
- 
�� ������:
- ID �������������� ������ (��������� ����������)
- ID �����������
- ������� � ����� ������, ������������ ��������� ����������, ���� ���� (���� ��� - 0)
- ��� � ������� ����� ������, ������������ ������������, ���� ���� (���� ��� - ������ ������)
 */
select
	`vdependence_parametrization`.dep_param_id, 
    `vdependence_parametrization`.`dep_id`, 
    `modelparametrization`.`codeorder`, 
    `getDependentCodeByDepID`(`vdependence_parametrization`.`dep_id`)
from
	`vdependence_parametrization`, `modelparametrization`, `intparams`, `floatparams`, `stringparams`
where
	`vdependence_parametrization`.`indepentent_modelparam_id` = :indepentent_modelparam_id
    and
	`vdependence_parametrization`.`val_id` = :val_id
    and
    `vdependence_parametrization`.`dep_id` = :dep_id
    and(
/*
*/
    	(`vdependence_parametrization`.`paramtype_id` = 5 or `vdependence_parametrization`.`paramtype_id` = 8
        	and `vdependence_parametrization`.`indepentent_modelparam_id` = `modelparametrization`.`id`
            and `modelparametrization`.`id` = `intparams`.`modelparametrization_id`
            and `intparams`.`id` = :val_id
        ) 
        or
    	(`vdependence_parametrization`.`paramtype_id` = 6
        	and `vdependence_parametrization`.`indepentent_modelparam_id` = `modelparametrization`.`id`
            and `modelparametrization`.`id` = `floatparams`.`modelparametrization_id`
            and `floatparams`.`id` = :val_id
        ) 
/*
*/    
        or
    	(`vdependence_parametrization`.`paramtype_id` = 7
        	and `vdependence_parametrization`.`indepentent_modelparam_id` = `modelparametrization`.`id`
            and `modelparametrization`.`id` = `stringparams`.`modelparametrization_id`
            and `stringparams`.`id` = :val_id
        )
    )
group by
	`vdependence_parametrization`.`dep_id`
/* ������������� ��������� 2: �������� ���������� ��� ������������ ������� ���� ������ �� ��������� ���������� */    

/* ������������: ��������� ����� ������ �� ����������� 
������ (���� ����� �� ������, ������� �� ����� ���� ���������) 
�� ������ ������� ������� ����� ������ � ������������ ��� ������ ����� � ���� ��������.
*/