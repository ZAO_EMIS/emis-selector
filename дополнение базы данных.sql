/* ���������� ���� ��������� ��� ����-���� - ���������� ��� ��� ������ "����������" */
update `modelline` set `modelline`.`devicetype_id` = 11 where `modelline`.`devicetype_id` = 10 or `modelline`.`devicetype_id` = 9;
/* ���������� ������� `modelparametrization` - ��������� �� ������... ��� �������� � ����������� ���������� � ������� ���� */
insert into `modelparametrization`(`model_id`, `param_id`, `measurebase_id`) values(29, 44, 20); commit;
insert into `modelparametrization`(`model_id`, `param_id`, `measurebase_id`) values(29, 77, 27); commit;
insert into `modelparametrization`(`model_id`, `param_id`, `measurebase_id`) values(29, 45, 14); commit;
insert into `modelparametrization`(`model_id`, `param_id`, `measurebase_id`) values(29, 76, 14); commit;
/* ��� �������������� �������� ����-���� ����������� ������� 34 - �/�, 36 - ���.�/�*/
insert into `modelparametrization`(`model_id`, `param_id`, `measurebase_id`) values(29, 98, 34); commit;
insert into `modelparametrization`(`model_id`, `param_id`, `measurebase_id`) values(31, 98, 34); commit;
insert into `modelparametrization`(`model_id`, `param_id`, `measurebase_id`) values(29, 98, 36); commit;
insert into `modelparametrization`(`model_id`, `param_id`, `measurebase_id`) values(31, 98, 36); commit;

/* ���������� ������� floatparams ��� ���������� ������������  ���������� ������� */
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(132, 0, 0, 1); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(133, 0, 0, 5); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(134, 0, -20, 80); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(135, 0, -40, 70); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.002, 0.02); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.004, 0.04); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.006, 0.06); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.01, 0.1); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.03, 0.24); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.06, 0.42); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.12, 1.08); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.016, 0.16); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.025, 0.25); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.04, 0.4); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.06, 0.6); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.16, 1.6); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.25, 2.5); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.3, 2.1); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.24, 3.6); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.6, 4.2); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.4, 4.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.6, 6.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 0.9, 9.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 1.5, 15.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 1.8, 18.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 3.0, 21.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 1.0, 10.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 1.6, 16.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 5.0, 25.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 8.0, 40.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 12.0, 60.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 14.0, 90.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 18.0, 120.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 25.0, 200.0); commit;

insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 2.88, 28.8); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 4.32, 43.2); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 7.0, 70.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(138, 0, 10.0, 100.0); commit;

insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 0.07, 0.7); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 0.11, 1.1); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 0.18, 1.8); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 0.28, 2.8); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 0.48, 4.8); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 0.7, 7.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 1.0, 10.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 1.6, 16.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 3.0, 30.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 4.5, 45.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 7.0, 70.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 11.0, 110.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 12.0, 120.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 16.0, 160.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 18.0, 180.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 25.0, 250.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 40.0, 400.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 60.0, 600.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 80.0, 800.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 100.0, 1000.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(137, 0, 600.0, 3000.0); commit;

insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 2.5, 25.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 4.0, 40.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 6.3, 63.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 10.0, 100.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 16.0, 160.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 25.0, 250.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 40.0, 400.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 63.0, 630.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 100.0, 1000.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 160.0, 1600.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 250.0, 2500.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 400.0, 4000.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 500.0, 5000.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 600.0, 6000.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 630.0, 6300.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 1000.0, 10000.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 1600.0, 16000.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 2500.0, 25000.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 4000.0, 40000.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 6300.0, 63000.0); commit;
insert into `floatparams`(`modelparametrization_id`, `value`, `minval`, `maxval`) values(139, 0, 20000.0, 100000.0); commit;

/* ������������ ���������� ������� start_page_choose */
/* ���������� �� �� ������� ��������� ������� */
/* �� ��� ���� ����� �٨ ��������������, ���� ���� �������� */
insert into `start_page_choose`(`devicetype_id`, `parameter_id`) VALUES(11, 75);
/* �������� ���������� ����� */
insert into `start_page_choose`(`devicetype_id`, `parameter_id`) VALUES(11, 44);
/* ����������� ���������� ����� */
insert into `start_page_choose`(`devicetype_id`, `parameter_id`) VALUES(11, 45);
/* ����������� ���������� ����� */
insert into `start_page_choose`(`devicetype_id`, `parameter_id`) VALUES(11, 76);
/* ��� ���������� ����� */
insert into `start_page_choose`(`devicetype_id`, `parameter_id`) VALUES(11, 51);
/* �������� ������� */
insert into `start_page_choose`(`devicetype_id`, `parameter_id`) VALUES(11, 98);
