/* ��������� �������� �� `dependences`.`id`, ������ ���, ������������ � ������� ����� ������, */
/* ��� ID, � ��� ������� � ����� ������ */
/* �� ����: :dep_id - ID �������������� ����������� */
/* �� ������: */
/* - codevalue_id  - ID ����, ������������� � ������� ��������������*/
/*  - codeorder  - ����� ������� � ����� ������, ������������ ����������� ����������*/
/* - codeval  - ��������, ������������ � ������� ����� ������, ������������ ��������� ������������ ���������*/
select
    `codevalue`.`id` as codevalue_id, 
    `modelparametrization`.`codeorder` as codeorder,
    `codevalue`.`codeval` as codeval
from
	`dependences`, `floatparams`, `intparams`, `stringparams`, `codevalue`, `modelparametrization`
where
	`dependences`.`id` = :dep_id
    and
    `dependences`.`modelparametrization_id` = `modelparametrization`.`id`
    and(
    	(`typedAs`(`dependences`.`modelparametrization_id`) = 5 or `typedAs`(`dependences`.`modelparametrization_id`) = 8
        	and `dependences`.`value_id` = `intparams`.`id` and `intparams`.`codeval_id` = `codevalue`.`id`
        )
        or
    	(`typedAs`(`dependences`.`modelparametrization_id`) = 6
        	and `dependences`.`value_id` = `floatparams`.`id` and `floatparams`.`codeval_id` = `codevalue`.`id`
        )
        or
    	(`typedAs`(`dependences`.`modelparametrization_id`) = 7
        	and `dependences`.`value_id` = `stringparams`.`id` and `stringparams`.`codeval_id` = `codevalue`.`id`
        )
    )
group by
	`codevalue`.`codeval`