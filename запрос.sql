/* ��������� �������� �� `dependence_parametrization`.`id`, ������ ���, ������������ � ������� ����� ������, */
/* ��� ID, � ��� ������� � ����� ������ */
/* �� ����: :depparam_id - ID �������������� ����������� */
/* �� ������: */
/* - codevalue_id  - ID ����, ������������� � ������� ��������������*/
/*  - codeorder  - ����� ������� � ����� ������, ������������ ����������� ����������*/
/* - codeval  - ��������, ������������ � ������� ����� ������, ������������ ��������� ������������ ���������*/
select
    `codevalue`.`id` as codevalue_id, 
    `modelparametrization`.`codeorder` as codeorder,
    `codevalue`.`codeval` as codeval
from
	`dependence_parametrization`, `floatparams`, `intparams`, `stringparams`, `codevalue`, `modelparametrization`
where
	`dependence_parametrization`.`id` = :depparam_id
    and
    `dependence_parametrization`.`modelparametrization_independent_id` = `modelparametrization`.`id`
    and(
    	(`typedAs`(`dependence_parametrization`.`modelparametrization_independent_id`) = 5 or `typedAs`(`dependence_parametrization`.`modelparametrization_independent_id`) = 8
        	and `dependence_parametrization`.`value_id` = `intparams`.`id` and `intparams`.`codeval_id` = `codevalue`.`id`
        )
        or
    	(`typedAs`(`dependence_parametrization`.`modelparametrization_independent_id`) = 6
        	and `dependence_parametrization`.`value_id` = `floatparams`.`id` and `floatparams`.`codeval_id` = `codevalue`.`id`
        )
        or
    	(`typedAs`(`dependence_parametrization`.`modelparametrization_independent_id`) = 7
        	and `dependence_parametrization`.`value_id` = `stringparams`.`id` and `stringparams`.`codeval_id` = `codevalue`.`id`
        )
    )
group by
    `codevalue`.`codeval`
    